﻿using RapidReport.Models.VDF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RapidReport.Controllers
{
    public class VDFController : ApiController
    {
         private readonly VDFBusiness VDFBusinessObj;
         public VDFController()
        {
            VDFBusinessObj = new VDFBusiness();
        }
        [HttpGet]
        [Route("app/GetVDFDashboardDetails/{Account}")]
        public VDFBusinessObject GetVDFDashboardDetails(string Account)
        {
            return VDFBusinessObj.GetVDFDashboardDetails(Account);
        }
    }
}
