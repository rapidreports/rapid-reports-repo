﻿using RapidReport.Models.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RapidReport.Controllers
{
    public class DashboardController : ApiController
    {
        private readonly DashboardBusiness dashboardbusinessobj;
        public DashboardController()
        {
            dashboardbusinessobj = new DashboardBusiness();
        }

        [HttpGet]
        [Route("app/getDashboardTile")]
        public List<MainTileBusinessObject> getDashboardTiles() 
        {           
            return dashboardbusinessobj.getDashboardTiles();
        }

    }
}
