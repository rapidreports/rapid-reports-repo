﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RapidReport.Models.Operational;

namespace RapidReport.Controllers.Operational
{
    public class DashboardAPIController : ApiController
    {
        private readonly DashboardBusiness dashbusiness;

        public DashboardAPIController()
        {
            dashbusiness = new DashboardBusiness();
        }

        [HttpGet]
        [Route("api/operational/dashboard/resourceutiliztion/{month}/{year}")]
        public IList<ResourceUtilization> GetResourceUtilization(string month, string year) //[FromBody] Changeview cv
        {

            return dashbusiness.GetResourceUtilization(month,year);

        }

        [HttpGet]
        [Route("api/operational/dashboard/details/manageroperational/{year}/{manager}")]
        public HttpResponseMessage GetManagerOperational(int year, string manager)
        {
            if (year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    dashbusiness.ManagerOperational(year, manager)
                    );
        }
    }
}