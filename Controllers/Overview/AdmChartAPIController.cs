﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RapidReport.Models.ADM;
using System.Web;
using RapidReport.Models.Common;

namespace RapidReport.Controllers
{
    public class AdmChartAPIController : ApiController
    {
      
        private readonly ADMChartBusiness admApi;
        private readonly CommonBusiness commonApi;
        private string currentUser;
        public AdmChartAPIController()
        {
            admApi = new ADMChartBusiness();
            commonApi = new CommonBusiness();
            currentUser = HttpContext.Current.User.Identity.Name; 

        }

        [HttpGet]
        [Route("app/admchart/{category}/{accountname}/{serviceline}")]
        public List<ADMChartObject> getAdmchart(string category, string accountname, string serviceline) 
        {

            return admApi.getAdmchart(category, accountname, serviceline);

        }

        [HttpGet]
        [Route("app/admprioritychart/{category}/{accountname}/{serviceline}")]
        public List<ADMChartObject> getAdmprioritychart(string category, string accountname, string serviceline) 
        {

            return admApi.getAdmPrioritychart(category, accountname, serviceline);

        }

        [HttpGet]
        [Route("app/admbeyondSLAchart/{category}/{accountname}/{serviceline}")]
        public List<ADMChartObject> getAdmBeyondSLAchart(string category, string accountname, string serviceline) 
        {

            return admApi.getAdmBeyondSLAchart(category, accountname, serviceline);

        }

        [HttpGet]
        [Route("app/admbacklogprioritychart/{category}/{accountname}/{serviceline}")]
        public List<ADMChartObject> getAdmBackLogsByPrioritychart(string category, string accountname, string serviceline)
        {

            return admApi.getAdmBackLogsByPrioritychart(category, accountname, serviceline);

        }

        [HttpGet]
        [Route("app/admeffortperticketchart/{category}/{accountname}/{serviceline}")]
        public List<ADMChartObject> getAdmEffortPerTicketchart(string category, string accountname, string serviceline)
        {

            return admApi.getAdmEffortPerTicketchart(category, accountname, serviceline);

        }

        [HttpGet]
        [Route("app/admcostperticketchart/{category}/{accountname}/{serviceline}")]
        public List<ADMChartObject> getAdmCostPerTicketchart(string category, string accountname, string serviceline)
        {

            return admApi.getAdmCostPerTicketchart(category, accountname, serviceline);

        }

        [HttpGet]
        [Route("app/accountname")]
        public List<ADMChartObject> getAccountname() 
        {

            return admApi.getAccountname();

        }

        [HttpGet]
        [Route("app/serviceline")]
        public List<ADMChartObject> getServiceLine()
        {

            return admApi.getServiceLine();

        }
    }
}