﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RapidReport.Models.FTE;
using System.Web;
using RapidReport.Models.Common.Overview;


namespace RapidReport.Controllers
{
    public class fteApiController : ApiController
    {
        private readonly fteBusiness fteApi;
        private readonly CommonBusiness commonApi;
        private string currentUser;
        public fteApiController()
        {
            fteApi = new fteBusiness();
            commonApi = new CommonBusiness();
            currentUser = HttpContext.Current.User.Identity.Name; 

        }
        
        [HttpGet]
        [Route("app/Accounts")]
        public List<fteBusinessObjects> getAccounts() //[FromBody] Changeview cv
        {
            
            return fteApi.getAccounts();
            
        }

        [HttpGet]
        [Route("app/WBSById/{Id}/{Month}/{Year}")]
        public List<fteWbsBusinessobject> getWbsById(int Id,string Month, string Year) //[FromBody] Changeview cv
        {

            return fteApi.getWbsById(Id,Month,Year);

        }

        [HttpGet]
        [Route("app/NDWBSById/{Id}/{Year}")]
        public List<ndfteWbsBusinessobject> getWbsById(int Id, string Year) //[FromBody] Changeview cv
        {

            return fteApi.getNDWbsById(Id, Year);

        }

        [HttpGet]
        [Route("app/getfte/{month}/{year}/{AccountID}")]
        public IList<MonthFTE> GetMonthlyFTE(string month, string year, int Accountid) //[FromBody] Changeview cv
        {

            return commonApi.GetMonthlyFTE(month,year,Accountid);

        }

        [HttpGet]
        [Route("app/getContract/{AccountID}")]
        public IList<Contract> GetContract(int Accountid) //[FromBody] Changeview cv
        {

            return commonApi.GetContract(Accountid);

        }

        [HttpGet]
        [Route("app/getNDFTEContract/{AccountID}")]
        public IList<Contract> GetNDFTEContract(int Accountid) //[FromBody] Changeview cv
        {

            return commonApi.GetNDFTEContract(Accountid);

        }

        [HttpGet]
        [Route("app/getdfte/{AccountID}/{Year}")]
        public IList<dfteBusinessObjects> GetDfteDetails(int Accountid, string year) //[FromBody] Changeview cv
        {

            return fteApi.GetDfteDetails(year, Accountid);

        }
        [HttpGet]
        [Route("app/GetCr/{AccountId}/{Year}")]
        public List<ndfteCR> getNDFTECr(int AccountId, string Year) //[FromBody] Changeview cv
        {

            return fteApi.getndfteCr(AccountId, Year);

        }

        [HttpGet]
        [Route("app/GetdfteCr/{AccountId}/{Month}/{Year}")]
        public List<dfteCR> getDFTECr(int AccountId, string Month, string Year) //[FromBody] Changeview cv
        {

            return fteApi.getdfteCr(AccountId, Month , Year);

        }

        [HttpPost]
        [Route("app/savecr")]
        // POST api/<controller>
        public void Insertndftecr([FromBody]ndfteCR data)
        {
            try
            {
                fteApi.Insertndftecr(data);

            }
            catch (Exception exception)
            {

            }
        }

        [HttpPost]
        [Route("app/savedftecr")]
        // POST api/<controller>
        public void Insertdftecr([FromBody]dfteCR data)
        {
            try
            {
                fteApi.Insertdftecr(data);

            }
            catch (Exception exception)
            {

            }
        }

        [HttpPost]
        [Route("app/UpdateCrStatus")]
        // POST api/<controller>
        public void UpdateCrStatus([FromBody]ndfteCR data)
        {
            try
            {
                fteApi.UpdateNDFTECrStatus(data);

            }
            catch (Exception exception)
            {

            }
        }

        [HttpPost]
        [Route("app/UpdateDFTECrStatus")]
        // POST api/<controller>
        public void UpdateDFTECrStatus([FromBody]dfteCR data)
        {
            try
            {
                fteApi.UpdateDFTECrStatus(data);

            }
            catch (Exception exception)
            {

            }
        }

        [HttpPost]
        [Route("app/contractpost/{AccountId}/{Month}/{Year}")]
        // POST api/<controller>
        public void InsertorUpdateContract(int AccountID, int Month, string year, [FromBody]Contract contract)
        {
            try
            {
                //insertorupdatewbsdetails.LastUpdatedBy = currentUser;

                commonApi.InsertorUpdateContract(AccountID, Month, year,contract);

            }
            catch (Exception exception)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                //    string.Format(@"Inner Exception : {0} \\nMessage : {1}", exception.InnerException,
                //        exception.Message));
            }
        }

        [HttpPost]
        [Route("app/ndftecontractpost/{AccountId}/{Year}")]
        // POST api/<controller>
        public void InsertorUpdateNDFTEContract(int AccountID, string year, [FromBody]Contract contract)
        {
            try
            {
                //insertorupdatewbsdetails.LastUpdatedBy = currentUser;

                commonApi.InsertorUpdateNDFTEContract(AccountID, year, contract);

            }
            catch (Exception exception)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                //    string.Format(@"Inner Exception : {0} \\nMessage : {1}", exception.InnerException,
                //        exception.Message));
            }
        }

        [HttpPost]
        [Route("app/contractdel/{AccountId}/{Month}/{Year}")]
        // POST api/<controller>
        public void DeleteContract(int AccountID, int Month, string year, [FromBody]Contract contract)
        {
            try
            {
                //insertorupdatewbsdetails.LastUpdatedBy = currentUser;

                commonApi.DeleteContract(AccountID, Month, year, contract);

            }
            catch (Exception exception)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                //    string.Format(@"Inner Exception : {0} \\nMessage : {1}", exception.InnerException,
                //        exception.Message));
            }
        }

        [HttpPost]
        [Route("app/ndftecontractdel/{AccountId}/{Year}")]
        // POST api/<controller>
        public void DeleteNDFTEContract(int AccountID, string year, [FromBody]Contract contract)
        {
            try
            {
                //insertorupdatewbsdetails.LastUpdatedBy = currentUser;

                commonApi.DeleteNDFTEContract(AccountID, year, contract);

            }
            catch (Exception exception)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                //    string.Format(@"Inner Exception : {0} \\nMessage : {1}", exception.InnerException,
                //        exception.Message));
            }
        }

        [HttpPost]
        [Route("app/wbspost/{Id}/{Month}/{Year}")]
        // POST api/<controller>
        public void InsertorUpdateWbsDetails(string Month, string Year,[FromBody]InsertorUpdateWbsDetails insertorupdatewbsdetails)
        {
            try
            {
                insertorupdatewbsdetails.LastUpdatedBy = currentUser;

                fteApi.InsertorUpdateWbsDetails(Month,Year,insertorupdatewbsdetails);
               
            }
            catch (Exception exception)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                //    string.Format(@"Inner Exception : {0} \\nMessage : {1}", exception.InnerException,
                //        exception.Message));
            }
        }

        [HttpPost]
        [Route("app/ndwbspost/{AccountId}/{Year}/{Month}")]
        // POST api/<controller>
        public void InsertorUpdateWbsDetails(int AccountId, string Year,string Month,[FromBody]ndfteWbsBusinessobject insertorupdatendwbsdetails)
        {
            try
            {
                insertorupdatendwbsdetails.LastUpdatedBy = currentUser;

                fteApi.InsertorUpdateNDWbsDetails(AccountId, Year,Month,insertorupdatendwbsdetails);

            }
            catch (Exception exception)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                //    string.Format(@"Inner Exception : {0} \\nMessage : {1}", exception.InnerException,
                //        exception.Message));
            }
        }

        [HttpPost]
        [Route("app/wbsdel/{Id}")]
        // POST api/<controller>
        public void DelWbsDetails([FromBody]DelWbsDetails del)
        {
            try
            {
                fteApi.DelWbsDetails(del);

            }
            catch (Exception exception)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                //    string.Format(@"Inner Exception : {0} \\nMessage : {1}", exception.InnerException,
                //        exception.Message));
            }
        }

        [HttpPost]
        [Route("app/ndwbsdel/{Id}")]
        // POST api/<controller>
        public void DelWbsDetails(int Id, [FromBody]ndfteWbsBusinessobject del)
        {
            try
            {
                fteApi.DelNDWbsDetails(Id, del);

            }
            catch (Exception exception)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                //    string.Format(@"Inner Exception : {0} \\nMessage : {1}", exception.InnerException,
                //        exception.Message));
            }
        }

        [HttpPost]
        [Route("app/savefte")]
        // POST api/<controller>
        public void SaveFte([FromBody]List<MonthFTE> savedetails)
        {
            try
            {
               commonApi.SaveMonthlyFte(savedetails,currentUser);

            }
            catch (Exception exception)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                //    string.Format(@"Inner Exception : {0} \\nMessage : {1}", exception.InnerException,
                //        exception.Message));
            }
        }

        [HttpPost]
        [Route("app/postdfte")]
        // POST api/<controller>
        public void InsertorUpdatedfte([FromBody]List<dfteBusinessObjects> data)
        {
            try
            {
                fteApi.InsertorUpdatedfte(data);

            }
            catch (Exception exception)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                //    string.Format(@"Inner Exception : {0} \\nMessage : {1}", exception.InnerException,
                //        exception.Message));
            }
        }
    }
}