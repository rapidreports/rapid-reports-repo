﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RapidReport.Models.Report;
using RapidReport.Models.Common.Overview;
using System.Web;

namespace RapidReport.Controllers
{
    public class ReportAPIController : ApiController
    {
        private readonly ReportBusiness rptApi;
        private readonly CommonBusiness cmnApi;
        private string currentUser;

        public ReportAPIController()
        {
            rptApi = new ReportBusiness();
            cmnApi = new CommonBusiness();
            currentUser = HttpContext.Current.User.Identity.Name;
        }

        #region gridreport
        [HttpGet]
        [Route("api/report/contactcost/{month}/{year}")]
        public HttpResponseMessage GetContactCost(int month, int year)
        {
            if(month == 0 ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.CustContractCost(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/billingtotal/{month}/{year}")]
        public HttpResponseMessage GetBillingCost(string month, int year)
        {
            if (month == "" ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.CustBillingCost(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/accounts/{month}/{year}")]
        public HttpResponseMessage GetAccounts(string month, int year)
        {
            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.AccountTyp(month,year)
                    );
        }

        [HttpGet]
        [Route("api/report/monthlydumpwbs/{month}/{year}")]
        public HttpResponseMessage GetMonthlyDumpWBS(string month, int year)
        {
            if (month == "" ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.MonthlyDumpWBS(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/custmonthlywbs/{month}/{year}")]
        public HttpResponseMessage GetCustMonthlyWBS(string month, int year)
        {
            if (month == "" ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    cmnApi.CustMonthlyWBS(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/monthlyfte/{month}/{year}")]
        public HttpResponseMessage GetMonthlyFTE(string month, int year)
        {
            if (month == "" ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    cmnApi.MonthlyFTE(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/monthlyratecard/{month}/{year}")]
        public HttpResponseMessage GetMonthlyRateCard(string month, int year)
        {
            if (month == "" ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.MonthlyRateCard(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/monthlyndfte/{month}/{year}")]
        public HttpResponseMessage GetMonthlyNDFTE(string month, int year)
        {
            if (month == string.Empty ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.MonthlyNDFTE(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/monthlyndwbs/{month}/{year}")]
        public HttpResponseMessage GetMonthlyNDWBS(string month, int year)
        {
            if (month == string.Empty ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.MonthlyNDWBS(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/monthlyndcontract/{month}/{year}")]
        public HttpResponseMessage GetMonthlyNDContract(string month, int year)
        {
            if (month == string.Empty ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.MonthlyNDContract(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/monthlyndchangereq/{month}/{year}")]
        public HttpResponseMessage GetMonthlyNDChangeRequest(string month, int year)
        {
            if (month == string.Empty ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.MonthlyNDChangeRequest(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/monthlymasterwbs/{month}/{year}")]
        public HttpResponseMessage GetMonthlyMasterWBS(string month, int year)
        {
            if (month == string.Empty ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.MonthlyMasterWBS(month, year)
                    );
        }

        [HttpGet]
        [Route("api/report/monthlymastercontract/{month}/{year}")]
        public HttpResponseMessage GetMonthlyMasterContract(string month, int year)
        {
            if (month == string.Empty ||
                year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.MonthlyMasterContract(month, year)
                    );
        }
        #endregion

        #region chartreport
        [HttpGet]
        [Route("api/report/accounttypeyearly/{year}")]
        public HttpResponseMessage GetYearlyAccounts(int year)
        {
            if (year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.YearlyAccountType(year)
                    );
        }

        [HttpGet]
        [Route("api/report/contactcostyearly/{year}")]
        public HttpResponseMessage GetContactCost(int year)
        {
            if (year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.CustContractCostYealy(year)
                    );
        }

        [HttpGet]
        [Route("api/report/billingtotalyearly/{year}")]
        public HttpResponseMessage GetBillingCost(int year)
        {
            if (year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.CustBillingCostYealy(year)
                    );
        }

        [HttpGet]
        [Route("api/report/fte/{year}")]
        public HttpResponseMessage GetFTE(int year)
        {
            if (year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.CustomerYearlyFTE(year)
                    );
        }

        [HttpGet]
        [Route("api/report/yearlyratecard/{year}")]
        public HttpResponseMessage GetRateCard(int year)
        {
            if (year == 0)
            {
                return Request.CreateResponse(
                    HttpStatusCode.BadRequest,
                    new HttpError("Params not correct.")
                    );
            }

            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.YearlyRateCard(year)
                    );
        }

        [HttpGet]
        [Route("api/report/alltimewbs")]
        public HttpResponseMessage GetAllTimeWBS()
        {
            return Request.CreateResponse(
                    HttpStatusCode.OK,
                    rptApi.AllTimeWBS()
                    );
        }
        #endregion
    }
}