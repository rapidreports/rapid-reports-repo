﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RapidReport.Models.Overview.VDF;
using System.Web;
using RapidReport.Models.Common;


namespace RapidReport.Controllers.Overview
{
    public class VdfChartAPIController : ApiController
    {
      
        private readonly VDFChartBusiness admApi;
        private readonly CommonBusiness commonApi;
        private string currentUser;
        public VdfChartAPIController()
        {
            admApi = new VDFChartBusiness();
            commonApi = new CommonBusiness();
            currentUser = HttpContext.Current.User.Identity.Name; 

        }

        [HttpGet]
        [Route("app/vdfmonDollarCustomerchart/{accountname}")]
        public List<VDFChartObject> getVdfMonthDollarBenefitforcustomerchart(string accountname) 
        {

            return admApi.getVdfMonthDollarBenefitforcustomerchart(accountname);

        }

        [HttpGet]
        [Route("app/vdfmonDollarHPchart/{accountname}")]
        public List<VDFChartObject> getVdfMonthDollarBenefitHPchart(string accountname)
        {

            return admApi.getVdfMonthDollarBenefitHPchart(accountname);

        }

        [HttpGet]
        [Route("app/vdfmonEffortSavingchart/{accountname}")]
        public List<VDFChartObject> getVdfMonthEffortSavingchart(string accountname)
        {

            return admApi.getVdfMonthEffortSavingchart(accountname);

        }

        [HttpGet]
        [Route("app/vdfmonIncidentreductionchart/{accountname}")]
        public List<VDFChartObject> getVdfMonthIncidentreductionchart(string accountname)
        {

            return admApi.getVdfMonthIncidentreductionchart(accountname);

        }

        [HttpGet]
        [Route("app/vdfQuaDollarCustomerchart/{accountname}")]
        public List<VDFChartObject> getVdfQuarterDollarBenefitforcustomerchart(string accountname)
        {

            return admApi.getVdfQuarterDollarBenefitforcustomerchart(accountname);

        }

        [HttpGet]
        [Route("app/vdfQuaDollarHPchart/{accountname}")]
        public List<VDFChartObject> getVdfQuarterDollarBenefitHPchart(string accountname)
        {

            return admApi.getVdfQuarterDollarBenefitHPchart(accountname);

        }

        [HttpGet]
        [Route("app/vdfQuaEffortSavingchart/{accountname}")]
        public List<VDFChartObject> getVdfQuarterEffortSavingchart(string accountname)
        {

            return admApi.getVdfQuarterEffortSavingchart(accountname);

        }

        [HttpGet]
        [Route("app/vdfQuaIncidentreductionchart/{accountname}")]
        public List<VDFChartObject> getVdfQuarterIncidentreductionchart(string accountname)
        {

            return admApi.getVdfQuarterIncidentreductionchart(accountname);

        }

        [HttpGet]
        [Route("app/vdfaccountname")]
        public List<VDFChartObject> getVdfaccountname()
        {

            return admApi.getVdfaccountname();

        }

        [HttpGet]
        [Route("app/vdfcompleted")]
        public List<VDFChartObject> getVdfstatus()
        {

            return admApi.getVdfstatus();

        }
    }
}