﻿using RapidReport.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RapidReport.Controllers
{
    public class CommonSubTileController : ApiController
    {
        private readonly CommonBusiness CommonBusinessObj;
        public CommonSubTileController()
        {
            CommonBusinessObj = new CommonBusiness();
        }
        [HttpGet]
        [Route("app/getSubTileInformation/{SubTileName}")]
        public List<CommonBusinessSubTileObject> GetSubTileInformation(string SubTileName)
        {
            return CommonBusinessObj.GetSubTileInformation(SubTileName);
        }
    }
}
