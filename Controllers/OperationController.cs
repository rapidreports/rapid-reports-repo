﻿using RapidReport.Models.Operation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RapidReport.Controllers
{
    public class OperationController : ApiController
    {
        private readonly OperationBusiness OperationBusinessObj;
        public OperationController()
        {
            OperationBusinessObj = new OperationBusiness();
        }
        [HttpGet]
        [Route("app/getOperationDashboardDetails/{serviceline}/{year}/{month}")]
        public OperationBusinessObject GetOperationDashboardDetails(string serviceline, string year, string month)
        {
            return OperationBusinessObj.GetOperationDashboardDetails(serviceline,month,year);
        }
    }
}
