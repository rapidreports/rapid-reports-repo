﻿using RapidReport.Models.IDS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RapidReport.Controllers
{
    public class IDSController : ApiController
    {

        private readonly IDSBusiness IDSBusinessObj;
        public IDSController()
        {
            IDSBusinessObj = new IDSBusiness();
        }
        [HttpGet]
        [Route("app/getIDSProjectsDetails/{year}/{month}")]
        public IDSObjects GetIDSProjectsDetails(string year, string month)
        {
            return IDSBusinessObj.GetIDSProjectsDetails(month, year);
        }

    }
}