﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.Common.Overview
{
    public class Accounts
    {
        public int AccountId { get; set; }
        public string AccountName { get; set; }
    }

    public class CustomerMonthlyWBS
    {
        public int WBSId { get; set; }
        public int AccountId { get; set; }
        public string WBSCode { get; set; }
        public string WBSDescription { get; set; }
        public string Month { get; set; }
        public int? Year { get; set; }
        public DateTime? LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
    }

    public class MonthFTE
    {
        public int? FTEId { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string FTE { get; set; }
        public decimal FTEValue { get; set; }
        public string FTEType { get; set; }
        public string LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }
    }

    public class Contract
    {
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string ContractId { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }

    }

    public class MasterContract
    {
        public string ContractId { get; set; }
        public string Month { get; set; }
        public int? Year { get; set; }
        public decimal? Amount { get; set; }
    }
}