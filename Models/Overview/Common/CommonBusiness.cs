﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using RapidReport.Extension;

namespace RapidReport.Models.Common.Overview
{
    public class CommonBusiness
    {
        public IList<Accounts> Account()
        {
            IList<Accounts> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getAccounts");

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<Accounts>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        Accounts acc = new Accounts
                        {
                            AccountName = drow["AccountName"].ToString(),
                            AccountId = int.Parse(drow["AccountId"].ToString())
                        };

                        res.Add(acc);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<CustomerMonthlyWBS> CustMonthlyWBS(string month, int year)
        {
            IList<CustomerMonthlyWBS> res = null;

            try
            {
                DataSet rec = DB.ReadDS("");
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<MonthFTE> MonthlyFTE(string month, int year)
        {
            IList<MonthFTE> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_CustomersMonthlyFTE",
                    new object[] { "@Month", month, "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<MonthFTE>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        MonthFTE monfte = new MonthFTE
                        {
                            FTEId = int.Parse(drow["FTEId"].ToString()),
                            AccountId = int.Parse(drow["AccountId"].ToString()),
                            FTE = drow["FTE"].ToString(),
                            FTEValue = decimal.Parse(drow["FTEValue"].ToString()),
                            FTEType = drow["FTEType"].ToString(),
                            LastUpdatedBy = drow["LastUpdatedBy"].ToString(),
                            LastUpdatedOn = DateTime.Parse(drow["LastUpdatedOn"].ToString()),
                            Month = drow["Month"].ToString(),
                            Year = int.Parse(drow["Year"].ToString()),
                            AccountName = drow["AccountName"].ToString()
                        };

                        res.Add(monfte);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<MonthFTE> GetMonthlyFTE(string month, string year, int Accountid)
        {
            IList<MonthFTE> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getMonthlyFTE",
                    new object[] { "@Month", month, "@Year", year, "@AccountId",Accountid });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<MonthFTE>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        MonthFTE monfte = new MonthFTE
                        {
                            FTEId = int.Parse(drow["FTEId"].ToString()),
                            AccountId = int.Parse(drow["AccountId"].ToString()),
                            FTE = drow["FTE"].ToString(),
                            FTEValue = decimal.Parse(drow["FTEValue"].ToString()),
                            FTEType = drow["FTEType"].ToString(),
                            LastUpdatedBy = drow["LastUpdatedBy"].ToString(),
                            LastUpdatedOn = DateTime.Parse(drow["LastUpdatedOn"].ToString()),
                            Month = drow["Month"].ToString(),
                            Year = int.Parse(drow["Year"].ToString()),
                            AccountName = drow["AccountName"].ToString()
                        };

                        res.Add(monfte);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<Contract> GetContract(int Accountid)
        {
            IList<Contract> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getContractByAccountId",
                    new object[] { "@AccountId", Accountid });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<Contract>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        Contract contract = new Contract
                        {
                            ContractId = drow["ContractId"].ToString(),
                            Month = drow["Month"].ToString(),
                            Year = drow["Year"].ToString()
                        };

                        res.Add(contract);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<Contract> GetNDFTEContract(int Accountid)
        {
            IList<Contract> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getNDFTEContractByAccountId",
                    new object[] { "@AccountId", Accountid });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<Contract>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        Contract contract = new Contract
                        {
                            ContractId = drow["ContractId"].ToString(),
                            //Month = drow["Month"].ToString(),
                            Year = drow["Year"].ToString(),
                            Month=drow["Month"].ToString()
                        };

                        res.Add(contract);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public void SaveMonthlyFte(List<MonthFTE> savedetails, string currentuser)
        {
           
           
            
            try
            {
                foreach (MonthFTE data in savedetails)
                {
                    object[] param = {
                                 "@AccountId", data.AccountId,
                                 "@FTE", data.FTE,
                                 "@FTEValue", data.FTEValue,
                                 "@FTEType", data.FTEType,
                                 "@LastUpdatedBy", currentuser,
                                 "@Month", data.Month,
                                 "@Year", data.Year
                           };
                    DB.InsertorUpdate("usp_InsertOrUpdateMonthlyFTE", param);
                }
                
                
            }
            catch (Exception ex)
            {

            }
        }

        public void InsertorUpdateContract(int AccountId, int Month, string Year, Contract contract)
        {



            try
            {
              
                    object[] param = {
                                 "@AccountId", AccountId,
                                 "@Month", Month,
                                 "@Year", Year,
                                 "@ContractId", contract.ContractId
                           };
                    DB.InsertorUpdate("usp_InserOrUpdateContract", param);
               


            }
            catch (Exception ex)
            {

            }
        }

        public void InsertorUpdateNDFTEContract(int AccountId, string Year, Contract contract)
        {



            try
            {

                object[] param = {
                                 "@AccountId", AccountId,
                                 "@Year", Year,
                                 "@Month",contract.Month,
                                 "@ContractId", contract.ContractId
                           };
                DB.InsertorUpdate("usp_InserOrUpdateNDFTEContract", param);



            }
            catch (Exception ex)
            {

            }
        }
        public void DeleteContract(int AccountId, int Month, string Year, Contract contract)
        {



            try
            {

                object[] param = {
                                 "@AccountId", AccountId,
                                 "@Month", Month,
                                 "@Year", Year,
                                 "@ContractId", contract.ContractId
                           };
                DB.InsertorUpdate("usp_deleteContract", param);



            }
            catch (Exception ex)
            {

            }
        }
        public void DeleteNDFTEContract(int AccountId, string Year, Contract contract)
        {



            try
            {

                object[] param = {
                                 "@AccountId", AccountId,
                                 "@Year", Year,
                                 "@ContractId", contract.ContractId
                           };
                DB.InsertorUpdate("usp_deleteNDFTEContract", param);



            }
            catch (Exception ex)
            {

            }
        }
    }
}