﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using RapidReport.Extension;

namespace RapidReport.Models.Operational
{
    public class DashboardBusiness
    {
        public IList<ResourceUtilization> GetResourceUtilization(string month, string year)
        {
            IList<ResourceUtilization> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getOperationaldetails", new object[] { "@Month", month, "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<ResourceUtilization>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        ResourceUtilization data = new ResourceUtilization
                        {
                            Month = drow["Month"].ToString(),
                            Year = drow["Year"].ToString(),
                            FTE = drow["FTE"].ToString(),
                            ActiveInactive = drow["ActiveInactive"].ToString(),
                            REGCWF = drow["REGCWF"].ToString(),
                            JobLevel = drow["JobLevel"].ToString(),
                            OrgDesc = drow["OrgDesc"].ToString(),
                            AllocationStatus = drow["AllocationStatus"].ToString(),
                            BenchAgeing = drow["BenchAgeing"].ToString(),
                            FreshersLaterals = drow["FreshersLaterals"].ToString(),
                            DOJ = drow["DOJ"].ToString(),
                            Manger = drow["Manger"].ToString(),
                            LTName = drow["LTName"].ToString(),
                            MRUCode = drow["MRUCode"].ToString(),
                            Mrunoncomp = drow["Mrunoncomp"].ToString(),
                            Environmentlttstt = drow["Environmentlttstt"].ToString(),
                            Lhccvsmobility = drow["Lhccvsmobility"].ToString(),
                            Sabacompliance = drow["Sabacompliance"].ToString()
                            
                        };

                        res.Add(data);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<ResourceUtilization> ManagerOperational(int year, string manager)
        {
            IList<ResourceUtilization> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getManagerOperational", new object[] { 
                    "@Year", year,
                    "@Manager", manager
                });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<ResourceUtilization>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        ResourceUtilization data = new ResourceUtilization
                        {
                            Month = drow["Month"].ToString(),
                            Year = drow["Year"].ToString(),
                            FTE = drow["FTE"].ToString(),
                            ActiveInactive = drow["ActiveInactive"].ToString(),
                            REGCWF = drow["REGCWF"].ToString(),
                            JobLevel = drow["JobLevel"].ToString(),
                            OrgDesc = drow["OrgDesc"].ToString(),
                            AllocationStatus = drow["AllocationStatus"].ToString(),
                            BenchAgeing = drow["BenchAgeing"].ToString(),
                            FreshersLaterals = drow["FreshersLaterals"].ToString(),
                            DOJ = drow["DOJ"].ToString(),
                            Manger = drow["Manger"].ToString(),
                            LTName = drow["LTName"].ToString(),
                            MRUCode = drow["MRUCode"].ToString()
                        };

                        res.Add(data);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }
    }
}