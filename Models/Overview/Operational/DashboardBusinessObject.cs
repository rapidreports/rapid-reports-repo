﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.Operational
{
    public class ResourceUtilization
    {
        public string Month { get; set; }
        public string Year { get; set; }
        public string FTE { get; set; }
        public string ActiveInactive { get; set; }
        public string REGCWF { get; set; }
        public string JobLevel { get; set; }
        public string OrgDesc { get; set; }
        public string AllocationStatus { get; set; }
        public string BenchAgeing { get; set; }
        public string FreshersLaterals { get; set; }
        public string DOJ { get; set; }
        public string Manger { get; set; }
        public string LTName { get; set; }
        public string MRUCode { get; set; }
        public string Mrunoncomp { get; set; }
        public string Environmentlttstt { get; set; }
        public string Lhccvsmobility { get; set; }
        public string Sabacompliance { get; set; }

        
    }
}