﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.Report
{
    public class MonthWBS
    {
        public int Id { get; set; }
        public string WBSCode { get; set; }
        public string JobLevel { get; set; }
        public string ResourceCategory { get; set; }
        public int? BilledHours { get; set; }
        public decimal? BilledAmount { get; set; }
        public string Month { get; set; }
    }

    public class MonthRateCard
    {
        public int RateCardId { get; set; }
        public string BSASLevel { get; set; }
        public string BSASLevelType { get; set; }
        public decimal? HourlyRateINR { get; set; }
        public decimal? HourlyRateUSD { get; set; }
        public string LastUpdatedBy { get; set; }
        public DateTime? LastUpdatedOn { get; set; }
        public string Month { get; set; }
        public int Year { get; set; }
    }

    public class ContractCost
    {
        public string AccountName { get; set; }
        public decimal Total { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }

    }

    public class BillingCost
    {
        public string AccountName { get; set; }
        public decimal Total { get; set; }
        public string Month { get; set; }
        public int? Year { get; set; }
    }

    public class AccountBillType
    {
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountType { get; set; }
        public string Month { get; set; }
        public int? Year { get; set; }
    }

    public class WBS
    {
        public string WBSCode { get; set; }
        public string JobLevel { get; set; }
        public string ResourceCategory { get; set; }
        public decimal? BilledHours { get; set; }
        public decimal? BilledAmount { get; set; }
        public string Month { get; set; }
        public int? AccountId { get; set; }
        public string AccountName { get; set; }
        public string ServiceLine { get; set; }
        public decimal? FTE { get; set; }
        public string ManagerL6 { get; set; }
        public string MRUL4 { get; set; }
    }
}