﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using RapidReport.Extension;
using RapidReport.Models.Common.Overview;
using RapidReport.Models.FTE;

namespace RapidReport.Models.Report
{
    public class ReportBusiness
    {
        public IList<ContractCost> CustContractCost(int month, int year)
        {
            IList<ContractCost> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_ContractCost",
                    new object[] { "@Month", month, "@Year", year });

                if(rec!=null &&
                    rec.Tables.Count>0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<ContractCost>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        ContractCost concost = new ContractCost
                        {
                            AccountName = drow["AccountName"].ToString(),
                            Total = decimal.Parse(drow["Total"].ToString())
                        };

                        res.Add(concost);
                    }
                }
            }
            catch (Exception ex)
            {
                
            }

            return res;
        }

        public IList<BillingCost> CustBillingCost(string month, int year)
        {
            IList<BillingCost> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_BillingCost",
                    new object[] { "@Month", month, "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<BillingCost>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        BillingCost billcost = new BillingCost
                        {
                            AccountName = drow["AccountName"].ToString(),
                            Total = decimal.Parse(drow["Total"].ToString())
                        };

                        res.Add(billcost);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<MonthWBS> MonthlyDumpWBS(string month, int year)
        {
            IList<MonthWBS> res = null;            

            try
            {
                DataSet rec = DB.ReadDS("");
            }
            catch (Exception ex)
            {
                
            }

            return res;
        }

        public IList<MonthRateCard> MonthlyRateCard(string month, int year)
        {
            IList<MonthRateCard> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_RateCard",
                    new object[] { "@Month", month, "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<MonthRateCard>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        MonthRateCard rateCard = new MonthRateCard
                        {
                            BSASLevel = drow["BSASLevel"].ToString(),
                            BSASLevelType = drow["BSASLevelType"].ToString(),
                            HourlyRateUSD = decimal.Parse(drow["HourlyRateUSD"].ToString())
                        };

                        res.Add(rateCard);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<AccountBillType> AccountTyp(string month, int year)
        {
            IList<AccountBillType> res = null;

            try
            {
                //DataSet rec = DB.ReadDS("usp_reportAccounts",
                //    new object[] { "@Month", month, "@Year", year });
                DataSet rec = DB.ReadDS("usp_getAccounts");

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<AccountBillType>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        AccountBillType acc = new AccountBillType
                        {
                            AccountName = drow["AccountName"].ToString(),
                            AccountId = int.Parse(drow["AccountId"].ToString())//,
                            //AccountType = drow["AccountType"].ToString()
                        };

                        res.Add(acc);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<BillingCost> CustBillingCostYealy(int year)
        {
            IList<BillingCost> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_CustomerBillingCostYearly",
                    new object[] { "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<BillingCost>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        BillingCost billcost = new BillingCost
                        {
                            AccountName = drow["AccountName"].ToString(),
                            Total = decimal.Parse(drow["Total"].ToString()),
                            Month = drow["Month"].ToString(),
                            Year = int.Parse(drow["Year"].ToString())
                        };

                        res.Add(billcost);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<ContractCost> CustContractCostYealy(int year)
        {
            IList<ContractCost> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_CustomerContractCostYearly",
                    new object[] { "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<ContractCost>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        ContractCost billcost = new ContractCost
                        {
                            AccountName = drow["AccountName"].ToString(),
                            Total = decimal.Parse(drow["Total"].ToString()),
                            Month = int.Parse(drow["Month"].ToString()),
                            Year = int.Parse(drow["Year"].ToString())
                        };

                        res.Add(billcost);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<MonthFTE> CustomerYearlyFTE(int year)
        {
            IList<MonthFTE> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_CustomerYearlyFTE",
                    new object[] { "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<MonthFTE>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        MonthFTE monfte = new MonthFTE
                        {
                            AccountId = int.Parse(drow["AccountId"].ToString()),
                            FTE = drow["FTE"].ToString(),
                            FTEValue = decimal.Parse(drow["FTEValue"].ToString()),
                            FTEType = drow["FTEType"].ToString(),
                            Month = drow["Month"].ToString(),
                            Year = int.Parse(drow["Year"].ToString()),
                            AccountName = drow["AccountName"].ToString()
                        };

                        res.Add(monfte);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<MonthRateCard> YearlyRateCard(int year)
        {
            IList<MonthRateCard> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_YearlyRateCard",
                    new object[] { "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<MonthRateCard>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        MonthRateCard rateCard = new MonthRateCard
                        {
                            BSASLevel = drow["BSASLevel"].ToString(),
                            BSASLevelType = drow["BSASLevelType"].ToString(),
                            HourlyRateUSD = decimal.Parse(drow["HourlyRateUSD"].ToString()),
                            Month = drow["Month"].ToString(),
                            Year = int.Parse(drow["Year"].ToString())
                        };

                        res.Add(rateCard);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<AccountBillType> YearlyAccountType(int year)
        {
            IList<AccountBillType> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_YearlyReportAccounts",
                    new object[] { "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<AccountBillType>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        AccountBillType acc = new AccountBillType
                        {
                            AccountName = drow["AccountName"].ToString(),
                            AccountId = int.Parse(drow["AccountId"].ToString()),
                            AccountType = drow["AccountType"].ToString(),
                            Month = drow["Month"].ToString(),
                            Year = int.Parse(drow["Year"].ToString())
                        };

                        res.Add(acc);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<WBS> AllTimeWBS()
        {
            IList<WBS> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getAllTimeWBS");

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<WBS>();
                    int i = 0;
                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        WBS item = new WBS
                        {
                            //AccountName = drow["AccountName"].ToString(),
                            //AccountId = int.Parse(drow["AccountId"].ToString()),
                            Month = drow["Month"].ToString(),
                            WBSCode = drow["WBSCode"].ToString(),
                            JobLevel = drow["JobLevel"].ToString(),
                            ResourceCategory = drow["ResourceCategory"].ToString(),
                            BilledHours = decimal.Parse(drow["BilledHours"].ToString(), System.Globalization.NumberStyles.Float),
                            BilledAmount = decimal.Parse(drow["BilledAmount"].ToString()),
                            ServiceLine = drow["ServiceLine"].ToString(),
                            FTE = decimal.Parse(drow["FTE"].ToString()),
                            ManagerL6 = drow["ManagerL6"].ToString(),
                            MRUL4 = drow["MRUL4"].ToString()
                        };

                        res.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<dfteBusinessObjects> MonthlyNDFTE(string month, int year)
        {
            IList<dfteBusinessObjects> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getNDFTEMonthly",
                    new object[] { "@Month", month, "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<dfteBusinessObjects>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        dfteBusinessObjects billcost = new dfteBusinessObjects
                        {
                            AccountId = int.Parse(drow["AccountId"].ToString()),
                            AccountName = drow["AccountName"].ToString(),
                            Year = drow["Year"].ToString(),
                            Month = drow["Month"].ToString(),
                            CostBaselined = decimal.Parse(drow["CostBaselined"].ToString()),
                            AdditionalCost = drow["AdditionalCost"].ToString() == string.Empty ? 0 : decimal.Parse(drow["AdditionalCost"].ToString()),
                            RiskAmount = drow["RiskAmount"].ToString() == string.Empty ? 0 : decimal.Parse(drow["RiskAmount"].ToString()),
                            RevenueBaseline = drow["RevenueBaseline"].ToString() == string.Empty ? 0 : decimal.Parse(drow["RevenueBaseline"].ToString())
                        };

                        res.Add(billcost);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<ndfteWbsBusinessobject> MonthlyNDWBS(string month, int year)
        {
            IList<ndfteWbsBusinessobject> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getNDWBSMonthly",
                    new object[] { "@Month", month, "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<ndfteWbsBusinessobject>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        ndfteWbsBusinessobject billcost = new ndfteWbsBusinessobject
                        {
                            AccountName = drow["AccountName"].ToString(),
                            WBSId = int.Parse(drow["WBSId"].ToString()),
                            WBSCode = drow["WBSCode"].ToString(),
                            WBSDescription = drow["WBSDescription"].ToString(),
                            LastUpdatedBy = drow["LastUpdatedBy"].ToString(),
                            IsActive = bool.Parse(drow["IsActive"].ToString()),
                            Month = drow["Month"].ToString()
                        };

                        res.Add(billcost);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<Contract> MonthlyNDContract(string month, int year)
        {
            IList<Contract> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getNDContractMonthly",
                    new object[] { "@Month", month, "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<Contract>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        Contract billcost = new Contract
                        {
                            AccountName = drow["AccountName"].ToString(),
                            ContractId = drow["ContractId"].ToString(),
                            Month = drow["Month"].ToString(),
                            Year = drow["Year"].ToString()
                        };

                        res.Add(billcost);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<ndfteCR> MonthlyNDChangeRequest(string month, int year)
        {
            IList<ndfteCR> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getNDChangeRequestMonthly",
                    new object[] { "@Month", month, "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<ndfteCR>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        ndfteCR billcost = new ndfteCR
                        {
                            AccountName = drow["AccountName"].ToString(),
                            Cr = drow["CR"].ToString(),
                            AccountId = Convert.ToInt32(drow["AccountId"].ToString()),
                            Year = drow["Year"].ToString(),
                            Month = drow["Month"].ToString(),
                            Reason = drow["Reason"].ToString(),
                            AdditionalFTE = decimal.Parse(drow["AdditionalFTE"].ToString()),
                            Amount = decimal.Parse(drow["Amount"].ToString()),
                            Status = Convert.ToInt32(drow["Status"].ToString()), //== null ? null : (bool?)((bool)drow["Status"])),//(bool?)drow["Status"], //? null : (bool?)(bool)(drow["Status"]), //bool.Parse(drow["Status"].ToString()),
                            CrId = Convert.ToInt32(drow["Id"].ToString())
                        };

                        res.Add(billcost);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<WBS> MonthlyMasterWBS(string month, int year)
        {
            IList<WBS> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getMonthlyMasterWBS",
                    new object[] { "@Month", month, "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<WBS>();
                    int i = 0;
                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        WBS item = new WBS
                        {
                            Month = drow["Month"].ToString(),
                            WBSCode = drow["WBSCode"].ToString(),
                            JobLevel = drow["JobLevel"].ToString(),
                            ResourceCategory = drow["ResourceCategory"].ToString(),
                            BilledHours = decimal.Parse(drow["BilledHours"].ToString(), System.Globalization.NumberStyles.Float),
                            BilledAmount = decimal.Parse(drow["BilledAmount"].ToString()),
                            ServiceLine = drow["ServiceLine"].ToString(),
                            FTE = decimal.Parse(drow["FTE"].ToString()),
                            ManagerL6 = drow["ManagerL6"].ToString(),
                            MRUL4 = drow["MRUL4"].ToString()
                        };

                        res.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }

        public IList<MasterContract> MonthlyMasterContract(string month, int year)
        {
            IList<MasterContract> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getMonthlyMasterContract",
                    new object[] { "@Month", month, "@Year", year });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<MasterContract>();
                    int i = 0;
                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        MasterContract item = new MasterContract
                        {
                            Month = drow["Month"].ToString(),
                            Year = int.Parse(drow["Year"].ToString()),
                            ContractId = drow["ContractId"].ToString(),
                            Amount = decimal.Parse(drow["Amount"].ToString())
                        };

                        res.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }
    }
}