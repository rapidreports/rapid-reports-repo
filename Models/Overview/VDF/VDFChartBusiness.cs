﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RapidReport.Extension;
using System.Data.SqlClient;
using System.Data;

namespace RapidReport.Models.Overview.VDF
{
    public class VDFChartBusiness
    {
        int defaultvalue = 0;
        public List<VDFChartObject> getVdfMonthDollarBenefitforcustomerchart(string accountname)
        {

            List<VDFChartObject> admchartVdfMonthDollarBenefitforcustomer = new List<VDFChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@accountname", accountname };
            try
            {
                result = DB.ReadDS("usp_getMonthDollarBenefitforcustomer", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        VDFChartObject row = new VDFChartObject();

                        row.Closuremonth = drow["Closuremonth"].ToString();
                        if (drow["DollarBenefitforcustomer"].ToString() != "")
                        {
                            row.DollarBenefitforcustomer = float.Parse(drow["DollarBenefitforcustomer"].ToString());
                        }
                        else
                        {
                            row.DollarBenefitforcustomer = float.Parse(defaultvalue.ToString());
                        }

                        admchartVdfMonthDollarBenefitforcustomer.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartVdfMonthDollarBenefitforcustomer;
        }

        public List<VDFChartObject> getVdfMonthDollarBenefitHPchart(string accountname)
        {
            List<VDFChartObject> admchartVdfMonthDollarBenefitHP = new List<VDFChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@accountname", accountname };
            try
            {
                result = DB.ReadDS("usp_getMonthDollarBenefitHP", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        VDFChartObject row = new VDFChartObject();

                        row.Closuremonth = drow["Closuremonth"].ToString();
                        if (drow["DollarBenefitHP"].ToString() != "")
                        {
                            row.DollarBenefitHP = float.Parse(drow["DollarBenefitHP"].ToString());
                        }
                        else
                        {
                            row.DollarBenefitHP = float.Parse(defaultvalue.ToString());
                        }


                        admchartVdfMonthDollarBenefitHP.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartVdfMonthDollarBenefitHP;
        }

        public List<VDFChartObject> getVdfMonthEffortSavingchart(string accountname)
        {
            List<VDFChartObject> admchartVdfMonthEffortSaving = new List<VDFChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@accountname", accountname };
            try
            {
                result = DB.ReadDS("usp_getMonthEffortSaving", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        VDFChartObject row = new VDFChartObject();

                        row.Closuremonth = drow["Closuremonth"].ToString();
                        if (drow["EffortSaving"].ToString() != "")
                        {
                            row.EffortSaving = drow["EffortSaving"].ToString();
                        }

                        else
                        {
                            row.EffortSaving = defaultvalue.ToString();
                        }
                        admchartVdfMonthEffortSaving.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartVdfMonthEffortSaving;
        }

        public List<VDFChartObject> getVdfMonthIncidentreductionchart(string accountname)
        {
            List<VDFChartObject> admchartVdfMonthIncidentreduction = new List<VDFChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@accountname", accountname };
            try
            {
                result = DB.ReadDS("usp_getMonthIncidentreduction", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        VDFChartObject row = new VDFChartObject();

                        row.Closuremonth = drow["Closuremonth"].ToString();
                        if (drow["Incidentreduction"].ToString() != "")
                        {
                            row.Incidentreduction = drow["Incidentreduction"].ToString();
                        }
                        else
                        {
                            row.Incidentreduction = defaultvalue.ToString();
                        }

                        admchartVdfMonthIncidentreduction.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartVdfMonthIncidentreduction;
        }

        public List<VDFChartObject> getVdfQuarterDollarBenefitforcustomerchart(string accountname)
        {
            List<VDFChartObject> admchartVdfQuarterDollarBenefitforcustomer = new List<VDFChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@accountname", accountname };
            try
            {
                result = DB.ReadDS("usp_getQuarterDollarBenefitforcustomer", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        VDFChartObject row = new VDFChartObject();

                        row.ClosureQuarter = drow["ClosureQuarter"].ToString();
                        if (drow["DollarBenefitforcustomer"].ToString() != "")
                        {
                            row.DollarBenefitforcustomer = float.Parse(drow["DollarBenefitforcustomer"].ToString());
                        }
                        else
                        {
                            row.DollarBenefitforcustomer = float.Parse(defaultvalue.ToString());
                        }

                        admchartVdfQuarterDollarBenefitforcustomer.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartVdfQuarterDollarBenefitforcustomer;
        }

        public List<VDFChartObject> getVdfQuarterDollarBenefitHPchart(string accountname)
        {
            List<VDFChartObject> admchartVdfQuarterDollarBenefitHP = new List<VDFChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@accountname", accountname };
            try
            {
                result = DB.ReadDS("usp_getQuarterDollarBenefitHP", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        VDFChartObject row = new VDFChartObject();

                        row.ClosureQuarter = drow["ClosureQuarter"].ToString();
                        if (drow["DollarBenefitHP"].ToString() != "")
                        {
                            row.DollarBenefitHP = float.Parse(drow["DollarBenefitHP"].ToString());
                        }
                        else
                        {
                            row.DollarBenefitHP = float.Parse(defaultvalue.ToString());
                        }

                        admchartVdfQuarterDollarBenefitHP.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartVdfQuarterDollarBenefitHP;
        }

        public List<VDFChartObject> getVdfQuarterEffortSavingchart(string accountname)
        {
            List<VDFChartObject> admchartVdfQuarterEffortSaving = new List<VDFChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@accountname", accountname };
            try
            {
                result = DB.ReadDS("usp_getQuarterEffortSaving", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        VDFChartObject row = new VDFChartObject();

                        row.ClosureQuarter = drow["ClosureQuarter"].ToString();
                        if (drow["EffortSaving"].ToString() != "")
                        {
                            row.EffortSaving = drow["EffortSaving"].ToString();
                        }
                        else
                        {
                            row.EffortSaving = defaultvalue.ToString();
                        }


                        admchartVdfQuarterEffortSaving.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartVdfQuarterEffortSaving;
        }

        public List<VDFChartObject> getVdfQuarterIncidentreductionchart(string accountname)
        {
            List<VDFChartObject> admchartVdfQuarterIncidentreduction = new List<VDFChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@accountname", accountname };
            try
            {
                result = DB.ReadDS("usp_getQuarterIncidentreduction", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        VDFChartObject row = new VDFChartObject();

                        row.ClosureQuarter = drow["ClosureQuarter"].ToString();
                        if (drow["Incidentreduction"].ToString() != "")
                        {
                            row.Incidentreduction = drow["Incidentreduction"].ToString();
                        }
                        else
                        {
                            row.Incidentreduction = defaultvalue.ToString();
                        }


                        admchartVdfQuarterIncidentreduction.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartVdfQuarterIncidentreduction;
        }

        public List<VDFChartObject> getVdfaccountname()
        {
            List<VDFChartObject> vdfaccountname = new List<VDFChartObject>();
            DataSet result;
            //var para = ""; 

            try
            {
                result = DB.ReadDS("usp_getVDFAccountname");

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        VDFChartObject row = new VDFChartObject
                        {
                            accountname = drow["accountname"].ToString(),


                        };

                        vdfaccountname.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return vdfaccountname;
        }

        public List<VDFChartObject> getVdfstatus()
        {
            List<VDFChartObject> vdfstatus = new List<VDFChartObject>();
            DataSet result;
            //var para = ""; 

            try
            {
                result = DB.ReadDS("usp_getVDFStatus");

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        VDFChartObject row = new VDFChartObject
                        {
                            VDFCompleted = drow["VDFCompleted"].ToString(),


                        };

                        vdfstatus.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return vdfstatus;
        }
    }
}