﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.Overview.VDF
{
    public class VDFChartObject
    {

        public string accountname { get; set; }
        public string VDFCompleted { get; set; }
        public float DollarBenefitHP { get; set; }
        public float DollarBenefitforcustomer { get; set; }
        public string EffortSaving { get; set; }
        public string Incidentreduction { get; set; }
        public string Closuremonth { get; set; }
        public string ClosureQuarter { get; set; }
    }
}