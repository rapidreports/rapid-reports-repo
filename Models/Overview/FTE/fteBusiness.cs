﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RapidReport.Extension;
using System.Data.SqlClient;
using System.Data; 

namespace RapidReport.Models.FTE
{
    public class fteBusiness
    {
        public List<fteBusinessObjects> getAccounts()
        {
            List<fteBusinessObjects> prosessedresult = new List<fteBusinessObjects>();
            DataSet result;
            //var para = ""; 
            try
            {
                result = DB.ReadDS("usp_getAccounts");//,para);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        fteBusinessObjects row = new fteBusinessObjects
                        {
                            AccountId = int.Parse(drow["AccountId"].ToString()),
                            AccountName = drow["AccountName"].ToString()
                        };

                        prosessedresult.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return prosessedresult;
        }

        public List<fteWbsBusinessobject> getWbsById(int Id, string Month, string Year)
        {
            List<fteWbsBusinessobject> prosessedresult = new List<fteWbsBusinessobject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@AccountId", Id , "@Month", Month, "@Year", Year};
            try
            {
                result = DB.ReadDS("usp_getWBSByAccountId", param);//,para);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        fteWbsBusinessobject row = new fteWbsBusinessobject
                        {
                            WBSId = int.Parse(drow["WBSId"].ToString()),
                            WBSCode = drow["WBSCode"].ToString(),
                            WBSDescription = drow["WBSDescription"].ToString(),
                            LastUpdatedBy = drow["LastUpdatedBy"].ToString(),
                            //LastUpdatedOn = drow["LastUpdatedOn"].ToString() == string.Empty ?  : DateTime.Parse(drow["LastUpdatedOn"].ToString()),
                            IsActive = bool.Parse(drow["IsActive"].ToString())
                        };

                        prosessedresult.Add(row);
                    }
                    }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return prosessedresult;
        }

        public IList<dfteBusinessObjects> GetDfteDetails(string year, int Accountid)
        {
            IList<dfteBusinessObjects> res = null;

            try
            {
                DataSet rec = DB.ReadDS("usp_getndfteByAccountId",
                    new object[] { "@Year", year, "@AccountId", Accountid });

                if (rec != null &&
                    rec.Tables.Count > 0 &&
                    rec.Tables[0].Rows.Count > 0)
                {
                    res = new List<dfteBusinessObjects>();

                    foreach (DataRow drow in rec.Tables[0].Rows)
                    {
                        dfteBusinessObjects mondfte = new dfteBusinessObjects
                        {
                            AccountId = int.Parse(drow["AccountId"].ToString()),
                            Year = drow["Year"].ToString(),
                            Month = drow["Month"].ToString(),
                            CostBaselined = decimal.Parse(drow["CostBaselined"].ToString()),
                            AdditionalCost = drow["AdditionalCost"].ToString() == string.Empty ? 0 : decimal.Parse(drow["AdditionalCost"].ToString()),
                            RiskAmount = drow["RiskAmount"].ToString() == string.Empty ? 0 : decimal.Parse(drow["RiskAmount"].ToString()),
                            RevenueBaseline = drow["RevenueBaseline"].ToString() == string.Empty ? 0 : decimal.Parse(drow["RevenueBaseline"].ToString())
                        };

                        res.Add(mondfte);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return res;
        }
        public List<ndfteCR> getndfteCr(int AccountId, string Year)
        {
            List<ndfteCR> dfteCrResult = new List<ndfteCR>();
            DataSet result1;

            object[] param = { "@AccountId", AccountId, "@Year", Year };
            try
            {
                result1 = DB.ReadDS("usp_getNDFTECr",param);

                if (result1 != null)
                {
                    foreach (DataRow drow in result1.Tables[0].Rows)
                    {
                        ndfteCR crDetails = new ndfteCR
                        {
                            Cr = drow["CR"].ToString(),
                            AccountId = Convert.ToInt32(drow["AccountId"].ToString()),
                            Year = drow["Year"].ToString(),
                            Month=drow["Month"].ToString(),
                            Reason = drow["Reason"].ToString(),
                            AdditionalFTE = decimal.Parse(drow["AdditionalFTE"].ToString()),
                            Amount = decimal.Parse(drow["Amount"].ToString()),
                            Status = Convert.ToInt32(drow["Status"].ToString()), //== null ? null : (bool?)((bool)drow["Status"])),//(bool?)drow["Status"], //? null : (bool?)(bool)(drow["Status"]), //bool.Parse(drow["Status"].ToString()),
                            CrId = Convert.ToInt32(drow["Id"].ToString())
                        };

                        dfteCrResult.Add(crDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                result1 = null;
            }

            return dfteCrResult;
        }

        public List<dfteCR> getdfteCr(int AccountId,string Month, string Year)
        {
            List<dfteCR> dfteCrResult = new List<dfteCR>();
            DataSet result1;

            object[] param = { "@AccountId", AccountId,"@Month", Month, "@Year", Year };
            try
            {
                result1 = DB.ReadDS("usp_getDFTECr", param);

                if (result1 != null)
                {
                    foreach (DataRow drow in result1.Tables[0].Rows)
                    {
                        dfteCR crDetails = new dfteCR
                        {
                            Cr = drow["CR"].ToString(),
                            AccountId = Convert.ToInt32(drow["AccountId"].ToString()),
                            Year = drow["Year"].ToString(),
                            Month = drow["Month"].ToString(),
                            Reason = drow["Reason"].ToString(),
                            AdditionalFTE = decimal.Parse(drow["AdditionalFTE"].ToString()),
                            Amount = decimal.Parse(drow["Amount"].ToString()),
                            Status = Convert.ToInt32(drow["Status"].ToString()), //== null ? null : (bool?)((bool)drow["Status"])),//(bool?)drow["Status"], //? null : (bool?)(bool)(drow["Status"]), //bool.Parse(drow["Status"].ToString()),
                            CrId = Convert.ToInt32(drow["Id"].ToString())
                        };

                        dfteCrResult.Add(crDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                result1 = null;
            }

            return dfteCrResult;
        }

        public void Insertndftecr(ndfteCR data)
        {
            try
            {

                object[] param = {
                                 "@AccountId", data.AccountId,
                                 "@Year", data.Year,
                                 "@Month",data.Month,
                                 "@CR", data.Cr,
                                 "@Reason", data.Reason,
                                 "@AdditionalFTE", data.AdditionalFTE,
                                 "@Amount", data.Amount,
                                 "@Status", 2
                             };


                DB.InsertorUpdate("usp_InsertNDFTEChangeRequest", param);

            }
            catch (Exception ex)
            {

            }
        }

        public void Insertdftecr(dfteCR data)
        {
            try
            {

                object[] param = {
                                 "@AccountId", data.AccountId,
                                 "@Year", data.Year,
                                 "@Month", data.Month,
                                 "@CR", data.Cr,
                                 "@Reason", data.Reason,
                                 "@AdditionalFTE", data.AdditionalFTE,
                                 "@Amount", data.Amount,
                                 "@Status", 2
                             };


                DB.InsertorUpdate("usp_InsertDFTEChangeRequest", param);

            }
            catch (Exception ex)
            {

            }
        }


        public void UpdateNDFTECrStatus(ndfteCR data)
        {
            try
            {

                object[] param = {
                                 "@crId", data.CrId,
                                 "@status", data.Status
                             };


                DB.InsertorUpdate("usp_UpdateNDFTECrStatus", param);

            }
            catch (Exception ex)
            {

            }
        }

        public void UpdateDFTECrStatus(dfteCR data)
        {
            try
            {

                object[] param = {
                                 "@crId", data.CrId,
                                 "@status", data.Status
                             };


                DB.InsertorUpdate("usp_UpdateDFTECrStatus", param);

            }
            catch (Exception ex)
            {

            }
        }

        public void InsertorUpdateWbsDetails(string Month, string Year, InsertorUpdateWbsDetails insertorupdatewbsdetails)
        {
            object[] param = {
                                 "@AccountId", insertorupdatewbsdetails.AccountId,
                                 "@WBSCode", insertorupdatewbsdetails.WBSCode,
                                 "@WBSDescription", insertorupdatewbsdetails.WBSDescription,
                                 "@LastUpdatedBy", insertorupdatewbsdetails.LastUpdatedBy,
                                 "@Month", Month,
                                 "@Year", Year
                             };
            try
            {
                DB.InsertorUpdate("usp_InsertOrUpdateWBSDetails", param);
            }
            catch (Exception ex)
            {

            }
        }

        public List<ndfteWbsBusinessobject> getNDWbsById(int Id, string Year)
        {
            List<ndfteWbsBusinessobject> prosessedresult = new List<ndfteWbsBusinessobject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@AccountId", Id, "@Year", Year };
            try
            {
                result = DB.ReadDS("usp_getNDWBSByAccountId", param);//,para);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        ndfteWbsBusinessobject row = new ndfteWbsBusinessobject
                        {
                            WBSId = int.Parse(drow["WBSId"].ToString()),
                            WBSCode = drow["WBSCode"].ToString(),
                            WBSDescription = drow["WBSDescription"].ToString(),
                            LastUpdatedBy = drow["LastUpdatedBy"].ToString(),
                            //LastUpdatedOn = drow["LastUpdatedOn"].ToString() == string.Empty ?  : DateTime.Parse(drow["LastUpdatedOn"].ToString()),
                            IsActive = bool.Parse(drow["IsActive"].ToString()),
                            Month=drow["Month"].ToString()
                        };

                        prosessedresult.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return prosessedresult;
        }

        public void InsertorUpdateNDWbsDetails(int AccountId, string Year,string Month ,ndfteWbsBusinessobject insertorupdatendwbsdetails)
        {
            object[] param = {
                                 "@AccountId", AccountId,
                                 "@WBSCode", insertorupdatendwbsdetails.WBSCode,
                                 "@WBSDescription", insertorupdatendwbsdetails.WBSDescription,
                                 "@LastUpdatedBy", insertorupdatendwbsdetails.LastUpdatedBy,
                                 "@Year", Year,
                                 "@Month", Month
                             };
            try
            {
                DB.InsertorUpdate("usp_InsertOrUpdateNDWBSDetails", param);
            }
            catch (Exception ex)
            {

            }
        }

        public void DelWbsDetails(DelWbsDetails del)
        {
            object[] param = {
                                 "@AccountId", del.AccountId,
                                 "@WBSCode", del.WBSCode,
                                 "@IsActive", del.IsActive
                             };
            try
            {
                DB.InsertorUpdate("usp_DelWBSDetails", param);
            }
            catch (Exception ex)
            {

            }
        }

        public void DelNDWbsDetails(int Id, ndfteWbsBusinessobject del)
        {
            object[] param = {
                                 "@AccountId", Id,
                                 "@WBSCode", del.WBSCode,
                                 "@IsActive", del.IsActive
                             };
            try
            {
                DB.InsertorUpdate("usp_DelNDWBSDetails", param);
            }
            catch (Exception ex)
            {

            }
        }

        public void InsertorUpdatedfte(List<dfteBusinessObjects> data)
        {
            try
            {
            foreach (dfteBusinessObjects para in data)
            {
                object[] param = {
                                 "@AccountId", para.AccountId,
                                 "@Year", para.Year,
                                 "@Month", para.Month,
                                 "@CostBaselined", para.CostBaselined,
                                 "@AdditionalCost", para.AdditionalCost,
                                 "@RiskAmount", para.RiskAmount,
                                 "@RevenueBaseline", para.RevenueBaseline
                             };
           
            
                DB.InsertorUpdate("usp_InsertOrUpdateNDFTE", param);
            }
            }
            catch (Exception ex)
            {

            }
        }

    }
}