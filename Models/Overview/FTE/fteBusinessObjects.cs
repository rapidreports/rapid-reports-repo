﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.FTE
{
    public class fteBusinessObjects
    {
        public int AccountId { get; set; }
        public string AccountName { get; set; }
    }

    public class dfteBusinessObjects
    {
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public decimal? CostBaselined { get; set; }
        public decimal? AdditionalCost { get; set; }
        public decimal? RiskAmount { get; set; }
        public decimal? RevenueBaseline { get; set; }
    }
    public class ndfteCR
    {
        public int? CrId { get; set; }
        public int? AccountId { get; set; }
        public string AccountName { get; set; }
        public string Year { get; set; }
        public string Cr { get; set; }
        public string Month { get; set; }
        public string Reason { get; set; }
        public decimal? AdditionalFTE { get; set; }
        public decimal? Amount { get; set; }
        public int? Status { get; set; }
    }

    public class dfteCR
    {
        public int CrId { get; set; }
        public int AccountId { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string Cr { get; set; }
        public string Reason { get; set; }
        public decimal AdditionalFTE { get; set; }
        public decimal Amount { get; set; }
        public int Status { get; set; }
    }
}
