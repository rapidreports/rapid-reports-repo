﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.FTE
{
    public class fteWbsBusinessobject
    {
        public int WBSId { get; set; }
        public string WBSCode { get; set; }
        public string WBSDescription { get; set; }
        public string LastUpdatedBy { get; set; }
        public Nullable<DateTime> LastUpdatedOn { get; set; }
        public bool IsActive { get; set; }


    }

    public class InsertorUpdateWbsDetails
    {
        public int AccountId { get; set; }
        public string WBSCode { get; set; }
        public string WBSDescription { get; set; }
        public string LastUpdatedBy { get; set; }
    }

    public class DelWbsDetails
    {
        public int AccountId { get; set; }
        public string WBSCode { get; set; }
        public bool IsActive { get; set; }
    }

    public class ndfteWbsBusinessobject
    {
        public int WBSId { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string WBSCode { get; set; }
        public string WBSDescription { get; set; }
        public string LastUpdatedBy { get; set; }
        public Nullable<DateTime> LastUpdatedOn { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public bool IsActive { get; set; }


    }
}