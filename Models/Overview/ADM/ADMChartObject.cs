﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.ADM
{
    public class ADMChartObject
    {
        public string MonthYear { get; set; }
        public float CriticalOpen { get; set; }
        public float MediumOpen { get; set; }
        public float LowOpen { get; set; }
        public float NoneOpen { get; set; }
        public float TotalReceived { get; set; }
        public float TotalResolved { get; set; }

        public string AccountName { get; set; }
        public string ServiceLine { get; set; }

        public float Priority1 { get; set; }
        public float Priority2 { get; set; }
        public float Priority3 { get; set; }
        public float Priority4 { get; set; }
        public float Priority5 { get; set; }

        public float ticketbeyondsla { get; set; }

        public float backlog1 { get; set; }
        public float backlog2 { get; set; }
        public float backlog3 { get; set; }
        public float backlog4 { get; set; }
        public float backlog5 { get; set; }

        public float effortperticket { get; set; }

        public float costperticketusd { get; set; }
    }
}