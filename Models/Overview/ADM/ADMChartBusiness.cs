﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RapidReport.Extension;
using System.Data.SqlClient;
using System.Data;


namespace RapidReport.Models.ADM
{
    public class ADMChartBusiness
    {
        public List<ADMChartObject> getAdmchart(string category, string accountname, string serviceline)
        {
            List<ADMChartObject> admchartresult = new List<ADMChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@incidentcategory", category, "@accountname", accountname, "@serviceline", serviceline };
            try
            {
                result = DB.ReadDS("usp_getADMIncidentchart", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        ADMChartObject row = new ADMChartObject
                        {
                            MonthYear = drow["MonthYear"].ToString(),
                            CriticalOpen = float.Parse(drow["CriticalOpen"].ToString()),
                            MediumOpen = float.Parse(drow["MediumOpen"].ToString()),
                            LowOpen = float.Parse(drow["LowOpen"].ToString()),
                            NoneOpen = float.Parse(drow["NoneOpen"].ToString()),
                            TotalReceived = float.Parse(drow["TotalReceived"].ToString()),
                            TotalResolved = float.Parse(drow["TotalResolved"].ToString())
                        };

                        admchartresult.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartresult;
        }

        public List<ADMChartObject> getAdmPrioritychart(string category, string accountname, string serviceline)
        {
            List<ADMChartObject> admchartpriorityresult = new List<ADMChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@incidentcategory", category, "@accountname", accountname, "@serviceline", serviceline };
            try
            {
                result = DB.ReadDS("usp_getADMIncidentPrioritychart", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        ADMChartObject row = new ADMChartObject
                        {
                            MonthYear = drow["MonthYear"].ToString(),
                            Priority1 = float.Parse(drow["Priority1"].ToString()),
                            Priority2 = float.Parse(drow["Priority2"].ToString()),
                            Priority3 = float.Parse(drow["Priority3"].ToString()),
                            Priority4 = float.Parse(drow["Priority4"].ToString()),
                            Priority5 = float.Parse(drow["Priority5"].ToString())
                        };

                        admchartpriorityresult.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartpriorityresult;
        }

        public List<ADMChartObject> getAdmBeyondSLAchart(string category, string accountname, string serviceline)
        {
            List<ADMChartObject> admchartbeyondSLAresult = new List<ADMChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@incidentcategory", category, "@accountname", accountname, "@serviceline", serviceline };
            try
            {
                result = DB.ReadDS("usp_getTicketsOpenedBeyondSLAChart", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        ADMChartObject row = new ADMChartObject
                        {
                            MonthYear = drow["MonthYear"].ToString(),
                            ticketbeyondsla = float.Parse(drow["ticketbeyondsla"].ToString())

                        };

                        admchartbeyondSLAresult.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartbeyondSLAresult;
        }

        public List<ADMChartObject> getAdmBackLogsByPrioritychart(string category, string accountname, string serviceline)
        {
            List<ADMChartObject> admchartbacklogbypriority = new List<ADMChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@incidentcategory", category, "@accountname", accountname, "@serviceline", serviceline };
            try
            {
                result = DB.ReadDS("usp_getBackLogsByPriorityChart", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {


                        ADMChartObject row = new ADMChartObject();
                        row.MonthYear = drow["MonthYear"].ToString();
                        if (drow["backlog1"].ToString() != "")
                        {
                            row.backlog1 = float.Parse(drow["backlog1"].ToString());
                        }
                        else
                        {
                            row.backlog1 = 0;
                        }
                        if (drow["backlog2"].ToString() != "")
                        {
                            row.backlog2 = float.Parse(drow["backlog2"].ToString());
                        }
                        else
                        {
                            row.backlog2 = 0;
                        }
                        if (drow["backlog3"].ToString() != "")
                        {
                            row.backlog3 = float.Parse(drow["backlog3"].ToString());
                        }
                        else
                        {
                            row.backlog3 = 0;
                        }
                        if (drow["backlog4"].ToString() != "")
                        {
                            row.backlog4 = float.Parse(drow["backlog4"].ToString());
                        }
                        else
                        {
                            row.backlog4 = 0;
                        }
                        if (drow["backlog5"].ToString() != "")
                        {
                            row.backlog5 = float.Parse(drow["backlog5"].ToString());
                        }
                        else
                        {
                            row.backlog5 = 0;
                        }


                        admchartbacklogbypriority.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartbacklogbypriority;
        }

        public List<ADMChartObject> getAdmEffortPerTicketchart(string category, string accountname, string serviceline)
        {
            List<ADMChartObject> admcharteffortperticket = new List<ADMChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@incidentcategory", category, "@accountname", accountname, "@serviceline", serviceline };
            try
            {
                result = DB.ReadDS("usp_getEffortPerTicketChart", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {


                        ADMChartObject row = new ADMChartObject();
                        row.MonthYear = drow["MonthYear"].ToString();
                        if (drow["effortperticket"].ToString() != "")
                        {
                            row.effortperticket = float.Parse(drow["effortperticket"].ToString());
                        }
                        else
                        {
                            row.backlog1 = 0;
                        }


                        admcharteffortperticket.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admcharteffortperticket;
        }

        public List<ADMChartObject> getAdmCostPerTicketchart(string category, string accountname, string serviceline)
        {
            List<ADMChartObject> admchartcostperticket = new List<ADMChartObject>();
            DataSet result;
            //var para = ""; 
            object[] param = { "@incidentcategory", category, "@accountname", accountname, "@serviceline", serviceline };
            try
            {
                result = DB.ReadDS("usp_getCostPerTicketUSDChart", param);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {


                        ADMChartObject row = new ADMChartObject();
                        row.MonthYear = drow["MonthYear"].ToString();
                        if (drow["costperticketusd"].ToString() != "")
                        {
                            row.costperticketusd = float.Parse(drow["costperticketusd"].ToString());
                        }
                        else
                        {
                            row.backlog1 = 0;
                        }
                        admchartcostperticket.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return admchartcostperticket;
        }

        public List<ADMChartObject> getAccountname()
        {
            List<ADMChartObject> accountnameresult = new List<ADMChartObject>();
            DataSet result;
            //var para = ""; 
            try
            {
                result = DB.ReadDS("usp_getADMIncidentAccountname");//,para);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        ADMChartObject row = new ADMChartObject
                        {

                            AccountName = drow["AccountName"].ToString()
                        };

                        accountnameresult.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return accountnameresult;
        }

        public List<ADMChartObject> getServiceLine()
        {
            List<ADMChartObject> servicelineresult = new List<ADMChartObject>();
            DataSet result;
            //var para = ""; 
            try
            {
                result = DB.ReadDS("usp_getADMIncidentServiceline");//,para);

                if (result != null)
                {
                    foreach (DataRow drow in result.Tables[0].Rows)
                    {
                        ADMChartObject row = new ADMChartObject
                        {

                            ServiceLine = drow["ServiceLine"].ToString()
                        };

                        servicelineresult.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return servicelineresult;
        }

    }
}