﻿using RapidReport.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace RapidReport.Models.Dashboard
{
    public class DashboardBusiness
    {
        public List<MainTileBusinessObject> getDashboardTiles()
        {
            //DataSet ds = DB.ReadDS("usb_get");
           
            List<MainTileBusinessObject> obj = new List<MainTileBusinessObject>();
            obj.Add(new MainTileBusinessObject() { ID = 1, TileName = "Transition Management", TileURL = "transitionmanagement", TileText = "124", TileImagePath = "client/Components/img/TransitionMgmt.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 2, TileName = "Delivery Models", TileURL = "deliveryModals", TileText = "124", TileImagePath = "client/Components/img/Delivery Modals.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 3, TileName = "Standard Cost Optimization", TileURL = "standardCostOptimization", TileText = "124", TileImagePath = "client/Components/img/StandardCostOptimization.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 4, TileName = "Standard AMS Processes", TileURL = "standardAMSProcesses", TileText = "124", TileImagePath = "client/Components/img/Standard AMS Processes.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 5, TileName = "AMS Metrics", TileURL = "asmmatrics", TileText = "124", TileImagePath = "client/Components/img/StandardCostOptimization.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 6, TileName = "AMS Analytics", TileURL = "amsanalytics", TileText = "124", TileImagePath = "client/Components/img/AMS Analytics.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 7, TileName = "Knowledge Management", TileURL = "knowledgemanagement", TileText = "124", TileImagePath = "client/Components/img/Standard AMS Processes.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 8, TileName = "Automation and Tooling", TileURL = "automationandtooling", TileText = "124", TileImagePath = "client/Components/img/Automation and Tooling.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 9, TileName = "Universities", TileURL = "universities", TileText = "124", TileImagePath = "client/Components/img/Delivery Modals.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 10, TileName = "Finance", TileURL = "finance", TileText = "124", TileImagePath = "client/Components/img/AMS Analytics.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 11, TileName = "Operation", TileURL = "operation", TileText = "124", TileImagePath = "client/Components/img/Standard AMS Processes.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 12, TileName = "Delivery", TileURL = "delivery", TileText = "124", TileImagePath = "client/Components/img/TransitionMgmt.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 13, TileName = "AD Metrics", TileURL = "ids", TileText = "124", TileImagePath = "client/Components/img/StandardCostOptimization.jpg" });
            obj.Add(new MainTileBusinessObject() { ID = 14, TileName = "VDF", TileURL = "vdf", TileText = "124", TileImagePath = "client/Components/img/Delivery Modals.jpg" });
            return obj;           
        }
    }
}