﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.Dashboard
{
    public class MainTileBusinessObject
    {
        public int ID { get; set; }
        public string TileName { get; set; }
        public string TileURL { get; set; }
        public string TileText { get; set; }
        public string TileImagePath { get; set; }
    }
}