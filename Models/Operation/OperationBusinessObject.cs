﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.Operation
{
    public class OperationBusinessObject
    {
        public List<int> Year { get; set; }
        public List<string> Month { get; set; }
        public List<string> Account { get; set; }
        public string HeadCountNumber { get; set; }
        public string AttritionNumber { get; set; }
        public string BenchPercent { get; set; }
        public string UtilizationPercent { get; set; }
        public string LPIPercent { get; set; }
        public string BenchNumber { get; set; }
        public string FullyallocatedNumber { get; set; }
        public string ThirtyOneToSixtyNumber { get; set; }
        public string SixtyToNinetyNumber { get; set; }
        public string NinetyPlusNumber { get; set; }
    }
}