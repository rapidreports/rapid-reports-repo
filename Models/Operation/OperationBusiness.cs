﻿using RapidReport.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace RapidReport.Models.Operation
{
    public class OperationBusiness
    {
        #region Private Variable

        private OperationBusinessObject obj;

        #endregion

        public OperationBusinessObject GetOperationDashboardDetails(string ServiceLine, string Month, string Year)
        {            
            obj = new OperationBusinessObject();
            DataSet result;
            object[] param = { "@ServiceLine", ServiceLine, "@Month", Month, "@Year", Year };
            try
            {               
                List<string> AccountList = new List<string>();

                #region Value Allocation

             
                obj = new OperationBusinessObject();
                result = DB.ReadDS("usp_GetOperationDashboardDetails", param);

                if (result != null)
                {

                    if (result.Tables[0] != null)
                    {
                        obj.HeadCountNumber = Convert.ToString(result.Tables[0].Rows[0][0]);
                        obj.AttritionNumber = Convert.ToString(result.Tables[0].Rows[1][0]);
                        obj.BenchPercent = Convert.ToString(result.Tables[0].Rows[4][0]);
                        obj.UtilizationPercent = Convert.ToString(result.Tables[0].Rows[3][0]);
                        obj.LPIPercent = Convert.ToString(result.Tables[0].Rows[5][0]);
                        obj.BenchNumber = Convert.ToString(result.Tables[0].Rows[2][0]);
                        obj.FullyallocatedNumber = Convert.ToString(result.Tables[0].Rows[6][0]);
                        obj.ThirtyOneToSixtyNumber = Convert.ToString(result.Tables[0].Rows[7][0]);
                        obj.SixtyToNinetyNumber = Convert.ToString(result.Tables[0].Rows[8][0]);
                        obj.NinetyPlusNumber = Convert.ToString(result.Tables[0].Rows[9][0]);
                    }

                }

                #endregion

            }
            catch (Exception ex)
            {
                result = null;
            }

            return obj;
        }
    }
}