﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.VDF
{
    public class VDFBusinessObject
    {
        public List<string> AccountNames{get; set;}
        public List<VDFRawDetails> vdfRawDetails { get; set; }
        public List<CSIP> CSIPDetails { get; set; }
        public List<VDFStatus> VDFStatusDetails { get; set; }
    }

    public class VDFRawDetails
    {
        public string TotalProjects { get; set; }
        public string HPFundedHPProposed { get; set; }
        public string CustomerFundedHPProposed { get; set; }
        public string HPFundedCustomerProposed { get; set; }
        public string CustomerFundedCustomerProposed { get; set; }
    }
    public class CSIP
    {
        public string CSIPPercentage { get; set; }
        public string CSIPTYPE { get; set; }
    }
  
    public class VDFStatus
    {
        public string CurrentStatus { get; set; }
            public string count {get;set;}
    }
}