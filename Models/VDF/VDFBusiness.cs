﻿using RapidReport.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace RapidReport.Models.VDF
{
    public class VDFBusiness
    {
        #region Private Variable

        private VDFBusinessObject obj;
        private VDFRawDetails vdfRawDetailsObject;
        private CSIP CSIPObject;
        public VDFStatus VDFStatusObject;

        #endregion

        public VDFBusinessObject GetVDFDashboardDetails(string Account)
        {
            obj = new VDFBusinessObject();
            DataSet result;
            object[] param = { "@AccountName", Account };
            try
            {


                #region Value Allocation

                result = DB.ReadDS("usp_GetVDFDashboardDetails", param);

                if (result != null)
                {
                    obj.AccountNames = new List<string>();
                    int i = 0;
                    if (result.Tables[0] != null)
                    {
                        obj.AccountNames.Add("All");
                        foreach (DataRow drow in result.Tables[0].Rows)
                        {
                            obj.AccountNames.Add(Convert.ToString(result.Tables[0].Rows[i]["AccountName"]));
                            i++;
                        }

                    }
                    obj.vdfRawDetails = new List<VDFRawDetails>();
                    if (result.Tables[1] != null)
                    {
                        vdfRawDetailsObject = new VDFRawDetails();
                        foreach (DataRow drow in result.Tables[1].Rows)
                        {
                            vdfRawDetailsObject.TotalProjects = Convert.ToString(result.Tables[1].Rows[0]["TotalProject"]);
                            vdfRawDetailsObject.HPFundedHPProposed = Convert.ToString(result.Tables[1].Rows[0]["HPFundedHPProposed"]);
                            vdfRawDetailsObject.HPFundedCustomerProposed = Convert.ToString(result.Tables[1].Rows[0]["HPFundedCustomerProposed"]);
                            vdfRawDetailsObject.CustomerFundedCustomerProposed = Convert.ToString(result.Tables[1].Rows[0]["CustomerFundedCustomerProposed"]);
                            vdfRawDetailsObject.CustomerFundedHPProposed = Convert.ToString(result.Tables[1].Rows[0]["CustomerFundedHPProposed"]);
                            obj.vdfRawDetails.Add(vdfRawDetailsObject);
                        }
                    }
                    if (result.Tables[2] != null)
                    {
                        foreach (DataRow drow in result.Tables[2].Rows)
                        {
                            vdfRawDetailsObject = new VDFRawDetails();
                            vdfRawDetailsObject.TotalProjects = Convert.ToString(result.Tables[2].Rows[0]["TotalProjectEffortsSaved"]);
                            vdfRawDetailsObject.HPFundedHPProposed = Convert.ToString(result.Tables[2].Rows[0]["HPFundedHPProposedEffortsSaved"]);
                            vdfRawDetailsObject.HPFundedCustomerProposed = Convert.ToString(result.Tables[2].Rows[0]["HPFundedCustomerProposedEffortsSaved"]);
                            vdfRawDetailsObject.CustomerFundedCustomerProposed = Convert.ToString(result.Tables[2].Rows[0]["CustomerFundedCustomerProposedEffortsSaved"]);
                            vdfRawDetailsObject.CustomerFundedHPProposed = Convert.ToString(result.Tables[2].Rows[0]["CustomerFundedHPProposedEffortsSaved"]);
                            obj.vdfRawDetails.Add(vdfRawDetailsObject);
                        }
                    }
                    if (result.Tables[3] != null)
                    {
                        foreach (DataRow drow in result.Tables[3].Rows)
                        {
                            vdfRawDetailsObject = new VDFRawDetails();
                            vdfRawDetailsObject.TotalProjects = Convert.ToString(result.Tables[3].Rows[0]["TotalProjectCostSaved"]);
                            vdfRawDetailsObject.HPFundedHPProposed = Convert.ToString(result.Tables[3].Rows[0]["HPFundedHPProposedCostSaved"]);
                            vdfRawDetailsObject.HPFundedCustomerProposed = Convert.ToString(result.Tables[3].Rows[0]["HPFundedCustomerProposedCostSaved"]);
                            vdfRawDetailsObject.CustomerFundedCustomerProposed = Convert.ToString(result.Tables[3].Rows[0]["CustomerProposedCustomerFundedCostSaved"]);
                            vdfRawDetailsObject.CustomerFundedHPProposed = Convert.ToString(result.Tables[3].Rows[0]["CustomerFundedHPProposedCostSaved"]);
                            obj.vdfRawDetails.Add(vdfRawDetailsObject);
                        }
                    }
                    if (result.Tables[4] != null)
                    {
                        foreach (DataRow drow in result.Tables[4].Rows)
                        {
                            vdfRawDetailsObject = new VDFRawDetails();
                            vdfRawDetailsObject.TotalProjects = Convert.ToString(result.Tables[4].Rows[0]["TotalProjectCostSavedForCustomer"]);
                            vdfRawDetailsObject.HPFundedHPProposed = Convert.ToString(result.Tables[4].Rows[0]["HPFundedHPProposedCostSavedForCustomer"]);
                            vdfRawDetailsObject.HPFundedCustomerProposed = Convert.ToString(result.Tables[4].Rows[0]["HPFundedCustomerProposedCostSavedForCustomer"]);
                            vdfRawDetailsObject.CustomerFundedCustomerProposed = Convert.ToString(result.Tables[4].Rows[0]["CustomerProposedCustomerFundedCostSavedForCustomer"]);
                            vdfRawDetailsObject.CustomerFundedHPProposed = Convert.ToString(result.Tables[4].Rows[0]["CustomerFundedHPProposedCostSavedForCustomer"]);
                            obj.vdfRawDetails.Add(vdfRawDetailsObject);
                        }
                    }
                    if (result.Tables[5] != null)
                    {
                        foreach (DataRow drow in result.Tables[5].Rows)
                        {
                            vdfRawDetailsObject = new VDFRawDetails();
                            vdfRawDetailsObject.TotalProjects = Convert.ToString(result.Tables[5].Rows[0]["TotalProjectIncidentReductionMonth"]);
                            vdfRawDetailsObject.HPFundedHPProposed = Convert.ToString(result.Tables[5].Rows[0]["HPFundedHPProposedIncidentReductionMonth"]);
                            vdfRawDetailsObject.HPFundedCustomerProposed = Convert.ToString(result.Tables[5].Rows[0]["HPFundedCustomerProposedIncidentReductionMonth"]);
                            vdfRawDetailsObject.CustomerFundedCustomerProposed = Convert.ToString(result.Tables[5].Rows[0]["CustomerFundedCustomerProposedIncidentReductionMonth"]);
                            vdfRawDetailsObject.CustomerFundedHPProposed = Convert.ToString(result.Tables[5].Rows[0]["CustomerFundedHPProposedIncidentReductionMonth"]);
                            obj.vdfRawDetails.Add(vdfRawDetailsObject);
                        }
                    }
                    if (result.Tables[6] != null)
                    {
                        foreach (DataRow drow in result.Tables[6].Rows)
                        {
                            vdfRawDetailsObject = new VDFRawDetails();
                            vdfRawDetailsObject.TotalProjects = Convert.ToString(result.Tables[6].Rows[0]["TotalProjectMajorIncidentReductionMonth"]);
                            vdfRawDetailsObject.HPFundedHPProposed = Convert.ToString(result.Tables[6].Rows[0]["HPFundedHPProposedMajorIncidentReductionMonth"]);
                            vdfRawDetailsObject.HPFundedCustomerProposed = Convert.ToString(result.Tables[6].Rows[0]["HPFundedCustomerProposedMajorIncidentReductionMonth"]);
                            vdfRawDetailsObject.CustomerFundedCustomerProposed = Convert.ToString(result.Tables[6].Rows[0]["CustomerFundedCustomerProposedMajorIncidentReductionMonth"]);
                            vdfRawDetailsObject.CustomerFundedHPProposed = Convert.ToString(result.Tables[6].Rows[0]["CustomerFundedHPProposedMajorIncidentReductionMonth"]);
                            obj.vdfRawDetails.Add(vdfRawDetailsObject);
                        }
                    }
                    if (result.Tables[7] != null)
                    {
                        obj.CSIPDetails = new List<CSIP>();
                        int j = 0;
                        foreach (DataRow drow in result.Tables[7].Rows)
                        {
                            CSIPObject = new CSIP();
                            CSIPObject.CSIPPercentage = Convert.ToString(result.Tables[7].Rows[j]["CSIPPercentage"]);
                            CSIPObject.CSIPTYPE = Convert.ToString(result.Tables[7].Rows[j]["CSIPType"]);
                            obj.CSIPDetails.Add(CSIPObject);
                            j++;
                            
                        }
                    }
                    if (result.Tables[8] != null)
                    {
                        int k = 0;
                        obj.VDFStatusDetails = new List<VDFStatus>();
                        foreach (DataRow drow in result.Tables[8].Rows)
                        {
                            VDFStatusObject = new VDFStatus();
                            VDFStatusObject.count = Convert.ToString(result.Tables[8].Rows[k]["Count"]);
                            VDFStatusObject.CurrentStatus = Convert.ToString(result.Tables[8].Rows[k]["CurrentStatus"]);
                            obj.VDFStatusDetails.Add(VDFStatusObject);
                            k++;
                           
                        }
                    }
                }

                #endregion

            }
            catch (Exception ex)
            {
                result = null;
            }

            return obj;
        }
    }
}