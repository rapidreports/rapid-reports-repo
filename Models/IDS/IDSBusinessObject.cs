﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.IDS
{
    public class PercentageManagedProjectObject
    {
        public string ProjectType { get; set; }
        public string Red { get; set; }
        public string Green { get; set; }
        public string Yellow { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public int TotalProject { get; set; }
    }

    public class Productivity
    {
        public string ProductAmount { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }

    public class ProductDeliveryMetrics
    {
        public string FP { get; set; }
        public string TotalDefects { get; set; }
        public string DefectDensity { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }

    public class IDSObjects
    {
        public List<PercentageManagedProjectObject> PMP { get; set; }
        public Productivity productivityAmount { get; set; }
        public ProductDeliveryMetrics PDM { get; set; } 
    }
}