﻿using RapidReport.Extension;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace RapidReport.Models.IDS
{
    public class IDSBusiness
    {
        #region Private Variable

        private List<PercentageManagedProjectObject> Listobj;
        private PercentageManagedProjectObject obj;
        private IDSObjects objects;
        private Productivity productivityobj;
        private ProductDeliveryMetrics productDeliveryMetricsobj;
        #endregion

        public IDSObjects GetIDSProjectsDetails(string Month, string Year)
        {
            objects = new IDSObjects();
            Listobj = new List<PercentageManagedProjectObject>();
            productivityobj = new Productivity();
            productDeliveryMetricsobj = new ProductDeliveryMetrics();
            DataSet result;
            object[] param = { "@Month", Month, "@Year", Year };
            try
            {

                #region Value Allocation

                result = DB.ReadDS("usp_GetPercentageManagedProjectsDetails", param);

                if (result != null)
                {

                    if (result.Tables[0] != null)
                    {
                        int i = 0;
                        foreach (DataRow drow in result.Tables[0].Rows)
                        {
                            obj = new PercentageManagedProjectObject();


                            obj.ProjectType = Convert.ToString(result.Tables[0].Rows[i]["ProjectType"]);
                            obj.Red = Convert.ToString(result.Tables[0].Rows[i]["Red"]);
                            obj.Green = Convert.ToString(result.Tables[0].Rows[i]["Green"]);
                            obj.Month = Convert.ToString(result.Tables[0].Rows[i]["Month"]);
                            obj.Year = Convert.ToString(result.Tables[0].Rows[i]["Year"]);
                            obj.Yellow = Convert.ToString(result.Tables[0].Rows[i]["Yellow"]);
                            i++;
                            Listobj.Add(obj);
                        }
                        
                    }
                    if (result.Tables[1] != null)                    
                    {
                        foreach (DataRow drow in result.Tables[1].Rows)
                        {
                            productDeliveryMetricsobj.FP = Convert.ToString(result.Tables[1].Rows[0]["FP"]);
                            productDeliveryMetricsobj.DefectDensity = Convert.ToString(result.Tables[1].Rows[0]["DefectDensity"]);
                            productDeliveryMetricsobj.TotalDefects = Convert.ToString(result.Tables[1].Rows[0]["TotalDefects"]);
                            productDeliveryMetricsobj.Month = Convert.ToString(result.Tables[0].Rows[1]["Month"]);
                            productDeliveryMetricsobj.Year = Convert.ToString(result.Tables[0].Rows[1]["Year"]);                           
                        }
                    }
                    if (result.Tables[2] != null)
                    {
                        foreach (DataRow drow in result.Tables[2].Rows)
                        {

                            productivityobj.ProductAmount = Convert.ToString(result.Tables[2].Rows[0]["Productivity"]);
                            productivityobj.Month = Convert.ToString(result.Tables[2].Rows[0]["Month"]);
                            productivityobj.Year = Convert.ToString(result.Tables[2].Rows[0]["Year"]);                            
                       }
                    }
                }

                #endregion

                objects.PMP = Listobj;
                objects.PDM = productDeliveryMetricsobj;
                objects.productivityAmount = productivityobj;

            }
            catch (Exception ex)
            {
                result = null;
            }

            return objects;
        }
    }
}