﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.Common
{
    public class CommonBusiness
    {
        public List<CommonBusinessSubTileObject> GetSubTileInformation(string SubTileName)
        {
            List<CommonBusinessSubTileObject> CommonBusinessSubTileObjectList = new List<CommonBusinessSubTileObject>();
            if (SubTileName == "TransitionManagement")
            {
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Transition Management", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/transitionmanagement/", TileImageuri = "client/Components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 2, TileName = "Transition Coach", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/transitionmanagement/transitioncoach", TileImageuri = "client/Components/img/StandardCostOptimization.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 3, TileName = "Transition Tools", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/transitionmanagement/transitiontools", TileImageuri = "client/Components/img/Delivery Modals.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 4, TileName = "Transition Proof Points", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/transitionmanagement/transitionstories", TileImageuri = "client/Components/img/Standard AMS Processes.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 5, TileName = "Standard Transition WBS", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/transitionmanagement/transitionstandard", TileImageuri = "client/Components/img/AMS Analytics.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 6, TileName = "Transition Metrics", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/transitionmanagement/transitionmetrics", TileImageuri = "client/Components/img/Automation and Tooling.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 7, TileName = "External Links", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/transitionmanagement/TransitionLinks", TileImageuri = "client/Components/img/Standard AMS Processes.jpg" });
            }
            else if (SubTileName == "AMSAnalytics")
            {
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "AMS Analytics", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/operationanalytics/", TileImageuri = "client/components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 2, TileName = "Features and Frameworks", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/operationanalytics/oafeaturesframeworks", TileImageuri = "client/components/img/StandardCostOptimization.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 3, TileName = "Use Case", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/operationanalytics/oausecase", TileImageuri = "client/components/img/Delivery Modals.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 4, TileName = "Catalogue", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/operationanalytics/oacatalogue", TileImageuri = "client/components/img/Standard AMS Processes.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 5, TileName = "Intelligent Decision Support", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/operationanalytics/oadecisionsupport", TileImageuri = "client/components/img/AMS Analytics.jpg" });
            }
            else if (SubTileName == "AMSMatrics")
            {
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "AMS Metrics", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/amsmetrics/", TileImageuri = "client/components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 2, TileName = "Standard IDS Metrics", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/amsmetrics/industrializedsystem", TileImageuri = "client/components/img/StandardCostOptimization.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 3, TileName = "Standard AMS Metrics", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/amsmetrics/standardmetrics", TileImageuri = "client/components/img/Delivery Modals.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 4, TileName = "AMS Metrics Benchmark Tool (IAMB)", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/amsmetrics/iambtool", TileImageuri = "client/components/img/Standard AMS Processes.jpg" });
                
            }
            else if (SubTileName == "AutomationAndTooling")
            {
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Automation and Tooling", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/automationandtooling/", TileImageuri = "client/components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 2, TileName = "Automation Services", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/automationandtooling/atservices", TileImageuri = "client/components/img/StandardCostOptimization.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 3, TileName = "Tooling", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/automationandtooling/attooling", TileImageuri = "client/components/img/Delivery Modals.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 4, TileName = "Success Stories", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/automationandtooling/atstories", TileImageuri = "client/components/img/Standard AMS Processes.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 5, TileName = "External Links", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/automationandtooling/atlinks", TileImageuri = "client/components/img/AMS Analytics.jpg" });
            }
            else if (SubTileName == "Delivery")
            {
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Delivery", TileRedirectURL = "#", TileImageuri = "client/components/img/TransitionMgmt.jpg" });               
            }
            else if (SubTileName == "KnowledgeManagement")
            {
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Knowledge Management", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/knowledgemanagement/", TileImageuri = "client/components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 2, TileName = "ITSM KM Approach", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/knowledgemanagement/itsmkmapproach", TileImageuri = "client/components/img/StandardCostOptimization.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 3, TileName = "KM Tools", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/knowledgemanagement/kmtools", TileImageuri = "client/components/img/Delivery Modals.jpg" });
            }
            else if (SubTileName == "StandardAMSProcesses")
            {
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Standard AMS Processes", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/standardamsprocesses/", TileImageuri = "client/components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 2, TileName = "Frameworks", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/standardamsprocesses/frameworks", TileImageuri = "client/components/img/StandardCostOptimization.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 3, TileName = "Process Definitions", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/standardamsprocesses/processdefinitions", TileImageuri = "client/components/img/Delivery Modals.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 4, TileName = "Quick Guides", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/standardamsprocesses/quickguides", TileImageuri = "client/components/img/Standard AMS Processes.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 5, TileName = "Implementation Guidance", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/standardamsprocesses/implemguidance", TileImageuri = "client/components/img/AMS Analytics.jpg" });
            }
            else if (SubTileName == "StandardCostOptimization")
            {
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Standard Cost Optimization", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/standardcostoptimization/", TileImageuri = "client/components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 2, TileName = "Productivity Playbook", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/standardcostoptimization/scoplaybook", TileImageuri = "client/components/img/StandardCostOptimization.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 3, TileName = "Productivity Initiatives", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/standardcostoptimization/prodinitiatives", TileImageuri = "client/components/img/Delivery Modals.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 4, TileName = "External Links", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/standardcostoptimization/scolinks", TileImageuri = "client/components/img/Standard AMS Processes.jpg" });
            }
            else if (SubTileName == "Universities")
            {
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Universities", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePtePages/index.aspx#/link/acceleratinguclientuniversity/aucuaccelerate", TileImageuri = "client/components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 2, TileName = "Client University", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMSages/index.aspx#/link/acceleratinguclientuniversity/", TileImageuri = "client/components/img/StandardCostOptimization.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 3, TileName = "Accelerating U", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/Si/SitePages/index.aspx#/link/acceleratinguclientuniversity/aucuclient", TileImageuri = "client/components/img/Delivery Modals.jpg" });
            }
            else if (SubTileName == "DeliveryModals")
            {
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Delivery Models", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/deliverymodels/", TileImageuri = "client/components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 2, TileName = "Dedicated Delivery Model", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/deliverymodels/dedicatedmodel", TileImageuri = "client/components/img/StandardCostOptimization.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 3, TileName = "Leveraged Delivery Model", TileRedirectURL = "https://hpenterprise.sharepoint.com/teams/IDSforAMS/SitePages/index.aspx#/link/deliverymodels/leveragedmodel", TileImageuri = "client/components/img/Delivery Modals.jpg" });
            }
            else if (SubTileName == "Finance")
            {
                //CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Finance", TileRedirectURL = "http://16.181.232.111:8080/client/index.html#/fte", TileImageuri = "Components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Finance", TileRedirectURL = "Client/Index.html#overview", TileImageuri = "client/components/img/TransitionMgmt.jpg" });             
            }
            else if (SubTileName == "Operation")
            {
                //CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Operation", TileRedirectURL = "http://16.181.232.111:8080/client/index.html#/operational/dashboard/", TileImageuri = "client/components/img/TransitionMgmt.jpg" });
                CommonBusinessSubTileObjectList.Add(new CommonBusinessSubTileObject { ID = 1, TileName = "Operation", TileRedirectURL = "operationDashboard", TileImageuri = "client/components/img/TransitionMgmt.jpg" });
            }
            return CommonBusinessSubTileObjectList;
        }
    }
}