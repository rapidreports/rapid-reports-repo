﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RapidReport.Models.Common
{
    public class CommonBusinessSubTileObject
    {
        public int ID { get; set; }
        public string TileName { get; set; }
        public string TileRedirectURL { get; set; }
        public string TileImageuri { get; set; }
    }
}