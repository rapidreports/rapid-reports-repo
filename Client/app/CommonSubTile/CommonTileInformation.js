﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('commontileinformation', {
        url: '/:TileName/TileInformation',
        templateUrl: 'client/app/CommonSubTile/CommonTileInformation.html',
        controller: "CommonController"
    });
});