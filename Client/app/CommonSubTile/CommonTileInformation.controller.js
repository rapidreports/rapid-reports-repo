﻿RapidReportsModule.controller("CommonController", function ($scope, $http, $state) {
    var param = $state.params;

    $scope.GetCommonTileInformation = function () {
        var promise = $http({
            method: 'GET',
            url: '/app/GetCommonTileInformation/' + param.TileName
        });
        promise.success(function (data) {
            $scope.subtiledetails = data;
        });
        promise.error(function (response) {
            alert('Fail');
            $scope.error_message = response.error_message;
        });
    };
    $scope.GetCommonTileInformation();
   
});