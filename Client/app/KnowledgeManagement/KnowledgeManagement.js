﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('knowledgemanagement', {
        url: '/KnowledgeManagement',
        templateUrl: 'client/app/CommonSubTile/CommonSubTile.html',
        controller: "KnowledgeManagementController"
    });
});