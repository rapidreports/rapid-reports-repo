﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('universities', {
        url: '/Universities',
        templateUrl: 'client/app/CommonSubTile/CommonSubTile.html',
        controller: "UniversitiesController"
    });
});