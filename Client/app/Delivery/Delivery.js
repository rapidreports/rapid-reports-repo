﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('delivery', {
        url: '/Delivery',
        templateUrl: 'client/app/CommonSubTile/CommonSubTile.html',
        controller: "DeliveryController"
    });
});