﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('standardAMSProcesses', {
        url: '/StandardAMSProcesses',
        templateUrl: 'client/app/CommonSubTile/CommonSubTile.html',
        controller: "StandardAMSProcessesController"
    });
});