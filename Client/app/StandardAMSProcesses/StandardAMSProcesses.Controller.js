﻿RapidReportsModule.controller("StandardAMSProcessesController", function ($scope, $http) {
    $scope.ContentHeading = "Standard AMS Processes";
    $scope.getSubTileInformatinon = function () {
        var promise = $http({
            method: 'GET',
            url: '/app/getSubTileInformation/StandardAMSProcesses'
        });
        promise.success(function (data) {
            // alert(data);
            $scope.subtiledetails = data;
        });
        promise.error(function (response) {
            alert('Fail');
            $scope.error_message = response.error_message;
        });
    };
    $scope.getSubTileInformatinon();
});