﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('deliveryModals', {
        url: '/DeliveryModals',
        templateUrl: 'client/app/CommonSubTile/CommonSubTile.html',
        controller: "DeliveryModalsController"
    });
});

