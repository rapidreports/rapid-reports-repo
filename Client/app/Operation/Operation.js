﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('operation', {
        url: '/Operation',
        templateUrl: 'client/app/CommonSubTile/FDOSubTile.html',
        controller: "OperationController"
    })
    .state('operationDashboard', {
        url: '/OperationDashboard',
        templateUrl: 'client/app/Operation/OperationDashboard.html',
        controller: "OperationDahboardController"
    });
});

