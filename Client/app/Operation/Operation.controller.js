﻿RapidReportsModule.controller("OperationController", function ($scope, $http) {
    $scope.ContentHeading = "Operation";
    $scope.getSubTileInformatinon = function () {
        var promise = $http({
            method: 'GET',
            url: '/app/getSubTileInformation/Operation'
        });
        promise.success(function (data) {
            // alert(data);
            $scope.subtiledetails = data;
        });
        promise.error(function (response) {
            alert('Fail');
            $scope.error_message = response.error_message;
        });
    };
    $scope.getSubTileInformatinon();
});