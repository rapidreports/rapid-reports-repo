﻿RapidReportsModule.controller("OperationDahboardController", function ($scope, $http) {
    var d = new Date();
    $scope.years = [d.getFullYear(), d.getFullYear() - 1, d.getFullYear() - 2, d.getFullYear() - 3, d.getFullYear() - 4, d.getFullYear() - 5, d.getFullYear() - 6, d.getFullYear() - 7, d.getFullYear() - 8, d.getFullYear() - 9, d.getFullYear() - 10];
    $scope.Months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    $scope.ServiceLines = ["ADM", "SAP"];
    $scope.selectedAccount = _.first($scope.ServiceLines);
    $scope.selectedYear = _.first($scope.years);
    $scope.selectedMonth = $scope.Months[d.getMonth() - 1];

    $scope.getSubTileInformatinon = function () {           
        var promise = $http({
            method: 'GET',
            url: '/app/getOperationDashboardDetails/' + $scope.selectedAccount + '/' + $scope.selectedYear + '/' + $scope.selectedMonth
        });
        promise.success(function (data) {
                    
            $scope.AttritionNumber =  data.attritionNumber;
            $scope.BenchNumber =  data.benchNumber;
            $scope.BenchPercent =  data.benchPercent;
            $scope.FullyallocatedNumber =  data.fullyallocatedNumber;
            $scope.HeadCountNumber =  data.headCountNumber
            $scope.LPIPercent =  data.lpiPercent;           
            $scope.NinetyPlusNumber =  data.ninetyPlusNumber;
            $scope.SixtyToNinetyNumber =  data.sixtyToNinetyNumber;
            $scope.ThirtyOneToSixtyNumber = data.thirtyOneToSixtyNumber;
            $scope.UtilizationPercent = data.utilizationPercent;
        });
        promise.error(function (response) {
            alert('Fail');
            $scope.error_message = response.error_message;
        });
    };
    $scope.getSubTileInformatinon();

    $scope.OperationlistChanged =  function (data)
    {
        $scope.getSubTileInformatinon();        
    }
});