﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('amsanalytics', {
        url: '/AMSAnalytics',
        templateUrl: 'client/app/CommonSubTile/CommonSubTile.html',
        controller: "AMSAnalyticsController"
    });
});