﻿RapidReportsModule.controller("AMSAnalyticsController", function ($scope, $http) {
    $scope.ContentHeading = "AMS Analytics";
    $scope.getSubTileInformatinon = function () {
        var promise = $http({
            method: 'GET',
            url: '/app/getSubTileInformation/AMSAnalytics'
        });
        promise.success(function (data) {            
            $scope.subtiledetails = data;
        });
        promise.error(function (response) {
            alert('Fail');
            $scope.error_message = response.error_message;
        });
    };
    $scope.getSubTileInformatinon();
});