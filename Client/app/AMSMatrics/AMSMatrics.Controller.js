﻿RapidReportsModule.controller("AMSMatricsController", function ($scope,
    $state,
    $timeout,
    DateHelper,
    AppDefault,
    GenerateReportService,
    ExportService,
    AdmChartService,
    PageHeaderService) {
    $scope.ContainerOverall2 = true;

    $scope.showgraph = function () {
        $state.go('overview.adm');
    }

    $scope.admChart = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.admChart1 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.admChart2 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.admChart3 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.admChart4 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.admChart5 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    //region load
    //$scope.serviceline = [{ serviceLine: "ADM" }];

    //$scope.accountname = [{ id: 1, accountName: 'TELSTRA CORPORATION LIMITED' }, { id: 2, accountName: 'SEADRILL' }];

    $scope.incidentcategory = [
     {
         id: '1',
         name: 'Incident'
     },
     {
         id: '2',
         name: 'Service Request'
     }
    ];

    $scope.typeOptions = [
       {
           id: 1,
           name: 'line'
       },
       {
           id: 2,
           name: 'bar'
       },
       {
           id: 3,
           name: 'spline'
       },
       {
           id: 4,
           name: 'area'
       }
    ];

    AdmChartService.getAdmAccountName()
  .$promise
  .then(function (admaccountname) {
      $scope.accountname = admaccountname;

  });

    AdmChartService.getAdmServiceLine()
    .$promise
    .then(function (admserviceline) {
        $scope.serviceline = admserviceline;

    });

    $scope.ddlserviceline = "ADM";
    $scope.ddlaccountname = "TELSTRA CORPORATION LIMITED";
    $scope.ddlincidcategory = "Incident";
   
  

    var admchartdata = [];
    var admchartdata1 = [];
    var admchartdata2 = [];
    var admchartdata3 = [];
    var admchartdata4 = [];
    var admchartdata5 = [];

    $scope.griddata;
    $scope.griddata1;
    $scope.griddata2;
    $scope.griddata3;
    $scope.griddata4;
    $scope.griddata5;

    var graphLoad = function () {
        var chartObject = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        //chartObject.chartdata = [];
        //chartObject.chartdatax = {};
        //chartObject.chartdatacols = [];

        $scope.totalbacklog = 0;
        $scope.totalreceived = 0;
        $scope.totalresolved = 0;

        if (!_.isUndefined(admchartdata)) {
            //var monthyearary = _.uniq(_.map(local.monthWBS, 'wbsCode'));
            _.forEach(admchartdata, function (loopmonth) {
                var chartitem = {};
                chartitem.monthYear = loopmonth.monthYear;
                chartitem.backlog = loopmonth.criticalOpen + loopmonth.mediumOpen + loopmonth.lowOpen + loopmonth.noneOpen;
                chartitem.totalReceived = loopmonth.totalReceived;
                chartitem.totalResolved = loopmonth.totalResolved;
                $scope.totalbacklog = $scope.totalbacklog + chartitem.backlog;
                $scope.totalreceived = $scope.totalreceived + chartitem.totalReceived;
                $scope.totalresolved = $scope.totalresolved + chartitem.totalResolved;
                chartObject.chartdata.push(chartitem);
            });


            chartObject.chartdatacols.push({ "id": "backlog", "type": "line", "name": "Backlog" });
            chartObject.chartdatacols.push({ "id": "totalReceived", "type": "line", "name": "Total Received" });
            chartObject.chartdatacols.push({ "id": "totalResolved", "type": "line", "name": "Total Resolved" });

            chartObject.chartdatax = { "id": "monthYear" };

            $scope.griddata = chartObject.chartdata;

        }

        return chartObject;
    }

    var graphLoad1 = function () {
        var chartObject1 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        //chartObject.chartdata = [];
        //chartObject.chartdatax = {};
        //chartObject.chartdatacols = [];

        $scope.totalpriority1 = 0;
        $scope.totalpriority2 = 0;
        $scope.totalpriority3 = 0;
        $scope.totalpriority4 = 0;
        $scope.totalpriority5 = 0;

        if (!_.isUndefined(admchartdata1)) {
            //var monthyearary = _.uniq(_.map(local.monthWBS, 'wbsCode'));
            _.forEach(admchartdata1, function (loopmonth1) {
                var chartitem1 = {};
                chartitem1.monthYear = loopmonth1.monthYear;
                chartitem1.priority1 = loopmonth1.priority1;
                chartitem1.priority2 = loopmonth1.priority2;
                chartitem1.priority3 = loopmonth1.priority3;
                chartitem1.priority4 = loopmonth1.priority4;
                chartitem1.priority5 = loopmonth1.priority5;
                $scope.totalpriority1 = $scope.totalpriority1 + chartitem1.priority1;
                $scope.totalpriority2 = $scope.totalpriority2 + chartitem1.priority2;
                $scope.totalpriority3 = $scope.totalpriority3 + chartitem1.priority3;
                $scope.totalpriority4 = $scope.totalpriority4 + chartitem1.priority4;
                $scope.totalpriority5 = $scope.totalpriority5 + chartitem1.priority5;
                chartObject1.chartdata.push(chartitem1);
            });


            chartObject1.chartdatacols.push({ "id": "priority1", "type": "line", "name": "Priority 1" });
            chartObject1.chartdatacols.push({ "id": "priority2", "type": "line", "name": "Priority 2" });
            chartObject1.chartdatacols.push({ "id": "priority3", "type": "line", "name": "Priority 3" });
            chartObject1.chartdatacols.push({ "id": "priority4", "type": "line", "name": "Priority 4" });
            chartObject1.chartdatacols.push({ "id": "priority5", "type": "line", "name": "Priority 5" });

            chartObject1.chartdatax = { "id": "monthYear" };

            $scope.griddata1 = chartObject1.chartdata;

        }

        return chartObject1;
    }

    var graphLoad2 = function () {
        var chartObject2 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        //chartObject.chartdata = [];
        //chartObject.chartdatax = {};
        //chartObject.chartdatacols = [];

        $scope.totalbeyondSLA = 0;


        if (!_.isUndefined(admchartdata2)) {
            //var monthyearary = _.uniq(_.map(local.monthWBS, 'wbsCode'));
            _.forEach(admchartdata2, function (loopmonth2) {
                var chartitem2 = {};
                chartitem2.monthYear = loopmonth2.monthYear;
                chartitem2.ticketbeyondsla = loopmonth2.ticketbeyondsla;

                $scope.totalbeyondSLA = $scope.totalbeyondSLA + chartitem2.ticketbeyondsla;


                chartObject2.chartdata.push(chartitem2);
            });


            chartObject2.chartdatacols.push({ "id": "ticketbeyondsla", "type": "line", "name": "Ticket Beyond SLA" });


            chartObject2.chartdatax = { "id": "monthYear" };

            $scope.griddata2 = chartObject2.chartdata;

        }

        return chartObject2;
    }

    var graphLoad3 = function () {
        var chartObject3 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        //chartObject.chartdata = [];
        //chartObject.chartdatax = {};
        //chartObject.chartdatacols = [];

        $scope.totalbacklog1 = 0;
        $scope.totalbacklog2 = 0;
        $scope.totalbacklog3 = 0;
        $scope.totalbacklog4 = 0;
        $scope.totalbacklog5 = 0;

        if (!_.isUndefined(admchartdata3)) {
            //var monthyearary = _.uniq(_.map(local.monthWBS, 'wbsCode'));
            _.forEach(admchartdata3, function (loopmonth3) {
                var chartitem3 = {};
                chartitem3.monthYear = loopmonth3.monthYear;
                chartitem3.backlog1 = loopmonth3.backlog1;
                chartitem3.backlog2 = loopmonth3.backlog2;
                chartitem3.backlog3 = loopmonth3.backlog3;
                chartitem3.backlog4 = loopmonth3.backlog4;
                chartitem3.backlog5 = loopmonth3.backlog5;
                $scope.totalbacklog1 = $scope.totalbacklog1 + chartitem3.backlog1;
                $scope.totalbacklog2 = $scope.totalbacklog2 + chartitem3.backlog2;
                $scope.totalbacklog3 = $scope.totalbacklog3 + chartitem3.backlog3;
                $scope.totalbacklog4 = $scope.totalbacklog4 + chartitem3.backlog4;
                $scope.totalbacklog5 = $scope.totalbacklog5 + chartitem3.backlog5;
                chartObject3.chartdata.push(chartitem3);
            });


            chartObject3.chartdatacols.push({ "id": "backlog1", "type": "line", "name": "Backlog 1" });
            chartObject3.chartdatacols.push({ "id": "backlog2", "type": "line", "name": "Backlog 2" });
            chartObject3.chartdatacols.push({ "id": "backlog3", "type": "line", "name": "Backlog 3" });
            chartObject3.chartdatacols.push({ "id": "backlog4", "type": "line", "name": "Backlog 4" });
            chartObject3.chartdatacols.push({ "id": "backlog5", "type": "line", "name": "Backlog 5" });

            chartObject3.chartdatax = { "id": "monthYear" };

            $scope.griddata3 = chartObject3.chartdata;

        }

        return chartObject3;
    }

    var graphLoad4 = function () {
        var chartObject4 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        //chartObject.chartdata = [];
        //chartObject.chartdatax = {};
        //chartObject.chartdatacols = [];

        $scope.totaleffortperticket = 0;

        if (!_.isUndefined(admchartdata4)) {
            //var monthyearary = _.uniq(_.map(local.monthWBS, 'wbsCode'));
            _.forEach(admchartdata4, function (loopmonth4) {
                var chartitem4 = {};
                chartitem4.monthYear = loopmonth4.monthYear;
                chartitem4.effortperticket = parseFloat((loopmonth4.effortperticket).toFixed(2));

                $scope.totaleffortperticket = $scope.totaleffortperticket + chartitem4.effortperticket;

                chartObject4.chartdata.push(chartitem4);
            });


            chartObject4.chartdatacols.push({ "id": "effortperticket", "type": "line", "name": "Effort Per Ticket" });


            chartObject4.chartdatax = { "id": "monthYear" };

            $scope.griddata4 = chartObject4.chartdata;

        }

        return chartObject4;
    }

    var graphLoad5 = function () {
        var chartObject5 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        //chartObject.chartdata = [];
        //chartObject.chartdatax = {};
        //chartObject.chartdatacols = [];

        $scope.totalcostperticket = 0;

        if (!_.isUndefined(admchartdata5)) {
            //var monthyearary = _.uniq(_.map(local.monthWBS, 'wbsCode'));
            _.forEach(admchartdata5, function (loopmonth5) {
                var chartitem5 = {};
                chartitem5.monthYear = loopmonth5.monthYear;
                chartitem5.costperticketusd = parseFloat(loopmonth5.costperticketusd.toFixed(2));

                $scope.totalcostperticket = $scope.totalcostperticket + chartitem5.costperticketusd;

                chartObject5.chartdata.push(chartitem5);
            });


            chartObject5.chartdatacols.push({ "id": "costperticketusd", "type": "line", "name": "Cost Per Ticket" });


            chartObject5.chartdatax = { "id": "monthYear" };

            $scope.griddata5 = chartObject5.chartdata;

        }

        return chartObject5;
    }

    var dataLoad = function () {
        var chartobj = [];

        AdmChartService.getAdmChartdata({ category: $scope.ddlincidcategory, accountname: $scope.ddlaccountname, serviceline: $scope.ddlserviceline })
        .$promise
        .then(function (admdata) {
            admchartdata = admdata;
            chartobj = graphLoad();

            if (!_.isUndefined(chartobj)) {
                $scope.admChart = chartobj;
            }
        });

    }

    var dataLoad1 = function () {
        var chartobj1 = [];
        AdmChartService.getAdmChartPrioritydata({ category: $scope.ddlincidcategory, accountname: $scope.ddlaccountname, serviceline: $scope.ddlserviceline })
    .$promise
    .then(function (admdata1) {
        admchartdata1 = admdata1;
        chartobj1 = graphLoad1();

        if (!_.isUndefined(chartobj1)) {
            $scope.admChart1 = chartobj1;
        }
    });
    }

    var dataLoad2 = function () {
        var chartobj2 = [];
        AdmChartService.getAdmChartBeyondSLAdata({ category: $scope.ddlincidcategory, accountname: $scope.ddlaccountname, serviceline: $scope.ddlserviceline })
    .$promise
    .then(function (admdata2) {
        admchartdata2 = admdata2;
        chartobj2 = graphLoad2();

        if (!_.isUndefined(chartobj2)) {
            $scope.admChart2 = chartobj2;
        }
    });
    }

    var dataLoad3 = function () {
        var chartobj3 = [];
        AdmChartService.getAdmChartBacklogPrioritydata({ category: $scope.ddlincidcategory, accountname: $scope.ddlaccountname, serviceline: $scope.ddlserviceline })
    .$promise
    .then(function (admdata3) {
        admchartdata3 = admdata3;
        chartobj3 = graphLoad3();

        if (!_.isUndefined(chartobj3)) {
            $scope.admChart3 = chartobj3;
        }
    });
    }

    var dataLoad4 = function () {
        var chartobj4 = {};
        AdmChartService.getAdmChartEffortPerTicketdata({ category: $scope.ddlincidcategory, accountname: $scope.ddlaccountname, serviceline: $scope.ddlserviceline })
    .$promise
    .then(function (admdata4) {
        admchartdata4 = admdata4;
        chartobj4 = graphLoad4();

        if (!_.isUndefined(chartobj4)) {
            $scope.admChart4 = chartobj4;
        }
    });
    }

    var dataLoad5 = function () {
        var chartobj5 = [];
        AdmChartService.getAdmChartCostPerTicketdata({ category: $scope.ddlincidcategory, accountname: $scope.ddlaccountname, serviceline: $scope.ddlserviceline })
    .$promise
    .then(function (admdata5) {
        admchartdata5 = admdata5;
        chartobj5 = graphLoad5();

        if (!_.isUndefined(chartobj5)) {
            $scope.admChart5 = chartobj5;
        }
    });
    }

    var startlaod = function () {

        dataLoad();
    }

    var startlaod1 = function () {

        dataLoad1();
    }

    var startlaod2 = function () {

        dataLoad2();
    }

    var startlaod3 = function () {

        dataLoad3();
    }

    var startlaod4 = function () {

        dataLoad4();
    }

    var startlaod5 = function () {

        dataLoad5();
    }

    startlaod();
    startlaod1();
    startlaod2();
    startlaod3();
    startlaod4();
    startlaod5();

    //endregion

    //region events
    $scope.servicelin = function (sline) {

        $scope.ddlserviceline = sline;

        dataLoad();
        dataLoad1();
        dataLoad2();
        dataLoad3();
        dataLoad4();
        dataLoad5();

    };

    $scope.accountnam = function (aname) {

        $scope.ddlaccountname = aname;

        dataLoad();
        dataLoad1();
        dataLoad2();
        dataLoad3();
        dataLoad4();
        dataLoad5();

    };

    $scope.category = function (cat) {

        $scope.ddlincidcategory = cat;

        dataLoad();
        dataLoad1();
        dataLoad2();
        dataLoad3();
        dataLoad4();
        dataLoad5();

    };

 


    //endregion
});