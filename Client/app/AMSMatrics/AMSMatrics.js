﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('asmmatrics', {
        url: '/AMSMatrics',
        templateUrl: 'Client/app/AMSMatrics/AMSMatrics.html',
        controller: "AMSMatricsController"
    });
});

