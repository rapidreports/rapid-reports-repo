﻿RapidReportsModule.controller('DashboardController', function ($scope, $http) {
    $scope.getgetDashboardTile = function () {
        var promise = $http({
            method: 'GET',
            url: '/app/getDashboardTile'
        });
        promise.success(function (data) {
           // alert(data);
            $scope.tiledetails = data;
        });       
        promise.error(function (response) {
            alert('Fail');
            $scope.error_message = response.error_message;
        });    
    };
    $scope.getgetDashboardTile();
});