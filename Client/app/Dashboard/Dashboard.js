﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('dashboard', {
        url: '/Dashboard',
        templateUrl: 'client/app/Dashboard/Dashboard.html',
        controller: "DashboardController"
    });
});

