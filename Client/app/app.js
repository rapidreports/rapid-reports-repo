﻿var RapidReportsModule = angular.module('RapidReportsModule', [
    'ui.router',
    'ngResource',
    'nvd3',
    'ngMaterial',
    'gridshore.c3js.chart',
    'ngCookies'
])

.run(function ($rootScope, $cookies) {
    $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from) {
        $cookies.previousState = from.name;
    });
});

_.mixin({
    'findByValues': function (collection, property, values) {
        return _.filter(collection, function (item) {
            return _.includes(values, item[property]);
        });
    },

    'validUrl': function (url) {
        var pattern = new RegExp('^(https?:\/\/)?');

        if (pattern.test(url)) {
            return false;
        }
        else {
            return true;
        }
    }
});