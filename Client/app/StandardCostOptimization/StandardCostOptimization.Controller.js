﻿RapidReportsModule.controller("StandardCostOptimizationController", function ($scope, $http) {
    $scope.ContentHeading = "Standard Cost Optimization";
    $scope.getSubTileInformatinon = function () {
        var promise = $http({
            method: 'GET',
            url: '/app/getSubTileInformation/StandardCostOptimization'
        });
        promise.success(function (data) {
            // alert(data);
            $scope.subtiledetails = data;
        });
        promise.error(function (response) {
            alert('Fail');
            $scope.error_message = response.error_message;
        });
    };
    $scope.getSubTileInformatinon();
});