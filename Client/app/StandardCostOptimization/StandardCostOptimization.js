﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('standardCostOptimization', {
        url: '/StandardCostOptimization',
        templateUrl: 'client/app/CommonSubTile/CommonSubTile.html',
        controller: "StandardCostOptimizationController"
    });
});