﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('transitionmanagement', {
        url: '/TransitionManagement',
        templateUrl: 'client/app/CommonSubTile/CommonSubTile.html',
        controller: "TransitionController"
    });
});