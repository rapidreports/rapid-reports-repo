﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('ids', {
        url: '/AD Metrics',
        templateUrl: 'client/app/IDS/IDS.html',
        controller: "IDSController"
    })
        .state('idsprojecttype', {
            url: '/IDSProjectType',
            templateUrl: 'client/app/IDS/IDSProjectType.html',
            controller: "IDSProjectTypeController"
        })
    .state("idsboxes", {
        url: '/IDSBoxes',
        templateUrl: 'client/app/IDS/IDSBoxes.html',
        controller: "IDSBoxesController"
    });
});

