﻿RapidReportsModule.controller("IDSController", function ($scope,
    $http,
    MonthlyAccountService,
    $state,
    ReportService,
    ChartService,
    DateHelper,
    AppDefault,
    GenerateReportService,
    ExportService,
    $cookies) {

    var d = new Date();
    $scope.years = [d.getFullYear(), d.getFullYear() - 1, d.getFullYear() - 2, d.getFullYear() - 3, d.getFullYear() - 4, d.getFullYear() - 5, d.getFullYear() - 6, d.getFullYear() - 7, d.getFullYear() - 8, d.getFullYear() - 9, d.getFullYear() - 10];
    $scope.Months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    $scope.ProjectType = ["CMMI", "ITIL"];
    $scope.selectedProjectType = _.first($scope.ProjectType);
    $scope.selectedYear = _.first($scope.years);
    //$scope.selectedMonth = $scope.Months[d.getMonth() - 1];

   
    $scope.selectedMonth = "Apr";
   

    $scope.getIDSProjectsDetails = function () {
        $scope.CMMIRed = 0;
        $scope.CMMIYellow = 0;
        $scope.CMMIGreen = 0;
        $scope.ITILRed = 0;
        $scope.ITILYellow = 0;
        $scope.ITILGreen = 0;
        $scope.fp = 0;
        $scope.totalDefects = 0;
        $scope.defectDensity = 0;
        $scope.Productivity = 0;

        var promise = $http({
            method: 'GET',
            url: '/app/getIDSProjectsDetails/' + $scope.selectedYear + '/' + $scope.selectedMonth
        });
        promise.success(function (data) {

            if (data != undefined) {
                if (data.pmp != undefined) {
                    for (var i = 0; i < data.pmp.length; i++) {
                        if (data.pmp[i].projectType == "CMMI") {
                            $scope.CMMIRed = data.pmp[i].red;
                            $scope.CMMIYellow = data.pmp[i].yellow;
                            $scope.CMMIGreen = data.pmp[i].green;
                        }
                        else if (data.pmp[i].projectType == "ITIL") {
                            $scope.ITILRed = data.pmp[i].red;
                            $scope.ITILYellow = data.pmp[i].yellow;
                            $scope.ITILGreen = data.pmp[i].green;
                        }
                    }
                }
                if (data.pdm != null) {
                    $scope.fp = data.pdm.fp;
                    $scope.totalDefects = data.pdm.totalDefects;
                    $scope.defectDensity = data.pdm.defectDensity;

                    var chart = c3.generate({
                        bindto: '#chart',
                        size: {
                            width: 500,
                            height:370
                        },
                        data: {
                            columns: [
                              ['FP', $scope.fp],
                              ['Total Defects', $scope.totalDefects],
                              ['Defect Density', $scope.defectDensity]
                            ],
                            type: 'bar'
                        }
                    });
                  
                }
                if (data.productivityAmount != null) {
                    $scope.Productivity = data.productivityAmount.productAmount;
                }
            }
        })
        promise.error(function (response) {
            alert('Fail');
            $scope.error_message = response.error_message;
        });
    };
    $scope.getIDSProjectsDetails();

     

    $scope.IDSlistChanged = function (data) {
        $scope.getIDSProjectsDetails();
    }

});