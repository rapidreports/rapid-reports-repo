﻿'use strict';

angular.module('RapidReportsModule')
.config(function ($stateProvider) {
    $stateProvider
    .state('fte', {
        url: '/fte',
        templateUrl: 'client/app/overview/fte/fte.html',
        controller: 'FTECtrl'
    });
});