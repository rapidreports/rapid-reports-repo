﻿'use strict';

angular.module('RapidReportsModule')
    .run(function ($rootScope, $state, $log) {
        $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            if (toState.name === 'overview') {
                $log.debug('Auto-transitioning state :: overview -> overview.main');
                $state.go('overview.main', { opts: $state.params });
            }
        });
    })
.config(function ($stateProvider) {
    $stateProvider
    .state('overview', {
        url: '/overview',
        template: '<div ui-view></div>'
    })
    .state('overview.main', {
        url: '/',
        templateUrl: 'client/app/overview/overview.html',
        controller: 'OverviewCtrl'
    });
});