﻿'use strict';

angular.module('RapidReportsModule')
.controller('VDFCtrl', function (
    $scope,
    $timeout,
    DateHelper,
    AppDefault,
    GenerateReportService,
    ExportService,
    VdfChartService,
    PageHeaderService
    ) {

    PageHeaderService.setPageHeaderTitle('VDF');
    var chartTypes = AppDefault.chartTypes;
    $scope.vdfChart = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.vdfChart1 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.vdfChart2 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.vdfChart3 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.vdfChart4 = {
        chartdata:[],
        chartdatacols: [],
        chartdatax:{}
    };
    $scope.vdfChart5 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.vdfChart6 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    $scope.vdfChart7 = {
        chartdata: [],
        chartdatacols: [],
        chartdatax: {}
    };
    //region load
       

    VdfChartService.getVdfAccountName()
    .$promise
    .then(function (vdfaccountname) {
        $scope.accountname = vdfaccountname;

    });

    //VdfChartService.getvdfServiceLine()
    //  .$promise
    //  .then(function (vdfserviceline) {
    //      $scope.serviceline = vdfserviceline;

    //  });

   
    $scope.ddlaccountname = "TELSTRA CORPORATION LIMITED";
  
    $scope.ddlaccountname1 = "TELSTRA CORPORATION LIMITED";
   
    $scope.ddlaccountname2 = "TELSTRA CORPORATION LIMITED";
 
    $scope.ddlaccountname3 = "TELSTRA CORPORATION LIMITED";
   
    $scope.ddlaccountname4 = "TELSTRA CORPORATION LIMITED";

    $scope.ddlaccountname5 = "TELSTRA CORPORATION LIMITED";

    $scope.ddlaccountname6 = "TELSTRA CORPORATION LIMITED";

    $scope.ddlaccountname7 = "TELSTRA CORPORATION LIMITED";
   

    var vdfchartdata = [];
    var vdfchartdata1 = [];
    var vdfchartdata2 = [];
    var vdfchartdata3 = [];
    var vdfchartdata4 = [];
    var vdfchartdata5 = [];
    var vdfchartdata6 = [];
    var vdfchartdata7 = [];

    $scope.griddata;
    $scope.griddata1;
    $scope.griddata2;
    $scope.griddata3;
    $scope.griddata4;
    $scope.griddata5;
    $scope.griddata6;
    $scope.griddata7;

    var graphLoad = function () {
        var chartObject = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        $scope.totaldollarcustomer = 0;
       

        if (!_.isUndefined(vdfchartdata)) {
           
            _.forEach(vdfchartdata, function (loopmonth) {
                var chartitem = {};
                chartitem.closuremonth = loopmonth.closuremonth;
                chartitem.dollarBenefitforcustomer = loopmonth.dollarBenefitforcustomer;
               
                $scope.totaldollarcustomer = $scope.totaldollarcustomer + chartitem.dollarBenefitforcustomer;
            
                chartObject.chartdata.push(chartitem);
            });


            chartObject.chartdatacols.push({ "id": "dollarBenefitforcustomer", "type": "line", "name": "Dollar Benefit for customer" });
          

            chartObject.chartdatax = { "id": "closuremonth" };

            $scope.griddata = chartObject.chartdata;

        }

        return chartObject;
    }

    var graphLoad1 = function () {
        var chartObject1 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        $scope.totaldollarhp = 0;


        if (!_.isUndefined(vdfchartdata1)) {

            _.forEach(vdfchartdata1, function (loopmonth1) {
                var chartitem1 = {};
                chartitem1.closuremonth = loopmonth1.closuremonth;
                chartitem1.dollarBenefitHP = parseFloat(loopmonth1.dollarBenefitHP).toFixed(2);

                $scope.totaldollarhp = $scope.totaldollarhp + parseFloat(chartitem1.dollarBenefitHP);

                chartObject1.chartdata.push(chartitem1);
            });


            chartObject1.chartdatacols.push({ "id": "dollarBenefitHP", "type": "line", "name": "Dollar Benefit for HP" });


            chartObject1.chartdatax = { "id": "closuremonth" };

            $scope.griddata1 = chartObject1.chartdata;

        }

        return chartObject1;
    }

    var graphLoad2 = function () {
        var chartObject2 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        $scope.totalEffortSaving = 0;


        if (!_.isUndefined(vdfchartdata2)) {

            _.forEach(vdfchartdata2, function (loopmonth2) {
                var chartitem2 = {};
                chartitem2.closuremonth = loopmonth2.closuremonth;
                chartitem2.effortSaving =  parseFloat(loopmonth2.effortSaving).toFixed(2);

                $scope.totalEffortSaving = $scope.totalEffortSaving +  parseFloat(chartitem2.effortSaving);

                chartObject2.chartdata.push(chartitem2);
            });


            chartObject2.chartdatacols.push({ "id": "effortSaving", "type": "line", "name": "Effort Saving" });


            chartObject2.chartdatax = { "id": "closuremonth" };

            $scope.griddata2 = chartObject2.chartdata;

        }

        return chartObject2;
    }

    var graphLoad3 = function () {
        var chartObject3 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        $scope.totalIncidentreduction = 0;


        if (!_.isUndefined(vdfchartdata3)) {

            _.forEach(vdfchartdata3, function (loopmonth3) {
                var chartitem3 = {};
                chartitem3.closuremonth = loopmonth3.closuremonth;
                chartitem3.incidentreduction = parseFloat(loopmonth3.incidentreduction).toFixed(2);

                $scope.totalIncidentreduction = $scope.totalIncidentreduction + parseFloat(chartitem3.incidentreduction);

                chartObject3.chartdata.push(chartitem3);
            });


            chartObject3.chartdatacols.push({ "id": "incidentreduction", "type": "line", "name": "Incident reduction" });


            chartObject3.chartdatax = { "id": "closuremonth" };

            $scope.griddata3 = chartObject3.chartdata;

        }

        return chartObject3;
    }

    var graphLoad4 = function () {
        var chartObject4 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        $scope.totaldollarcustomerqua = 0;


        if (!_.isUndefined(vdfchartdata4)) {

            _.forEach(vdfchartdata4, function (loopmonth4) {
                var chartitem4 = {};
                chartitem4.closureQuarter = loopmonth4.closureQuarter;
                chartitem4.dollarBenefitforcustomer = parseFloat(loopmonth4.dollarBenefitforcustomer).toFixed(2);

                $scope.totaldollarcustomerqua = $scope.totaldollarcustomerqua + parseFloat(chartitem4.dollarBenefitforcustomer);

                chartObject4.chartdata.push(chartitem4);
            });


            chartObject4.chartdatacols.push({ "id": "dollarBenefitforcustomer", "type": "line", "name": "Quater Dollar Benefit for customer" });


            chartObject4.chartdatax = { "id": "closureQuarter" };

            $scope.griddata4 = chartObject4.chartdata;

        }

        return chartObject4;
    }

    var graphLoad5 = function () {
        var chartObject5 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        $scope.totaldollarhpqua = 0;


        if (!_.isUndefined(vdfchartdata5)) {

            _.forEach(vdfchartdata5, function (loopmonth5) {
                var chartitem5 = {};
                chartitem5.closureQuarter = loopmonth5.closureQuarter;
                chartitem5.dollarBenefitHP = parseFloat(loopmonth5.dollarBenefitHP).toFixed(2);

                $scope.totaldollarhpqua = $scope.totaldollarhpqua + parseFloat(chartitem5.dollarBenefitHP);

                chartObject5.chartdata.push(chartitem5);
            });


            chartObject5.chartdatacols.push({ "id": "dollarBenefitHP", "type": "line", "name": "Quater Dollar Benefit for HP" });


            chartObject5.chartdatax = { "id": "closureQuarter" };

            $scope.griddata5 = chartObject5.chartdata;

        }

        return chartObject5;
    }

    var graphLoad6 = function () {
        var chartObject6 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        $scope.totalEffortSavingqua = 0;


        if (!_.isUndefined(vdfchartdata6)) {

            _.forEach(vdfchartdata6, function (loopmonth6) {
                var chartitem6 = {};
                chartitem6.closureQuarter = loopmonth6.closureQuarter;
                chartitem6.effortSaving = parseFloat(loopmonth6.effortSaving).toFixed(2);

                $scope.totalEffortSavingqua = $scope.totalEffortSavingqua + parseFloat(chartitem6.effortSaving);

                chartObject6.chartdata.push(chartitem6);
            });


            chartObject6.chartdatacols.push({ "id": "effortSaving", "type": "line", "name": "Quater Effort Saving" });


            chartObject6.chartdatax = { "id": "closureQuarter" };

            $scope.griddata6 = chartObject6.chartdata;

        }

        return chartObject6;
    }

    var graphLoad7 = function () {
        var chartObject7 = {
            chartdata: [],
            chartdatax: {},
            chartdatacols: []
        };

        $scope.totalIncidentreductionqua = 0;


        if (!_.isUndefined(vdfchartdata7)) {

            _.forEach(vdfchartdata7, function (loopmonth7) {
                var chartitem7 = {};
                chartitem7.closureQuarter = loopmonth7.closureQuarter;
                chartitem7.incidentreduction = parseFloat(loopmonth7.incidentreduction).toFixed(2);

                $scope.totalIncidentreductionqua = $scope.totalIncidentreductionqua + parseFloat(chartitem7.incidentreduction);

                chartObject7.chartdata.push(chartitem7);
            });


            chartObject7.chartdatacols.push({ "id": "incidentreduction", "type": "line", "name": "Quater Incident reduction" });


            chartObject7.chartdatax = { "id": "closureQuarter" };

            $scope.griddata7 = chartObject7.chartdata;

        }

        return chartObject7;
    }

    var dataLoad = function () {
        var chartobj = [];

        VdfChartService.getVdfmoncustomerChartdata({ accountname: $scope.ddlaccountname })
        .$promise
        .then(function (vdfdata) {
            vdfchartdata = vdfdata;
            chartobj = graphLoad();

            if (!_.isUndefined(chartobj)) {
                $scope.vdfChart = chartobj;
            }
        });

    }

    var dataLoad1 = function () {
        var chartobj1 = [];
        VdfChartService.getVdfmonHPChartdata({ accountname: $scope.ddlaccountname1 })
    .$promise
    .then(function (vdfdata1) {
        vdfchartdata1 = vdfdata1;
        chartobj1 = graphLoad1();

        if (!_.isUndefined(chartobj1)) {
            $scope.vdfChart1 = chartobj1;
        }
    });
    }

    var dataLoad2 = function () {
        var chartobj2 = [];
        VdfChartService.getVdfmonSavingChartdata({ accountname: $scope.ddlaccountname2 })
    .$promise
    .then(function (vdfdata2) {
        vdfchartdata2 = vdfdata2;
        chartobj2 = graphLoad2();

        if (!_.isUndefined(chartobj2)) {
            $scope.vdfChart2 = chartobj2;
        }
    });
    }

    var dataLoad3 = function () {
        var chartobj3 = [];
        VdfChartService.getVdfmonIncidentReductionChartdata({ accountname: $scope.ddlaccountname3 })
    .$promise
    .then(function (vdfdata3) {
        vdfchartdata3 = vdfdata3;
        chartobj3 = graphLoad3();

        if (!_.isUndefined(chartobj3)) {
            $scope.vdfChart3 = chartobj3;
        }
    });
    }

    var dataLoad4 = function () {
        var chartobj4 = {};
        VdfChartService.getVdfquacustomerChartdata({ accountname: $scope.ddlaccountname4 })
    .$promise
    .then(function (vdfdata4) {
        vdfchartdata4 = vdfdata4;
        chartobj4 = graphLoad4();

        if (!_.isUndefined(chartobj4)) {
            $scope.vdfChart4 = chartobj4;
        }
    });
    }

    var dataLoad5 = function () {
        var chartobj5 = [];
        VdfChartService.getVdfquaHPChartdata({ accountname: $scope.ddlaccountname5 })
    .$promise
    .then(function (vdfdata5) {
        vdfchartdata5 = vdfdata5;
        chartobj5 = graphLoad5();

        if (!_.isUndefined(chartobj5)) {
            $scope.vdfChart5 = chartobj5;
        }
    });
    }

    var dataLoad6 = function () {
        var chartobj6 = [];
        VdfChartService.getVdfquaSavingChartdata({ accountname: $scope.ddlaccountname6 })
    .$promise
    .then(function (vdfdata6) {
        vdfchartdata6 = vdfdata6;
        chartobj6 = graphLoad6();

        if (!_.isUndefined(chartobj6)) {
            $scope.vdfChart6 = chartobj6;
        }
    });
    }

    var dataLoad7 = function () {
        var chartobj7 = [];
        VdfChartService.getVdfquaIncidentReductionChartdata({ accountname: $scope.ddlaccountname7 })
    .$promise
    .then(function (vdfdata7) {
        vdfchartdata7 = vdfdata7;
        chartobj7 = graphLoad7();

        if (!_.isUndefined(chartobj7)) {
            $scope.vdfChart7 = chartobj7;
        }
    });
    }

    var startlaod = function () {

        dataLoad();
    }

    var startlaod1 = function () {

        dataLoad1();
    }

    var startlaod2 = function () {

        dataLoad2();
    }

    var startlaod3 = function () {

        dataLoad3();
    }

    var startlaod4 = function () {

        dataLoad4();
    }

    var startlaod5 = function () {

        dataLoad5();
    }

    var startlaod6 = function () {

        dataLoad6();
    }

    var startlaod7 = function () {

        dataLoad7();
    }

    startlaod();
    startlaod1();
    startlaod2();
    startlaod3();
    startlaod4();
    startlaod5();
    startlaod6();
    startlaod7();

    //endregion

    //region events
  

    $scope.accountnam = function (aname) {

        $scope.ddlaccountname = aname;

        dataLoad();

    };

   
    $scope.accountnam1 = function (aname1) {

        $scope.ddlaccountname1 = aname1;

        dataLoad1();

    };


    $scope.accountnam2 = function (aname2) {

        $scope.ddlaccountname2 = aname2;

        dataLoad2();

    };

   
    $scope.accountnam3 = function (aname3) {

        $scope.ddlaccountname3 = aname3;

        dataLoad3();

    };

    $scope.accountnam4 = function (aname4) {

        $scope.ddlaccountname4 = aname4;

        dataLoad4();

    };

    $scope.accountnam5 = function (aname5) {

        $scope.ddlaccountname5 = aname5;

        dataLoad5();

    };

    $scope.accountnam6 = function (aname5) {

        $scope.ddlaccountname6 = aname6;

        dataLoad6();

    };

    $scope.accountnam7 = function (aname7) {

        $scope.ddlaccountname7 = aname7;

        dataLoad7();

    };
   
    //endregion


});