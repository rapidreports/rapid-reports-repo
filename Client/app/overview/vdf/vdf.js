﻿'use strict';

angular.module('RapidReportsModule')
.config(function ($stateProvider) {
    $stateProvider
    .state('overview.vdf', {
        url: '/vdf',
        templateUrl: 'client/app/overview/vdf/vdf.html',
        controller: 'VDFCtrl'
    });
});