﻿'use strict';

angular.module('RapidReportsModule')
.controller('ReportCtrl', function (
    $scope,
    ReportService,
    $filter,
    ChartService,
    $interval,
    dateFilter,
    $state,
    MonthlyAccountService,
    AppDefault,
    DateHelper,
    //PageHeaderService,
    GenerateReportService,
    ExportService,
    $timeout
    ) {
    //PageHeaderService.setPageHeaderTitle('Reports');

    var contractCost = [];
    var yearlyContract = [];
    var billingCost = [];
    var yearlyBilling = [];
    var accounts = [];
    var monthlyFTE = [];
    var yearlyFTE = [];
    var monthlyRateCard = [];
    var yearlyRateCard = [];
    var conOnshore = 'onshore';
    var conOffshore = 'offshore';
    $scope.revenueReport = [];
    $scope.costReport = [];
    $scope.cumRevenueReport = [];
    $scope.cumCostReport = [];
    //!! TODO: change to below code
    var currMonNum = 2;//$filter('date')(new Date(), 'M');
    $scope.currMon = 'Feb';//$filter('date')(new Date(), 'MMM');
    $scope.currYear = parseInt($filter('date')(new Date(), 'yyyy'));
    $scope.CalendarMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    $scope.revenueyrly = [];
    $scope.costyrly = [];
    $scope.overallrevenueyrly = [];
    $scope.overallcostyrly = [];
    $scope.calendarYears = [];
    $scope.totals = {
        cummCostTarget: 0,
        cummCostActual: 0,
        cummCostVariance: 0,
        cummRevenueRev: 0,
        cummRevActual: 0,
        cummRevMargin: 0,
        costTarget: 0,
        costActual: 0,
        costVariance: 0,
        ndcostTarget: 0,
        ndcostActual: 0,
        ndcostVariance: 0,
        revRevenue: 0,
        revActual: 0,
        revMargin: 0
    };
    var monthObj = DateHelper.getMonthArray();

    $scope.overall = {
        chartdata: {
            serviceLineOverall: [],
            serviceLineFinYear: [],
            pyramidOverall: [],
            pyramidFinancial: [],
            pyramidServiceLineOverall: [],
            pyramidServiceLineFinancial: []
        },
        chartOptions: {
            serviceLineOverallCols: [],
            serviceLineFinYearCols: [],
            pyramidOverallCols: [],
            pyramidFinancialCols: [],
            pyramidServiceLineOverallCols: [],
            pyramidServiceLineFinancialCols: [],
            serviceLineOverallDataX: [],
            serviceLineFinYearDataX: [],
            pyramidOverallDataX: [],
            pyramidFinancialDataX: [],
            pyramidServiceLineOverallDataX: [],
            pyramidServiceLineFinancialDataX: [],
            serviceLineOverallType: chartTypes.spline,
            serviceLineFinYearType: chartTypes.spline,
            pyramidOverallType: chartTypes.spline,
            pyramidFinancialType: chartTypes.spline,
            pyramidServiceLineOverallType: chartTypes.spline,
            pyramidServiceLineFinancialType: chartTypes.spline
        },
        dropdown: {
            serviceLineOverallYear: 0,
            serviceLineFinancialYear: 0,
            pyramidFinancialOverallYear: 0,
            pyramidServiceLineFinancialYear: 0
        },
        gridData: {
            serviceLineOverallGrid: [],
            pyramidOverallGrid: [],
            pyramidServiceLineOverallGrid: []
        },
        totalFooter: {
            serviceLineOverallTotal: {}
        }
    };

    var pageLoad = function () {
        ReportService.getAccounts({ month: $scope.currMon, year: $scope.currYear })
        .$promise
        .then(function (accdata) {
            accounts = accdata;
            gridLoad();
            graphLoad();
        });
    }

    pageLoad();

    //region grid
    var gridLoad = function () {
        contractCost = [];
        billingCost = [];
        monthlyFTE = [];
        monthlyRateCard = [];
        $scope.revenueReport = [];
        $scope.costReport = [];

        ReportService.getFTE({ month: $scope.currMon, year: $scope.currYear })
        .$promise
        .then(function (ftedata) {
            monthlyFTE = ftedata;

            ReportService.getRateCard({ month: $scope.currMon, year: $scope.currYear })
            .$promise
            .then(function (ratecarddata) {
                monthlyRateCard = ratecarddata;

                ReportService.getContractCost({ month: monthNumeric($scope.currMon), year: $scope.currYear })
                .$promise
                .then(function (contdata) {
                    contractCost = contdata;

                    ReportService.getBillingCost({ month: $scope.currMon, year: $scope.currYear })
                    .$promise
                    .then(function (billdata) {
                        billingCost = billdata;

                        $scope.revenueReport = revenueReport(monthlyFTE, billingCost, contractCost);
                        $scope.costReport = costReport(monthlyFTE, monthlyRateCard, billingCost, contractCost);
                        ndData();
                        // costforecast();
                    });
                });
            });
        });
    }

    //!! shows arrows in the grid
    var costforecast = function () {
        var prevMonthFTE = [];
        var prevMonthContractCost = [];
        var prevMonthBillingCost = [];
        var prevMonthRateCard = [];
        var prevMonthCostReport = [];
        var prevMonthRevReport = [];

        var nd = {
            FTE: [],
            WBS: [],
            Contract: [],
            ChangeRequest: [],
            MasterWBS: [],
            MasterContract: [],
            month: '',
            year: 0
        };

        var currMonthObj = _.find(monthObj, function (item) { return item.shortMon === $scope.currMon; });
        var prevMonthObj = {};
        var yearnum = 0;

        if (currMonthObj.num > 1) {
            prevMonthObj = _.find(monthObj, function (item) { return item.num === currMonthObj.num - 1; });
            yearnum = $scope.currYear;
        }
        else {
            prevMonthObj = _.find(monthObj, function (item) { return item.num === 12; });
            yearnum = $scope.currYear - 1;
        }

        //!! fetch FTE of prev month
        ReportService.getFTE({ month: prevMonthObj.shortMon, year: yearnum })
        .$promise
        .then(function (ftedata) {
            prevMonthFTE = ftedata;

            ReportService.getContractCost({ month: prevMonthObj.num, year: yearnum })
            .$promise
            .then(function (contdata) {
                prevMonthContractCost = contdata;

                ReportService.getBillingCost({ month: prevMonthObj.shortMon, year: yearnum })
                .$promise
                .then(function (billdata) {
                    prevMonthBillingCost = billdata;

                    //!! start revenue report
                    //revenueforecast(prevMonthFTE, prevMonthBillingCost, prevMonthContractCost);
                    prevMonthRevReport = revenueReport(prevMonthFTE, prevMonthBillingCost, prevMonthContractCost);

                    ReportService.getRateCard({ month: prevMonthObj.shortMon, year: yearnum })
                    .$promise
                    .then(function (ratecarddata) {
                        prevMonthRateCard = ratecarddata;

                        //!! get cost report for prev month to compare
                        prevMonthCostReport = costReport(prevMonthFTE, prevMonthRateCard, prevMonthBillingCost, prevMonthContractCost);

                        //!! ND
                        ReportService.getMonthNDFTE({ month: prevMonthObj.shortMon, year: yearnum })
                        .$promise
                        .then(function (ndftedata) {
                            nd.FTE = ndftedata;

                            ReportService.getMonthNDWBS({ month: prevMonthObj.shortMon, year: yearnum })
                            .$promise
                            .then(function (ndwbsdata) {
                                nd.WBS = ndwbsdata;

                                ReportService.getMonthNDContract({ month: prevMonthObj.shortMon, year: yearnum })
                                .$promise
                                .then(function (ndcontractdata) {
                                    nd.Contract = ndcontractdata;

                                    ReportService.getMonthNDChangeRequest({ month: prevMonthObj.shortMon, year: yearnum })
                                    .$promise
                                    .then(function (ndcrdata) {
                                        nd.ChangeRequest = ndcrdata;

                                        ReportService.getMonthMasterWBS({ month: prevMonthObj.shortMon, year: yearnum })
                                        .$promise
                                        .then(function (masterwbs) {
                                            nd.MasterWBS = masterwbs;

                                            ReportService.getMonthMasterContract({ month: prevMonthObj.num, year: yearnum })
                                            .$promise
                                            .then(function (mastercontract) {
                                                nd.MasterContract = mastercontract;
                                                nd.month = $scope.currMon;
                                                nd.year = $scope.currYear;

                                                //!! mod cost report
                                                alterCostGridData(nd, prevMonthCostReport);

                                                //!! start revenue forecast
                                                revenueforecast(nd, prevMonthRevReport);

                                                _.forEach($scope.costReport, function (loopreport) {
                                                    var compareobj = _.find(prevMonthCostReport, function (item) {
                                                        return item.customer.accountName === loopreport.customer.accountName;
                                                    });

                                                    if (!_.isUndefined(compareobj)) {
                                                        if (parseFloat(compareobj.ndvariance) > parseFloat(loopreport.ndvariance)) {
                                                            loopreport.ndforecast = 'low';
                                                        }
                                                        else {
                                                            loopreport.ndforecast = 'high';
                                                        }

                                                        if (parseFloat(compareobj.variance) > parseFloat(loopreport.variance)) {
                                                            loopreport.forecast = 'low';
                                                        }
                                                        else {
                                                            loopreport.forecast = 'high';
                                                        }
                                                    }
                                                    else {
                                                        loopreport.forecast = 'high';
                                                        loopreport.ndforecast = 'high';
                                                    }
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }



    var revenueforecast = function (nd, prevMonthRevReport) {
        //!! mod revenue
        alterRevenueGridData(nd, prevMonthRevReport);

        _.forEach($scope.revenueReport, function (loopreport) {
            var compareobj = _.find(prevMonthRevReport, function (item) {
                return item.customer.accountName === loopreport.customer.accountName;
            });

            if (!_.isUndefined(compareobj)) {
                if (parseFloat(compareobj.ndvariance) > parseFloat(loopreport.ndvariance)) {
                    loopreport.ndforecast = 'low';
                }
                else {
                    loopreport.ndforecast = 'high';
                }

                if (parseFloat(compareobj.margin) > parseFloat(loopreport.margin)) {
                    loopreport.forecast = 'low';
                }
                else {
                    loopreport.forecast = 'high';
                }
            }
            else {
                loopreport.forecast = 'high';
                loopreport.ndforecast = 'high';
            }
        });
    }



    //!! alter scope for showing nddata in revenue grid
    var alterRevenueGridData = function (nd, gridrecs) {
        _.forEach(gridrecs, function (loopreport) {
            var baseline = 0;
            var actual = 0;
            var variance = 0;

            var contactcost = 0;
            var changereq = 0;

            var accountfterec = _.find(nd.FTE, function (fteitem) { return fteitem.accountName === loopreport.customer.accountName; });
            var contractrecs = _.filter(nd.Contract, function (contractitem) { return contractitem.accountName === loopreport.customer.accountName; });
            var changereqrec = _.find(nd.ChangeRequest, function (critem) { return critem.accountName === loopreport.customer.accountName; });
            var wbsrecs = _.filter(nd.WBS, function (wbsitem) { return wbsitem.accountName === loopreport.customer.accountName; });

            if (!_.isUndefined(accountfterec) && !_.isNull(accountfterec.revenueBaseline)) {
                baseline = accountfterec.revenueBaseline;
            }

            if (!_.isUndefined(changereqrec) && !_.isNull(changereqrec.amount)) {
                baseline += changereqrec.amount;
            }

            if (!_.isUndefined(wbsrecs)) {
                var wbssum = 0;
                var uniqwbsary = _.uniq(_.map(wbsrecs, 'wbsCode'));

                if (!_.isUndefined(nd.MasterWBS)) {
                    //!! filtering collection with WBS array
                    var filteredWBS = _.findByValues(nd.MasterWBS, 'wbsCode', uniqwbsary);

                    if (!_.isUndefined(filteredWBS)) {
                        wbssum = _.reduce(filteredWBS, function (wbssum, item) {
                            if (item.billedAmount === '') { return wbssum + 0; }
                            return wbssum + parseFloat(item.billedAmount);
                        }, 0);
                    }
                }

                actual = wbssum;
            }

            if (!_.isUndefined(contractrecs)) {
                var contractsum = 0;
                var uniqcontractary = _.uniq(_.map(contractrecs, 'contractId'));

                if (!_.isUndefined(nd.MasterContract)) {
                    //!! filtering collection with contract array
                    var filteredContract = _.findByValues(nd.MasterContract, 'contractId', uniqcontractary);

                    if (!_.isUndefined(filteredContract)) {
                        contractsum = _.reduce(filteredContract, function (contractsum, item) {
                            if (item.amount === '') { return contractsum + 0; }
                            return contractsum + parseFloat(item.amount);
                        }, 0);
                    }
                }

                actual += contractsum;
            }

            if (!_.isUndefined(accountfterec) && !_.isNull(accountfterec.additionalCost)) {
                actual += accountfterec.additionalCost;
            }

            if (baseline > 0) {
                variance = ((baseline - actual) / baseline) * 100;
            }
            baseline = baseline.toFixed(2);
            actual = actual.toFixed(2);
            variance = variance.toFixed(1);

            loopreport.ndbaseline = baseline;
            loopreport.ndactual = actual;
            loopreport.ndvariance = variance;
        });
    }

    //costforecast();

    //!! alter scope for showing nddata in cost grid
    var alterCostGridData = function (nd, gridrecs) {
        _.forEach(gridrecs, function (loopreport) {
            var target = 0;
            var actual = 0;
            var variance = 0;
            var contactcost = 0;
            var changereq = 0;

            var accountfterec = _.find(nd.FTE, function (fteitem) { return fteitem.accountName === loopreport.customer.accountName; });
            var contractrecs = _.filter(nd.Contract, function (contractitem) { return contractitem.accountName === loopreport.customer.accountName; });
            var changereqrec = _.find(nd.ChangeRequest, function (critem) { return critem.accountName === loopreport.customer.accountName; });
            var wbsrecs = _.filter(nd.WBS, function (wbsitem) { return wbsitem.accountName === loopreport.customer.accountName; });

            if (!_.isUndefined(accountfterec) &&
                !_.isNull(accountfterec.costBaselined) &&
                !_.isNull(accountfterec.additionalCost) &&
                !_.isNull(accountfterec.riskAmount)) {
                target = accountfterec.costBaselined +
                    //accountfterec.additionalCost +
                    accountfterec.riskAmount;
            }

            if (!_.isUndefined(changereqrec) &&
                !_.isNull(changereqrec.amount)) {
                target += changereqrec.amount;
            }

            if (!_.isUndefined(wbsrecs)) {
                var wbssum = 0;
                var uniqwbsary = _.uniq(_.map(wbsrecs, 'wbsCode'));

                if (!_.isUndefined(nd.MasterWBS)) {
                    //!! filtering collection with WBS array
                    var filteredWBS = _.findByValues(nd.MasterWBS, 'wbsCode', uniqwbsary);

                    if (!_.isUndefined(filteredWBS)) {
                        wbssum = _.reduce(filteredWBS, function (wbssum, item) {
                            if (item.billedAmount === '') { return wbssum + 0; }
                            return wbssum + parseFloat(item.billedAmount);
                        }, 0);
                    }
                }

                actual = wbssum;
            }

            if (!_.isUndefined(contractrecs)) {
                var contractsum = 0;
                var uniqcontractary = _.uniq(_.map(contractrecs, 'contractId'));

                if (!_.isUndefined(nd.MasterContract)) {
                    //!! filtering collection with contract array
                    var filteredContract = _.findByValues(nd.MasterContract, 'contractId', uniqcontractary);

                    if (!_.isUndefined(filteredContract)) {
                        contractsum = _.reduce(filteredContract, function (contractsum, item) {
                            if (item.amount === '') { return contractsum + 0; }
                            return contractsum + parseFloat(item.amount);
                        }, 0);
                    }
                }

                actual += contractsum;
            }

            if (target > 0) {
                variance = ((target - actual) / target) * 100;
            }
            target = target.toFixed(2);
            actual = actual.toFixed(2);
            variance = variance.toFixed(1);

            loopreport.ndtargetCost = target;
            loopreport.ndactualCost = actual;

            loopreport.ndvariance = variance;
        });
    }

    var ndData = function () {
        var nd = {
            FTE: [],
            WBS: [],
            Contract: [],
            ChangeRequest: [],
            MasterWBS: [],
            MasterContract: [],
            month: '',
            year: 0
        };

        ReportService.getMonthNDFTE({ month: $scope.currMon, year: $scope.currYear })
        .$promise
        .then(function (ftedata) {
            nd.FTE = ftedata;

            ReportService.getMonthNDWBS({ month: $scope.currMon, year: $scope.currYear })
            .$promise
            .then(function (wbsdata) {
                nd.WBS = wbsdata;

                ReportService.getMonthNDContract({ month: $scope.currMon, year: $scope.currYear })
                .$promise
                .then(function (contractdata) {
                    nd.Contract = contractdata;

                    ReportService.getMonthNDChangeRequest({ month: $scope.currMon, year: $scope.currYear })
                    .$promise
                    .then(function (crdata) {
                        nd.ChangeRequest = crdata;

                        ReportService.getMonthMasterWBS({ month: $scope.currMon, year: $scope.currYear })
                        .$promise
                        .then(function (masterwbs) {
                            nd.MasterWBS = masterwbs;

                            ReportService.getMonthMasterContract({ month: monthNumeric($scope.currMon), year: $scope.currYear })
                            .$promise
                            .then(function (mastercontract) {
                                nd.MasterContract = mastercontract;
                                nd.month = $scope.currMon;
                                nd.year = $scope.currYear;

                                //!! mod cost report grid
                                alterCostGridData(nd, $scope.costReport);
                                //!! mod revenue report grid
                                alterRevenueGridData(nd, $scope.revenueReport);
                                $scope.recompute();
                                //$timeout(function () { costforecast(); }, 3000);
                                costforecast();
                            });
                        });
                    });
                });
            });
        });
    }

    var filterFTE = function (typ, ftearray) {
        return _.filter(ftearray, function (item) {
            return item.fteType === typ;
        });
    }

    var filterRate = function (typ, ratecard) {
        return _.filter(ratecard, function (item) {
            return item.bsasLevelType === typ;
        });
    }

    var targetCostCust = function (fterecords, type, ratecard) {
        var res = 0;

        if (type === conOnshore) {
            res = targetCostCustRateType(fterecords, filterRate(conOnshore, ratecard), type);
        }
        else if (type === conOffshore) {
            res = targetCostCustRateType(fterecords, filterRate(conOffshore, ratecard), type);
        }

        return res;
    }

    var actualCostCust = function (billingrecords) {
        var res = 0;
        var rate = 0;

        res = _.reduce(billingrecords, function (sum, item) {
            return sum + parseFloat(item.billedAmount);
        }, 0);

        return res;
    }

    var targetCostCustRateType = function (fterecords, rateCard, type) {
        var res = 0;

        _.forEach(fterecords, function (item) {
            var rate = _.find(rateCard, function (rateitem) { return rateitem.bsasLevel === item.fte; });
            if (!_.isUndefined(rate)) {
                if (type === conOnshore) {
                    res += item.fteValue * rate.hourlyRateUSD * 21 * 8;
                }
                else if (type === conOffshore) {
                    res += item.fteValue * rate.hourlyRateUSD * 21 * 8.5;
                }
            }
        });

        return res;
    }

    var getWBSFor = function (customer) {
        return _.filter(wbsList, function (item) {
            return item.accountName === customer;
        });
    }

    var getFTEFor = function (fterecords, customer) {
        return _.filter(fterecords, function (item) {
            return item.accountName === customer;
        });
    }

    var revenueReport = function (ftearray, monthBillingCost, monthContractCost) {
        var custRevAry = [];
        //!! fetch revenue accounts
        //var revenueAccounts = _.filter(accounts, function (item) { return item.accountType === 'Revenue'; });
        //!! loop by customer
        _.forEach(accounts, function (customer) {
            var targetCost = 0;
            var actualCost = 0;
            var variance = 0;

            var custContractCost = 0;
            var custActualCost = 0;
            var revenue = 0;
            var additionalCost = 0;
            var overallActualCost = 0;
            var margin = 0;

            if (!_.isNull(ftearray)) {
                //!! get revenue for account
                var revobj = _.find(ftearray, function (item) {
                    return (item.fte === 'Revenue' &&
                        item.accountName === customer.accountName);
                });
                if (!_.isUndefined(revobj)) {
                    revenue = revobj.fteValue;
                }

                //!! get additional cost
                var addobj = _.find(ftearray, function (item) {
                    return (item.fte === 'AdditionalCost' &&
                        item.accountName === customer.accountName);
                });
                if (!_.isUndefined(addobj)) {
                    additionalCost = addobj.fteValue;
                }
            }

            if (!_.isNull(monthBillingCost)) {
                custActualCost = _.find(monthBillingCost, function (item) { return item.accountName === customer.accountName; });
            }

            if (!_.isNull(monthContractCost)) {
                //!! contractcost and billing amount for customer
                custContractCost = _.find(monthContractCost, function (item) { return item.accountName === customer.accountName; });
            }

            if (!_.isUndefined(custContractCost)) {
                actualCost += custContractCost.total;
            }

            if (!_.isUndefined(custActualCost)) {
                actualCost += custActualCost.total;
            }

            overallActualCost = actualCost + additionalCost;
            if (revenue > 0) {
                margin = ((revenue - overallActualCost) / revenue) * 100;
            }

            margin = margin.toFixed(1);
            actualCost = actualCost.toFixed(2);
            overallActualCost = overallActualCost.toFixed(2);

            var record = { 'customer': customer, 'revenue': revenue, 'actualCost': actualCost, 'overallactual': overallActualCost, 'margin': margin };
            custRevAry.push(record);
        });

        return custRevAry;
    }

    var costReport = function (ftearray, ratecard, monthBillingCost, monthContractCost) {
        //var costAccounts = _.filter(accounts, function (item) { return item.accountType === 'Cost'; });
        var custCostAry = [];
        _.forEach(accounts, function (customer) {
            var targetCost = 0;
            var actualCost = 0;
            var variance = 0;

            var custContractCost = 0;
            var custActualCost = 0;
            var custAdditionalExpense = 0;

            //var custWBSList = getWBSFor(customer);

            if (!_.isNull(ftearray) &&
                !_.isNull(ratecard)) {
                //!! get revenue for account
                var expobj = _.find(ftearray, function (item) {
                    return (item.fte === 'AdditionalExpenses' &&
                        item.accountName === customer.accountName);
                });
                if (!_.isUndefined(expobj)) {
                    custAdditionalExpense = expobj.fteValue;
                }

                var onshoreFTE = filterFTE(conOnshore, ftearray);
                var offshoreFTE = filterFTE(conOffshore, ftearray);

                targetCost = targetCostCust(getFTEFor(onshoreFTE, customer.accountName), conOnshore, ratecard);
                targetCost += targetCostCust(getFTEFor(offshoreFTE, customer.accountName), conOffshore, ratecard);
                targetCost += custAdditionalExpense;
                targetCost = targetCost.toFixed(2);
            }

            if (!_.isNull(monthBillingCost)) {
                custActualCost = _.find(monthBillingCost, function (item) { return item.accountName === customer.accountName; });
            }

            if (!_.isNull(monthContractCost)) {
                custContractCost = _.find(monthContractCost, function (item) { return item.accountName === customer.accountName; });
            }

            if (!_.isUndefined(custActualCost)) {
                actualCost += custActualCost.total;
            }

            if (!_.isUndefined(custContractCost)) {
                actualCost += custContractCost.total;
            }

            if (targetCost > 0) {
                variance = ((targetCost - actualCost) / targetCost) * 100;
            }
            actualCost = actualCost.toFixed(2);
            variance = variance.toFixed(1);


            var record = { 'customer': customer, 'targetCost': targetCost, 'actualCost': actualCost, 'variance': variance };
            custCostAry.push(record);
        });

        return custCostAry;
    }

    var cummulativeRevenueReport = function () {
        var cummrevarray = [];

        _.forEach(accounts, function (loopAccount) {
            var summrev = 0;
            var summoverall = 0;
            var summMargin = 0;

            _.forEach($scope.CalendarMonths, function (loopMonth) {
                var monthRevenue = 0;
                var monthAdditionalCost = 0;
                var monthActual = 0;
                var monthOverallActual = 0;
                var monthContract = 0;
                var monthBilling = 0;

                if (!_.isUndefined(yearlyFTE)) {
                    //!! get revenue for customer month
                    var revenuerecord = _.find(yearlyFTE, function (item) {
                        return (item.fte === 'Revenue' &&
                            item.month.toLowerCase() === loopMonth.toLowerCase() &&
                            item.accountName === loopAccount.accountName);
                    });

                    if (!_.isUndefined(revenuerecord)) {
                        monthRevenue = revenuerecord.fteValue;
                    }

                    //!! get additional cost for customer month
                    var addobj = _.find(yearlyFTE, function (item) {
                        return (item.fte === 'AdditionalCost' &&
                            item.month.toLowerCase() === loopMonth.toLowerCase() &&
                            item.accountName === loopAccount.accountName);
                    });

                    if (!_.isUndefined(addobj)) {
                        monthAdditionalCost = addobj.fteValue;
                    }
                }

                if (!_.isUndefined(yearlyBilling)) {
                    var billingobj = _.find(yearlyBilling, function (item) {
                        return (item.accountName === loopAccount.accountName &&
                            item.month.toLowerCase() === loopMonth.toLowerCase());
                    });

                    if (!_.isUndefined(billingobj)) {
                        monthBilling = billingobj.total;
                    }
                }

                if (!_.isUndefined(yearlyContract)) {
                    var contractobj = _.find(yearlyContract, function (item) {
                        return (item.accountName === loopAccount.accountName &&
                            item.month === monthNumeric(loopMonth));
                    });

                    if (!_.isUndefined(contractobj)) {
                        monthContract = contractobj.total;
                    }
                }

                monthActual = monthBilling + monthContract;
                monthOverallActual = monthActual + monthAdditionalCost;

                summrev += monthRevenue;
                summoverall += monthOverallActual;
            });

            if (summrev > 0) {
                summMargin = ((summrev - summoverall) / summrev) * 100;
            }

            summrev = summrev.toFixed(2);
            summoverall = summoverall.toFixed(2);
            summMargin = summMargin.toFixed(1);

            cummrevarray.push({ 'account': loopAccount, 'revenue': summrev, 'overallactual': summoverall, 'margin': summMargin });
        });

        $scope.cumRevenueReport = cummrevarray;
        $scope.recompute();
    }

    var cummulativeCostReport = function () {
        var cummcostarray = [];

        _.forEach(accounts, function (loopAccount) {
            var summtotal = 0;
            var summactual = 0;
            var summvariance = 0;

            _.forEach($scope.CalendarMonths, function (loopMonth) {
                var monthTotal = 0;
                var monthAdditionalCost = 0;
                var monthActual = 0;
                var monthOverallActual = 0;
                var monthContract = 0;
                var monthBilling = 0;
                var monthAdditionalExpense = 0;

                var expobj = _.find(yearlyFTE, function (item) {
                    return (item.fte === 'AdditionalExpenses' &&
                        item.accountName === loopAccount.accountName &&
                        item.month.toLowerCase() === loopMonth.toLowerCase());
                });
                if (!_.isUndefined(expobj)) {
                    monthAdditionalExpense = expobj.fteValue;
                }

                var onshoreFTE = filterCustomerFTE(conOnshore, loopAccount.accountName, loopMonth);
                var offshoreFTE = filterCustomerFTE(conOffshore, loopAccount.accountName, loopMonth);

                monthTotal = targetCostCustRateType(onshoreFTE, filterCustomerRate(conOnshore, loopMonth), conOnshore);
                monthTotal += targetCostCustRateType(offshoreFTE, filterCustomerRate(conOffshore, loopMonth), conOffshore);
                monthTotal += monthAdditionalExpense;

                if (!_.isUndefined(yearlyBilling)) {
                    var billingobj = _.find(yearlyBilling, function (item) {
                        return (item.accountName === loopAccount.accountName &&
                            item.month.toLowerCase() === loopMonth.toLowerCase());
                    });

                    if (!_.isUndefined(billingobj)) {
                        monthBilling = billingobj.total;
                    }
                }

                if (!_.isUndefined(yearlyContract)) {
                    var contractobj = _.find(yearlyContract, function (item) {
                        return (item.accountName === loopAccount.accountName &&
                            item.month === monthNumeric(loopMonth));
                    });

                    if (!_.isUndefined(contractobj)) {
                        monthContract = contractobj.total;
                    }
                }

                monthActual = monthBilling + monthContract;

                summactual += monthActual;
                summtotal += monthTotal;
            });

            if (summtotal > 0) {
                summvariance = ((summtotal - summactual) / summtotal) * 100;
            }

            summtotal = summtotal.toFixed(2);
            summactual = summactual.toFixed(2);
            summvariance = summvariance.toFixed(1);

            cummcostarray.push({ 'account': loopAccount, 'total': summtotal, 'actual': summactual, 'variance': summvariance });
        });

        $scope.cumCostReport = cummcostarray;
        $scope.recompute();
    }
    //endregion

    //region charts
    var filterCustomerFTE = function (typ, customer, month) {
        return _.filter(yearlyFTE, function (item) {
            return (item.fteType === typ &&
                item.accountName === customer &&
                item.month.toLowerCase() === month.toLowerCase());
        });
    }

    var filterCustomerRate = function (typ, month) {
        return _.filter(yearlyRateCard, function (item) {
            return (item.bsasLevelType === typ &&
                item.month.toLowerCase() === month.toLowerCase());
        });
    }

    var monthNumeric = function (mon) {
        switch (mon) {
            case 'Jan': return 1;
            case 'Feb': return 2;
            case 'Mar': return 3;
            case 'Apr': return 4;
            case 'May': return 5;
            case 'Jun': return 6;
            case 'Jul': return 7;
            case 'Aug': return 8;
            case 'Sep': return 9;
            case 'Oct': return 10;
            case 'Nov': return 11;
            case 'Dec': return 12;
        }
    }

    var overallRevenueChart = function (chartjson) {
        var seriesAccount = '';
        var seriesMonth = '';
        var seriesRevenue = 0;
        var seriesOverall = 0;
        var revarray = [];
        var overallarray = [];

        _.forEach($scope.CalendarMonths, function (monthitem) {
            seriesRevenue = 0;
            seriesOverall = 0;

            var revobj = _.find(chartjson, function (item) {
                return item.month === monthitem;
            });

            if (!_.isUndefined(revobj)) {
                seriesRevenue = revobj.revenue;
                seriesOverall = revobj.overall;
            }

            revarray.push({ x: monthitem, y: seriesRevenue });
            overallarray.push({ x: monthitem, y: seriesOverall });
        });

        var revrec = { key: 'Revenue', values: revarray };
        var overallrec = { key: 'Overall Actual Cost', values: overallarray };

        $scope.overallrevenueyrly.push(revrec);
        $scope.overallrevenueyrly.push(overallrec);
    }

    var overallCostChart = function (chartjson) {
        var seriesAccount = '';
        var seriesMonth = '';
        var seriesTotal = 0;
        var seriesActual = 0;
        var totalarray = [];
        var actualarray = [];

        _.forEach($scope.CalendarMonths, function (monthitem) {
            seriesTotal = 0;
            seriesActual = 0;

            var totalobj = _.find(chartjson, function (item) {
                return item.month === monthitem;
            });

            if (!_.isUndefined(totalobj)) {
                seriesTotal = totalobj.total;
                seriesActual = totalobj.actual;
            }

            totalarray.push({ x: monthitem, y: seriesTotal });
            actualarray.push({ x: monthitem, y: seriesActual });
        });

        var totalrec = { key: 'Target POS', values: totalarray };
        var actualrec = { key: 'Overall Actual Cost', values: actualarray };

        $scope.overallcostyrly.push(totalrec);
        $scope.overallcostyrly.push(actualrec);
    }

    var yearlyRevenueChart = function () {
        var yearRev = [];
        var yearRevOverall = [];
        var summrev = 0;
        var summoverall = 0;

        //!! loop by Months
        _.forEach($scope.CalendarMonths, function (loopMonth) {
            summrev = 0;
            summoverall = 0;

            _.forEach(accounts, function (loopAccount) {
                var monthRevenue = 0;
                var monthAdditionalCost = 0;
                var monthActual = 0;
                var monthOverallActual = 0;
                var monthContract = 0;
                var monthBilling = 0;

                if (!_.isUndefined(yearlyFTE)) {
                    //!! get revenue for customer month
                    var revenuerecord = _.find(yearlyFTE, function (item) {
                        return (item.fte === 'Revenue' &&
                            item.month.toLowerCase() === loopMonth.toLowerCase() &&
                            item.accountName === loopAccount.accountName);
                    });

                    if (!_.isUndefined(revenuerecord)) {
                        monthRevenue = revenuerecord.fteValue;
                    }

                    //!! get additional cost for customer month
                    var addobj = _.find(yearlyFTE, function (item) {
                        return (item.fte === 'AdditionalCost' &&
                            item.month.toLowerCase() === loopMonth.toLowerCase() &&
                            item.accountName === loopAccount.accountName);
                    });

                    if (!_.isUndefined(addobj)) {
                        monthAdditionalCost = addobj.fteValue;
                    }
                }

                if (!_.isUndefined(yearlyBilling)) {
                    var billingobj = _.find(yearlyBilling, function (item) {
                        return (item.accountName === loopAccount.accountName &&
                            item.month.toLowerCase() === loopMonth.toLowerCase());
                    });

                    if (!_.isUndefined(billingobj)) {
                        monthBilling = billingobj.total;
                    }
                }

                if (!_.isUndefined(yearlyContract)) {
                    var contractobj = _.find(yearlyContract, function (item) {
                        return (item.accountName === loopAccount.accountName &&
                            item.month === monthNumeric(loopMonth));
                    });

                    if (!_.isUndefined(contractobj)) {
                        monthContract = contractobj.total;
                    }
                }

                monthActual = monthBilling + monthContract;
                monthOverallActual = monthActual + monthAdditionalCost;

                summrev += monthRevenue;
                summoverall += monthOverallActual;

                var rec = { 'account': loopAccount.accountName, 'month': loopMonth, 'revenue': monthRevenue, 'overall': monthOverallActual };
                yearRev.push(rec);
            });

            var overallrec = { 'month': loopMonth, 'revenue': summrev, 'overall': summoverall };
            yearRevOverall.push(overallrec);
        });
        //!! overall chart load
        overallRevenueChart(yearRevOverall);

        //!! revenue chart
        var series = _.uniq(_.map(yearRev, 'account'));

        _.forEach(series, function (accountitem) {
            var seriesAccount = '';
            var seriesMonth = '';
            var seriesRevenue = 0;
            var seriesOverall = 0;
            var revarray = [];
            var overallarray = [];

            _.forEach($scope.CalendarMonths, function (monthitem) {
                seriesRevenue = 0;
                seriesOverall = 0;

                var revobj = _.find(yearRev, function (item) {
                    return (item.month === monthitem &&
                        item.account === accountitem);
                });

                if (!_.isUndefined(revobj)) {
                    seriesRevenue = revobj.revenue;
                    seriesOverall = revobj.overall;
                }

                revarray.push({ x: monthitem, y: seriesRevenue });
                overallarray.push({ x: monthitem, y: seriesOverall });
            });

            var revrec = { key: accountitem + ' - Revenue', values: revarray };
            var overallrec = { key: accountitem + '- Overall Actual Cost', values: overallarray };

            $scope.revenueyrly.push(revrec);
            $scope.revenueyrly.push(overallrec);
        });
    }

    var yearlyCostChart = function () {
        var yearCost = [];
        var yearCostOverall = [];
        var summtotal = 0;
        var summactual = 0;

        //!! loop by Months
        _.forEach($scope.CalendarMonths, function (loopMonth) {
            summactual = 0;
            summtotal = 0;

            _.forEach(accounts, function (loopAccount) {
                var monthTotal = 0;
                var monthAdditionalCost = 0;
                var monthActual = 0;
                var monthOverallActual = 0;
                var monthContract = 0;
                var monthBilling = 0;
                var monthAdditionalExpense = 0;

                var expobj = _.find(yearlyFTE, function (item) {
                    return (item.fte === 'AdditionalExpenses' &&
                        item.accountName === loopAccount.accountName &&
                        item.month.toLowerCase() === loopMonth.toLowerCase());
                });
                if (!_.isUndefined(expobj)) {
                    monthAdditionalExpense = expobj.fteValue;
                }

                var onshoreFTE = filterCustomerFTE(conOnshore, loopAccount.accountName, loopMonth);
                var offshoreFTE = filterCustomerFTE(conOffshore, loopAccount.accountName, loopMonth);

                monthTotal = targetCostCustRateType(onshoreFTE, filterCustomerRate(conOnshore, loopMonth), conOnshore);
                monthTotal += targetCostCustRateType(offshoreFTE, filterCustomerRate(conOffshore, loopMonth), conOffshore);
                monthTotal += monthAdditionalExpense;

                if (!_.isUndefined(yearlyBilling)) {
                    var billingobj = _.find(yearlyBilling, function (item) {
                        return (item.accountName === loopAccount.accountName &&
                            item.month.toLowerCase() === loopMonth.toLowerCase());
                    });

                    if (!_.isUndefined(billingobj)) {
                        monthBilling = billingobj.total;
                    }
                }

                if (!_.isUndefined(yearlyContract)) {
                    var contractobj = _.find(yearlyContract, function (item) {
                        return (item.accountName === loopAccount.accountName &&
                            item.month === monthNumeric(loopMonth));
                    });

                    if (!_.isUndefined(contractobj)) {
                        monthContract = contractobj.total;
                    }
                }

                monthActual = monthBilling + monthContract;

                summactual += monthActual;
                summtotal += monthTotal;

                var rec = { 'account': loopAccount.accountName, 'month': loopMonth, 'total': monthTotal, 'actual': monthActual };
                yearCost.push(rec);
            });

            var overallrec = { 'month': loopMonth, 'total': summtotal, 'actual': summactual };
            yearCostOverall.push(overallrec);
        });

        overallCostChart(yearCostOverall);

        var series = _.uniq(_.map(yearCost, 'account'));

        _.forEach(series, function (accountitem) {
            var seriesAccount = '';
            var seriesMonth = '';
            var seriesTotal = 0;
            var seriesActual = 0;
            var totalarray = [];
            var actualarray = [];

            _.forEach($scope.CalendarMonths, function (monthitem) {
                seriesTotal = 0;
                seriesActual = 0;

                var totalobj = _.find(yearCost, function (item) {
                    return (item.month === monthitem &&
                        item.account === accountitem);
                });

                if (!_.isUndefined(totalobj)) {
                    seriesTotal = totalobj.total;
                    seriesActual = totalobj.actual;
                }

                totalarray.push({ x: monthitem, y: seriesTotal });
                actualarray.push({ x: monthitem, y: seriesActual });
            });

            var totalrec = { key: accountitem + ' - Target POS', values: totalarray };
            var actualrec = { key: accountitem + '- Overall Actual Cost', values: actualarray };

            $scope.costyrly.push(totalrec);
            $scope.costyrly.push(actualrec);
        });
    }

    var graphLoad = function () {
        ReportService.getYearlyFTE({ year: $scope.currYear })
        .$promise
        .then(function (fte) {
            yearlyFTE = fte;

            ReportService.getYearlyBillingCost({ year: $scope.currYear })
            .$promise
            .then(function (billing) {
                yearlyBilling = billing;

                ReportService.getYearlyContractCost({ year: $scope.currYear })
                .$promise
                .then(function (contract) {
                    yearlyContract = contract;

                    cummulativeRevenueReport();
                    cummulativeCostReport();

                    yearlyRevenueChart();
                    yearlyCostChart();
                });
            });
        });

        ReportService.getYearlyRateCard({ year: $scope.currYear })
        .$promise
        .then(function (ratecard) {
            yearlyRateCard = ratecard;
        });
    }
    //endregion

    //region events
    $scope.monthChanged = function (monthItem) {
        gridLoad();
    };

    $scope.yearChanged = function (yearItem) {
        var ite = yearItem;
        gridLoad();
    };

    $scope.costAccountSelected = function (account) {
        MonthlyAccountService.setAccountDetails({
            monthlyFTE: monthlyFTE,
            billingCost: billingCost,
            contractCost: contractCost,
            allAccounts: accounts,
            allWBS: allWBS,
            account: account,
            month: $scope.currMon,
            year: $scope.currYear
        });
        $state.go('report.details', { account: account.accountName });
    };

    $scope.recompute = function () {
        var cummCostTarget = _.reduce($scope.cumCostReport, function (cummCostTarget, item) {
            if (item.total === '') { return cummCostTarget + 0; }
            return cummCostTarget + parseFloat(item.total);
        }, 0);

        var cummCostActual = _.reduce($scope.cumCostReport, function (cummCostActual, item) {
            if (item.actual === '') { return cummCostActual + 0; }
            return cummCostActual + parseFloat(item.actual);
        }, 0);

        var cummRevenueRev = _.reduce($scope.cumRevenueReport, function (cummRevenueRev, item) {
            if (item.revenue === '') { return cummRevenueRev + 0; }
            return cummRevenueRev + parseFloat(item.revenue);
        }, 0);

        var cummRevActual = _.reduce($scope.cumRevenueReport, function (cummRevActual, item) {
            if (item.overallactual === '') { return cummRevActual + 0; }
            return cummRevActual + parseFloat(item.overallactual);
        }, 0);

        var costTarget = _.reduce($scope.costReport, function (costTarget, item) {
            if (item.targetCost === '') { return costTarget + 0; }
            return costTarget + parseFloat(item.targetCost);
        }, 0);

        var costActual = _.reduce($scope.costReport, function (costActual, item) {
            if (item.actualCost === '') { return costActual + 0; }
            return costActual + parseFloat(item.actualCost);
        }, 0);

        var ndcostTarget = _.reduce($scope.costReport, function (ndcostTarget, item) {
            if (item.ndtargetCost === '') { return ndcostTarget + 0; }
            return ndcostTarget + parseFloat(item.ndtargetCost);
        }, 0);

        var ndcostActual = _.reduce($scope.costReport, function (ndcostActual, item) {
            if (item.ndactualCost === '') { return ndcostActual + 0; }
            return ndcostActual + parseFloat(item.ndactualCost);
        }, 0);

        var revRevenue = _.reduce($scope.revenueReport, function (revRevenue, item) {
            if (item.revenue === '') { return revRevenue + 0; }
            return revRevenue + parseFloat(item.revenue);
        }, 0);

        var revActual = _.reduce($scope.revenueReport, function (revActual, item) {
            if (item.overallactual === '') { return revActual + 0; }
            return revActual + parseFloat(item.overallactual);
        }, 0);

        var revNDBaseline = _.reduce($scope.revenueReport, function (revNDBaseline, item) {
            if (item.ndbaseline === '') { return revNDBaseline + 0; }
            return revNDBaseline + parseFloat(item.ndbaseline);
        }, 0);

        var revNDActual = _.reduce($scope.revenueReport, function (revNDActual, item) {
            if (item.ndactual === '') { return revNDActual + 0; }
            return revNDActual + parseFloat(item.ndactual);
        }, 0);

        $scope.totals.cummCostTarget = cummCostTarget;
        $scope.totals.cummCostActual = cummCostActual;
        if (cummCostTarget > 0) {
            $scope.totals.cummCostVariance = ((cummCostTarget - cummCostActual) / cummCostTarget) * 100;
            $scope.totals.cummCostVariance = $scope.totals.cummCostVariance.toFixed(1);
        }

        $scope.totals.cummRevenueRev = cummRevenueRev;
        $scope.totals.cummRevActual = cummRevActual;
        if (cummRevenueRev > 0) {
            $scope.totals.cummRevMargin = ((cummRevenueRev - cummRevActual) / cummRevenueRev) * 100;
            $scope.totals.cummRevMargin = $scope.totals.cummRevMargin.toFixed(1);
        }

        $scope.totals.costTarget = costTarget;
        $scope.totals.costActual = costActual;
        if (costTarget > 0) {
            $scope.totals.costVariance = ((costTarget - costActual) / costTarget) * 100;
            $scope.totals.costVariance = $scope.totals.costVariance.toFixed(1);
        }

        $scope.totals.ndcostTarget = ndcostTarget;
        $scope.totals.ndcostActual = ndcostActual;
        if (ndcostTarget > 0) {
            $scope.totals.ndcostVariance = ((ndcostTarget - ndcostActual) / ndcostTarget) * 100;
            $scope.totals.ndcostVariance = $scope.totals.ndcostVariance.toFixed(1);
        }

        $scope.totals.revRevenue = revRevenue;
        $scope.totals.revActual = revActual;
        if (revRevenue > 0) {
            $scope.totals.revMargin = ((revRevenue - revActual) / revRevenue) * 100;
            $scope.totals.revMargin = $scope.totals.revMargin.toFixed(1);
        }

        $scope.totals.revNDBaseline = revNDBaseline;
        $scope.totals.revNDActual = revNDActual;
        if (revNDBaseline > 0) {
            $scope.totals.revNDVariance = ((revNDBaseline - revNDActual) / revNDBaseline) * 100;
            $scope.totals.revNDVariance = $scope.totals.revNDVariance.toFixed(1);
        }
    };
    //endregion

    //region other
    var yearlist = function () {
        var year = (new Date().getFullYear()) - 5;
        $scope.calendarYears.push(year);
        for (var i = 1; i < 20; i++) {
            $scope.calendarYears.push(year + i);
        }
    };
    yearlist();

    $scope.options = ChartService.getMultiBarCustom({
        top: 20,
        right: 20,
        bottom: 60,
        left: 80
    }, false, false, true, false);
    $scope.overalloptions = ChartService.getMultiBarCustom({
        top: 20,
        right: 20,
        bottom: 60,
        left: 80
    }, false, true, true, false);
    //endregion

    //region overall
    var allWBS = [];
    var allaccounts = [];
    var availablemonths = [];
    var alljoblevels = [];
    var allservicelines = [];
    var currentDateObj = new Date();
    var finMonthYear = [];//['Nov 2015', 'Dec 2015', 'Jan 2016', 'Feb 2016', 'Mar 2016', 'Apr 2016', 'May 2016', 'Jun 2016', 'Jul 2016', 'Aug 2016', 'Sep 2016', 'Oct 2016'];
    var tempMonthYear = ['Apr 2015', 'May 2015', 'Jun 2015', 'Jul 2015', 'Aug 2015', 'Sep 2015', 'Oct 2015', 'Nov 2015', 'Dec 2015', 'Jan 2016', 'Feb 2016', 'Mar 2016'];

    var defaultPyramidTypes = AppDefault.pyramidJobLevel;
    var chartTypes = AppDefault.chartTypes;
    var serviceLineManagers = AppDefault.serviceLineManagers;
    var localpyramidservicelineoverall = {};

   

    //region chartload
    var pyramidCalculate = function (recs) {
        var percent = 0;

        var servicerecsA = _.filter(recs, function (item) {
            return _.includes(defaultPyramidTypes, item.jobLevel);
        });

        var servicesumA = 0;

        if (!_.isUndefined(servicerecsA)) {
            servicesumA = _.reduce(servicerecsA, function (servicesumA, item) {
                if (item.billedAmount === '') { return servicesumA + 0; }
                return servicesumA + parseFloat(item.fte);
            }, 0);
        }

        var servicesumB = 0;

        servicesumB = _.reduce(recs, function (servicesumB, item) {
            if (item.billedAmount === '') { return servicesumB + 0; }
            return servicesumB + parseFloat(item.fte);
        }, 0);

        if (servicesumA > 0) {
            percent = parseFloat(((servicesumA / servicesumB) * 100).toFixed(1));
        }

        return percent;
    }

    var pyramidServiceLineChart = function (
        admrecs,
        scsrecs,
        saprecs,
        testingrecs,
        oraclerecs,
        hpitrecs,
        aadmrecs,
        fsirecs
        ) {
        var servicesarray = {};

        servicesarray.ADM = parseFloat(pyramidCalculate(admrecs));
        servicesarray.SCS = parseFloat(pyramidCalculate(scsrecs));
        servicesarray.SAP = parseFloat(pyramidCalculate(saprecs));
        servicesarray.Testing = parseFloat(pyramidCalculate(testingrecs));
        servicesarray.Oracle = parseFloat(pyramidCalculate(oraclerecs));
        servicesarray.HPIT = parseFloat(pyramidCalculate(hpitrecs));
        servicesarray.AADM = parseFloat(pyramidCalculate(aadmrecs));
        servicesarray.FSI = parseFloat(pyramidCalculate(fsirecs));

        return servicesarray;
    }

    var customPyramidServiceLine = function (wbs) {
        //!! ADM
        var admrecs = _.filter(wbs, function (admitem) {
            return (_.includes(serviceLineManagers.ADM, admitem.managerL6) &&
                !_.includes(serviceLineManagers.SAPMRU, admitem.mruL4));
        });

        //!! SCS
        var scsrecs = _.filter(wbs, function (scsitem) {
            return _.includes(serviceLineManagers.SCS, scsitem.managerL6);
        });

        //!! SAP
        var saprecs = _.filter(wbs, function (sapitem) {
            return ((_.includes(serviceLineManagers.SAP, sapitem.managerL6) &&
                _.includes(serviceLineManagers.SAPMRU, sapitem.mruL4)) ||
                _.includes(serviceLineManagers.SIS, sapitem.managerL6));
        });

        //!! Testing
        var testingrecs = _.filter(wbs, function (testingitem) {
            return _.includes(serviceLineManagers.Testing, testingitem.managerL6);
        });

        //!! Oracle
        var oraclerecs = _.filter(wbs, function (oracleitem) {
            return _.includes(serviceLineManagers.Oracle, oracleitem.managerL6);
        });

        //!! HPIT
        var hpitrecs = _.filter(wbs, function (hpititem) {
            return _.includes(serviceLineManagers.HPIT, hpititem.managerL6);
        });

        //!! AADM
        var aadmrecs = _.filter(wbs, function (aadmitem) {
            return _.includes(serviceLineManagers.AADM, aadmitem.managerL6);
        });

        //!! FSI
        var fsirecs = _.filter(wbs, function (fsiitem) {
            return _.includes(serviceLineManagers.FSI, fsiitem.managerL6);
        });

        return pyramidServiceLineChart(
                                            admrecs,
                                            scsrecs,
                                            saprecs,
                                            testingrecs,
                                            oraclerecs,
                                            hpitrecs,
                                            aadmrecs,
                                            fsirecs);
    }

    var customServiceLine = function (wbs, isYearChange) {
        var sumADM = 0;
        var sumSCS = 0;
        var sumSAP = 0;
        var sumTesting = 0;
        var sumOracle = 0;
        var sumHPIT = 0;
        var sumAADM = 0;
        var sumFSI = 0;
        var sumSIS = 0;
        var servicesarray = {};

        //!! ADM
        var admrecs = _.filter(wbs, function (admitem) {
            return (_.includes(serviceLineManagers.ADM, admitem.managerL6) &&
                !_.includes(serviceLineManagers.SAPMRU, admitem.mruL4));
        });

        if (!_.isUndefined(admrecs)) {
            sumADM = _.reduce(admrecs, function (sumADM, item) {
                if (item.billedAmount === '') { return sumADM + 0; }
                return sumADM + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! SCS
        var scsrecs = _.filter(wbs, function (scsitem) {
            return _.includes(serviceLineManagers.SCS, scsitem.managerL6);
        });

        if (!_.isUndefined(scsrecs)) {
            sumSCS = _.reduce(scsrecs, function (sumSCS, item) {
                if (item.billedAmount === '') { return sumSCS + 0; }
                return sumSCS + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! SAP
        var saaprecs = _.filter(wbs, function (sapitem) {
            return _.includes(serviceLineManagers.SIS, sapitem.managerL6);
        });

        var saprecs = _.filter(wbs, function (sapitem) {
            return ((_.includes(serviceLineManagers.SAP, sapitem.managerL6) &&
                _.includes(serviceLineManagers.SAPMRU, sapitem.mruL4)) ||
                _.includes(serviceLineManagers.SIS, sapitem.managerL6));
        });

        if (!_.isUndefined(saprecs)) {
            sumSAP = _.reduce(saprecs, function (sumSAP, item) {
                if (item.billedAmount === '') { return sumSAP + 0; }
                return sumSAP + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! Testing
        var testingrecs = _.filter(wbs, function (testingitem) {
            return _.includes(serviceLineManagers.Testing, testingitem.managerL6);
        });

        if (!_.isUndefined(testingrecs)) {
            sumTesting = _.reduce(testingrecs, function (sumTesting, item) {
                if (item.billedAmount === '') { return sumTesting + 0; }
                return sumTesting + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! Oracle
        var oraclerecs = _.filter(wbs, function (oracleitem) {
            return _.includes(serviceLineManagers.Oracle, oracleitem.managerL6);
        });

        if (!_.isUndefined(oraclerecs)) {
            sumOracle = _.reduce(oraclerecs, function (sumOracle, item) {
                if (item.billedAmount === '') { return sumOracle + 0; }
                return sumOracle + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! HPIT
        var hpitrecs = _.filter(wbs, function (hpititem) {
            return _.includes(serviceLineManagers.HPIT, hpititem.managerL6);
        });

        if (!_.isUndefined(hpitrecs)) {
            sumHPIT = _.reduce(hpitrecs, function (sumHPIT, item) {
                if (item.billedAmount === '') { return sumHPIT + 0; }
                return sumHPIT + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! AADM
        var aadmrecs = _.filter(wbs, function (aadmitem) {
            return _.includes(serviceLineManagers.AADM, aadmitem.managerL6);
        });

        if (!_.isUndefined(aadmrecs)) {
            sumAADM = _.reduce(aadmrecs, function (sumAADM, item) {
                if (item.billedAmount === '') { return sumAADM + 0; }
                return sumAADM + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! FSI
        var fsirecs = _.filter(wbs, function (fsiitem) {
            return _.includes(serviceLineManagers.FSI, fsiitem.managerL6);
        });

        if (!_.isUndefined(fsirecs)) {
            sumFSI = _.reduce(fsirecs, function (sumFSI, item) {
                if (item.billedAmount === '') { return sumFSI + 0; }
                return sumFSI + parseFloat(item.billedAmount);
            }, 0);
        }

        if (!isYearChange) {
            localpyramidservicelineoverall = pyramidServiceLineChart(
                                            admrecs,
                                            scsrecs,
                                            saprecs,
                                            testingrecs,
                                            oraclerecs,
                                            hpitrecs,
                                            aadmrecs,
                                            fsirecs);
        }

        servicesarray.ADM = parseFloat(sumADM.toFixed(2));
        servicesarray.SCS = parseFloat(sumSCS.toFixed(2));
        servicesarray.SAP = parseFloat(sumSAP.toFixed(2));
        servicesarray.Testing = parseFloat(sumTesting.toFixed(2));
        servicesarray.Oracle = parseFloat(sumOracle.toFixed(2));
        servicesarray.HPIT = parseFloat(sumHPIT.toFixed(2));
        servicesarray.AADM = parseFloat(sumAADM.toFixed(2));
        servicesarray.FSI = parseFloat(sumFSI.toFixed(2));

        servicesarray.HeadCountADM = admrecs.length;
        servicesarray.HeadCountSCS = scsrecs.length;
        servicesarray.HeadCountSAP = saprecs.length;
        servicesarray.HeadCountTesting = testingrecs.length;
        servicesarray.HeadCountOracle = oraclerecs.length;
        servicesarray.HeadCountHPIT = hpitrecs.length;
        servicesarray.HeadCountAADM = aadmrecs.length;
        servicesarray.HeadCountFSI = fsirecs.length;

        return servicesarray;
    }

    var customfinancialYearChartData = function (finyrarry, isYearChange) {
        var servicesarray = [];
        var pyramidarray = [];
        localpyramidservicelineoverall = {};

        _.forEach(finyrarry, function (loopmonth) {
            var monthobj = {};
            var monthpyramidobj = {};

            var servicerecs = _.filter(allWBS, function (item) {
                return item.month === loopmonth;
            });

            monthobj = customServiceLine(servicerecs, isYearChange);
            monthobj.monthyear = loopmonth;
            servicesarray.push(monthobj);

            if (!isYearChange) {
                monthpyramidobj = localpyramidservicelineoverall;
                monthpyramidobj.monthyear = loopmonth;
                pyramidarray.push(monthpyramidobj);
            }
        });

        $scope.overall.chartOptions.serviceLineFinYearCols.push({
            "id": 'ADM',
            "type": $scope.overall.chartOptions.serviceLineFinYearType,
            "name": 'ADM'
        });
        $scope.overall.chartOptions.serviceLineFinYearCols.push({
            "id": 'SCS',
            "type": $scope.overall.chartOptions.serviceLineFinYearType,
            "name": 'SCS'
        });
        $scope.overall.chartOptions.serviceLineFinYearCols.push({
            "id": 'SAP',
            "type": $scope.overall.chartOptions.serviceLineFinYearType,
            "name": 'SAP'
        });
        $scope.overall.chartOptions.serviceLineFinYearCols.push({
            "id": 'Testing',
            "type": $scope.overall.chartOptions.serviceLineFinYearType,
            "name": 'Testing'
        });
        $scope.overall.chartOptions.serviceLineFinYearCols.push({
            "id": 'Oracle',
            "type": $scope.overall.chartOptions.serviceLineFinYearType,
            "name": 'Oracle'
        });
        $scope.overall.chartOptions.serviceLineFinYearCols.push({
            "id": 'HPIT',
            "type": $scope.overall.chartOptions.serviceLineFinYearType,
            "name": 'HP IT'
        });
        $scope.overall.chartOptions.serviceLineFinYearCols.push({
            "id": 'AADM',
            "type": $scope.overall.chartOptions.serviceLineFinYearType,
            "name": 'A&DM'
        });
        $scope.overall.chartOptions.serviceLineFinYearCols.push({
            "id": 'FSI',
            "type": $scope.overall.chartOptions.serviceLineFinYearType,
            "name": 'FSI'
        });

        $scope.overall.chartdata.serviceLineFinYear = servicesarray;
        $scope.overall.chartOptions.serviceLineFinYearDataX = { "id": "monthyear" };

        $scope.overall.chartOptions.pyramidServiceLineFinancialDataX = $scope.overall.chartOptions.serviceLineFinYearDataX;
        $scope.overall.chartOptions.pyramidServiceLineFinancialCols = $scope.overall.chartOptions.serviceLineFinYearCols;

        return pyramidarray;
    }

    var pyramidOverallChartLoad = function (montharry) {
        var servicesarray = [];

        _.forEach(montharry, function (loopmonth) {
            var monthobj = {};

            var servicerecsA = _.filter(allWBS, function (item) {
                return (item.month === loopmonth &&
                    _.includes(defaultPyramidTypes, item.jobLevel)
                    );
            });

            var servicesumA = 0;

            if (!_.isUndefined(servicerecsA)) {
                servicesumA = _.reduce(servicerecsA, function (servicesumA, item) {
                    if (item.billedAmount === '') { return servicesumA + 0; }
                    return servicesumA + parseFloat(item.fte);
                }, 0);
            }

            var servicerecsB = _.filter(allWBS, function (item) {
                return item.month === loopmonth;
            });

            var servicesumB = 0;

            if (!_.isUndefined(servicerecsB)) {
                servicesumB = _.reduce(servicerecsB, function (servicesumB, item) {
                    if (item.billedAmount === '') { return servicesumB + 0; }
                    return servicesumB + parseFloat(item.fte);
                }, 0);
            }

            monthobj.monthyear = loopmonth;
            monthobj.pyramid = 0;
            if (servicesumA > 0) {
                monthobj.pyramid = parseFloat(((servicesumA / servicesumB) * 100).toFixed(1));
            }

            servicesarray.push(monthobj);
        });

        return servicesarray;
    }

    var pyramidServiceLineChartLoad = function (montharry) {
        var servicesarray = [];

        _.forEach(montharry, function (loopmonth) {
            var monthobj = {};

            _.forEach(allservicelines, function (loopservice) {
                //!! exclude service line without name
                if (loopservice !== '-') {
                    var servicerecsA = _.filter(allWBS, function (item) {
                        return (item.month === loopmonth &&
                            item.serviceLine === loopservice &&
                            _.includes(defaultPyramidTypes, item.jobLevel)
                            );
                    });

                    var servicesumA = 0;

                    if (!_.isUndefined(servicerecsA)) {
                        servicesumA = _.reduce(servicerecsA, function (servicesumA, item) {
                            if (item.billedAmount === '') { return servicesumA + 0; }
                            return servicesumA + parseFloat(item.fte);
                        }, 0);
                    }

                    var servicerecsB = _.filter(allWBS, function (item) {
                        return (item.month === loopmonth &&
                            item.serviceLine === loopservice);
                    });

                    var servicesumB = 0;

                    if (!_.isUndefined(servicerecsB)) {
                        servicesumB = _.reduce(servicerecsB, function (servicesumB, item) {
                            if (item.billedAmount === '') { return servicesumB + 0; }
                            return servicesumB + parseFloat(item.fte);
                        }, 0);
                    }
                    monthobj[loopservice] = 0;
                    if (servicesumA > 0) {
                        monthobj[loopservice] = parseFloat(((servicesumA / servicesumB) * 100).toFixed(1));
                    }

                    $scope.overall.chartOptions.pyramidServiceLineFinancialCols.push({
                        "id": loopservice,
                        "type": $scope.overall.chartOptions.pyramidServiceLineOverallType,
                        "name": loopservice
                    });
                }
            });
            monthobj.monthyear = loopmonth;

            servicesarray.push(monthobj);
        });
        $scope.overall.chartOptions.pyramidServiceLineFinancialDataX = { "id": "monthyear" };

        $scope.overall.chartOptions.pyramidServiceLineOverallCols = $scope.overall.chartOptions.pyramidServiceLineFinancialCols;
        $scope.overall.chartOptions.pyramidServiceLineOverallDataX = $scope.overall.chartOptions.pyramidServiceLineFinancialDataX;

        return servicesarray;
    }

    var customPyramidServiceLineChartLoad = function (montharry) {
        var servicesarray = [];

        _.forEach(montharry, function (loopmonth) {
            var monthobj = {};

            var servicerecs = _.filter(allWBS, function (item) {
                return item.month === loopmonth;
            });

            monthobj = customPyramidServiceLine(servicerecs);
            monthobj.monthyear = loopmonth;
            servicesarray.push(monthobj);
        });

        return servicesarray;
    }

    var financialYearChartData = function (finyrarry) {
        var servicesarray = [];

        _.forEach(finyrarry, function (loopmonth) {
            var monthobj = {};
            _.forEach(allservicelines, function (loopservice) {
                if (loopservice !== '-') {
                    var servicerecs = _.filter(allWBS, function (item) {
                        return (item.serviceLine === loopservice &&
                            item.month === loopmonth
                            );
                    });

                    var servicesum = 0;

                    if (!_.isUndefined(servicerecs)) {
                        servicesum = _.reduce(servicerecs, function (servicesum, item) {
                            if (item.billedAmount === '') { return servicesum + 0; }
                            return servicesum + parseFloat(item.billedAmount);
                        }, 0);
                    }

                    $scope.overall.chartOptions.serviceLineFinYearCols.push({
                        "id": loopservice,
                        "type": $scope.overall.chartOptions.serviceLineFinYearType,
                        "name": loopservice
                    });
                    monthobj[loopservice] = parseFloat(servicesum.toFixed(2));
                }
            });
            monthobj['monthyear'] = loopmonth;
            servicesarray.push(monthobj);
        });

        $scope.overall.chartdata.serviceLineFinYear = servicesarray;
        $scope.overall.chartOptions.serviceLineFinYearDataX = { "id": "monthyear" };
    }

    var financialYearArray = function (yr) {
        var monthyeararray = [];
        var tempdate = new Date(yr + '/10/22');

        _.forEach(monthObj, function (item) {
            var month = _.find(monthObj, function (monobj) { return monobj.num === (tempdate.getMonth() + 1); });
            var year = tempdate.getFullYear();
            monthyeararray.push(month.shortMon + ' ' + year);
            tempdate.setDate(1);
            tempdate.setMonth(tempdate.getMonth() - 1);
        });

        return _.reverse(monthyeararray);
    }

    var financialYearChartLoad = function () {
        $scope.overall.dropdown.serviceLineFinancialYear = currentDateObj.getFullYear();
        var currentMonth = currentDateObj.getMonth() + 1;

        if (currentMonth < 11) {
            finMonthYear = financialYearArray($scope.overall.dropdown.serviceLineFinancialYear);
            //financialYearChartData(finMonthYear);
            //!! reused the looping hence pyramid
            $scope.overall.chartdata.pyramidServiceLineFinancial = customfinancialYearChartData(finMonthYear, false);
        }
        else {
            $scope.overall.dropdown.serviceLineFinancialYear += 1;
            finMonthYear = financialYearArray($scope.overall.dropdown.serviceLineFinancialYear);
            //financialYearChartData(finMonthYear);
            $scope.overall.chartdata.pyramidServiceLineFinancial = customfinancialYearChartData(finMonthYear, false);
        }
    }

    var servicelineOverallChartLoad = function () {
        var servicesarray = [];
        //!! TODO: change to below
        //_.forEach(availablemonths, function (loopmonth) {
        _.forEach(tempMonthYear, function (loopmonth) {
            var monthobj = {};
            _.forEach(allservicelines, function (loopservice) {
                if (loopservice !== '-') {
                    var servicerecs = _.filter(allWBS, function (item) {
                        return (item.serviceLine === loopservice &&
                            item.month === loopmonth
                            );
                    });

                    var servicesum = 0;

                    if (!_.isUndefined(servicerecs)) {
                        servicesum = _.reduce(servicerecs, function (servicesum, item) {
                            if (item.billedAmount === '') { return servicesum + 0; }
                            return servicesum + parseFloat(item.billedAmount);
                        }, 0);
                    }
                    monthobj[loopservice] = parseFloat(servicesum.toFixed(2));
                    $scope.overall.chartOptions.serviceLineOverallCols.push({
                        "id": loopservice,
                        "type": $scope.overall.chartOptions.serviceLineOverallType,
                        "name": loopservice
                    });
                }
            });

            monthobj['monthyear'] = loopmonth;
            servicesarray.push(monthobj);
        });

        $scope.overall.chartdata.serviceLineOverall = servicesarray;
        $scope.overall.chartOptions.serviceLineOverallDataX = { "id": "monthyear" };
    }
    //endregion

    //region gridload
    var servicelineOverallGridLoad = function () {
        var servicesarray = [];

        _.forEach(allservicelines, function (loopservice) {
            if (loopservice !== '-') {
                var servicerecs = _.filter(allWBS, function (item) {
                    return item.serviceLine === loopservice;
                });

                var servicesum = 0;

                if (!_.isUndefined(servicerecs)) {
                    servicesum = _.reduce(servicerecs, function (servicesum, item) {
                        if (item.billedAmount === '') { return servicesum + 0; }
                        return servicesum + parseFloat(item.billedAmount);
                    }, 0);
                }

                servicesarray.push({ serviceline: loopservice, sum: parseFloat(servicesum.toFixed(2)) });
            }
        });
        $scope.overall.gridData.serviceLineOverallGrid = servicesarray;
    }

    var customServicelineOverallGridLoad = function () {
        var servicesarray = [];
        var serviceobj = {};

        serviceobj = customServiceLine(allWBS, true);

        if (!_.isUndefined(serviceobj)) {
            servicesarray.push({ serviceline: 'ADM', sum: serviceobj.ADM });
            servicesarray.push({ serviceline: 'SCS', sum: serviceobj.SCS });
            servicesarray.push({ serviceline: 'SAP', sum: serviceobj.SAP });
            servicesarray.push({ serviceline: 'Testing', sum: serviceobj.Testing });
            servicesarray.push({ serviceline: 'Oracle', sum: serviceobj.Oracle });
            servicesarray.push({ serviceline: 'HP IT', sum: serviceobj.HPIT });
            servicesarray.push({ serviceline: 'A&DM', sum: serviceobj.AADM });
            servicesarray.push({ serviceline: 'FSI', sum: serviceobj.FSI });
        }

        $scope.overall.gridData.serviceLineOverallGrid = servicesarray;
    }

    var pyramidServiceLineGridLoad = function () {
        var servicesarray = [];

        _.forEach(allservicelines, function (loopservice) {
            //!! exclude service line without name
            if (loopservice !== '-') {
                var percent = '0%';

                var servicerecsA = _.filter(allWBS, function (item) {
                    return (item.serviceLine === loopservice &&
                        _.includes(defaultPyramidTypes, item.jobLevel)
                        );
                });

                var servicesumA = 0;

                if (!_.isUndefined(servicerecsA)) {
                    servicesumA = _.reduce(servicerecsA, function (servicesumA, item) {
                        if (item.billedAmount === '') { return servicesumA + 0; }
                        return servicesumA + parseFloat(item.fte);
                    }, 0);
                }

                var servicerecsB = _.filter(allWBS, function (item) {
                    return item.serviceLine === loopservice;
                });

                var servicesumB = 0;

                if (!_.isUndefined(servicerecsB)) {
                    servicesumB = _.reduce(servicerecsB, function (servicesumB, item) {
                        if (item.billedAmount === '') { return servicesumB + 0; }
                        return servicesumB + parseFloat(item.fte);
                    }, 0);
                }

                if (servicesumA > 0) {
                    percent = ((servicesumA / servicesumB) * 100).toFixed(1) + '%';
                }

                servicesarray.push({ serviceline: loopservice, sum: percent });
            }
        });

        return servicesarray;
    }

    var customPyramidServiceLineGridLoad = function () {
        var servicesarray = [];

        var serviceobj = {};

        serviceobj = customPyramidServiceLine(allWBS);

        if (!_.isUndefined(serviceobj)) {
            servicesarray.push({ serviceline: 'ADM', sum: serviceobj.ADM });
            servicesarray.push({ serviceline: 'SCS', sum: serviceobj.SCS });
            servicesarray.push({ serviceline: 'SAP', sum: serviceobj.SAP });
            servicesarray.push({ serviceline: 'Testing', sum: serviceobj.Testing });
            servicesarray.push({ serviceline: 'Oracle', sum: serviceobj.Oracle });
            servicesarray.push({ serviceline: 'HP IT', sum: serviceobj.HPIT });
            servicesarray.push({ serviceline: 'A&DM', sum: serviceobj.AADM });
            servicesarray.push({ serviceline: 'FSI', sum: serviceobj.FSI });
        }

        $scope.overall.gridData.pyramidServiceLineOverallGrid = servicesarray;
    }

    var pyramidOverallGridLoad = function () {
        var servicesarray = [];
        var percent = '0%';

        var servicerecsA = _.filter(allWBS, function (item) {
            return _.includes(defaultPyramidTypes, item.jobLevel);
        });

        var servicesumA = 0;

        if (!_.isUndefined(servicerecsA)) {
            servicesumA = _.reduce(servicerecsA, function (servicesumA, item) {
                if (item.billedAmount === '') { return servicesumA + 0; }
                return servicesumA + parseFloat(item.fte);
            }, 0);
        }

        var servicerecsB = allWBS;

        var servicesumB = 0;

        if (!_.isUndefined(servicerecsB)) {
            servicesumB = _.reduce(servicerecsB, function (servicesumB, item) {
                if (item.billedAmount === '') { return servicesumB + 0; }
                return servicesumB + parseFloat(item.fte);
            }, 0);
        }

        if (servicesumA > 0) {
            percent = ((servicesumA / servicesumB) * 100).toFixed(1) + '%';
        }

        servicesarray.push({ serviceline: 'Overall', sum: percent });

        return servicesarray;
    }
    //endregion

    //region events
    $scope.pyramidfinoverallYearChanged = function (yr) {
        $scope.overall.chartdata.pyramidFinancial = [];
        finMonthYear = financialYearArray(yr);
        $scope.overall.chartdata.pyramidFinancial = pyramidOverallChartLoad(finMonthYear);
    };

    $scope.pyramidservicelinefinYearChanged = function (yr) {
        $scope.overall.chartdata.pyramidServiceLineFinancial = [];
        finMonthYear = financialYearArray(yr);
        //$scope.overall.chartdata.pyramidServiceLineFinancial = pyramidServiceLineChartLoad(finMonthYear);
        $scope.overall.chartdata.pyramidServiceLineFinancial = customPyramidServiceLineChartLoad(finMonthYear);
    };

    $scope.finYearChanged = function (yr) {
        $scope.overall.chartdata.serviceLineFinYear = [];
        finMonthYear = financialYearArray(yr);
        //financialYearChartData(finMonthYear);
        customfinancialYearChartData(finMonthYear, true);
    };

    //!! format tooltip to show
    //!! head count for service line
    $scope.hcTooltipFormat = function (value, ratio, id) {
        var coll = $scope.overall.chartdata.serviceLineFinYear;

        var rec = _.find(coll, function (item) {
            return item[id] === value;
        });

        if (!_.isUndefined(rec)) {
            value = $filter('currency')(value);
            value = value.toString() + ' | ' + rec['HeadCount' + id];
        }

        return value;
    };
    //endregion

    //region other
    $scope.overallRecompute = function () {
        var cummactual = _.reduce($scope.overall.gridData.serviceLineOverallGrid, function (cummactual, item) {
            if (item.sum === '') { return cummactual + 0; }
            return cummactual + parseFloat(item.sum);
        }, 0);

        $scope.overall.totalFooter.serviceLineOverallTotal = cummactual;
    };

    var uniqueFields = function () {
        if (!_.isUndefined(allWBS)) {
            allaccounts = _.uniq(_.map(allWBS, 'accountName'));
            allservicelines = _.uniq(_.map(allWBS, 'serviceLine'));
            availablemonths = _.uniq(_.map(allWBS, 'month'));
        }
    }

    var completeWBS = function () {
        ReportService.getAllTimeWBS()
        .$promise
        .then(function (wbsdata) {
            allWBS = wbsdata;

            if (!_.isUndefined(allWBS)) {
                allaccounts = _.uniq(_.map(allWBS, 'accountName'));
                availablemonths = _.uniq(_.map(allWBS, 'month'));
                allservicelines = _.uniq(_.map(allWBS, 'serviceLine'));

                //servicelineOverallChartLoad();
                //servicelineOverallGridLoad();
                customServicelineOverallGridLoad();
                customPyramidServiceLineGridLoad();
                financialYearChartLoad();

                alljoblevels = _.uniq(_.map(allWBS, 'jobLevel'));

                $scope.overall.dropdown.pyramidFinancialOverallYear = currentDateObj.getFullYear();
                $scope.overall.dropdown.pyramidServiceLineFinancialYear = currentDateObj.getFullYear();

                //!! TODO: change to availablemonths
                //$scope.overall.chartdata.pyramidOverall = pyramidOverallChartLoad(tempMonthYear);
                $scope.overall.gridData.pyramidOverallGrid = pyramidOverallGridLoad();
                $scope.overall.chartOptions.pyramidOverallCols.push({
                    "id": "pyramid",
                    "type": $scope.overall.chartOptions.pyramidOverallType,
                    "name": "Pyramid"
                });
                $scope.overall.chartOptions.pyramidOverallDataX = { "id": "monthyear" };

                finMonthYear = financialYearArray(currentDateObj.getFullYear());
                $scope.overall.chartdata.pyramidFinancial = pyramidOverallChartLoad(finMonthYear);
                $scope.overall.chartOptions.pyramidFinancialCols.push({
                    "id": "pyramid",
                    "type": $scope.overall.chartOptions.pyramidFinancialType,
                    "name": "Pyramid"
                });
                $scope.overall.chartOptions.pyramidFinancialDataX = { "id": "monthyear" };

                //!! TODO: change to availablemonths
                //$scope.overall.chartdata.pyramidServiceLineOverall = pyramidServiceLineChartLoad(tempMonthYear);
                //$scope.overall.gridData.pyramidServiceLineOverallGrid = pyramidServiceLineGridLoad();
                //$scope.overall.chartdata.pyramidServiceLineFinancial = pyramidServiceLineChartLoad(finMonthYear);
                $scope.overallRecompute();
            }
        });
    }

    completeWBS();
    //$scope.serviceLineOverallOptions = ChartService.getLineChartDefault();
    //endregion
    //endregion

    //region export
    var alterCollectionForExport = function (coll) {
        var newcoll = angular.copy(coll);

        newcoll = _.map(newcoll, function (item) {
            return _.omit(item, 'customer');
        });

        var i = 0;

        _.forEach(coll, function (origitem) {
            if (!_.isUndefined(origitem.customer)) {
                newcoll[i].accountName = origitem.customer.accountName;
            }
            else if (!_.isUndefined(origitem.account)) {
                newcoll[i].accountName = origitem.account.accountName;
            }
            i += 1;
        });

        return newcoll;
    }

    $scope.createPDF = function (div) {
        var pdf = new jsPDF('p', 'pt', 'letter')

	// source can be HTML-formatted string, or a reference
	// to an actual DOM element from which the text will be scraped.
	, source = $('#' + div)[0]

	// we support special element handlers. Register them with jQuery-style 
	// ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
	// There is no support for any other type of selectors 
	// (class, of compound) at this time.
	, specialElementHandlers = {
	    // element with id of "bypass" - jQuery style selector
	    //'#bypassme': function (element, renderer) {
	    //    // true = "handled elsewhere, bypass text extraction"
	    //    return true
	    //}
	}

        var margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
            source // HTML string or DOM elem ref.
            , margins.left // x coord
            , margins.top // y coord
            , {
                'width': margins.width // max width of content on PDF
                , 'elementHandlers': specialElementHandlers
            },
            function (dispose) {
                // dispose: object with X, Y of the last line add to the PDF 
                //          this allow the insertion of new lines after html
                pdf.save('Report.pdf');
            },
            margins
        )
    };

    $scope.generateOverallReport = function () {
        var config = {};
        var chart = {};
        var tablecolumns = [];
        var reportProps = GenerateReportService.getReportProps();

        config.exportedFileName = 'chart';
        config.backgroundColor = '#ffffff';

        //!! service line financial year chart canvas
        chart = angular.element(document.getElementById('serviceLineFinYear'));
        reportProps.pageitems.push({ item: ExportService.createChartImages(chart, config), type: 'chart' });
        reportProps.pageitemheaders.push('Financial Year ' + $scope.overall.dropdown.serviceLineFinancialYear);

        //!! service line overall grid
        tablecolumns.push({ title: 'Service Line', dataKey: 'serviceline' });
        tablecolumns.push({ title: 'Actual', dataKey: 'sum', format: 'currency' });
        reportProps.pageitems.push({ item: $scope.overall.gridData.serviceLineOverallGrid, type: 'table', columns: tablecolumns });
        reportProps.pageitemheaders.push('Year to date');

        chart = {};
        tablecolumns = [];
        chart = angular.element(document.getElementById('pyramidServiceLineFinancial'));
        reportProps.pageitems.push({ item: ExportService.createChartImages(chart, config), type: 'chart' });
        reportProps.pageitemheaders.push('Pyramid by ServiceLine for Financial Year ' + $scope.overall.dropdown.pyramidServiceLineFinancialYear);

        tablecolumns.push({ title: 'Service Line', dataKey: 'serviceline' });
        tablecolumns.push({ title: 'Actual', dataKey: 'sum', format: 'percent' });
        reportProps.pageitems.push({ item: $scope.overall.gridData.pyramidServiceLineOverallGrid, type: 'table', columns: tablecolumns });
        reportProps.pageitemheaders.push('Pyramid by ServiceLine Year to date');

        chart = {};
        tablecolumns = [];
        chart = angular.element(document.getElementById('pyramidFinancial'));
        reportProps.pageitems.push({ item: ExportService.createChartImages(chart, config), type: 'chart' });
        reportProps.pageitemheaders.push('Pyramid for Financial Year ' + $scope.overall.dropdown.pyramidFinancialOverallYear);

        tablecolumns.push({ title: 'Service Line', dataKey: 'serviceline' });
        tablecolumns.push({ title: 'Actual', dataKey: 'sum' });
        reportProps.pageitems.push({ item: $scope.overall.gridData.pyramidOverallGrid, type: 'table', columns: tablecolumns });
        reportProps.pageitemheaders.push('Pyramid Year to date');

        reportProps.reportname = 'Financial Report';
        reportProps.hascharts = true;
        reportProps.hastables = true;

        GenerateReportService.setReportProps(reportProps);
        GenerateReportService.createReport();
    };

    $scope.generateAccountReport = function () {
        var tablecolumns = [];
        var reportProps = GenerateReportService.getReportProps();
        alterCollectionForExport($scope.revenueReport);
        //!! revenue grid
        tablecolumns.push({ title: 'Account', dataKey: 'accountName' });
        tablecolumns.push({ title: 'Revenue', dataKey: 'revenue', format: 'currency' });
        tablecolumns.push({ title: 'Overall Actual', dataKey: 'overallactual', format: 'currency' });
        tablecolumns.push({ title: 'Margin', dataKey: 'margin', format: 'percent' });
        tablecolumns.push({ title: 'ND Baseline', dataKey: 'ndbaseline', format: 'currency' });
        tablecolumns.push({ title: 'ND Actual', dataKey: 'ndactual', format: 'currency' });
        tablecolumns.push({ title: 'ND Variance', dataKey: 'ndvariance', format: 'percent' });
        reportProps.pageitems.push({ item: alterCollectionForExport($scope.revenueReport), type: 'table', columns: tablecolumns });
        reportProps.pageitemheaders.push('Revenue Report ' + $scope.currMon + '-' + $scope.currYear);

        tablecolumns = [];
        tablecolumns.push({ title: 'Account', dataKey: 'accountName' });
        tablecolumns.push({ title: 'D Total', dataKey: 'targetCost', format: 'currency' });
        tablecolumns.push({ title: 'D Actual', dataKey: 'actualCost', format: 'currency' });
        tablecolumns.push({ title: 'D Variance', dataKey: 'variance', format: 'percent' });
        tablecolumns.push({ title: 'ND Total', dataKey: 'ndtargetCost', format: 'currency' });
        tablecolumns.push({ title: 'ND Actual', dataKey: 'ndactualCost', format: 'currency' });
        tablecolumns.push({ title: 'ND Variance', dataKey: 'ndvariance', format: 'percent' });
        reportProps.pageitems.push({ item: alterCollectionForExport($scope.costReport), type: 'table', columns: tablecolumns });
        reportProps.pageitemheaders.push('Cost Report ' + $scope.currMon + '-' + $scope.currYear);

        tablecolumns = [];
        tablecolumns.push({ title: 'Account', dataKey: 'accountName' });
        tablecolumns.push({ title: 'Revenue', dataKey: 'revenue', format: 'currency' });
        tablecolumns.push({ title: 'Overall Actual', dataKey: 'overallactual', format: 'currency' });
        tablecolumns.push({ title: 'Margin', dataKey: 'margin', format: 'percent' });
        reportProps.pageitems.push({ item: alterCollectionForExport($scope.cumRevenueReport), type: 'table', columns: tablecolumns });
        reportProps.pageitemheaders.push('Cummulative Revenue - ' + $scope.currYear);

        tablecolumns = [];
        tablecolumns.push({ title: 'Account', dataKey: 'accountName' });
        tablecolumns.push({ title: 'Total', dataKey: 'total', format: 'currency' });
        tablecolumns.push({ title: 'Actual', dataKey: 'actual', format: 'currency' });
        tablecolumns.push({ title: 'Varience', dataKey: 'variance', format: 'percent' });
        reportProps.pageitems.push({ item: alterCollectionForExport($scope.cumCostReport), type: 'table', columns: tablecolumns });
        reportProps.pageitemheaders.push('Cummulative Cost - ' + $scope.currYear);

        reportProps.reportname = 'Financial Report';
        reportProps.hascharts = false;
        reportProps.hastables = true;

        GenerateReportService.setReportProps(reportProps);
        GenerateReportService.createReport();
    };
    //endregion
});
