﻿'use strict';

angular.module('RapidReportsModule')
.run(function ($rootScope, $state, $log) {
    $rootScope.$on('$stateChangeSuccess', function (event, toState) {
        if (toState.name === 'report') {
            $log.debug('Auto-transitioning state :: report -> report.main');
            $state.go('report.main');
        }
    });
})
.config(function ($stateProvider) {
    $stateProvider
    .state('report', {
        url: '/report',
        template: '<div ui-view></div>'
    })
    .state('report.main', {
        url: '/',
        templateUrl: 'client/app/overview/report/report.html',
        controller: 'ReportCtrl'
    });
});