﻿'use strict';

angular.module('RapidReportsModule')
.config(function ($stateProvider) {
    $stateProvider
    .state('report.details', {
        url: '/:account/details',
        templateUrl: 'client/app/overview/report/detail/detail.html',
        controller: 'DetailCtrl'
    });
});