﻿'use strict';

angular.module('RapidReportsModule')
.controller('DetailCtrl', function (
    $scope,
    MonthlyAccountService,
    $state,
    ReportService,
    ChartService,
    DateHelper,
    AppDefault,
    GenerateReportService,
    ExportService,
    $cookies
    ) {
    var params = $state.params;
    var local = MonthlyAccountService.getAccountDetails();
    var monthObj = DateHelper.getMonthArray();
    var defaultPyramidTypes = AppDefault.pyramidJobLevel;
    var reportingManagers = AppDefault.serviceLineManagers;

    $scope.detail = {
        charts: {
            dServiceline: [],
            ndServiceline: [],
            dPyramidServiceline: [],
            ndPyramidServiceline: []
        },
        chartOptions: {
            dServicelineOption: {},
            ndServicelineOption: {},
            dPyramidServicelineOption: {},
            ndPyramidServicelineOption: {}
        },
        dropdown: {
            accountsdropdown: [],
            monthdropdown: [],
            yeardropdown: DateHelper.getYearArray(),
            selectedAccount: '',
            selectedMonth: '',
            selectedYear: 0
        }
    };

    var chartOptions = function () {
        $scope.detail.chartOptions.dServicelineOption = ChartService.getBarChartCustom({
            top: 20,
            right: 20,
            bottom: 60,
            left: 80
        }, true, 'Service Lines', null);
        $scope.detail.chartOptions.dPyramidServicelineOption = ChartService.getBarChartCustom({
            top: 20,
            right: 20,
            bottom: 60,
            left: 40
        }, true, 'Service Lines', 'percent');

        $scope.detail.chartOptions.ndServicelineOption = ChartService.getBarChartCustom({
            top: 20,
            right: 20,
            bottom: 60,
            left: 80
        }, true, 'Service Lines', null);
        $scope.detail.chartOptions.ndPyramidServicelineOption = ChartService.getBarChartCustom({
            top: 20,
            right: 20,
            bottom: 60,
            left: 40
        }, true, 'Service Lines', 'percent');

    }

    //region events
    $scope.listChanged = function (val) {
        var accountobj = _.find(local.allAccounts, function (item) { return item.accountName === $scope.detail.dropdown.selectedAccount; });

        if (!_.isUndefined(accountobj)) {
            local.account = accountobj;
            local.month = $scope.detail.dropdown.selectedMonth;
            local.year = $scope.detail.dropdown.selectedYear;

            dataLoad();
        }
    }

    $scope.back = function () {
        $state.go($cookies.previousState);
    };
    //endregion

    //region other
    var listLoad = function () {
        $scope.detail.dropdown.monthdropdown = _.map(monthObj, 'shortMon');
        $scope.detail.dropdown.accountsdropdown = local.allAccounts;

        $scope.detail.dropdown.selectedAccount = local.account.accountName;
        $scope.detail.dropdown.selectedMonth = local.month;
        $scope.detail.dropdown.selectedYear = local.year;
    }

    var pyramidCalculate = function (recs) {
        var percent = 0;

        var servicerecsA = _.filter(recs, function (item) {
            return _.includes(defaultPyramidTypes, item.jobLevel);
        });

        var servicesumA = 0;

        if (!_.isUndefined(servicerecsA)) {
            servicesumA = _.reduce(servicerecsA, function (servicesumA, item) {
                if (item.billedAmount === '') { return servicesumA + 0; }
                return servicesumA + parseFloat(item.fte);
            }, 0);
        }

        var servicesumB = 0;

        servicesumB = _.reduce(recs, function (servicesumB, item) {
            if (item.billedAmount === '') { return servicesumB + 0; }
            return servicesumB + parseFloat(item.fte);
        }, 0);

        if (servicesumA > 0) {
            percent = parseFloat((servicesumA / servicesumB).toFixed(1));
        }

        return percent;
    }

    var pyramidServiceLineChart = function (
        admrecs,
        scsrecs,
        saprecs,
        testingrecs,
        oraclerecs,
        hpitrecs,
        aadmrecs,
        fsirecs
        ) {
        var servicesarray = [];

        servicesarray.push({ 'label': 'ADM', 'value': parseFloat(pyramidCalculate(admrecs)) });
        servicesarray.push({ 'label': 'SCS', 'value': parseFloat(pyramidCalculate(scsrecs)) });
        servicesarray.push({ 'label': 'SAP', 'value': parseFloat(pyramidCalculate(saprecs)) });
        servicesarray.push({ 'label': 'Testing', 'value': parseFloat(pyramidCalculate(testingrecs)) });
        servicesarray.push({ 'label': 'Oracle', 'value': parseFloat(pyramidCalculate(oraclerecs)) });
        servicesarray.push({ 'label': 'HP IT', 'value': parseFloat(pyramidCalculate(hpitrecs)) });
        servicesarray.push({ 'label': 'A&DM', 'value': parseFloat(pyramidCalculate(aadmrecs)) });
        servicesarray.push({ 'label': 'FSI', 'value': parseFloat(pyramidCalculate(fsirecs)) });

        return servicesarray;
    }

    var customServiceLineChart = function (wbs) {
        var sumADM = 0;
        var sumSCS = 0;
        var sumSAP = 0;
        var sumTesting = 0;
        var sumOracle = 0;
        var sumHPIT = 0;
        var sumAADM = 0;
        var sumFSI = 0;
        var sumSIS = 0;
        var servicesarray = [];

        //!! ADM
        var admrecs = _.filter(wbs, function (admitem) {
            return (_.includes(reportingManagers.ADM, admitem.managerL6) &&
                !_.includes(reportingManagers.SAPMRU, admitem.mruL4));
        });

        if (!_.isUndefined(admrecs)) {
            sumADM = _.reduce(admrecs, function (sumADM, item) {
                if (item.billedAmount === '') { return sumADM + 0; }
                return sumADM + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! SCS
        var scsrecs = _.filter(wbs, function (scsitem) {
            return _.includes(reportingManagers.SCS, scsitem.managerL6);
        });

        if (!_.isUndefined(scsrecs)) {
            sumSCS = _.reduce(scsrecs, function (sumSCS, item) {
                if (item.billedAmount === '') { return sumSCS + 0; }
                return sumSCS + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! SAP
        var saaprecs = _.filter(wbs, function (sapitem) {
            return _.includes(reportingManagers.SIS, sapitem.managerL6);
        });

        var saprecs = _.filter(wbs, function (sapitem) {
            return ((_.includes(reportingManagers.SAP, sapitem.managerL6) &&
                _.includes(reportingManagers.SAPMRU, sapitem.mruL4)) ||
                _.includes(reportingManagers.SIS, sapitem.managerL6));
        });

        if (!_.isUndefined(saprecs)) {
            sumSAP = _.reduce(saprecs, function (sumSAP, item) {
                if (item.billedAmount === '') { return sumSAP + 0; }
                return sumSAP + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! Testing
        var testingrecs = _.filter(wbs, function (testingitem) {
            return _.includes(reportingManagers.Testing, testingitem.managerL6);
        });

        if (!_.isUndefined(testingrecs)) {
            sumTesting = _.reduce(testingrecs, function (sumTesting, item) {
                if (item.billedAmount === '') { return sumTesting + 0; }
                return sumTesting + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! Oracle
        var oraclerecs = _.filter(wbs, function (oracleitem) {
            return _.includes(reportingManagers.Oracle, oracleitem.managerL6);
        });

        if (!_.isUndefined(oraclerecs)) {
            sumOracle = _.reduce(oraclerecs, function (sumOracle, item) {
                if (item.billedAmount === '') { return sumOracle + 0; }
                return sumOracle + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! HPIT
        var hpitrecs = _.filter(wbs, function (hpititem) {
            return _.includes(reportingManagers.HPIT, hpititem.managerL6);
        });

        if (!_.isUndefined(hpitrecs)) {
            sumHPIT = _.reduce(hpitrecs, function (sumHPIT, item) {
                if (item.billedAmount === '') { return sumHPIT + 0; }
                return sumHPIT + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! AADM
        var aadmrecs = _.filter(wbs, function (aadmitem) {
            return _.includes(reportingManagers.AADM, aadmitem.managerL6);
        });

        if (!_.isUndefined(aadmrecs)) {
            sumAADM = _.reduce(aadmrecs, function (sumAADM, item) {
                if (item.billedAmount === '') { return sumAADM + 0; }
                return sumAADM + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! FSI
        var fsirecs = _.filter(wbs, function (fsiitem) {
            return _.includes(reportingManagers.FSI, fsiitem.managerL6);
        });

        if (!_.isUndefined(fsirecs)) {
            sumFSI = _.reduce(fsirecs, function (sumFSI, item) {
                if (item.billedAmount === '') { return sumFSI + 0; }
                return sumFSI + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! SIS
        //var sisrecs = _.filter(wbs, function (sisitem) {
        //    return _.includes(reportingManagers.SIS, sisitem.managerL6);
        //});

        //if (!_.isUndefined(sisrecs)) {
        //    sumSIS = _.reduce(sisrecs, function (sumSIS, item) {
        //        if (item.billedAmount === '') { return sumSIS + 0; }
        //        return sumSIS + parseFloat(item.billedAmount);
        //    }, 0);
        //}
        $scope.detail.charts.dPyramidServiceline = [
            {
                key: local.account.accountName,
                values: pyramidServiceLineChart(
                                admrecs,
                                scsrecs,
                                saprecs,
                                testingrecs,
                                oraclerecs,
                                hpitrecs,
                                aadmrecs,
                                fsirecs)
            }
        ];

        servicesarray.push({ 'label': 'ADM', 'value': parseFloat(sumADM.toFixed(2)) });
        servicesarray.push({ 'label': 'SCS', 'value': parseFloat(sumSCS.toFixed(2)) });
        servicesarray.push({ 'label': 'SAP', 'value': parseFloat(sumSAP.toFixed(2)) });
        servicesarray.push({ 'label': 'Testing', 'value': parseFloat(sumTesting.toFixed(2)) });
        servicesarray.push({ 'label': 'Oracle', 'value': parseFloat(sumOracle.toFixed(2)) });
        servicesarray.push({ 'label': 'HP IT', 'value': parseFloat(sumHPIT.toFixed(2)) });
        servicesarray.push({ 'label': 'A&DM', 'value': parseFloat(sumAADM.toFixed(2)) });
        servicesarray.push({ 'label': 'FSI', 'value': parseFloat(sumFSI.toFixed(2)) });
        //servicesarray.push({ 'label': 'SIS', 'value': parseFloat(sumSIS.toFixed(2)) });

        return servicesarray;
    }

    var customNDServiceLineChart = function (wbs) {
        var sumADM = 0;
        var sumSCS = 0;
        var sumSAP = 0;
        var sumTesting = 0;
        var sumOracle = 0;
        var sumHPIT = 0;
        var sumAADM = 0;
        var sumFSI = 0;
        var sumSIS = 0;
        var servicesarray = [];

        //!! ADM
        var admrecs = _.filter(wbs, function (admitem) {
            return (_.includes(reportingManagers.ADM, admitem.managerL6) &&
                !_.includes(reportingManagers.SAPMRU, admitem.mruL4));
        });

        if (!_.isUndefined(admrecs)) {
            sumADM = _.reduce(admrecs, function (sumADM, item) {
                if (item.billedAmount === '') { return sumADM + 0; }
                return sumADM + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! SCS
        var scsrecs = _.filter(wbs, function (scsitem) {
            return _.includes(reportingManagers.SCS, scsitem.managerL6);
        });

        if (!_.isUndefined(scsrecs)) {
            sumSCS = _.reduce(scsrecs, function (sumSCS, item) {
                if (item.billedAmount === '') { return sumSCS + 0; }
                return sumSCS + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! SAP
        var saaprecs = _.filter(wbs, function (sapitem) {
            return _.includes(reportingManagers.SIS, sapitem.managerL6);
        });

        var saprecs = _.filter(wbs, function (sapitem) {
            return ((_.includes(reportingManagers.SAP, sapitem.managerL6) &&
                _.includes(reportingManagers.SAPMRU, sapitem.mruL4)) ||
                _.includes(reportingManagers.SIS, sapitem.managerL6));
        });

        if (!_.isUndefined(saprecs)) {
            sumSAP = _.reduce(saprecs, function (sumSAP, item) {
                if (item.billedAmount === '') { return sumSAP + 0; }
                return sumSAP + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! Testing
        var testingrecs = _.filter(wbs, function (testingitem) {
            return _.includes(reportingManagers.Testing, testingitem.managerL6);
        });

        if (!_.isUndefined(testingrecs)) {
            sumTesting = _.reduce(testingrecs, function (sumTesting, item) {
                if (item.billedAmount === '') { return sumTesting + 0; }
                return sumTesting + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! Oracle
        var oraclerecs = _.filter(wbs, function (oracleitem) {
            return _.includes(reportingManagers.Oracle, oracleitem.managerL6);
        });

        if (!_.isUndefined(oraclerecs)) {
            sumOracle = _.reduce(oraclerecs, function (sumOracle, item) {
                if (item.billedAmount === '') { return sumOracle + 0; }
                return sumOracle + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! HPIT
        var hpitrecs = _.filter(wbs, function (hpititem) {
            return _.includes(reportingManagers.HPIT, hpititem.managerL6);
        });

        if (!_.isUndefined(hpitrecs)) {
            sumHPIT = _.reduce(hpitrecs, function (sumHPIT, item) {
                if (item.billedAmount === '') { return sumHPIT + 0; }
                return sumHPIT + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! AADM
        var aadmrecs = _.filter(wbs, function (aadmitem) {
            return _.includes(reportingManagers.AADM, aadmitem.managerL6);
        });

        if (!_.isUndefined(aadmrecs)) {
            sumAADM = _.reduce(aadmrecs, function (sumAADM, item) {
                if (item.billedAmount === '') { return sumAADM + 0; }
                return sumAADM + parseFloat(item.billedAmount);
            }, 0);
        }

        //!! FSI
        var fsirecs = _.filter(wbs, function (fsiitem) {
            return _.includes(reportingManagers.FSI, fsiitem.managerL6);
        });

        if (!_.isUndefined(fsirecs)) {
            sumFSI = _.reduce(fsirecs, function (sumFSI, item) {
                if (item.billedAmount === '') { return sumFSI + 0; }
                return sumFSI + parseFloat(item.billedAmount);
            }, 0);
        }

        $scope.detail.charts.ndPyramidServiceline = [
            {
                key: local.account.accountName,
                values: pyramidServiceLineChart(
                                admrecs,
                                scsrecs,
                                saprecs,
                                testingrecs,
                                oraclerecs,
                                hpitrecs,
                                aadmrecs,
                                fsirecs)
            }
        ];

        servicesarray.push({ 'label': 'ADM', 'value': parseFloat(sumADM.toFixed(2)) });
        servicesarray.push({ 'label': 'SCS', 'value': parseFloat(sumSCS.toFixed(2)) });
        servicesarray.push({ 'label': 'SAP', 'value': parseFloat(sumSAP.toFixed(2)) });
        servicesarray.push({ 'label': 'Testing', 'value': parseFloat(sumTesting.toFixed(2)) });
        servicesarray.push({ 'label': 'Oracle', 'value': parseFloat(sumOracle.toFixed(2)) });
        servicesarray.push({ 'label': 'HP IT', 'value': parseFloat(sumHPIT.toFixed(2)) });
        servicesarray.push({ 'label': 'A&DM', 'value': parseFloat(sumAADM.toFixed(2)) });
        servicesarray.push({ 'label': 'FSI', 'value': parseFloat(sumFSI.toFixed(2)) });

        return servicesarray;
    }

  

    var serviceLineChart = function (wbs) {
        var servicesarray = [];

        _.forEach(local.availableServiceLines, function (loopservice) {
            var servicesum = 0;
            var servicerecs = _.filter(wbs, function (rec) {
                return rec.serviceLine === loopservice;
            });
            
            if (!_.isUndefined(servicerecs)) {
                servicesum = _.reduce(servicerecs, function (servicesum, item) {
                    if (item.billedAmount === '') { return servicesum + 0; }
                    return servicesum + parseFloat(item.billedAmount);
                }, 0);
            }
            servicesarray.push({ 'label': loopservice, 'value': parseFloat(servicesum.toFixed(2)) });
        });

        return servicesarray;
    }

    var accountDataProcess = function () {
        var wbscoll = [];
        var filteredWBS = [];

        if (!_.isUndefined(local.monthWBS)) {
            //!! uniq WBS code in monthWBS
            local.availableWBS = _.uniq(_.map(local.monthWBS, 'wbsCode'));

            if (!_.isUndefined(local.allWBS)) {
                //!! month WBS from WBS dump
                wbscoll = _.filter(local.allWBS, function (item) {
                    return item.month.toLowerCase() === (local.month.toLowerCase() + ' ' + local.year);
                });

                if (!_.isUndefined(wbscoll)) {
                    //!! filtering collection with WBS array
                    filteredWBS = _.findByValues(wbscoll, 'wbsCode', local.availableWBS);

                    if (!_.isUndefined(filteredWBS)) {

                        $scope.detail.charts.dServiceline = [
                            {
                                key: local.account.accountName,
                                values: customServiceLineChart(filteredWBS)
                            }
                        ];

                        //!! uniq servicelines in filtered WBS
                        //local.availableServiceLines = _.uniq(_.map(filteredWBS, 'serviceLine'));

                        //if (!_.isUndefined(local.availableServiceLines)) {
                        //    _.remove(local.availableServiceLines, function (srvcitem) { return srvcitem === '-'; });
                        //}

                        //if (!_.isUndefined(local.availableServiceLines)) {
                        //    $scope.detail.charts.dServiceline = [
                        //        {
                        //        key: local.account.accountName,
                        //        values: serviceLineChart(filteredWBS)
                        //        }
                        //    ];
                        //}
                    }
                }
            }
        }
    }
    
    var accountNDDataProcess = function () {
        var wbscoll = [];
        var filteredWBS = [];

        if (!_.isUndefined(local.monthNDWBS)) {
            //!! uniq WBS code in monthWBS
            local.availableWBS = _.uniq(_.map(local.monthNDWBS, 'wbsCode'));

            if (!_.isUndefined(local.allWBS)) {
                //!! month WBS from WBS dump
                wbscoll = _.filter(local.allWBS, function (item) {
                    return item.month.toLowerCase() === (local.month.toLowerCase() + ' ' + local.year);
                });

                if (!_.isUndefined(wbscoll)) {
                    //!! filtering collection with WBS array
                    filteredWBS = _.findByValues(wbscoll, 'wbsCode', local.availableWBS);

                    if (!_.isUndefined(filteredWBS)) {

                        $scope.detail.charts.ndServiceline = [
                            {
                                key: local.account.accountName,
                                values: customNDServiceLineChart(filteredWBS)
                            }
                        ];
                    }
                }
            }
        }
    }

    var dataLoad = function () {
        ReportService.getMonthWBS({
            accountid: local.account.accountId,
            month: local.month,
            year: local.year
        })
        .$promise
        .then(function (wbsdata) {
            local.monthWBS = wbsdata;
            accountDataProcess();
        });

        ReportService.getMonthNDWBS({
            month: local.month,
            year: local.year
        })
        .$promise
        .then(function (wbsdata) {
            local.monthNDWBS = wbsdata;
            accountNDDataProcess();
        });
    }

    var startupLoad = function () {
        listLoad();
        chartOptions();
        dataLoad();
    }

    startupLoad();
    //endregion

    //region export
    $scope.generateDetailReport = function () {
        var config = {};
        var chart = {};
        var reportProps = GenerateReportService.getReportProps();

        config.exportedFileName = 'chart';
        config.backgroundColor = '#ffffff';

        //!! service line financial year chart canvas
        chart = angular.element(document.getElementById('dServiceline'));
        reportProps.pageitems.push({ item: ExportService.createChartImages(chart, config), type: 'chart' });
        reportProps.pageitemheaders.push($scope.detail.dropdown.selectedAccount + ' Monthly Service Line ' + $scope.detail.dropdown.selectedMonth + ' - ' + $scope.detail.dropdown.selectedYear);

        chart = {};
        chart = angular.element(document.getElementById('dPyramidServiceline'));
        reportProps.pageitems.push({ item: ExportService.createChartImages(chart, config), type: 'chart' });
        reportProps.pageitemheaders.push($scope.detail.dropdown.selectedAccount + ' Pyramid Service Line ' + $scope.detail.dropdown.selectedMonth + ' - ' + $scope.detail.dropdown.selectedYear);

        reportProps.reportname = 'Financial Detail Report';
        reportProps.hascharts = true;
        reportProps.hastables = false;

        GenerateReportService.setReportProps(reportProps);
        GenerateReportService.createReport();
    };
    //endregion
});