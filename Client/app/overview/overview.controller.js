﻿'use strict';

angular.module('RapidReportsModule')
.controller('OverviewCtrl', function (
    $state
    ) {
    var param = $state.params;

    //if (!_.isUndefined(param.opts)) {
    //    switch (param.opts.toLowerCase()) {
    //        case 'report': $state.go('overview.' + param.opts);
    //            break;
    //        case 'operation': $state.go('overview.' + param.opts);
    //            break;
    //    }
    //}
    $state.go('overview.report');
});