﻿'use strict';

angular.module('RapidReportsModule')
.config(function ($stateProvider) {
    $stateProvider
    .state('admin', {
        url: '/admin',
        templateUrl: 'client/app/overview/admin/admin.html',
        controller: 'AdminCtrl'
    });
});