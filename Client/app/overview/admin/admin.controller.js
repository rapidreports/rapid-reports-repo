﻿'use strict';

angular.module('RapidReportsModule')
.controller('AdminCtrl', function (
    $scope,
    PageHeaderService
    ) {
    PageHeaderService.setPageHeaderTitle('Admin');

    $scope.msg='This is Admin Panel';
});