﻿'use strict';

angular.module('RapidReportsModule')
.config(function ($stateProvider) {
    $stateProvider
    .state('overview.adm', {
        url: '/adm',
        templateUrl: 'client/app/overview/adm/adm.html',
        controller: 'ADMCtrl'
    });
});