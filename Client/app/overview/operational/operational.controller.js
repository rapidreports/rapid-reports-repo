﻿'use strict';

angular.module('RapidReportsModule')
.controller('OperationalCtrl', function (
    $state
    ) {
    $state.go('overview.operational.dashboard');
});