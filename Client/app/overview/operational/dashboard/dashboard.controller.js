﻿'use strict';

angular.module('RapidReportsModule')
.controller('OperationalDashboardCtrl', function (
    $scope,
    $http,
    $timeout,
    OperationalDashboardService,
    ManagerDetailService,
    PageHeaderService,
    $state
    ) {
    PageHeaderService.setPageHeaderTitle('Dashboard');
    //var local = {};
    //$scope.dashboard = {
    //    resourceUtilization: []
    //};

    ////region events    
    ////endregion

    ////region other
    //var dataLoad = function () {
    //    OperationalDashboardService.getResourceUtilization()
    //    .$promise
    //    .then(function (data) {
    //        $scope.dashboard.resourceUtilization = data;
    //    });
    //}

    //var startupLoad = function () {
    //    dataLoad();
    //}

    //startupLoad();
    
    ////endregion
    $scope.show = false;
    $scope.monthsel = false;
    $scope.yearsel = false;
    $scope.servicesel = false;
    $scope.years = [];
    $scope.serviceline = [{ sl: 'ADM' }, {sl:'SAP'}];
    $scope.months = [
        { id: '01', month: 'JAN' },
        { id: '02', month: 'FEB' },
        { id: '03', month: 'MAR' },
        { id: '04', month: 'APR' },
        { id: '05', month: 'MAY' },
        { id: '06', month: 'JUN' },
        { id: '07', month: 'JUL' },
        { id: '08', month: 'AUG' },
        { id: '09', month: 'SEP' },
        { id: '10', month: 'OCT' },
        { id: '11', month: 'NOV' },
        { id: '12', month: 'DEC' }
    ];
    
    $scope.quarters = [
        { quarter: 'Q1', id1: '11', month1: 'NOV', id2:'12', month2: 'DEC', id3:'01', month3: 'JAN' },
        { quarter: 'Q2', id1:'02', month1: 'FEB', id2:'03', month2: 'MAR', id3:'04', month3: 'APR' },
        { quarter: 'Q3', id1:'05', month1: 'MAY', id2:'06', month2: 'JUN', id3:'07', month3: 'JUL' },
        { quarter: 'Q4', id1:'08', month1: 'AUG', id2:'09', month2: 'SEP', id3:'10', month3: 'OCT' }
    ];

    $scope.yearlist = function () {
         var year = (new Date().getFullYear()) - 5;
        //var range = [];
        $scope.years.push({ year: year });
        for (var i = 1; i < 20; i++) {
            //$scope.years.push(year + i);
            $scope.years.push({
                year: year + i
            });
        }
       
    };
    $scope.yearlist();

    $scope.servicelineselected = function (sl) {
        $scope.selServiceLine = '';
        $scope.selServiceLine = sl;
        var LTFilter = '';
        $scope.servicesel = true;
        if ($scope.monthsel === true && $scope.yearsel === true && $scope.servicesel === true) {
            $scope.getOperationalDetails();
        }
        $timeout(function () { $scope.show = true; }, 1000);
    };

    $scope.monthSelected = function (mmm) {
        $scope.oprMonthSelected = '';
        $scope.oprMonthSelected = mmm;
        $scope.selectmonthint = _.find($scope.months, ['month', mmm]);
        $scope.selmonthint = $scope.selectmonthint.id;
        $scope.monthsel = true;
        if ($scope.monthsel === true && $scope.yearsel === true && $scope.servicesel === true){
            $scope.getOperationalDetails();
        }
    };

    $scope.yearSelected = function (yyyy) {
        $scope.oprYearSelected = '';
        $scope.oprYearSelected = yyyy;
        $scope.yearsel = true;
        if ($scope.monthsel === true && $scope.yearsel === true && $scope.servicesel === true) {
            $scope.getOperationalDetails();
        }
    };
    
    

    $scope.getOperationalDetails = function () {
        var promise = $http({
            method: 'GET',
            url: 'api/operational/dashboard/resourceutiliztion/' + $scope.oprMonthSelected + '/' + $scope.oprYearSelected
        });
        promise.success(function (data) {
            $scope.overallData = data;
            var LTFilter = [];
            var mangerwiserecord = [];

            if ($scope.selServiceLine === 'ADM') {
                LTFilter = _.filter(data, function(ADM){
                    return (_.includes(['Ramachandran, Prem Kumar'], ADM.ltName) &&
                        !_.includes(['D270', 'D272'], ADM.mruCode));
                }); 
            }
            else if ($scope.selServiceLine === 'SAP') {
                LTFilter = _.filter(data, function (mrucheck) {
                    return (_.includes(['Ramachandran, Prem Kumar', 'M, Raghavendra'], mrucheck.ltName) &&
                        _.includes(['D270', 'D272'], mrucheck.mruCode));
                });
            }

            var Managers = _.uniq(_.map(LTFilter, 'manger'));
            if ($scope.selServiceLine === 'ADM'){
                Managers.push('ADM (Total)');
            }
            else if ($scope.selServiceLine === 'SAP') {
                Managers.push('SAP (Total)');
            }
		$scope.allMangers = Managers;
		var build = _.forEach(Managers, function (mangers) {

		    var totallen = _.filter(LTFilter, { 'manger': mangers });
		    var total = totallen.length;
            
		    var attritionlen = _.filter(LTFilter, { 'activeInactive': 'Terminated', 'manger': mangers });
		    var attrition = attritionlen.length;

		    var loalen = _.filter(LTFilter, { 'activeInactive': 'Leave of Absence', 'manger': mangers });
		    var loa = loalen.length;

		    var lwplen = _.filter(LTFilter, { 'activeInactive': 'Leave With Pay', 'manger': mangers });
		    var lwp = lwplen.length;

		    var advcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV', 'orgDesc': 'Project', 'manger': mangers });
		    var adv = advcount.length;
		    if (adv === 0) {
		        advcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV', 'manger': mangers });
		        adv = advcount.length;
		    }

		    var corcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR', 'orgDesc': 'Project', 'manger': mangers });
		    var cor = corcount.length;
		    if (cor === 0) {
		        corcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR', 'manger': mangers });
		        cor = corcount.length;
		    }

		    var entcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT', 'orgDesc': 'Project', 'manger': mangers });
		    var ent = entcount.length;
		    if (ent === 0) {
		        entcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT', 'manger': mangers });
		        ent = entcount.length;
		    }

		    var intcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT', 'orgDesc': 'Project', 'manger': mangers });
		    var int = intcount.length;
		    if (ent === 0) {
		        intcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT', 'manger': mangers });
		        int = intcount.length;
		    }

		    var sencount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN', 'orgDesc': 'Project', 'manger': mangers });
		    var sen = sencount.length;
		    if (sen === 0) {
		        sencount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN', 'manger': mangers });
		        sen = sencount.length;
		    }

		    var bascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'BAS Scope needs to be updated', 'orgDesc': 'Project', 'manger': mangers });
		    var bas = bascount.length;
		    if (bas === 0) {
		        bascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'BAS Scope needs to be updated', 'manger': mangers });
		        bas = bascount.length;
		    }

		    var dircount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'DIR', 'orgDesc': 'Project', 'manger': mangers });
		    var dir = dircount.length;
		    if (dir === 0) {
		        dircount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'DIR', 'manger': mangers });
		        dir = dircount.length;
		    }

		    var expcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'EXP', 'orgDesc': 'Project', 'manger': mangers });
		    var exp = expcount.length;
		    if (exp === 0) {
		        expcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'EXP', 'manger': mangers });
		        exp = expcount.length;
		    }

		    var mascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MAS', 'orgDesc': 'Project', 'manger': mangers });
		    var mas = mascount.length;
		    if (mas === 0) {
		        mascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MAS', 'manger': mangers });
		        mas = mascount.length;
		    }

		    var mg1count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG1', 'orgDesc': 'Project', 'manger': mangers });
		    var mg1 = mg1count.length;
		    if (mg1 === 0) {
		        mg1count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG1', 'manger': mangers });
		        mg1 = mg1count.length;
		    }

		    var mg2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG2', 'orgDesc': 'Project', 'manger': mangers });
		    var mg2 = mg2count.length;
		    if (mg2 === 0) {
		        mg2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG2', 'manger': mangers });
		        mg2 = mg2count.length;
		    }

		    var specount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SPE', 'orgDesc': 'Project', 'manger': mangers });
		    var spe = specount.length;
		    if (spe === 0) {
		        specount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SPE', 'manger': mangers });
		        spe = specount.length;
		    }

		    var su2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SU2', 'orgDesc': 'Project', 'manger': mangers });
		    var su2 = su2count.length;
		    if (su2 === 0) {
		        su2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SU2', 'manger': mangers });
		        su2 = su2count.length;
		    }

		    var vpcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'VP', 'orgDesc': 'Project', 'manger': mangers });
		    var vp = vpcount.length;
		    if (vp === 0) {
		        vpcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'VP', 'manger': mangers });
		        vp = vpcount.length;
		    }

		    var regularcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'orgDesc': 'Project', 'manger': mangers });
		    var regular = regularcount.length;
		    if (regular === 0) {
		        regularcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'manger': mangers });
		        regular = regularcount.length;
		    }

		    var cwfcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C', 'orgDesc': 'Project', 'manger': mangers });
		    var cwf = cwfcount.length;
		    if (cwf === 0) {
		        cwfcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C', 'manger': mangers });
		        cwf = cwfcount.length;
		    }

		    var benchcount = _.filter(LTFilter, function (bench) {
		        return ((_.includes(['Bench', 'Partial Bench'], bench.allocationStatus)) && (_.includes([mangers], bench.manger)));
		    });
		    if(!(benchcount.length === 0 || total === 0)){
		        var bench = (benchcount.length / total) * 100;
		    }
		    else {
		        bench = 0;
		    }
		    var utilcount = _.filter(LTFilter, function (util) {
		        return ((_.includes(['Fully Allocated', 'Partially Allocated', 'Over Allocated'], util.allocationStatus)) && (_.includes([mangers], util.manger)));
		    });
		    if (!(utilcount.length === 0 || total === 0)){
		        var util = (utilcount.length / total) * 100;
		    }
		    else {
		        util = 0;
		    }

		    if (!((adv + cor + ent + int + sen) === 0 || regular === 0)) {
		        var lpi = ((adv + cor + ent + int + sen) / regular) * 100;
		    }
		    else {
		        lpi = 0;
		    }

		    var socrccount = _.filter(LTFilter, function (soc) {
		        return ((_.includes(['MG1', 'MG2'], soc.jobLevel)) && _.includes([mangers], soc.manger));
		    });
		    if(!(total === 0 || socrccount.length === 0)){
		        var socrc = total / socrccount.length;
		    }
		    else {
		        socrc = 0;
		    }
		    var mg = socrccount.length
		    if(!(regular === 0 || (mg1 + mg2) === 0)){
		        var socr = regular / (mg1 + mg2);
		    }
		    else {
		        socr = 0;
		    }
		    var mgr = mg1 + mg2;

		    var fullycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Fully Allocated', 'manger': mangers });
		    var fully = fullycount.length;
		    if (fully === 0) {
		        fullycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Fully Allocated', 'manger': mangers });
		        fully = fullycount.length;
		    }

		    var partiallycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Partially Allocated', 'manger': mangers });
		    var partially = partiallycount.length;
		    if (partially === 0) {
		        partiallycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Partially Allocated', 'manger': mangers });
		        partially = partiallycount.length;
		    }

		    var overcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Over Allocated', 'manger': mangers });
		    var over = overcount.length;
		    if (over === 0) {
		        overcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Over Allocated', 'manger': mangers });
		        over = overcount.length;
		    }

		    var sharedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Shared Services', 'manger': mangers });
		    var shared = sharedcount.length;
		    if (shared === 0) {
		        sharedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Shared Services', 'manger': mangers });
		        shared = sharedcount.length;
		    }

		    var operationsohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Operations OH', 'manger': mangers });
		    var operationsoh = operationsohcount.length;
		    if (operationsoh === 0) {
		        operationsohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Operations OH', 'manger': mangers });
		        operationsoh = operationsohcount.length;
		    }

		    var businessfundedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Business Funded', 'manger': mangers });
		    var businessfunded = businessfundedcount.length;
		    if (businessfunded === 0) {
		        businessfundedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Business Funded', 'manger': mangers });
		        businessfunded = businessfundedcount.length;
		    }

		    var campuscount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Campus', 'manger': mangers });
		    var campus = campuscount.length;
		    if (campus === 0) {
		        campuscount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Campus', 'manger': mangers });
		        campus = campuscount.length;
		    }

		    var deliveryohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Delivery OH', 'manger': mangers });
		    var deliveryoh = deliveryohcount.length;
		    if (deliveryoh === 0) {
		        deliveryohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Delivery OH', 'manger': mangers });
		        deliveryoh = deliveryohcount.length;
		    }

		    var longleavecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Long Leave', 'manger': mangers });
		    var longleave = longleavecount.length;
		    if (longleave === 0) {
		        longleavecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Long Leave', 'manger': mangers });
		        longleave = longleavecount.length;
		    }

		    var supportcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Support', 'manger': mangers });
		    var support = supportcount.length;
		    if (support === 0) {
		        supportcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Support', 'manger': mangers });
		        support = supportcount.length;
		    }

		    var nongdicount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Non GDI', 'manger': mangers });
		    var nongdi = nongdicount.length;
		    if (nongdi === 0) {
		        nongdicount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Non GDI', 'manger': mangers });
		        nongdi = nongdicount.length;
		    }

		    var benchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Bench', 'manger': mangers });
		    var benchfte = benchftecount.length;
		    if (benchfte === 0) {
		        benchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Bench', 'manger': mangers });
		        benchfte = benchftecount.length;
		    }

		    var partialbenchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Partial Bench', 'manger': mangers });
		    var partialbenchfte = partialbenchftecount.length;
		    if (partialbenchfte === 0) {
		        partialbenchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Partial Bench', 'manger': mangers });
		        partialbenchfte = partialbenchftecount.length;
		    }

		    var benchthirtycount = _.filter(LTFilter, function(benthirty){ 
		        return ((_.includes(['Active'], benthirty.activeInactive) && 
                    _.includes(['Project'], benthirty.orgDesc) && 
                    (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                   _.includes(['0-30'], benthirty.benchAgeing) &&
                    _.includes([mangers], benthirty.manger)));
		    });
		    var benchthirty = benchthirtycount.length;

		    var benchsixtycount = _.filter(LTFilter, function (benthirty) {
		        return ((_.includes(['Active'], benthirty.activeInactive) &&
                    _.includes(['Project'], benthirty.orgDesc) &&
                    (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                   _.includes(['31-60'], benthirty.benchAgeing) &&
                    _.includes([mangers], benthirty.manger)));
		    });
		    var benchsixty = benchsixtycount.length;

		    var benchnintycount = _.filter(LTFilter, function (benthirty) {
		        return ((_.includes(['Active'], benthirty.activeInactive) &&
                    _.includes(['Project'], benthirty.orgDesc) &&
                    (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                   _.includes(['61-90'], benthirty.benchAgeing) &&
                    _.includes([mangers], benthirty.manger)));
		    });
		    var benchninty = benchnintycount.length;

		    var benchnintypluscount = _.filter(LTFilter, function (benthirty) {
		        return ((_.includes(['Active'], benthirty.activeInactive) &&
                    _.includes(['Project'], benthirty.orgDesc) &&
                    (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                   _.includes(['90+'], benthirty.benchAgeing) &&
                    _.includes([mangers], benthirty.manger)));
		    });
		    var benchnintyplus = benchnintypluscount.length;

		    var findquarter = _.filter($scope.quarters, function (data) {
		        return (_.includes([$scope.oprMonthSelected], data.month1)) ||
                    (_.includes([$scope.oprMonthSelected], data.month2)) ||
                    (_.includes([$scope.oprMonthSelected], data.month3));
		    });
		    $scope.displayquarter = findquarter[0].quarter;
		    $scope.displaymonth1 = findquarter[0].month1;
		    $scope.displaymonth2 = findquarter[0].month2;
		    $scope.displaymonth3 = findquarter[0].month3;

		    var doj = findquarter[0].id1 + '/' + $scope.oprYearSelected;
		    var fresherminuscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj, 'manger': mangers });
		    var fresherminus = fresherminuscount.length;
		    if (fresherminus === 0) {
		        fresherminuscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj, 'manger': mangers });
		        fresherminus = fresherminuscount.length;
		    }
		    var doj1 = findquarter[0].id2 + '/' + $scope.oprYearSelected;
		    var freshercount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj1, 'manger': mangers });
		    var fresher = freshercount.length;
		    if (fresher === 0) {
		        freshercount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj1, 'manger': mangers });
		        fresher = freshercount.length;
		    }
		    if (findquarter[0].id3 === '01'){
		        var year = parseInt($scope.oprYearSelected);
		        var doj2 = findquarter[0].id3 + '/' + (year + 1);
		        $scope.diffyear = (year + 1);
		    }
		    else {
		        var doj2 = findquarter[0].id3 + '/' + $scope.oprYearSelected;
		    }
		    var fresherpluscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj2, 'manger': mangers });
		    var fresherplus = fresherpluscount.length;
		    if (fresherplus === 0) {
		        fresherpluscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj2, 'manger': mangers });
		        fresherplus = fresherpluscount.length;
		    }
            
		    var doj3 = findquarter[0].id1 + '/' + $scope.oprYearSelected;
		    var lateralminuscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj3, 'manger': mangers });
		    var lateralminus = fresherpluscount.length;
		    if (lateralminus === 0) {
		        lateralminuscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj3, 'manger': mangers });
		        lateralminus = lateralminuscount.length;
		    }

		    var doj4 = findquarter[0].id2 + '/' + $scope.oprYearSelected;
		    var lateralcount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj4, 'manger': mangers });
		    var lateral = lateralcount.length;
		    if (lateral === 0) {
		        lateralcount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj4, 'manger': mangers });
		        lateral = lateralcount.length;
		    }

		    if (findquarter[0].id3 === '01') {
		        var year = parseInt($scope.oprYearSelected);
		        var doj5 = findquarter[0].id3 + '/' + (year + 1);
		        $scope.diffyear = (year + 1);
		    }
		    else {
		        var doj5 = findquarter[0].id3 + '/' + $scope.oprYearSelected;
		    }
		    var lateralpluscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj5, 'manger': mangers });
		    var lateralplus = lateralpluscount.length;
		    if (lateralplus === 0) {
		        lateralpluscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj5, 'manger': mangers });
		        lateralplus = lateralpluscount.length;
		    }

		    var freshertotal = fresherminus + fresher + fresherplus;
		    var lateraltotal = lateralminus + lateral + lateralplus;

		    var mrucount = _.filter(LTFilter, function (mrudata) { 
		        //'mrunoncomp': 'Non-Compliance', 'orgDesc': 'Project', 'activeInactive': 'Active', 'manger': mangers });'
		        return (_.includes(((['Non-Compliance, D263 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                    _.includes(['Non-Compliance, D268 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                    _.includes(['Non-Compliance, D277 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                    _.includes(['Non-Compliance, E912 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                    _.includes(['Non-Compliance, E917 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp)) &&
                    ['Project'], mrudata.orgDesc
                    && [mangers], mrudata.manger));
		});
            var mru = mrucount.length;
            if (mru === 0) {
                mrucount = _.filter(LTFilter, function(dat) { 
                    //'mrunoncomp': 'Non-Compliance', 'activeInactive': 'Active', 'manger': mangers 
                    return (_.includes(((['Non-Compliance, D263 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                    _.includes(['Non-Compliance, D268 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                    _.includes(['Non-Compliance, D277 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                    _.includes(['Non-Compliance, E912 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                    _.includes(['Non-Compliance, E917 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp)) &&
                    [mangers], dat.manger));
                });
                mru = mrucount.length;
            }

            var onsitebenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'environmentlttstt': 'LTT', 'allocationStatus': 'Bench', 'manger': mangers });
            var onsitebench = onsitebenchcount.length;
            if (onsitebench === 0) {
                onsitebenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'environmentlttstt': 'LTT', 'allocationStatus': 'Bench', 'manger': mangers });
                onsitebench = onsitebenchcount.length;
            }

            var cwfbenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'regcwf': 'C', 'allocationStatus': 'Bench', 'manger': mangers });
            var cwfbench = cwfbenchcount.length;
            if (cwfbench === 0) {
                cwfbenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C', 'allocationStatus': 'Bench', 'manger': mangers });
                cwfbench = cwfbenchcount.length;
            }

            var lhccbbcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'lhccvsmobility': 'Non Compliance/No DATA Available', 'manger': mangers });
            var lhccbb = lhccbbcount.length;
            if (lhccbb === 0) {
                lhccbbcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'lhccvsmobility': 'Non Compliance/No DATA Available', 'manger': mangers });
                lhccbb = lhccbbcount.length;
            }

            var sabacount = _.filter(LTFilter, { 'orgDesc': 'Project', 'sabacompliance': 'Non-Compliant', 'manger': mangers });
            var saba = sabacount.length;
            if (saba === 0) {
                sabacount = _.filter(LTFilter, { 'sabacompliance': 'Non-Compliant', 'manger': mangers });
                saba = sabacount.length;
            }

            if (mangers === 'ADM (Total)' || mangers === 'SAP (Total)') {
                var total = LTFilter.length;

                var attritionlen = _.filter(LTFilter, { 'activeInactive': 'Terminated' });
                var attrition = attritionlen.length;

                var loalen = _.filter(LTFilter, { 'activeInactive': 'Leave of Absence' });
                var loa = loalen.length;

                var lwplen = _.filter(LTFilter, { 'activeInactive': 'Leave With Pay' });
                var lwp = lwplen.length;

                var advcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV', 'orgDesc': 'Project' });
                var adv = advcount.length;
                if (adv === 0) {
                    advcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV' });
                    adv = advcount.length;
                }

                var corcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR', 'orgDesc': 'Project' });
                var cor = corcount.length;
                if (cor === 0) {
                    corcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR' });
                    cor = corcount.length;
                }

                var entcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT', 'orgDesc': 'Project' });
                var ent = entcount.length;
                if (ent === 0) {
                    entcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT' });
                    ent = entcount.length;
                }

                var intcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT', 'orgDesc': 'Project' });
                var int = intcount.length;
                if (ent === 0) {
                    intcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT' });
                    int = intcount.length;
                }

                var sencount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN', 'orgDesc': 'Project' });
                var sen = sencount.length;
                if (sen === 0) {
                    sencount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN' });
                    sen = sencount.length;
                }

                var bascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'BAS Scope needs to be updated', 'orgDesc': 'Project' });
                var bas = bascount.length;
                if (bas === 0) {
                    bascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'BAS Scope needs to be updated' });
                    bas = bascount.length;
                }

                var dircount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'DIR', 'orgDesc': 'Project' });
                var dir = dircount.length;
                if (dir === 0) {
                    dircount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'DIR' });
                    dir = dircount.length;
                }

                var expcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'EXP', 'orgDesc': 'Project' });
                var exp = expcount.length;
                if (exp === 0) {
                    expcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'EXP' });
                    exp = expcount.length;
                }

                var mascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MAS', 'orgDesc': 'Project' });
                var mas = mascount.length;
                if (mas === 0) {
                    mascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MAS' });
                    mas = mascount.length;
                }

                var mg1count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG1', 'orgDesc': 'Project' });
                var mg1 = mg1count.length;
                if (mg1 === 0) {
                    mg1count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG1' });
                    mg1 = mg1count.length;
                }

                var mg2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG2', 'orgDesc': 'Project' });
                var mg2 = mg2count.length;
                if (mg2 === 0) {
                    mg2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG2' });
                    mg2 = mg2count.length;
                }

                var specount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SPE', 'orgDesc': 'Project' });
                var spe = specount.length;
                if (spe === 0) {
                    specount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SPE' });
                    spe = specount.length;
                }

                var su2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SU2', 'orgDesc': 'Project' });
                var su2 = su2count.length;
                if (su2 === 0) {
                    su2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SU2' });
                    su2 = su2count.length;
                }

                var vpcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'VP', 'orgDesc': 'Project' });
                var vp = vpcount.length;
                if (vp === 0) {
                    vpcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'VP' });
                    vp = vpcount.length;
                }

                var regularcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'orgDesc': 'Project' });
                var regular = regularcount.length;
                if (regular === 0) {
                    regularcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R' });
                    regular = regularcount.length;
                }

                var cwfcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C', 'orgDesc': 'Project' });
                var cwf = cwfcount.length;
                if (cwf === 0) {
                    cwfcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C' });
                    cwf = cwfcount.length;
                }

                var benchcount = _.filter(LTFilter, function (bench) {
                    return ((_.includes(['Bench', 'Partial Bench'], bench.allocationStatus)));
                });
                if (!(benchcount.length === 0 || total === 0)) {
                    var bench = (benchcount.length / total) * 100;
                }
                else {
                    bench = 0;
                }
                var utilcount = _.filter(LTFilter, function (util) {
                    return ((_.includes(['Fully Allocated', 'Partially Allocated', 'Over Allocated'], util.allocationStatus)));
                });
                if (!(utilcount.length === 0 || total === 0)) {
                    var util = (utilcount.length / total) * 100;
                }
                else {
                    util = 0;
                }

                if (!((adv + cor + ent + int + sen) === 0 || regular === 0)) {
                    var lpi = ((adv + cor + ent + int + sen) / regular) * 100;
                }
                else {
                    lpi = 0;
                }

                var socrccount = _.filter(LTFilter, function (soc) {
                    return ((_.includes(['MG1', 'MG2'], soc.jobLevel)));
                });
                if (!(total === 0 || socrccount.length === 0)) {
                    var socrc = total / socrccount.length;
                }
                else {
                    socrc = 0;
                }
                var mg = socrccount.length
                if (!(regular === 0 || (mg1 + mg2) === 0)) {
                    var socr = regular / (mg1 + mg2);
                }
                else {
                    socr = 0;
                }
                var mgr = mg1 + mg2;

                var fullycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Fully Allocated' });
                var fully = fullycount.length;
                if (fully === 0) {
                    fullycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Fully Allocated'});
                    fully = fullycount.length;
                }

                var partiallycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Partially Allocated' });
                var partially = partiallycount.length;
                if (partially === 0) {
                    partiallycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Partially Allocated' });
                    partially = partiallycount.length;
                }

                var overcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Over Allocated' });
                var over = overcount.length;
                if (over === 0) {
                    overcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Over Allocated' });
                    over = overcount.length;
                }

                var sharedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Shared Services' });
                var shared = sharedcount.length;
                if (shared === 0) {
                    sharedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Shared Services' });
                    shared = sharedcount.length;
                }

                var operationsohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Operations OH' });
                var operationsoh = operationsohcount.length;
                if (operationsoh === 0) {
                    operationsohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Operations OH' });
                    operationsoh = operationsohcount.length;
                }

                var businessfundedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Business Funded' });
                var businessfunded = businessfundedcount.length;
                if (businessfunded === 0) {
                    businessfundedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Business Funded' });
                    businessfunded = businessfundedcount.length;
                }

                var campuscount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Campus' });
                var campus = campuscount.length;
                if (campus === 0) {
                    campuscount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Campus' });
                    campus = campuscount.length;
                }

                var deliveryohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Delivery OH' });
                var deliveryoh = deliveryohcount.length;
                if (deliveryoh === 0) {
                    deliveryohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Delivery OH' });
                    deliveryoh = deliveryohcount.length;
                }

                var longleavecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Long Leave' });
                var longleave = longleavecount.length;
                if (longleave === 0) {
                    longleavecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Long Leave'});
                    longleave = longleavecount.length;
                }

                var supportcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Support' });
                var support = supportcount.length;
                if (support === 0) {
                    supportcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Support'});
                    support = supportcount.length;
                }

                var nongdicount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Non GDI' });
                var nongdi = nongdicount.length;
                if (nongdi === 0) {
                    nongdicount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Non GDI' });
                    nongdi = nongdicount.length;
                }

                var benchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Bench' });
                var benchfte = benchftecount.length;
                if (benchfte === 0) {
                    benchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Bench' });
                    benchfte = benchftecount.length;
                }

                var partialbenchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Partial Bench' });
                var partialbenchfte = partialbenchftecount.length;
                if (partialbenchfte === 0) {
                    partialbenchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Partial Bench' });
                    partialbenchfte = partialbenchftecount.length;
                }

                var benchthirtycount = _.filter(LTFilter, function (benthirty) {
                    return ((_.includes(['Active'], benthirty.activeInactive) &&
                        _.includes(['Project'], benthirty.orgDesc) &&
                        (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                       _.includes(['0-30'], benthirty.benchAgeing)));
                });
                var benchthirty = benchthirtycount.length;

                var benchsixtycount = _.filter(LTFilter, function (benthirty) {
                    return ((_.includes(['Active'], benthirty.activeInactive) &&
                        _.includes(['Project'], benthirty.orgDesc) &&
                        (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                       _.includes(['31-60'], benthirty.benchAgeing)));
                });
                var benchsixty = benchsixtycount.length;

                var benchnintycount = _.filter(LTFilter, function (benthirty) {
                    return ((_.includes(['Active'], benthirty.activeInactive) &&
                        _.includes(['Project'], benthirty.orgDesc) &&
                        (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                       _.includes(['61-90'], benthirty.benchAgeing)));
                });
                var benchninty = benchnintycount.length;

                var benchnintypluscount = _.filter(LTFilter, function (benthirty) {
                    return ((_.includes(['Active'], benthirty.activeInactive) &&
                        _.includes(['Project'], benthirty.orgDesc) &&
                        (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                       _.includes(['90+'], benthirty.benchAgeing)));
                });
                var benchnintyplus = benchnintypluscount.length;

                var findquarter = _.filter($scope.quarters, function (data) {
                    return (_.includes([$scope.oprMonthSelected], data.month1)) ||
                        (_.includes([$scope.oprMonthSelected], data.month2)) ||
                        (_.includes([$scope.oprMonthSelected], data.month3));
                });
                $scope.displayquarter = findquarter[0].quarter;
                $scope.displaymonth1 = findquarter[0].month1;
                $scope.displaymonth2 = findquarter[0].month2;
                $scope.displaymonth3 = findquarter[0].month3;

                var doj = findquarter[0].id1 + '/' + $scope.oprYearSelected;
                var fresherminuscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj });
                var fresherminus = fresherminuscount.length;
                if (fresherminus === 0) {
                    fresherminuscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj });
                    fresherminus = fresherminuscount.length;
                }
                var doj1 = findquarter[0].id2 + '/' + $scope.oprYearSelected;
                var freshercount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj1 });
                var fresher = freshercount.length;
                if (fresher === 0) {
                    freshercount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj1 });
                    fresher = freshercount.length;
                }
                if (findquarter[0].id3 === '01') {
                    var year = parseInt($scope.oprYearSelected);
                    var doj2 = findquarter[0].id3 + '/' + (year + 1);
                    $scope.diffyear = (year + 1);
                }
                else {
                    var doj2 = findquarter[0].id3 + '/' + $scope.oprYearSelected;
                }
                var fresherpluscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj2 });
                var fresherplus = fresherpluscount.length;
                if (fresherplus === 0) {
                    fresherpluscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj2 });
                    fresherplus = fresherpluscount.length;
                }

                var doj3 = findquarter[0].id1 + '/' + $scope.oprYearSelected;
                var lateralminuscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj3 });
                var lateralminus = fresherpluscount.length;
                if (lateralminus === 0) {
                    lateralminuscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj3 });
                    lateralminus = lateralminuscount.length;
                }

                var doj4 = findquarter[0].id2 + '/' + $scope.oprYearSelected;
                var lateralcount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj4 });
                var lateral = lateralcount.length;
                if (lateral === 0) {
                    lateralcount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj4 });
                    lateral = lateralcount.length;
                }

                if (findquarter[0].id3 === '01') {
                    var year = parseInt($scope.oprYearSelected);
                    var doj5 = findquarter[0].id3 + '/' + (year + 1);
                    $scope.diffyear = (year + 1);
                }
                else {
                    var doj5 = findquarter[0].id3 + '/' + $scope.oprYearSelected;
                }
                var lateralpluscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj5});
                var lateralplus = lateralpluscount.length;
                if (lateralplus === 0) {
                    lateralpluscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj5 });
                    lateralplus = lateralpluscount.length;
                }

                var freshertotal = fresherminus + fresher + fresherplus;
                var lateraltotal = lateralminus + lateral + lateralplus;

                var mrucount = _.filter(LTFilter, function (mrudata) {
                    //'mrunoncomp': 'Non-Compliance', 'orgDesc': 'Project', 'activeInactive': 'Active', 'manger': mangers });'
                    return (_.includes(((['Non-Compliance, D263 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                        _.includes(['Non-Compliance, D268 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                        _.includes(['Non-Compliance, D277 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                        _.includes(['Non-Compliance, E912 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                        _.includes(['Non-Compliance, E917 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp)) &&
                        ['Project'], mrudata.orgDesc));
                });
                var mru = mrucount.length;
                if (mru === 0) {
                    mrucount = _.filter(LTFilter, function (dat) {
                        //'mrunoncomp': 'Non-Compliance', 'activeInactive': 'Active', 'manger': mangers 
                        return (_.includes(((['Non-Compliance, D263 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                        _.includes(['Non-Compliance, D268 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                        _.includes(['Non-Compliance, D277 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                        _.includes(['Non-Compliance, E912 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                        _.includes(['Non-Compliance, E917 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp))));
                    });
                    mru = mrucount.length;
                }

                var onsitebenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'environmentlttstt': 'LTT', 'allocationStatus': 'Bench'});
                var onsitebench = onsitebenchcount.length;
                if (onsitebench === 0) {
                    onsitebenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'environmentlttstt': 'LTT', 'allocationStatus': 'Bench'});
                    onsitebench = onsitebenchcount.length;
                }

                var cwfbenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'regcwf': 'C', 'allocationStatus': 'Bench' });
                var cwfbench = cwfbenchcount.length;
                if (cwfbench === 0) {
                    cwfbenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C', 'allocationStatus': 'Bench' });
                    cwfbench = cwfbenchcount.length;
                }

                var lhccbbcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'lhccvsmobility': 'Non Compliance/No DATA Available' });
                var lhccbb = lhccbbcount.length;
                if (lhccbb === 0) {
                    lhccbbcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'lhccvsmobility': 'Non Compliance/No DATA Available' });
                    lhccbb = lhccbbcount.length;
                }

                var sabacount = _.filter(LTFilter, { 'orgDesc': 'Project', 'sabacompliance': 'Non-Compliant' });
                var saba = sabacount.length;
                if (saba === 0) {
                    sabacount = _.filter(LTFilter, { 'sabacompliance': 'Non-Compliant' });
                    saba = sabacount.length;
                }
            }

            var record = {
                'total': total,
                'manger': mangers,
                'attrition': attrition,
                'loa': loa,
                'lwp': lwp,
                'adv': adv,
                'cor': cor,
                'ent': ent,
                'int': int,
                'sen': sen,
                'bas': bas,
                'dir': dir,
                'exp': exp,
                'mas': mas,
                'mg1': mg1,
                'mg2': mg2,
                'spe': spe,
                'su2': su2,
                'vp': vp,
                'regular': regular,
                'cwf': cwf,
                'bench': bench,
                'util': util,
                'lpi': lpi,
                'socrc': socrc,
                'mg': mg,
                'socr': socr,
                'mgr': mgr,
                'fully': fully,
                'partially': partially,
                'over': over,
                'shared': shared,
                'operationsoh': operationsoh,
                'businessfunded': businessfunded,
                'campus': campus,
                'deliveryoh': deliveryoh,
                'longleave': longleave,
                'support': support,
                'nongdi': nongdi,
                'benchfte': benchfte,
                'partialbenchfte': partialbenchfte,
                'benchthirty': benchthirty,
                'benchsixty': benchsixty,
                'benchninty': benchninty,
                'benchnintyplus': benchnintyplus,
                'fresherminus': fresherminus,
                'fresher': fresher,
                'fresherplus': fresherplus,
                'lateralminus': lateralminus,
                'lateral': lateral,
                'lateralplus': lateralplus,
                'freshertotal': freshertotal,
                'lateraltotal': lateraltotal,
                'mru': mru,
                'onsitebench': onsitebench,
                'cwfbench': cwfbench,
                'lhccbb': lhccbb,
                'saba': saba
            };
            mangerwiserecord.push(record);

            });

		$scope.display = mangerwiserecord;

            //cal of getOperationalDetailsPrmnth (getting data of previous month)
		$scope.getOperationalDetailsPrmnth();
        });
        /* jshint ignore:start 
        promise.error(function (response) {
             TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };


    // For getting previous month data

    $scope.getOperationalDetailsPrmnth = function () {
        $scope.oprPrMonthSelected = '';
        $scope.oprPrYearSelected = '';

        var curMonth = _.find($scope.months, function (item) { return item.month === $scope.oprMonthSelected });
        var prMonth;
        var prYear;
        if (parseInt(curMonth.id) > 1) {
             prMonth = _.find($scope.months, function (item) { return parseInt(item.id) === (curMonth.id - 1) });
            $scope.oprPrMonthSelected = prMonth;
            prYear = $scope.oprYearSelected;
            $scope.oprPrYearSelected = prYear;
        }

        else {
           prYear = $scope.oprYearSelected - 1;
           //prMonth = "DEC";
           $scope.oprPrMonthSelected = _.find($scope.months,function(item){return parseInt(item.id) === 12});
           $scope.oprPrYearSelected = prYear;
        }        
        
        var promise = $http({
            method: 'GET',
            url: 'api/operational/dashboard/resourceutiliztion/' + $scope.oprPrMonthSelected.month + '/' + $scope.oprPrYearSelected
        });
        promise.success(function (data) {
            $scope.overallData = data;
            var LTFilter = [];
            var mangerwiserecordprmnth = [];

            if ($scope.selServiceLine === 'ADM') {
                LTFilter = _.filter(data, function (ADM) {
                    return (_.includes(['Ramachandran, Prem Kumar'], ADM.ltName) &&
                        !_.includes(['D270', 'D272'], ADM.mruCode));
                });
            }
            else if ($scope.selServiceLine === 'SAP') {
                LTFilter = _.filter(data, function (mrucheck) {
                    return (_.includes(['Ramachandran, Prem Kumar', 'M, Raghavendra'], mrucheck.ltName) &&
                        _.includes(['D270', 'D272'], mrucheck.mruCode));
                });
            }

            var Managers = _.uniq(_.map(LTFilter, 'manger'));
            if ($scope.selServiceLine === 'ADM') {
                Managers.push('ADM (Total)');
            }
            else if ($scope.selServiceLine === 'SAP') {
                Managers.push('SAP (Total)');
            }
            $scope.allMangers = Managers;
            var build = _.forEach(Managers, function (mangers) {

                var totallen = _.filter(LTFilter, { 'manger': mangers });
                var total = totallen.length;

                var attritionlen = _.filter(LTFilter, { 'activeInactive': 'Terminated', 'manger': mangers });
                var attrition = attritionlen.length;

                var loalen = _.filter(LTFilter, { 'activeInactive': 'Leave of Absence', 'manger': mangers });
                var loa = loalen.length;

                var lwplen = _.filter(LTFilter, { 'activeInactive': 'Leave With Pay', 'manger': mangers });
                var lwp = lwplen.length;

                var advcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV', 'orgDesc': 'Project', 'manger': mangers });
                var adv = advcount.length;
                if (adv === 0) {
                    advcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV', 'manger': mangers });
                    adv = advcount.length;
                }

                var corcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR', 'orgDesc': 'Project', 'manger': mangers });
                var cor = corcount.length;
                if (cor === 0) {
                    corcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR', 'manger': mangers });
                    cor = corcount.length;
                }

                var entcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT', 'orgDesc': 'Project', 'manger': mangers });
                var ent = entcount.length;
                if (ent === 0) {
                    entcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT', 'manger': mangers });
                    ent = entcount.length;
                }

                var intcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT', 'orgDesc': 'Project', 'manger': mangers });
                var int = intcount.length;
                if (ent === 0) {
                    intcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT', 'manger': mangers });
                    int = intcount.length;
                }

                var sencount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN', 'orgDesc': 'Project', 'manger': mangers });
                var sen = sencount.length;
                if (sen === 0) {
                    sencount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN', 'manger': mangers });
                    sen = sencount.length;
                }

                var bascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'BAS Scope needs to be updated', 'orgDesc': 'Project', 'manger': mangers });
                var bas = bascount.length;
                if (bas === 0) {
                    bascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'BAS Scope needs to be updated', 'manger': mangers });
                    bas = bascount.length;
                }

                var dircount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'DIR', 'orgDesc': 'Project', 'manger': mangers });
                var dir = dircount.length;
                if (dir === 0) {
                    dircount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'DIR', 'manger': mangers });
                    dir = dircount.length;
                }

                var expcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'EXP', 'orgDesc': 'Project', 'manger': mangers });
                var exp = expcount.length;
                if (exp === 0) {
                    expcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'EXP', 'manger': mangers });
                    exp = expcount.length;
                }

                var mascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MAS', 'orgDesc': 'Project', 'manger': mangers });
                var mas = mascount.length;
                if (mas === 0) {
                    mascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MAS', 'manger': mangers });
                    mas = mascount.length;
                }

                var mg1count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG1', 'orgDesc': 'Project', 'manger': mangers });
                var mg1 = mg1count.length;
                if (mg1 === 0) {
                    mg1count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG1', 'manger': mangers });
                    mg1 = mg1count.length;
                }

                var mg2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG2', 'orgDesc': 'Project', 'manger': mangers });
                var mg2 = mg2count.length;
                if (mg2 === 0) {
                    mg2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG2', 'manger': mangers });
                    mg2 = mg2count.length;
                }

                var specount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SPE', 'orgDesc': 'Project', 'manger': mangers });
                var spe = specount.length;
                if (spe === 0) {
                    specount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SPE', 'manger': mangers });
                    spe = specount.length;
                }

                var su2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SU2', 'orgDesc': 'Project', 'manger': mangers });
                var su2 = su2count.length;
                if (su2 === 0) {
                    su2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SU2', 'manger': mangers });
                    su2 = su2count.length;
                }

                var vpcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'VP', 'orgDesc': 'Project', 'manger': mangers });
                var vp = vpcount.length;
                if (vp === 0) {
                    vpcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'VP', 'manger': mangers });
                    vp = vpcount.length;
                }

                var regularcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'orgDesc': 'Project', 'manger': mangers });
                var regular = regularcount.length;
                if (regular === 0) {
                    regularcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'manger': mangers });
                    regular = regularcount.length;
                }

                var cwfcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C', 'orgDesc': 'Project', 'manger': mangers });
                var cwf = cwfcount.length;
                if (cwf === 0) {
                    cwfcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C', 'manger': mangers });
                    cwf = cwfcount.length;
                }

                var benchcount = _.filter(LTFilter, function (bench) {
                    return ((_.includes(['Bench', 'Partial Bench'], bench.allocationStatus)) && (_.includes([mangers], bench.manger)));
                });
                if (!(benchcount.length === 0 || total === 0)) {
                    var bench = (benchcount.length / total) * 100;
                }
                else {
                    bench = 0;
                }
                var utilcount = _.filter(LTFilter, function (util) {
                    return ((_.includes(['Fully Allocated', 'Partially Allocated', 'Over Allocated'], util.allocationStatus)) && (_.includes([mangers], util.manger)));
                });
                if (!(utilcount.length === 0 || total === 0)) {
                    var util = (utilcount.length / total) * 100;
                }
                else {
                    util = 0;
                }

                if (!((adv + cor + ent + int + sen) === 0 || regular === 0)) {
                    var lpi = ((adv + cor + ent + int + sen) / regular) * 100;
                }
                else {
                    lpi = 0;
                }

                var socrccount = _.filter(LTFilter, function (soc) {
                    return ((_.includes(['MG1', 'MG2'], soc.jobLevel)) && _.includes([mangers], soc.manger));
                });
                if (!(total === 0 || socrccount.length === 0)) {
                    var socrc = total / socrccount.length;
                }
                else {
                    socrc = 0;
                }
                var mg = socrccount.length
                if (!(regular === 0 || (mg1 + mg2) === 0)) {
                    var socr = regular / (mg1 + mg2);
                }
                else {
                    socr = 0;
                }
                var mgr = mg1 + mg2;

                var fullycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Fully Allocated', 'manger': mangers });
                var fully = fullycount.length;
                if (fully === 0) {
                    fullycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Fully Allocated', 'manger': mangers });
                    fully = fullycount.length;
                }

                var partiallycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Partially Allocated', 'manger': mangers });
                var partially = partiallycount.length;
                if (partially === 0) {
                    partiallycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Partially Allocated', 'manger': mangers });
                    partially = partiallycount.length;
                }

                var overcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Over Allocated', 'manger': mangers });
                var over = overcount.length;
                if (over === 0) {
                    overcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Over Allocated', 'manger': mangers });
                    over = overcount.length;
                }

                var sharedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Shared Services', 'manger': mangers });
                var shared = sharedcount.length;
                if (shared === 0) {
                    sharedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Shared Services', 'manger': mangers });
                    shared = sharedcount.length;
                }

                var operationsohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Operations OH', 'manger': mangers });
                var operationsoh = operationsohcount.length;
                if (operationsoh === 0) {
                    operationsohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Operations OH', 'manger': mangers });
                    operationsoh = operationsohcount.length;
                }

                var businessfundedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Business Funded', 'manger': mangers });
                var businessfunded = businessfundedcount.length;
                if (businessfunded === 0) {
                    businessfundedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Business Funded', 'manger': mangers });
                    businessfunded = businessfundedcount.length;
                }

                var campuscount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Campus', 'manger': mangers });
                var campus = campuscount.length;
                if (campus === 0) {
                    campuscount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Campus', 'manger': mangers });
                    campus = campuscount.length;
                }

                var deliveryohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Delivery OH', 'manger': mangers });
                var deliveryoh = deliveryohcount.length;
                if (deliveryoh === 0) {
                    deliveryohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Delivery OH', 'manger': mangers });
                    deliveryoh = deliveryohcount.length;
                }

                var longleavecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Long Leave', 'manger': mangers });
                var longleave = longleavecount.length;
                if (longleave === 0) {
                    longleavecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Long Leave', 'manger': mangers });
                    longleave = longleavecount.length;
                }

                var supportcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Support', 'manger': mangers });
                var support = supportcount.length;
                if (support === 0) {
                    supportcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Support', 'manger': mangers });
                    support = supportcount.length;
                }

                var nongdicount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Non GDI', 'manger': mangers });
                var nongdi = nongdicount.length;
                if (nongdi === 0) {
                    nongdicount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Non GDI', 'manger': mangers });
                    nongdi = nongdicount.length;
                }

                var benchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Bench', 'manger': mangers });
                var benchfte = benchftecount.length;
                if (benchfte === 0) {
                    benchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Bench', 'manger': mangers });
                    benchfte = benchftecount.length;
                }

                var partialbenchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Partial Bench', 'manger': mangers });
                var partialbenchfte = partialbenchftecount.length;
                if (partialbenchfte === 0) {
                    partialbenchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Partial Bench', 'manger': mangers });
                    partialbenchfte = partialbenchftecount.length;
                }

                var benchthirtycount = _.filter(LTFilter, function (benthirty) {
                    return ((_.includes(['Active'], benthirty.activeInactive) &&
                        _.includes(['Project'], benthirty.orgDesc) &&
                        (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                       _.includes(['0-30'], benthirty.benchAgeing) &&
                        _.includes([mangers], benthirty.manger)));
                });
                var benchthirty = benchthirtycount.length;

                var benchsixtycount = _.filter(LTFilter, function (benthirty) {
                    return ((_.includes(['Active'], benthirty.activeInactive) &&
                        _.includes(['Project'], benthirty.orgDesc) &&
                        (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                       _.includes(['31-60'], benthirty.benchAgeing) &&
                        _.includes([mangers], benthirty.manger)));
                });
                var benchsixty = benchsixtycount.length;

                var benchnintycount = _.filter(LTFilter, function (benthirty) {
                    return ((_.includes(['Active'], benthirty.activeInactive) &&
                        _.includes(['Project'], benthirty.orgDesc) &&
                        (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                       _.includes(['61-90'], benthirty.benchAgeing) &&
                        _.includes([mangers], benthirty.manger)));
                });
                var benchninty = benchnintycount.length;

                var benchnintypluscount = _.filter(LTFilter, function (benthirty) {
                    return ((_.includes(['Active'], benthirty.activeInactive) &&
                        _.includes(['Project'], benthirty.orgDesc) &&
                        (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                       _.includes(['90+'], benthirty.benchAgeing) &&
                        _.includes([mangers], benthirty.manger)));
                });
                var benchnintyplus = benchnintypluscount.length;

                var findquarter = _.filter($scope.quarters, function (data) {
                    return (_.includes([$scope.oprMonthSelected], data.month1)) ||
                        (_.includes([$scope.oprMonthSelected], data.month2)) ||
                        (_.includes([$scope.oprMonthSelected], data.month3));
                });
                $scope.displayquarter = findquarter[0].quarter;
                $scope.displaymonth1 = findquarter[0].month1;
                $scope.displaymonth2 = findquarter[0].month2;
                $scope.displaymonth3 = findquarter[0].month3;

                var doj = findquarter[0].id1 + '/' + $scope.oprYearSelected;
                var fresherminuscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj, 'manger': mangers });
                var fresherminus = fresherminuscount.length;
                if (fresherminus === 0) {
                    fresherminuscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj, 'manger': mangers });
                    fresherminus = fresherminuscount.length;
                }
                var doj1 = findquarter[0].id2 + '/' + $scope.oprYearSelected;
                var freshercount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj1, 'manger': mangers });
                var fresher = freshercount.length;
                if (fresher === 0) {
                    freshercount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj1, 'manger': mangers });
                    fresher = freshercount.length;
                }
                if (findquarter[0].id3 === '01') {
                    var year = parseInt($scope.oprYearSelected);
                    var doj2 = findquarter[0].id3 + '/' + (year + 1);
                    $scope.diffyear = (year + 1);
                }
                else {
                    var doj2 = findquarter[0].id3 + '/' + $scope.oprYearSelected;
                }
                var fresherpluscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj2, 'manger': mangers });
                var fresherplus = fresherpluscount.length;
                if (fresherplus === 0) {
                    fresherpluscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj2, 'manger': mangers });
                    fresherplus = fresherpluscount.length;
                }

                var doj3 = findquarter[0].id1 + '/' + $scope.oprYearSelected;
                var lateralminuscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj3, 'manger': mangers });
                var lateralminus = fresherpluscount.length;
                if (lateralminus === 0) {
                    lateralminuscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj3, 'manger': mangers });
                    lateralminus = lateralminuscount.length;
                }

                var doj4 = findquarter[0].id2 + '/' + $scope.oprYearSelected;
                var lateralcount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj4, 'manger': mangers });
                var lateral = lateralcount.length;
                if (lateral === 0) {
                    lateralcount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj4, 'manger': mangers });
                    lateral = lateralcount.length;
                }

                if (findquarter[0].id3 === '01') {
                    var year = parseInt($scope.oprYearSelected);
                    var doj5 = findquarter[0].id3 + '/' + (year + 1);
                    $scope.diffyear = (year + 1);
                }
                else {
                    var doj5 = findquarter[0].id3 + '/' + $scope.oprYearSelected;
                }
                var lateralpluscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj5, 'manger': mangers });
                var lateralplus = lateralpluscount.length;
                if (lateralplus === 0) {
                    lateralpluscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj5, 'manger': mangers });
                    lateralplus = lateralpluscount.length;
                }

                var freshertotal = fresherminus + fresher + fresherplus;
                var lateraltotal = lateralminus + lateral + lateralplus;

                var mrucount = _.filter(LTFilter, function (mrudata) {
                    //'mrunoncomp': 'Non-Compliance', 'orgDesc': 'Project', 'activeInactive': 'Active', 'manger': mangers });'
                    return (_.includes(((['Non-Compliance, D263 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                        _.includes(['Non-Compliance, D268 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                        _.includes(['Non-Compliance, D277 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                        _.includes(['Non-Compliance, E912 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                        _.includes(['Non-Compliance, E917 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp)) &&
                        ['Project'], mrudata.orgDesc
                        && [mangers], mrudata.manger));
                });
                var mru = mrucount.length;
                if (mru === 0) {
                    mrucount = _.filter(LTFilter, function (dat) {
                        //'mrunoncomp': 'Non-Compliance', 'activeInactive': 'Active', 'manger': mangers 
                        return (_.includes(((['Non-Compliance, D263 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                        _.includes(['Non-Compliance, D268 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                        _.includes(['Non-Compliance, D277 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                        _.includes(['Non-Compliance, E912 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                        _.includes(['Non-Compliance, E917 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp)) &&
                        [mangers], dat.manger));
                    });
                    mru = mrucount.length;
                }

                var onsitebenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'environmentlttstt': 'LTT', 'allocationStatus': 'Bench', 'manger': mangers });
                var onsitebench = onsitebenchcount.length;
                if (onsitebench === 0) {
                    onsitebenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'environmentlttstt': 'LTT', 'allocationStatus': 'Bench', 'manger': mangers });
                    onsitebench = onsitebenchcount.length;
                }

                var cwfbenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'regcwf': 'C', 'allocationStatus': 'Bench', 'manger': mangers });
                var cwfbench = cwfbenchcount.length;
                if (cwfbench === 0) {
                    cwfbenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C', 'allocationStatus': 'Bench', 'manger': mangers });
                    cwfbench = cwfbenchcount.length;
                }

                var lhccbbcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'lhccvsmobility': 'Non Compliance/No DATA Available', 'manger': mangers });
                var lhccbb = lhccbbcount.length;
                if (lhccbb === 0) {
                    lhccbbcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'lhccvsmobility': 'Non Compliance/No DATA Available', 'manger': mangers });
                    lhccbb = lhccbbcount.length;
                }

                var sabacount = _.filter(LTFilter, { 'orgDesc': 'Project', 'sabacompliance': 'Non-Compliant', 'manger': mangers });
                var saba = sabacount.length;
                if (saba === 0) {
                    sabacount = _.filter(LTFilter, { 'sabacompliance': 'Non-Compliant', 'manger': mangers });
                    saba = sabacount.length;
                }

                if (mangers === 'ADM (Total)' || mangers === 'SAP (Total)') {
                    var total = LTFilter.length;

                    var attritionlen = _.filter(LTFilter, { 'activeInactive': 'Terminated' });
                    var attrition = attritionlen.length;

                    var loalen = _.filter(LTFilter, { 'activeInactive': 'Leave of Absence' });
                    var loa = loalen.length;

                    var lwplen = _.filter(LTFilter, { 'activeInactive': 'Leave With Pay' });
                    var lwp = lwplen.length;

                    var advcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV', 'orgDesc': 'Project' });
                    var adv = advcount.length;
                    if (adv === 0) {
                        advcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV' });
                        adv = advcount.length;
                    }

                    var corcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR', 'orgDesc': 'Project' });
                    var cor = corcount.length;
                    if (cor === 0) {
                        corcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR' });
                        cor = corcount.length;
                    }

                    var entcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT', 'orgDesc': 'Project' });
                    var ent = entcount.length;
                    if (ent === 0) {
                        entcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT' });
                        ent = entcount.length;
                    }

                    var intcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT', 'orgDesc': 'Project' });
                    var int = intcount.length;
                    if (ent === 0) {
                        intcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT' });
                        int = intcount.length;
                    }

                    var sencount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN', 'orgDesc': 'Project' });
                    var sen = sencount.length;
                    if (sen === 0) {
                        sencount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN' });
                        sen = sencount.length;
                    }

                    var bascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'BAS Scope needs to be updated', 'orgDesc': 'Project' });
                    var bas = bascount.length;
                    if (bas === 0) {
                        bascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'BAS Scope needs to be updated' });
                        bas = bascount.length;
                    }

                    var dircount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'DIR', 'orgDesc': 'Project' });
                    var dir = dircount.length;
                    if (dir === 0) {
                        dircount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'DIR' });
                        dir = dircount.length;
                    }

                    var expcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'EXP', 'orgDesc': 'Project' });
                    var exp = expcount.length;
                    if (exp === 0) {
                        expcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'EXP' });
                        exp = expcount.length;
                    }

                    var mascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MAS', 'orgDesc': 'Project' });
                    var mas = mascount.length;
                    if (mas === 0) {
                        mascount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MAS' });
                        mas = mascount.length;
                    }

                    var mg1count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG1', 'orgDesc': 'Project' });
                    var mg1 = mg1count.length;
                    if (mg1 === 0) {
                        mg1count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG1' });
                        mg1 = mg1count.length;
                    }

                    var mg2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG2', 'orgDesc': 'Project' });
                    var mg2 = mg2count.length;
                    if (mg2 === 0) {
                        mg2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG2' });
                        mg2 = mg2count.length;
                    }

                    var specount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SPE', 'orgDesc': 'Project' });
                    var spe = specount.length;
                    if (spe === 0) {
                        specount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SPE' });
                        spe = specount.length;
                    }

                    var su2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SU2', 'orgDesc': 'Project' });
                    var su2 = su2count.length;
                    if (su2 === 0) {
                        su2count = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SU2' });
                        su2 = su2count.length;
                    }

                    var vpcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'VP', 'orgDesc': 'Project' });
                    var vp = vpcount.length;
                    if (vp === 0) {
                        vpcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'VP' });
                        vp = vpcount.length;
                    }

                    var regularcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R', 'orgDesc': 'Project' });
                    var regular = regularcount.length;
                    if (regular === 0) {
                        regularcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'R' });
                        regular = regularcount.length;
                    }

                    var cwfcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C', 'orgDesc': 'Project' });
                    var cwf = cwfcount.length;
                    if (cwf === 0) {
                        cwfcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C' });
                        cwf = cwfcount.length;
                    }

                    var benchcount = _.filter(LTFilter, function (bench) {
                        return ((_.includes(['Bench', 'Partial Bench'], bench.allocationStatus)));
                    });
                    if (!(benchcount.length === 0 || total === 0)) {
                        var bench = (benchcount.length / total) * 100;
                    }
                    else {
                        bench = 0;
                    }
                    var utilcount = _.filter(LTFilter, function (util) {
                        return ((_.includes(['Fully Allocated', 'Partially Allocated', 'Over Allocated'], util.allocationStatus)));
                    });
                    if (!(utilcount.length === 0 || total === 0)) {
                        var util = (utilcount.length / total) * 100;
                    }
                    else {
                        util = 0;
                    }

                    if (!((adv + cor + ent + int + sen) === 0 || regular === 0)) {
                        var lpi = ((adv + cor + ent + int + sen) / regular) * 100;
                    }
                    else {
                        lpi = 0;
                    }

                    var socrccount = _.filter(LTFilter, function (soc) {
                        return ((_.includes(['MG1', 'MG2'], soc.jobLevel)));
                    });
                    if (!(total === 0 || socrccount.length === 0)) {
                        var socrc = total / socrccount.length;
                    }
                    else {
                        socrc = 0;
                    }
                    var mg = socrccount.length
                    if (!(regular === 0 || (mg1 + mg2) === 0)) {
                        var socr = regular / (mg1 + mg2);
                    }
                    else {
                        socr = 0;
                    }
                    var mgr = mg1 + mg2;

                    var fullycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Fully Allocated' });
                    var fully = fullycount.length;
                    if (fully === 0) {
                        fullycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Fully Allocated' });
                        fully = fullycount.length;
                    }

                    var partiallycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Partially Allocated' });
                    var partially = partiallycount.length;
                    if (partially === 0) {
                        partiallycount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Partially Allocated' });
                        partially = partiallycount.length;
                    }

                    var overcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Over Allocated' });
                    var over = overcount.length;
                    if (over === 0) {
                        overcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Over Allocated' });
                        over = overcount.length;
                    }

                    var sharedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Shared Services' });
                    var shared = sharedcount.length;
                    if (shared === 0) {
                        sharedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Shared Services' });
                        shared = sharedcount.length;
                    }

                    var operationsohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Operations OH' });
                    var operationsoh = operationsohcount.length;
                    if (operationsoh === 0) {
                        operationsohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Operations OH' });
                        operationsoh = operationsohcount.length;
                    }

                    var businessfundedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Business Funded' });
                    var businessfunded = businessfundedcount.length;
                    if (businessfunded === 0) {
                        businessfundedcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Business Funded' });
                        businessfunded = businessfundedcount.length;
                    }

                    var campuscount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Campus' });
                    var campus = campuscount.length;
                    if (campus === 0) {
                        campuscount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Campus' });
                        campus = campuscount.length;
                    }

                    var deliveryohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Delivery OH' });
                    var deliveryoh = deliveryohcount.length;
                    if (deliveryoh === 0) {
                        deliveryohcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Delivery OH' });
                        deliveryoh = deliveryohcount.length;
                    }

                    var longleavecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Long Leave' });
                    var longleave = longleavecount.length;
                    if (longleave === 0) {
                        longleavecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Long Leave' });
                        longleave = longleavecount.length;
                    }

                    var supportcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Support' });
                    var support = supportcount.length;
                    if (support === 0) {
                        supportcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Support' });
                        support = supportcount.length;
                    }

                    var nongdicount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Non GDI' });
                    var nongdi = nongdicount.length;
                    if (nongdi === 0) {
                        nongdicount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Non GDI' });
                        nongdi = nongdicount.length;
                    }

                    var benchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Bench' });
                    var benchfte = benchftecount.length;
                    if (benchfte === 0) {
                        benchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Bench' });
                        benchfte = benchftecount.length;
                    }

                    var partialbenchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'allocationStatus': 'Partial Bench' });
                    var partialbenchfte = partialbenchftecount.length;
                    if (partialbenchfte === 0) {
                        partialbenchftecount = _.filter(LTFilter, { 'activeInactive': 'Active', 'allocationStatus': 'Partial Bench' });
                        partialbenchfte = partialbenchftecount.length;
                    }

                    var benchthirtycount = _.filter(LTFilter, function (benthirty) {
                        return ((_.includes(['Active'], benthirty.activeInactive) &&
                            _.includes(['Project'], benthirty.orgDesc) &&
                            (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                           _.includes(['0-30'], benthirty.benchAgeing)));
                    });
                    var benchthirty = benchthirtycount.length;

                    var benchsixtycount = _.filter(LTFilter, function (benthirty) {
                        return ((_.includes(['Active'], benthirty.activeInactive) &&
                            _.includes(['Project'], benthirty.orgDesc) &&
                            (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                           _.includes(['31-60'], benthirty.benchAgeing)));
                    });
                    var benchsixty = benchsixtycount.length;

                    var benchnintycount = _.filter(LTFilter, function (benthirty) {
                        return ((_.includes(['Active'], benthirty.activeInactive) &&
                            _.includes(['Project'], benthirty.orgDesc) &&
                            (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                           _.includes(['61-90'], benthirty.benchAgeing)));
                    });
                    var benchninty = benchnintycount.length;

                    var benchnintypluscount = _.filter(LTFilter, function (benthirty) {
                        return ((_.includes(['Active'], benthirty.activeInactive) &&
                            _.includes(['Project'], benthirty.orgDesc) &&
                            (_.includes(['Bench'], benthirty.allocationStatus) || _.includes(['Partial Bench'], benthirty.allocationStatus)) &&
                           _.includes(['90+'], benthirty.benchAgeing)));
                    });
                    var benchnintyplus = benchnintypluscount.length;

                    var findquarter = _.filter($scope.quarters, function (data) {
                        return (_.includes([$scope.oprMonthSelected], data.month1)) ||
                            (_.includes([$scope.oprMonthSelected], data.month2)) ||
                            (_.includes([$scope.oprMonthSelected], data.month3));
                    });
                    $scope.displayquarter = findquarter[0].quarter;
                    $scope.displaymonth1 = findquarter[0].month1;
                    $scope.displaymonth2 = findquarter[0].month2;
                    $scope.displaymonth3 = findquarter[0].month3;

                    var doj = findquarter[0].id1 + '/' + $scope.oprYearSelected;
                    var fresherminuscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj });
                    var fresherminus = fresherminuscount.length;
                    if (fresherminus === 0) {
                        fresherminuscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj });
                        fresherminus = fresherminuscount.length;
                    }
                    var doj1 = findquarter[0].id2 + '/' + $scope.oprYearSelected;
                    var freshercount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj1 });
                    var fresher = freshercount.length;
                    if (fresher === 0) {
                        freshercount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj1 });
                        fresher = freshercount.length;
                    }
                    if (findquarter[0].id3 === '01') {
                        var year = parseInt($scope.oprYearSelected);
                        var doj2 = findquarter[0].id3 + '/' + (year + 1);
                        $scope.diffyear = (year + 1);
                    }
                    else {
                        var doj2 = findquarter[0].id3 + '/' + $scope.oprYearSelected;
                    }
                    var fresherpluscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj2 });
                    var fresherplus = fresherpluscount.length;
                    if (fresherplus === 0) {
                        fresherpluscount = _.filter(LTFilter, { 'freshersLaterals': 'Fresher', 'activeInactive': 'Active', 'doj': doj2 });
                        fresherplus = fresherpluscount.length;
                    }

                    var doj3 = findquarter[0].id1 + '/' + $scope.oprYearSelected;
                    var lateralminuscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj3 });
                    var lateralminus = fresherpluscount.length;
                    if (lateralminus === 0) {
                        lateralminuscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj3 });
                        lateralminus = lateralminuscount.length;
                    }

                    var doj4 = findquarter[0].id2 + '/' + $scope.oprYearSelected;
                    var lateralcount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj4 });
                    var lateral = lateralcount.length;
                    if (lateral === 0) {
                        lateralcount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj4 });
                        lateral = lateralcount.length;
                    }

                    if (findquarter[0].id3 === '01') {
                        var year = parseInt($scope.oprYearSelected);
                        var doj5 = findquarter[0].id3 + '/' + (year + 1);
                        $scope.diffyear = (year + 1);
                    }
                    else {
                        var doj5 = findquarter[0].id3 + '/' + $scope.oprYearSelected;
                    }
                    var lateralpluscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'orgDesc': 'Project', 'doj': doj5 });
                    var lateralplus = lateralpluscount.length;
                    if (lateralplus === 0) {
                        lateralpluscount = _.filter(LTFilter, { 'freshersLaterals': 'lateral', 'activeInactive': 'Active', 'doj': doj5 });
                        lateralplus = lateralpluscount.length;
                    }

                    var freshertotal = fresherminus + fresher + fresherplus;
                    var lateraltotal = lateralminus + lateral + lateralplus;

                    var mrucount = _.filter(LTFilter, function (mrudata) {
                        //'mrunoncomp': 'Non-Compliance', 'orgDesc': 'Project', 'activeInactive': 'Active', 'manger': mangers });'
                        return (_.includes(((['Non-Compliance, D263 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                            _.includes(['Non-Compliance, D268 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                            _.includes(['Non-Compliance, D277 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                            _.includes(['Non-Compliance, E912 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp) ||
                            _.includes(['Non-Compliance, E917 is assigned to Ramachandran, Prem Kumar'], mrudata.mrunoncomp)) &&
                            ['Project'], mrudata.orgDesc));
                    });
                    var mru = mrucount.length;
                    if (mru === 0) {
                        mrucount = _.filter(LTFilter, function (dat) {
                            //'mrunoncomp': 'Non-Compliance', 'activeInactive': 'Active', 'manger': mangers 
                            return (_.includes(((['Non-Compliance, D263 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                            _.includes(['Non-Compliance, D268 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                            _.includes(['Non-Compliance, D277 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                            _.includes(['Non-Compliance, E912 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp) ||
                            _.includes(['Non-Compliance, E917 is assigned to Ramachandran, Prem Kumar'], dat.mrunoncomp))));
                        });
                        mru = mrucount.length;
                    }

                    var onsitebenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'environmentlttstt': 'LTT', 'allocationStatus': 'Bench' });
                    var onsitebench = onsitebenchcount.length;
                    if (onsitebench === 0) {
                        onsitebenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'environmentlttstt': 'LTT', 'allocationStatus': 'Bench' });
                        onsitebench = onsitebenchcount.length;
                    }

                    var cwfbenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'regcwf': 'C', 'allocationStatus': 'Bench' });
                    var cwfbench = cwfbenchcount.length;
                    if (cwfbench === 0) {
                        cwfbenchcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'regcwf': 'C', 'allocationStatus': 'Bench' });
                        cwfbench = cwfbenchcount.length;
                    }

                    var lhccbbcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'orgDesc': 'Project', 'lhccvsmobility': 'Non Compliance/No DATA Available' });
                    var lhccbb = lhccbbcount.length;
                    if (lhccbb === 0) {
                        lhccbbcount = _.filter(LTFilter, { 'activeInactive': 'Active', 'lhccvsmobility': 'Non Compliance/No DATA Available' });
                        lhccbb = lhccbbcount.length;
                    }

                    var sabacount = _.filter(LTFilter, { 'orgDesc': 'Project', 'sabacompliance': 'Non-Compliant' });
                    var saba = sabacount.length;
                    if (saba === 0) {
                        sabacount = _.filter(LTFilter, { 'sabacompliance': 'Non-Compliant' });
                        saba = sabacount.length;
                    }
                }

                var record = {
                    'total': total,
                    'manger': mangers,
                    'attrition': attrition,
                    'loa': loa,
                    'lwp': lwp,
                    'adv': adv,
                    'cor': cor,
                    'ent': ent,
                    'int': int,
                    'sen': sen,
                    'bas': bas,
                    'dir': dir,
                    'exp': exp,
                    'mas': mas,
                    'mg1': mg1,
                    'mg2': mg2,
                    'spe': spe,
                    'su2': su2,
                    'vp': vp,
                    'regular': regular,
                    'cwf': cwf,
                    'bench': bench,
                    'util': util,
                    'lpi': lpi,
                    'socrc': socrc,
                    'mg': mg,
                    'socr': socr,
                    'mgr': mgr,
                    'fully': fully,
                    'partially': partially,
                    'over': over,
                    'shared': shared,
                    'operationsoh': operationsoh,
                    'businessfunded': businessfunded,
                    'campus': campus,
                    'deliveryoh': deliveryoh,
                    'longleave': longleave,
                    'support': support,
                    'nongdi': nongdi,
                    'benchfte': benchfte,
                    'partialbenchfte': partialbenchfte,
                    'benchthirty': benchthirty,
                    'benchsixty': benchsixty,
                    'benchninty': benchninty,
                    'benchnintyplus': benchnintyplus,
                    'fresherminus': fresherminus,
                    'fresher': fresher,
                    'fresherplus': fresherplus,
                    'lateralminus': lateralminus,
                    'lateral': lateral,
                    'lateralplus': lateralplus,
                    'freshertotal': freshertotal,
                    'lateraltotal': lateraltotal,
                    'mru': mru,
                    'onsitebench': onsitebench,
                    'cwfbench': cwfbench,
                    'lhccbb': lhccbb,
                    'saba': saba
                };
                
                mangerwiserecordprmnth.push(record);

            });

            $scope.displayprmnth = mangerwiserecordprmnth;
            
            $scope.comparison();
          
        });
        /* jshint ignore:start 
        promise.error(function (response) {
             TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };

    //End of getting previous month data

    $scope.comparison=function(){

        _.forEach($scope.display, function (loopmgr) {
            var prevmonthrec = _.find($scope.displayprmnth, function (item) {
                return item.manger === loopmgr.manger;
            });

            //Arrow up down for total head count
            if (!_.isUndefined(prevmonthrec) && !_.isUndefined(prevmonthrec.total) && !_.isUndefined(loopmgr.total)) {
                if (loopmgr.total > prevmonthrec.total) {
                    loopmgr.forecastHC = 'high';
                }
                else {
                    loopmgr.forecastHC = 'low';
                }
            }

            //Arrow up down for bench
            if (!_.isUndefined(prevmonthrec) && !_.isUndefined(prevmonthrec.bench) && !_.isUndefined(loopmgr.bench)) {
                if (loopmgr.bench > prevmonthrec.bench) {
                    loopmgr.forecastBENCH = 'high';
                }
                else {
                    loopmgr.forecastBENCH = 'low';
                }
            }

            //Arrow up down for utilization
            if (!_.isUndefined(prevmonthrec) && !_.isUndefined(prevmonthrec.util) && !_.isUndefined(loopmgr.util)) {
                if (loopmgr.util > prevmonthrec.util) {
                    loopmgr.forecastUTIL = 'high';
                }
                else {
                    loopmgr.forecastUTIL = 'low';
                }
            }

            //Arrow up down for LPI
            if (!_.isUndefined(prevmonthrec) && !_.isUndefined(prevmonthrec.lpi) && !_.isUndefined(loopmgr.lpi)) {
                if (loopmgr.lpi > prevmonthrec.lpi) {
                    loopmgr.forecastLPI = 'high';
                }
                else {
                    loopmgr.forecastLPI = 'low';
                }
            }
            

        });

   };

    $scope.defaultLoad = function () {
       
        var d = new Date();
        // TODO -- $scope.oprMonthSelected = $scope.months[d.getMonth()].month;
        $scope.oprMonthSelected = 'APR';
        $scope.oprYearSelected = (d.getFullYear()).toString();
        $scope.selServiceLine = 'ADM';
        $scope.selectedMonth = $scope.oprMonthSelected;
        $scope.selectedYear = $scope.oprYearSelected;
        $scope.selectedserviceline = $scope.selServiceLine;
        $scope.getOperationalDetails();
        
        $timeout(function () { $scope.show = true; }, 1000);
        $scope.monthsel = true;
        $scope.yearsel = true;
        $scope.servicesel = true;
       
    };
    $scope.defaultLoad();

    $scope.showTrend = function (mgr, typ) {
        
        OperationalDashboardService.getManagerOperational({
            manager: mgr,
            year: $scope.selectedYear
        })
        .$promise
        .then(function (mgrdata) {
            ManagerDetailService.setOperationalDetails(
                    {
                        mangeroperational: mgrdata,
                        allmanagers: $scope.allMangers,
                        allmanagersoperation: $scope.overallData,
                        servicelinetype : $scope.selServiceLine,
                        calculationtyp: typ,
                        manager: mgr,
                        year: $scope.selectedYear
                    }
                );
            $state.go('overview.operational.dashboard.details');
        });
        
    };

});