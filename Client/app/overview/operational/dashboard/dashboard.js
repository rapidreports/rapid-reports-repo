﻿'use strict';

angular.module('RapidReportsModule')
    .run(function ($rootScope, $state, $log) {
        $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            if (toState.name === 'overview.operational.dashboard') {
                $log.debug('Auto-transitioning state :: dashboard -> dashboard.main');
                $state.go('overview.operational.dashboard.main');
            }
        });
    })

.config(function ($stateProvider) {
    $stateProvider
    .state('overview.operational.dashboard', {
        url: '/dashboard',
        template: '<div ui-view></div>'
    })
    .state('overview.operational.dashboard.main', {
        url: '/',
        templateUrl: 'client/app/overview/operational/dashboard/dashboard.html',
        controller: 'OperationalDashboardCtrl'
    });
});