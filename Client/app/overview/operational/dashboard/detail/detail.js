﻿'use strict';

angular.module('RapidReportsModule')
.config(function ($stateProvider) {
    $stateProvider
    .state('overview.operational.dashboard.details', {
        url: '/details',
        templateUrl: 'client/app/overview/operational/dashboard/detail/detail.html',
        controller: 'ManagerDetailCtrl'
    });
});