﻿'use strict';

angular.module('RapidReportsModule')
.controller('ManagerDetailCtrl', function (
    $scope,
    ManagerDetailService,
    OperationalDashboardService,
    DateHelper,
    AppDefault,
    $state,
    $cookies,
    GenerateReportService,
    ExportService
    ) {
    var local = ManagerDetailService.getOperationalDetails();
    local.chartTypes = AppDefault.chartTypes;

    $scope.detail = {
        charts: {
            calculationTrend: []
        },
        chartOptions: {
            calculationTrendOption: {},
            calculationTrendCols: [],
            calculationTrendDataX: {},
            calculationTrendChartType: local.chartTypes.spline
        },
        dropdown: {
            managersdropdown: local.allmanagers,
            calculationsdropdown: [],
            yeardropdown: DateHelper.getYearArray(),
            selectedManager: '',
            selectedCalculation: '',
            selectedYear: 0
        }
    };

    
    //region events
    $scope.back = function () {
        //$state.go('operational.dashboard');
        $state.go($cookies.previousState);
    };
    $scope.listChanged = function (val) {
        fetchData();
    };
    //endregion

    //region other
    var listLoad = function () {
        $scope.detail.dropdown.selectedManager = local.manager;
        $scope.detail.dropdown.selectedCalculation = local.calculationtyp;
        $scope.detail.dropdown.selectedYear = local.year;
    }

    var chartOptions = function () {
        $scope.detail.chartOptions.calculationTrendCols.push({
            'id': local.calculationtyp,
            'type': $scope.detail.chartOptions.calculationTrendChartType,
            'name': local.calculationtyp
        });
        $scope.detail.chartOptions.calculationTrendDataX = { 'id': 'monthyear' };
    }

    var managerTrendChart = function () {
        var trendary = [];
        var filteredoverallData = [];

        var overalldata = local.allmanagersoperation;
        var serviceline = local.servicelinetype;
        
        if (serviceline === 'ADM') {
            filteredoverallData = _.filter(overalldata, function (ADM) {
                return (_.includes(['Ramachandran, Prem Kumar'], ADM.ltName) &&
                    !_.includes(['D270', 'D272'], ADM.mruCode));
            });
        }

        else if (serviceline === 'SAP') {
            filteredoverallData = _.filter(overalldata, function (SAP) {
                return (!_.includes(['Ramachandran, Prem Kumar'], SAP.ltName) &&
                    _.includes(['D270', 'D272'], SAP.mruCode));
            });
        }

        if (!_.isUndefined(local.mangeroperational)) {
            _.forEach(DateHelper.financialMonthsAry, function (loopmonth) {
                var monthobj = { count: 0, monthyear: '' };
                var bench = 0;
                var util = 0;
                var lpi = 0;
                var socrccount = 0;
                var adv = 0;
                var cor = 0;
                var ent = 0;
                var int = 0;
                var sen = 0;
                var bas = 0;
                var dir = 0;
                var exp = 0;
                var mas = 0;
                var mg1 = 0;
                var mg2 = 0;
                var spe = 0;
                var su2 = 0;
                var vp = 0;
                var regular = 0;
                var cwf = 0;


                var monthrecs = _.filter(local.mangeroperational, function (item) {
                    return item.month === loopmonth;
                });

                if (!_.isUndefined(monthrecs)) {
                    switch (local.calculationtyp) {
                        case 'TotalHC':
                            monthobj['TotalHC'] = monthrecs.length;
                            break;
                        case 'Attrition':
                            monthobj['Attrition'] = (_.filter(monthrecs, { 'activeInactive': 'Terminated' })).length;
                            break;
                        case 'Leave Of Absence':
                            monthobj['Leave Of Absence'] = (_.filter(monthrecs, { 'activeInactive': 'Leave of Absence' })).length;
                            break;
                        case 'Leave With Pay':
                            monthobj['Leave With Pay'] = (_.filter(monthrecs, { 'activeInactive': 'Leave With Pay' })).length;
                            break;
                        case 'ADV':
                            adv = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV', 'orgDesc': 'Project' })).length;
                            if (adv === 0) {
                                adv = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV'})).length;
                            }
                            monthobj['ADV'] = adv;
                            break;
                        case 'COR':
                            cor = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR', 'orgDesc': 'Project' })).length;
                            if (cor === 0) {
                                cor = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR'})).length;
                            }
                            monthobj['COR'] = cor;
                            break;
                        case 'ENT':
                            ent = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT', 'orgDesc': 'Project' })).length;
                            if (ent === 0) {
                                ent = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT' })).length;
                            }
                            monthobj['ENT'] = ent;
                            break;
                        case 'INT':
                            int = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT', 'orgDesc': 'Project' })).length;
                            if (int === 0) {
                                int = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT'})).length;
                            }
                            monthobj['INT'] = int;
                            break;
                        case 'SEN':
                            sen = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN', 'orgDesc': 'Project' })).length;
                            if (sen === 0) {
                                sen = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN'})).length;
                            }
                            monthobj['SEN'] = sen;
                            break;
                        case 'BAS':
                            bas = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'BAS Scope needs to be updated', 'orgDesc': 'Project' })).length;
                            if (bas === 0) {
                                bas = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'BAS Scope needs to be updated'})).length;
                            }
                            monthobj['BAS'] = bas;
                            break;
                        case 'DIR':
                            dir = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'DIR', 'orgDesc': 'Project' })).length;
                            if (dir === 0) {
                                dir = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'DIR'})).length;
                            }
                            monthobj['DIR'] = dir;
                            break;
                        case 'EXP':
                            exp = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'EXP', 'orgDesc': 'Project' })).length;
                            if (exp === 0) {
                                exp = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'EXP' })).length;
                            }
                            monthobj['EXP'] = exp;
                            break;
                        case 'MAS':
                            mas = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MAS', 'orgDesc': 'Project' })).length;
                            if (mas === 0) {
                                mas = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MAS' })).length;
                            }
                            monthobj['MAS'] = mas;
                            break;
                        case 'MG1':
                            mg1 = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG1', 'orgDesc': 'Project' })).length;
                            if (mg1 === 0) {
                                mg1 = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG1' })).length;
                            }
                            monthobj['MG1'] = mg1;
                            break;
                        case 'MG2':
                            mg2 = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG2', 'orgDesc': 'Project' })).length;
                            if (mg2 === 0) {
                                mg2 = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'MG2' })).length;
                            }
                            monthobj['MG2'] = mg2;
                            break;
                        case 'SPE':
                            spe = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SPE', 'orgDesc': 'Project' })).length;
                            if (spe === 0) {
                                spe = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SPE' })).length;
                            }
                            monthobj['SPE'] = spe;
                            break;
                        case 'SU2':
                            su2 = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SU2', 'orgDesc': 'Project' })).length;
                            if (su2 === 0) {
                                su2 = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SU2'})).length;
                            }
                            monthobj['SU2'] = su2;
                            break;
                        case 'VP':
                            vp = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'VP', 'orgDesc': 'Project' })).length;
                            if (vp === 0) {
                                vp = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'VP' })).length;
                            }
                            monthobj['VP'] = vp;
                            break;
                        case 'Regular':
                            regular = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'orgDesc': 'Project' })).length;
                            if (regular === 0) {
                                regular = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R' })).length;
                            }
                            monthobj['Regular'] = regular;
                            break;
                        case 'CWF':
                            cwf = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'C', 'orgDesc': 'Project' })).length;
                            if (cwf === 0) {
                                cwf = (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'C' })).length;
                            }
                            monthobj['CWF'] = cwf;
                            break;
                        case 'Bench %':
                            var benchcount = _.filter(monthrecs, function (bench) {
                                return ((_.includes(['Bench', 'Partial Bench'], bench.allocationStatus)));
                            });
                            if (!(benchcount.length === 0 || monthrecs.length === 0)) {
                                bench = (benchcount.length / monthrecs.length) *100;
                                bench = Math.round(bench * 100) / 100;

                            }
                            monthobj['Bench %'] = bench;
                            break;
                        case 'Utilization %':
                            var utilcount = _.filter(monthrecs, function (util) {
                                return ((_.includes(['Fully Allocated', 'Partially Allocated', 'Over Allocated'], util.allocationStatus)));
                            });
                            if (!(utilcount.length === 0 || monthrecs.length === 0)) {
                                util = (utilcount.length / monthrecs.length) * 100;
                                util = Math.round(util * 100) / 100;
                            }
                            monthobj['Utilization %'] = util;
                            break;
                        case 'LPI %':
                            lpi = (((_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ADV', 'orgDesc': 'Project' })).length +
                                (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'COR', 'orgDesc': 'Project' })).length +
                                (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'ENT', 'orgDesc': 'Project' })).length +
                                (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'INT', 'orgDesc': 'Project' })).length +
                                (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'jobLevel': 'SEN', 'orgDesc': 'Project' })).length));
                           // lpi = (lpi / (_.filter(monthrecs, { 'activeInactive': 'Active', 'regcwf': 'R', 'orgDesc': 'Project' })).length) * 100;

                            monthobj['LPI %'] = lpi;
                            break;
                        case 'Span Of Control (Reg & CWF)':
                            socrccount = _.filter(monthrecs, function (soc) {
                                return ((_.includes(['MG1', 'MG2'], soc.jobLevel)));
                            });
                            monthobj['Span Of Control (Reg & CWF)'] = socrccount.length;
                            break;
                           
                    }
                }
                if (loopmonth === 'Nov' || loopmonth === 'Dec') {
                    monthobj.monthyear = loopmonth + ' ' + ($scope.detail.dropdown.selectedYear - 1);
                }
                else {
                    monthobj.monthyear = loopmonth + ' ' + $scope.detail.dropdown.selectedYear;
                }

                trendary.push(monthobj);
            });
        }

        return trendary;
    }

    var dataLoad = function () {
        $scope.detail.charts.calculationTrend = managerTrendChart();
    }

    var fetchData = function () {
        OperationalDashboardService.getManagerOperational({
            manager: $scope.detail.dropdown.selectedManager,
            year: $scope.detail.dropdown.selectedYear
        })
        .$promise
        .then(function (mgrdata) {
            local.mangeroperational = mgrdata;
            dataLoad();
        });
    }

    var startupLoad = function () {
        listLoad();
        chartOptions();
        dataLoad();
    }

    startupLoad();
    //endregion

    //region export
    $scope.generateDetailReport = function () {
        var config = {};
        var chart = {};
        var reportProps = GenerateReportService.getReportProps();

        config.exportedFileName = 'chart';
        config.backgroundColor = '#ffffff';

        //!! service line financial year chart canvas
        chart = angular.element(document.getElementById('managertrend'));
        reportProps.pageitems.push({ item: ExportService.createChartImages(chart, config), type: 'chart' });
        reportProps.pageitemheaders.push('Trend for ' + $scope.detail.dropdown.selectedManager +' '+ $scope.detail.dropdown.selectedCalculation + ' for Financial Year ' + $scope.detail.dropdown.selectedYear);

        reportProps.reportname = 'Operational Detail Report';
        reportProps.hascharts = true;
        reportProps.hastables = false;

        GenerateReportService.setReportProps(reportProps);
        GenerateReportService.createReport();
    };
    //endregion

});