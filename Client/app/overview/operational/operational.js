﻿'use strict';

angular.module('RapidReportsModule')
    .run(function ($rootScope, $state, $log) {
        $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            if (toState.name === 'overview.operational') {
                $log.debug('Auto-transitioning state :: operational -> operational.main');
                $state.go('overview.operational.main');
            }
        });
    })
.config(function ($stateProvider) {
    $stateProvider
    .state('overview.operational', {
        url: '/operational',
        template: '<div ui-view></div>'
    })
    .state('overview.operational.main', {
        url: '/',
        templateUrl: 'client/app/overview/operational/operational.html',
        controller: 'OperationalCtrl'
    });
});