﻿RapidReportsModule.controller("VDFController", function ($scope, $http, $state) {



    $scope.VDFTile = true;
    $scope.VDFSummary = false;
    $scope.EffortSaved = false;
    $scope.CostSaved = false;
    $scope.CostSavedForCustomer = false;
    $scope.IncidentReductionMonth = false;
    $scope.MajorIncidentReductionMonth = false;
    $scope.CSIPDetailsGrid = false;
    $scope.CurrentStatusGrid = false;
    $scope.QuartarlyChart = false;
    $scope.selectedAccount = 'All';

    $scope.ShowVDFgraph = function () {
        $state.go('overview.vdf');
    }

    $scope.graphload = function () {

    }

    $scope.GetVDFDashboardDetails = function () {
        var promise = $http({
            method: 'GET',
            url: '/app/GetVDFDashboardDetails/' + $scope.selectedAccount
        });
        promise.success(function (data) {
            $scope.VDFTotalProject = 0;
            $scope.VDFhpFundedHPProposed = 0;
            $scope.VDFhpFundedCustomerProposed = 0;
            $scope.VDFcustomerFundedHPProposed = 0;
            $scope.VDFcustomerFundedCustomerProposed = 0;
            $scope.TotalProjectEffortsSaved = 0;
            $scope.HPFundedHPProposedEffortsSaved = 0;
            $scope.HPFundedCustomerProposedEffortsSaved = 0;
            $scope.CustomerFundedHPProposedEffortsSaved = 0;
            $scope.CustomerFundedCustomerProposedEffortsSaved = 0;
            $scope.TotalProjectCostSaved = 0;
            $scope.HPFundedHPProposedCostSaved = 0;
            $scope.HPFundedCustomerProposedCostSaved = 0;
            $scope.CustomerFundedHPProposedCostSaved = 0;
            $scope.CustomerProposedCustomerFundedCostSaved = 0;
            $scope.TotalProjectCostSavedForCustomer = 0;
            $scope.HPFundedHPProposedCostSavedForCustomer = 0;
            $scope.HPFundedCustomerProposedCostSavedForCustomer = 0;
            $scope.CustomerFundedHPProposedCostSavedForCustomer = 0;
            $scope.CustomerProposedCustomerFundedCostSavedForCustomer = 0;
            $scope.TotalProjectIncidentReductionMonth = 0;
            $scope.HPFundedHPProposedIncidentReductionMonth = 0;
            $scope.HPFundedCustomerProposedIncidentReductionMonth = 0;
            $scope.CustomerFundedHPProposedIncidentReductionMonth = 0;
            $scope.CustomerFundedCustomerProposedIncidentReductionMonth = 0;
            $scope.TotalProjectMajorIncidentReductionMonth = 0;
            $scope.HPFundedHPProposedMajorIncidentReductionMonth = 0;
            $scope.HPFundedCustomerProposedMajorIncidentReductionMonth = 0;
            $scope.CustomerFundedHPProposedMajorIncidentReductionMonth = 0;
            $scope.CustomerFundedCustomerProposedMajorIncidentReductionMonth = 0;

            var chartobj1 = [];
            $scope.VDFhpFundedHPProposed;
            $scope.VDFhpFundedCustomerProposed;
            $scope.VDFcustomerFundedHPProposed;
            $scope.VDFcustomerFundedCustomerProposed;
            if (data != undefined) {

                $scope.AccountNames = data.accountNames;
                // VDF 

                $scope.VDFTotalProject = data.vdfRawDetails[0].totalProjects;
                $scope.VDFhpFundedHPProposed = data.vdfRawDetails[0].hpFundedHPProposed;
                $scope.VDFhpFundedCustomerProposed = data.vdfRawDetails[0].hpFundedCustomerProposed;
                $scope.VDFcustomerFundedHPProposed = data.vdfRawDetails[0].customerFundedHPProposed;
                $scope.VDFcustomerFundedCustomerProposed = data.vdfRawDetails[0].customerFundedCustomerProposed;

                var chart = c3.generate({
                    bindto: '#chart',
                    size: {
                        width: 750
                    },
                    data: {
                        columns: [
                          ['HP Proposed & Funded', $scope.VDFhpFundedHPProposed],
                          ['HP Proposed, Customer Funded', $scope.VDFhpFundedCustomerProposed],
                          ['Customer Proposed, HP Funded', $scope.VDFcustomerFundedHPProposed],
                          ['Customer Proposed & Funded', $scope.VDFcustomerFundedCustomerProposed]
                        ],
                        type: 'bar'
                    }
                });

                // Efforts Saved

                $scope.TotalProjectEffortsSaved = data.vdfRawDetails[1].totalProjects;
                $scope.HPFundedHPProposedEffortsSaved = data.vdfRawDetails[1].hpFundedHPProposed;
                $scope.HPFundedCustomerProposedEffortsSaved = data.vdfRawDetails[1].hpFundedCustomerProposed;
                $scope.CustomerFundedHPProposedEffortsSaved = data.vdfRawDetails[1].customerFundedHPProposed;
                $scope.CustomerFundedCustomerProposedEffortsSaved = data.vdfRawDetails[1].customerFundedCustomerProposed;

                var chart1 = c3.generate({
                    bindto: '#chart1',
                    size: {
                        width: 750
                    },
                    data: {
                        columns: [
                          ['HP Proposed & Funded', $scope.HPFundedHPProposedEffortsSaved],
                          ['HP Proposed, Customer Funded', $scope.CustomerFundedHPProposedEffortsSaved],
                          ['Customer Proposed, HP Funded', $scope.HPFundedCustomerProposedEffortsSaved],
                          ['Customer Proposed & Funded', $scope.CustomerFundedCustomerProposedEffortsSaved]
                        ],
                        type: 'bar'
                    }
                });
                // Cost Saved

               

                $scope.TotalProjectCostSaved = data.vdfRawDetails[2].totalProjects;
                $scope.HPFundedHPProposedCostSaved = data.vdfRawDetails[2].hpFundedHPProposed;
                $scope.HPFundedCustomerProposedCostSaved = data.vdfRawDetails[2].hpFundedCustomerProposed;
                $scope.CustomerFundedHPProposedCostSaved = data.vdfRawDetails[2].customerFundedHPProposed;
                $scope.CustomerProposedCustomerFundedCostSaved = data.vdfRawDetails[2].customerFundedCustomerProposed;

                var chart2 = c3.generate({
                    bindto: '#chart2',
                    size: {
                        width: 750
                    },
                    data: {
                        columns: [
                          ['HP Proposed & Funded', $scope.HPFundedHPProposedCostSaved],
                          ['HP Proposed, Customer Funded', $scope.CustomerFundedHPProposedCostSaved],
                          ['Customer Proposed, HP Funded', $scope.HPFundedCustomerProposedCostSaved],
                          ['Customer Proposed & Funded', $scope.CustomerProposedCustomerFundedCostSaved]
                        ],
                        type: 'bar'
                    }
                });
                //Cost Saved for Customer

                $scope.TotalProjectCostSavedForCustomer = data.vdfRawDetails[3].totalProjects;
                $scope.HPFundedHPProposedCostSavedForCustomer = data.vdfRawDetails[3].hpFundedHPProposed;
                $scope.HPFundedCustomerProposedCostSavedForCustomer = data.vdfRawDetails[3].hpFundedCustomerProposed;
                $scope.CustomerFundedHPProposedCostSavedForCustomer = data.vdfRawDetails[3].customerFundedHPProposed;
                $scope.CustomerProposedCustomerFundedCostSavedForCustomer = data.vdfRawDetails[3].customerFundedCustomerProposed;
                var chart3 = c3.generate({
                    bindto: '#chart3',
                    size: {
                        width: 750
                    },
                    data: {
                        columns: [
                          ['HP Proposed & Funded', $scope.HPFundedHPProposedCostSavedForCustomer],
                          ['HP Proposed, Customer Funded', $scope.CustomerFundedHPProposedCostSavedForCustomer],
                          ['Customer Proposed, HP Funded', $scope.HPFundedCustomerProposedCostSavedForCustomer],
                          ['Customer Proposed & Funded', $scope.CustomerProposedCustomerFundedCostSavedForCustomer]
                        ],
                        type: 'bar'
                    }
                });
                //Incident Reduction Month

                $scope.TotalProjectIncidentReductionMonth = data.vdfRawDetails[4].totalProjects;
                $scope.HPFundedHPProposedIncidentReductionMonth = data.vdfRawDetails[4].hpFundedHPProposed;
                $scope.HPFundedCustomerProposedIncidentReductionMonth = data.vdfRawDetails[4].hpFundedCustomerProposed;
                $scope.CustomerFundedHPProposedIncidentReductionMonth = data.vdfRawDetails[4].customerFundedHPProposed;
                $scope.CustomerFundedCustomerProposedIncidentReductionMonth = data.vdfRawDetails[4].customerFundedCustomerProposed;
                //$scope.CustomerFundedCustomerProposedIncidentReductionMonth = String(data.vfdRawDetails[4].customerFundedCustomerProposed);
                var chart4 = c3.generate({
                    bindto: '#chart4',
                    size: {
                        width: 750
                    },
                    data: {
                        columns: [
                          ['HP Proposed & Funded', $scope.HPFundedHPProposedIncidentReductionMonth],
                          ['HP Proposed, Customer Funded', $scope.CustomerFundedHPProposedIncidentReductionMonth],
                          ['Customer Proposed, HP Funded', $scope.HPFundedCustomerProposedIncidentReductionMonth],
                          ['Customer Proposed & Funded', $scope.CustomerFundedCustomerProposedIncidentReductionMonth]
                        ],
                        type: 'bar'
                    }
                });
                // Major Incident Reduction Month

                $scope.TotalProjectMajorIncidentReductionMonth = data.vdfRawDetails[5].totalProjects;
                $scope.HPFundedHPProposedMajorIncidentReductionMonth = data.vdfRawDetails[5].hpFundedHPProposed;
                $scope.HPFundedCustomerProposedMajorIncidentReductionMonth = data.vdfRawDetails[5].hpFundedCustomerProposed;
                $scope.CustomerFundedHPProposedMajorIncidentReductionMonth = data.vdfRawDetails[5].customerFundedHPProposed;
                $scope.CustomerFundedCustomerProposedMajorIncidentReductionMonth = data.vdfRawDetails[5].customerFundedCustomerProposed;
                var chart5 = c3.generate({
                    bindto: '#chart5',
                    size: {
                        width: 750
                    },
                    data: {
                        columns: [
                          ['HP Proposed & Funded', $scope.HPFundedHPProposedMajorIncidentReductionMonth],
                          ['HP Proposed, Customer Funded', $scope.CustomerFundedHPProposedMajorIncidentReductionMonth],
                          ['Customer Proposed, HP Funded', $scope.HPFundedCustomerProposedMajorIncidentReductionMonth],
                          ['Customer Proposed & Funded', $scope.CustomerFundedCustomerProposedMajorIncidentReductionMonth]
                        ],
                        type: 'bar'
                    }
                });
                //CsipDetails

                $scope.csipDetails = data.csipDetails;

                var chart6 = c3.generate({
                    bindto: '#chart6',
                    size: {
                        width: 750
                    },
                    data: {
                        columns: [
                          [$scope.csipDetails[0].csiptype, $scope.csipDetails[0].csipPercentage],
                          [$scope.csipDetails[1].csiptype, $scope.csipDetails[1].csipPercentage],
                          [$scope.csipDetails[2].csiptype, $scope.csipDetails[2].csipPercentage],
                          [$scope.csipDetails[3].csiptype, $scope.csipDetails[3].csipPercentage],
                          [$scope.csipDetails[4].csiptype, $scope.csipDetails[4].csipPercentage],
                          [$scope.csipDetails[5].csiptype, $scope.csipDetails[5].csipPercentage],
                          [$scope.csipDetails[6].csiptype, $scope.csipDetails[6].csipPercentage],
                          [$scope.csipDetails[7].csiptype, $scope.csipDetails[7].csipPercentage]
                        ],
                        type: 'bar'
                    }
                });
              
           
                //VDF Status

                $scope.vdfStatusDetails = data.vdfStatusDetails;

                var chart7 = c3.generate({
                    bindto: '#chart7',
                    size: {
                        width: 750
                    },
                    data: {
                        columns: [
                          [$scope.vdfStatusDetails[0].currentStatus, $scope.vdfStatusDetails[0].count],
                          [$scope.vdfStatusDetails[1].currentStatus, $scope.vdfStatusDetails[1].count],
                          [$scope.vdfStatusDetails[2].currentStatus, $scope.vdfStatusDetails[2].count],
                          [$scope.vdfStatusDetails[3].currentStatus, $scope.vdfStatusDetails[3].count],
                          [$scope.vdfStatusDetails[4].currentStatus, $scope.vdfStatusDetails[4].count],
                          [$scope.vdfStatusDetails[5].currentStatus, $scope.vdfStatusDetails[5].count]
                         
                        ],
                        type: 'bar'
                    }
                });

            }
        });
        promise.error(function (response) {
            alert('Fail');
            $scope.error_message = response.error_message;
        });
    };

    $scope.GetVDFDashboardDetails();

    $scope.vdflistChanged = function (data) {
        $scope.selectedAccount = data;
        $scope.GetVDFDashboardDetails();
    }
    $scope.ShowGrid = function (GridName) {
        if (GridName == 'FDIProjects') {
            $scope.VDFSummary = true;
            $scope.VDFTile = false;
            $scope.EffortSaved = false;
            $scope.CostSaved = false;
            $scope.CostSavedForCustomer = false;
            $scope.IncidentReductionMonth = false;
            $scope.MajorIncidentReductionMonth = false;
            $scope.CSIPDetailsGrid = false;
            $scope.CurrentStatusGrid = false;
            $scope.QuartarlyChart = false;
        }
        else if (GridName == 'EffortSaved') {
            $scope.VDFSummary = false;
            $scope.VDFTile = false;
            $scope.EffortSaved = true;
            $scope.CostSaved = false;
            $scope.CostSavedForCustomer = false;
            $scope.IncidentReductionMonth = false;
            $scope.MajorIncidentReductionMonth = false;
            $scope.CSIPDetailsGrid = false;
            $scope.CurrentStatusGrid = false;
            $scope.QuartarlyChart = false;
        }
        else if (GridName == 'CostSaved') {
            $scope.VDFSummary = false;
            $scope.VDFTile = false;
            $scope.EffortSaved = false;
            $scope.CostSaved = true;
            $scope.CostSavedForCustomer = false;
            $scope.IncidentReductionMonth = false;
            $scope.MajorIncidentReductionMonth = false;
            $scope.CSIPDetailsGrid = false;
            $scope.CurrentStatusGrid = false;
            $scope.QuartarlyChart = false;
        }
        else if (GridName == 'CostSavedForCustomer') {
            $scope.VDFSummary = false;
            $scope.VDFTile = false;
            $scope.EffortSaved = false;
            $scope.CostSaved = false;
            $scope.CostSavedForCustomer = true;
            $scope.IncidentReductionMonth = false;
            $scope.MajorIncidentReductionMonth = false;
            $scope.CSIPDetailsGrid = false;
            $scope.CurrentStatusGrid = false;
            $scope.QuartarlyChart = false;
        }
        else if (GridName == 'IncidentReductionMonth') {
            $scope.VDFSummary = false;
            $scope.VDFTile = false;
            $scope.EffortSaved = false;
            $scope.CostSaved = false;
            $scope.CostSavedForCustomer = false;
            $scope.IncidentReductionMonth = true;
            $scope.MajorIncidentReductionMonth = false;
            $scope.CSIPDetailsGrid = false;
            $scope.CurrentStatusGrid = false;
            $scope.QuartarlyChart = false;
        }
        else if (GridName == 'MajorIncidentReductionMonth') {
            $scope.VDFSummary = false;
            $scope.VDFTile = false;
            $scope.EffortSaved = false;
            $scope.CostSaved = false;
            $scope.CostSavedForCustomer = false;
            $scope.IncidentReductionMonth = false;
            $scope.MajorIncidentReductionMonth = true;
            $scope.CSIPDetailsGrid = false;
            $scope.CurrentStatusGrid = false;
            $scope.QuartarlyChart = false;
        }
        else if (GridName == 'CSIPDetailsGrid') {
            $scope.VDFSummary = false;
            $scope.VDFTile = false;
            $scope.EffortSaved = false;
            $scope.CostSaved = false;
            $scope.CostSavedForCustomer = false;
            $scope.IncidentReductionMonth = false;
            $scope.MajorIncidentReductionMonth = false;
            $scope.CSIPDetailsGrid = true;
            $scope.CurrentStatusGrid = false;
            $scope.QuartarlyChart = false;
        }
        else if (GridName == 'CurrentStatusGrid') {
            $scope.VDFSummary = false;
            $scope.VDFTile = false;
            $scope.EffortSaved = false;
            $scope.CostSaved = false;
            $scope.CostSavedForCustomer = false;
            $scope.IncidentReductionMonth = false;
            $scope.MajorIncidentReductionMonth = false;
            $scope.CSIPDetailsGrid = false;
            $scope.CurrentStatusGrid = true;
            $scope.QuartarlyChart = false;
        }
        else if (GridName == 'QuartarlyChart') {
            $scope.VDFSummary = false;
            $scope.VDFTile = false;
            $scope.EffortSaved = false;
            $scope.CostSaved = false;
            $scope.CostSavedForCustomer = false;
            $scope.IncidentReductionMonth = false;
            $scope.MajorIncidentReductionMonth = false;
            $scope.CSIPDetailsGrid = false;
            $scope.CurrentStatusGrid = false;
            $scope.QuartarlyChart = true;
        }
        else if (GridName == 'Back') {
            $scope.VDFSummary = false;
            $scope.VDFTile = true;
            $scope.EffortSaved = false;
            $scope.CostSaved = false;
            $scope.CostSavedForCustomer = false;
            $scope.IncidentReductionMonth = false;
            $scope.MajorIncidentReductionMonth = false;
            $scope.CSIPDetailsGrid = false;
            $scope.CurrentStatusGrid = false;
            $scope.QuartarlyChart = false;
        }


    }
});