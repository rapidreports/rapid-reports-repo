﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('vdf', {
        url: '/VDF',
        templateUrl: 'client/app/VDF/VDF.html',
        controller: "VDFController"
    });
});