﻿RapidReportsModule
//    .config(function ($stateProvider) {
//    $stateProvider
//    .state('financer', {
//        url: '/Finance',
//        templateUrl: 'client/app/CommonSubTile/CommonSubTile.html',
//        controller: "FinanceController"
//    });
//});

.run(function ($rootScope, $state, $log) {
    $rootScope.$on('$stateChangeSuccess', function (event, toState) {
        if (toState.name === 'finance') {
            $log.debug('Auto-transitioning state :: finance -> finance.main');
            $state.go('finance.main');
        }
    });
})
.config(function ($stateProvider) {
    $stateProvider
    .state('finance', {
        url: '/finance',
        template: '<div ui-view></div>'
    })
    .state('finance.main', {
        url: '/',
        templateUrl: 'client/app/Finance/Finance.html',
        controller: 'FinanceController'
    });
});

