﻿'use strict';

angular.module('RapidReportsModule')
.config(function ($stateProvider) {
    $stateProvider
    .state('finance.report', {
        url: '/report',
        templateUrl: 'client/app/Finance/FinanceReports/financereport.html',
        controller: 'FinanceReportCtrl'
    });
});