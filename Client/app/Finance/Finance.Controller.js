﻿RapidReportsModule.controller("FinanceController", function ($scope, $http, $state) {
    $scope.contentHeading = "Finance";
    $scope.getSubTileInformatinon = function () {
        var promise = $http({
            method: 'GET',
            url: '/app/getSubTileInformation/Finance'
        });
        promise.success(function (data) {
            // alert(data);
            $scope.subtiledetails = data;
        });
        promise.error(function (response) {
            alert('Fail');
            $scope.error_message = response.error_message;
        });
    };
    $scope.getSubTileInformatinon();

    $scope.navigate = function (url) {
        if (!_.validUrl(url)) {
            $state.go('overview', { opts: url });
        }
        else {
            var win = window.open(url, '_blank');
            win.focus();
        }
    };
    $state.go('finance.report');
});