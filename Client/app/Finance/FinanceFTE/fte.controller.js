﻿'use strict';

angular.module('RapidReportsModule')
.controller('FTECtrl', function ($scope, $http, PageHeaderService) {
    PageHeaderService.setPageHeaderTitle('FTE');

    //$scope.msg = 'page under construction';
    $scope.show = false;
    $scope.showwbs = false;
    $scope.accoundIdSelected = false;
    $scope.monthselected = false;
    $scope.monthSel = false;
    $scope.yearSel = false;
    $scope.wbscodeisValid = true;
    $scope.contractisValid = true;
    $scope.wbs = {};
    $scope.ftedata = {};
    $scope.years = [];
    $scope.contractdetails = {};
    $scope.contractdeldetails = {};
    $scope.currentmonth = '';
    $scope.currentyear = '';
    // $scope.accountdetails.accountId = 1;
    //$scope.selectedMonth = 'FEB';
    //$scope.selectedYear = '2016';


    $scope.months = [
        { id: 1, month: 'JAN' },
        { id: 2, month: 'FEB' },
        { id: 3, month: 'MAR' },
        { id: 4, month: 'APR' },
        { id: 5, month: 'MAY' },
        { id: 6, month: 'JUN' },
        { id: 7, month: 'JUL' },
        { id: 8, month: 'AUG' },
        { id: 9, month: 'SEP' },
        { id: 10, month: 'OCT' },
        { id: 11, month: 'NOV' },
        { id: 12, month: 'DEC' }
    ];

    $scope.yearlist = function () {
        var year = (new Date().getFullYear()) - 5;
        //var range = [];
        $scope.years.push({ year: year });
        for (var i = 1; i < 20; i++) {
            //$scope.years.push(year + i);
            $scope.years.push({
                year: year + i
            });
        }
    };
    $scope.yearlist();

    $scope.monthSelected = function (mmm) {
        //$scope.selectedMonth = mmm;
        $scope.currentmonth = mmm;
        $scope.currentmonthintdata = _.find($scope.months, function (monthdata) { return monthdata.month === mmm; });
        $scope.currentmonthint = $scope.currentmonthintdata.id;
        $scope.monthSel = true;
        if (!((_.isUndefined($scope.currentmonth) || ($scope.currentmonth === '')) && (_.isUndefined($scope.currentyear)) || ($scope.currentyear === ''))) {
            $scope.getMonthlyFte();
            $scope.getContractIdByAccountId();
            $scope.getWBSByAccountId();
            $scope.getdfteCr();
            $scope.monthselected = true;
        }
    };

    $scope.yearSelected = function (yyyy) {
        //$scope.selectedYear = yyyy;
        $scope.currentyear = yyyy;
        $scope.yearSel = true;
        if (!((_.isUndefined($scope.currentmonth) || ($scope.currentmonth === '')) && (_.isUndefined($scope.currentyear)) || ($scope.currentyear === ''))) {
            $scope.getMonthlyFte();
            $scope.getdfteCr();
            $scope.getContractIdByAccountId();
        }
        //$scope.savecontractandwbsdetails();
    };
    $scope.getAccounts = function () {
        var promise = $http({
            method: 'GET',
            url: 'app/Accounts'
        });
        promise.success(function (data) {

            $scope.accountdetails = data;

        });
        /* jshint ignore:start */
        promise.error(function (response) {
            // TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };
    $scope.getAccounts();

    $scope.accountIdselected = function (accountid) {
        $scope.accountId = accountid;
        if (!((_.isUndefined($scope.currentmonth) || ($scope.currentmonth === '')) && (_.isUndefined($scope.currentyear)) || ($scope.currentyear === ''))) {
            $scope.getWBSByAccountId();
            $scope.getMonthlyFte();
            $scope.getContractIdByAccountId();
            $scope.getdfteCr();
        }
        $scope.accoundIdSelected = true;
    };
    //$scope.accountIdselected();

    $scope.addChip = function (chip) {
        //this make a copy and allow repeated items in the list.
        //var newChip = {};
        //angular.copy(chip, newChip)
        //return newChip;
        if (!_.isUndefined(chip) && 
            !_.isNull(chip)){
            $http({
                method: 'POST',
                url: 'app/contractpost/' + $scope.accountId + '/' + $scope.currentmonthint + '/' + $scope.currentyear,
                data: { 'contractId': chip },
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                   .success(function (data) {
                       $scope.test = data;
                       $scope.getContractIdByAccountId();
                       document.getElementById('contract').value = '';
                       return chip;
                       //data.raisedByName = $scope.currentuser.fullName;
                       //data.closedByName = $scope.currentuser.fullName;
                       //modalObjects.issues.push(data);
                       //$modalInstance.dismiss('dismiss');

                       /* jshint ignore:start */
                       // TODO correct [success_message] -> [success_message]
                       $scope.succes_message = 'Success';
                       /* jshint ignore:end */
                   })
                   /* jshint ignore:start */
                   .error(function (response) {
                       // TODO correct [error_message] -> [errorMessage]
                       $scope.error_message = response.error_message;
                   })
            /* jshint ignore:end */
            ;
        }
    };

    $scope.chipRemove = function (chip) {
        $http({
            method: 'POST',
            url: 'app/contractdel/' + $scope.accountId + '/' + $scope.currentmonthint + '/' + $scope.currentyear,
            data: { 'contractId': chip },
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .success(function (data) {
                $scope.test = data;
                //$scope.accountIdselected($scope.accountId);
                $scope.getContractIdByAccountId();
                //$scope.succes_message = 'Success';
                /* jshint ignore:end */
            })
            /* jshint ignore:start */
            .error(function (response) {
                // TODO correct [error_message] -> [errorMessage]
                //$scope.error_message = response.error_message;
            })
        /* jshint ignore:end */
        ;

    }

    $scope.getContractIdByAccountId = function () {
        var promise = $http({
            method: 'GET',
            url: 'app/getContract/' + $scope.accountId
        });
        promise.success(function (contract) {

            //$scope.contractId = contract;
            $scope.contractId = _.filter(contract, function (filtercontract) { return (filtercontract.month === $scope.currentmonthint.toString() && filtercontract.year === $scope.currentyear.toString()); });
            $scope.contractIdsAry = _.uniq(_.map($scope.contractId, 'contractId'));
            //$scope.showwbs = true;
        });
        /* jshint ignore:start 
        promise.error(function (response) {
             TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };

    $scope.getWBSByAccountId = function () {
        var promise = $http({
            method: 'GET',
            url: 'app/WBSById/' + $scope.accountId + '/' + $scope.currentmonth + '/' + $scope.currentyear
        });
        promise.success(function (wbs) {

            $scope.wbsdetails = wbs;
            $scope.showwbs = true;
        });
        /* jshint ignore:start 
        promise.error(function (response) {
             TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };

    //$scope.wbs.wbscode;
    //$scope.wbs.wbsdescription;

    $scope.getMonthlyFte = function () {
        var promise = $http({
            method: 'GET',
            url: 'app/getfte/' + $scope.currentmonth + '/' + $scope.currentyear + '/' + $scope.accountId
        });
        promise.success(function (data) {

            if (data !== null) {
                $scope.revenuedata = _.find(data, function (ftedata) { return ftedata.fte === 'Revenue' && ftedata.fteType === 'Revenue'; });
                $scope.ftedata.revenue = $scope.revenuedata.fteValue;
                $scope.additionalcostdata = _.find(data, function (ftedata) { return ftedata.fte === 'AdditionalCost' && ftedata.fteType === 'AdditionalCost'; });
                $scope.ftedata.additionalCost = $scope.additionalcostdata.fteValue;
                $scope.onshoreentdata = _.find(data, function (ftedata) { return ftedata.fte === 'ENT' && ftedata.fteType === 'onshore'; });
                $scope.ftedata.onshoreEnt = $scope.onshoreentdata.fteValue;
                $scope.onshoreintdata = _.find(data, function (ftedata) { return ftedata.fte === 'INT' && ftedata.fteType === 'onshore'; });
                $scope.ftedata.onshoreInt = $scope.onshoreintdata.fteValue;
                $scope.onshorespedata = _.find(data, function (ftedata) { return ftedata.fte === 'SPE' && ftedata.fteType === 'onshore'; });
                $scope.ftedata.onshoreSpe = $scope.onshorespedata.fteValue;
                $scope.onshoreexpdata = _.find(data, function (ftedata) { return ftedata.fte === 'EXP' && ftedata.fteType === 'onshore'; });
                $scope.ftedata.onshoreExp = $scope.onshoreexpdata.fteValue;
                $scope.onshoremg1data = _.find(data, function (ftedata) { return ftedata.fte === 'MG1' && ftedata.fteType === 'onshore';});
                $scope.ftedata.onshoreMg1 = $scope.onshoremg1data.fteValue;
                $scope.onshoremg2data = _.find(data, function (ftedata) { return ftedata.fte === 'MG2' && ftedata.fteType === 'onshore'; });
                $scope.ftedata.onshoreMg2 = $scope.onshoremg2data.fteValue;
                $scope.offshoreadvdata = _.find(data, function (ftedata) { return ftedata.fte === 'ADV' && ftedata.fteType === 'offshore'; });
                $scope.ftedata.offshoreAdv = $scope.offshoreadvdata.fteValue;
                $scope.offshoreintdata = _.find(data, function (ftedata) { return ftedata.fte === 'INT' && ftedata.fteType === 'offshore'; });
                $scope.ftedata.offshoreInt = $scope.offshoreintdata.fteValue;
                $scope.offshoreentdata = _.find(data, function (ftedata) { return ftedata.fte === 'ENT' && ftedata.fteType === 'offshore'; });
                $scope.ftedata.offshoreEnt = $scope.offshoreentdata.fteValue;
                $scope.offshorespedata = _.find(data, function (ftedata) { return ftedata.fte === 'SPE' && ftedata.fteType === 'offshore'; });
                $scope.ftedata.offshoreSpe = $scope.offshorespedata.fteValue;
                $scope.offshoreexpdata = _.find(data, function (ftedata) { return ftedata.fte === 'EXP' && ftedata.fteType === 'offshore'; });
                $scope.ftedata.offshoreExp = $scope.offshoreexpdata.fteValue;
                $scope.offshoremg1data = _.find(data, function (ftedata) { return ftedata.fte === 'MG1' && ftedata.fteType === 'offshore'; });
                $scope.ftedata.offshoreMg1 = $scope.offshoremg1data.fteValue;
                $scope.offshoremg2data = _.find(data, function (ftedata) { return ftedata.fte === 'MG2' && ftedata.fteType === 'offshore'; });
                $scope.ftedata.offshoreMg2 = $scope.offshoremg2data.fteValue;
                $scope.additionalexpensedata = _.find(data, function (ftedata) { return ftedata.fte === 'AdditionalExpenses' && ftedata.fteType === 'AdditionalExpenses'; });
                $scope.ftedata.additonalExpenses = $scope.additionalexpensedata.fteValue;
                $scope.lastUpdatedBydata = _.find(data, function (ftedata) { return ftedata.fte === 'Revenue' && ftedata.fteType === 'Revenue'; });
                $scope.ftedata.lastUpdatedBy = $scope.lastUpdatedBydata.lastUpdatedBy;
                $scope.lastUpdatedOndata = _.find(data, function (ftedata) { return ftedata.fte === 'Revenue' && ftedata.fteType === 'Revenue'; });
                $scope.ftedata.lastUpdatedOn = $scope.lastUpdatedOndata.lastUpdatedOn;
            }
            else {
                $scope.ftedata = {};
            }

            //$scope.accountdetails = data;
        });
        /* jshint ignore:start */
        promise.error(function (response) {
            // TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };



    $scope.addrow = function () {
        $scope.validatewbscode();
        $scope.wbs.accountId = $scope.accountId;
        if ($scope.wbscodeisValid) {
            //$scope.show = true;
            $http({
                method: 'POST',
                url: 'app/wbspost/' + $scope.accountId + '/' + $scope.currentmonth + '/' + $scope.currentyear,
                data: $scope.wbs,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                    .success(function (data) {
                        //$scope.accountIdselected($scope.accountId);
                        $scope.test = data; 
                        document.getElementById('wbscode').value = '';
                        document.getElementById('wbsdescription').value = '';
                        //$scope.wbs.wbscode = '';
                        $scope.getWBSByAccountId();

                        //data.raisedByName = $scope.currentuser.fullName;
                        //data.closedByName = $scope.currentuser.fullName;
                        //modalObjects.issues.push(data);
                        //$modalInstance.dismiss('dismiss');

                        /* jshint ignore:start */
                        // TODO correct [success_message] -> [success_message]
                        $scope.succes_message = 'Success';
                        /* jshint ignore:end */
                    })
                    /* jshint ignore:start */
                    .error(function (response) {
                        // TODO correct [error_message] -> [errorMessage]
                        $scope.error_message = response.error_message;
                    })
            /* jshint ignore:end */
            ;
        }
    };

    $scope.addcontract = function () {
        $scope.validatecontract();
        if ($scope.contractisValid) {
            $http({
                method: 'POST',
                url: 'app/contractpost/' + $scope.accountId + '/' + $scope.currentmonthint + '/' + $scope.currentyear,
                data: $scope.contractdetails,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                   .success(function (data) {
                       $scope.test = data;
                       $scope.getContractIdByAccountId();
                       document.getElementById('contract').value = '';
                       //data.raisedByName = $scope.currentuser.fullName;
                       //data.closedByName = $scope.currentuser.fullName;
                       //modalObjects.issues.push(data);
                       //$modalInstance.dismiss('dismiss');

                       /* jshint ignore:start */
                       // TODO correct [success_message] -> [success_message]
                       $scope.succes_message = 'Success';
                       /* jshint ignore:end */
                   })
                   /* jshint ignore:start */
                   .error(function (response) {
                       // TODO correct [error_message] -> [errorMessage]
                       $scope.error_message = response.error_message;
                   })
            /* jshint ignore:end */
            ;
        }
    };

    $scope.validatecontract = function () {
        if (document.getElementById('contract').value === '') {
            $scope.contractisValid = false;
        }
        else {
            $scope.contractisValid = true;
        }
    };

    $scope.validatewbscode = function () {
        if (_.isUndefined($scope.wbs.wbscode) || $scope.wbs.wbscode === '') {
            $scope.wbscodeisValid = false;
        }
        else {
            $scope.wbscodeisValid = true;
        }
    };

    $scope.delrow = function (wbscode) {
        // $scope.show = false;
        $scope.wbsdeldetails = {};
        $scope.wbsdeldetails.accountId = $scope.accountId;
        $scope.wbsdeldetails.wbsCode = wbscode;
        $scope.wbsdeldetails.isActive = false;
        //$scope.show = true;
        $http({
            method: 'POST',
            url: 'app/wbsdel/' + $scope.accountId,
            data: $scope.wbsdeldetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
                .success(function (data) {
                    $scope.test = data;
                    //$scope.accountIdselected($scope.accountId);
                    $scope.getWBSByAccountId();
                    //$scope.succes_message = 'Success';
                    /* jshint ignore:end */
                })
                /* jshint ignore:start */
                .error(function (response) {
                    // TODO correct [error_message] -> [errorMessage]
                    //$scope.error_message = response.error_message;
                })
        /* jshint ignore:end */
        ;

    };

    $scope.delcontract = function (contract) {

        $scope.contractdeldetails.contractId = contract;
        $http({
            method: 'POST',
            url: 'app/contractdel/' + $scope.accountId + '/' + $scope.currentmonthint + '/' + $scope.currentyear,
            data: $scope.contractdeldetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
                    .success(function (data) {
                        $scope.test = data;
                        //$scope.accountIdselected($scope.accountId);
                        $scope.getContractIdByAccountId();
                        //$scope.succes_message = 'Success';
                        /* jshint ignore:end */
                    })
                    /* jshint ignore:start */
                    .error(function (response) {
                        // TODO correct [error_message] -> [errorMessage]
                        //$scope.error_message = response.error_message;
                    })
        /* jshint ignore:end */
        ;
    };

    $scope.save = function () {

        $scope.ftedetails = [
            { accountId: $scope.accountId, fte: 'Revenue', fteValue: $scope.ftedata.revenue, fteType: 'Revenue', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'AdditionalCost', fteValue: $scope.ftedata.additionalCost, fteType: 'AdditionalCost', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'ENT', fteValue: $scope.ftedata.onshoreEnt, fteType: 'onshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'INT', fteValue: $scope.ftedata.onshoreInt, fteType: 'onshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'SPE', fteValue: $scope.ftedata.onshoreSpe, fteType: 'onshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'EXP', fteValue: $scope.ftedata.onshoreExp, fteType: 'onshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'MG1', fteValue: $scope.ftedata.onshoreMg1, fteType: 'onshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'MG2', fteValue: $scope.ftedata.onshoreMg2, fteType: 'onshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'ADV', fteValue: $scope.ftedata.offshoreAdv, fteType: 'offshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'ENT', fteValue: $scope.ftedata.offshoreEnt, fteType: 'offshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'INT', fteValue: $scope.ftedata.offshoreInt, fteType: 'offshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'SPE', fteValue: $scope.ftedata.offshoreSpe, fteType: 'offshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'EXP', fteValue: $scope.ftedata.offshoreExp, fteType: 'offshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'MG1', fteValue: $scope.ftedata.offshoreMg1, fteType: 'offshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'MG2', fteValue: $scope.ftedata.offshoreMg2, fteType: 'offshore', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear },
            { accountId: $scope.accountId, fte: 'AdditionalExpenses', fteValue: $scope.ftedata.additonalExpenses, fteType: 'AdditionalExpenses', lastUpdatedBy: null, month: $scope.currentmonth, year: $scope.currentyear }

        ];

        $http({
            method: 'POST',
            url: 'app/savefte/',
            data: $scope.ftedetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
                .success(function (data) {
                    $scope.test = data;
                    //$scope.accountIdselected($scope.accountId);
                    //$scope.savecontractandwbsdetails();
                    //$scope.succes_message = 'Success';
                    /* jshint ignore:end */
                })
                /* jshint ignore:start */
                .error(function (response) {
                    // TODO correct [error_message] -> [errorMessage]
                    //$scope.error_message = response.error_message;
                })
        /* jshint ignore:end */
        ;

        //alert("Data Saved !!... Thank You")

    };

    $scope.adddfteCR = function () {

        $scope.fteCRDetails = {
            AccountId: $scope.accountId, Year: $scope.currentyear, Month: $scope.currentmonth, cr: $scope.ftedata.cr, Reason: $scope.ftedata.reason, AdditionalFTE: $scope.ftedata.additionalFte, Amount: $scope.ftedata.amount
        };
        $http({
            method: 'POST',
            url: 'app/savedftecr/',
            data: $scope.fteCRDetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
              .success(function (data) {
                  $scope.test = data;
                  $scope.getdfteCr();
                  $scope.ftedata.cr = '';
                  $scope.ftedata.reason = '';
                  $scope.ftedata.additionalFte = '';
                  $scope.ftedata.amount = '';
                  //$scope.succes_message = 'Success';
                  $scope.clearCrForm();
              })

              .error(function (response) {
                  $scope.error = response;
                  // $scope.error_message = response.error_message;
              });


    };

    $scope.getdfteCr = function () {
        var promise = $http({
            method: 'GET',
            url: 'app/GetdfteCr/' + $scope.accountId + '/' + $scope.currentmonth + '/' + $scope.currentyear,
        });
        promise.success(function (crData) {

            $scope.ftechangeRequests = crData;
            //$scope.getCr();
        });
        /* jshint ignore:start */
        promise.error(function (response) {
            //TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };

    $scope.approvefteCR = function (id) {
        $scope.fteCRApprovalDetails = {
            crId: id, Status: 1
        };
        $http({
            method: 'POST',
            url: 'app/UpdateDFTECrStatus/',
            data: $scope.fteCRApprovalDetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
                .success(function (data) {
                    $scope.test = data;
                    $scope.getdfteCr();
                    //$scope.succes_message = 'Success';
                })
                .error(function (response) {
                    $scope.error = response;
                    //  $scope.error_message = response.error_message;
                });
    };
    $scope.rejectfteCR = function (id) {
        $scope.fteCRApprovalDetails = {
            crId: id, Status: 0
        };
        $http({
            method: 'POST',
            url: 'app/UpdateDFTECrStatus/',
            data: $scope.fteCRApprovalDetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
                .success(function (data) {
                    $scope.test = data;
                    $scope.getdfteCr();
                    //$scope.succes_message = 'Success';
                })
                .error(function (response) {
                    $scope.error = response;

                    //$scope.error_message = response.error_message;
                });

    };

    //$scope.savecontractandwbsdetails = function () {
    //    $scope.getContractIdByAccountId();
    //    $scope.getWBSByAccountId();
    //    $scope.contractandwbs = [{ 'contract': $scope.contractId, 'wbs': $scope.wbsdetails }];

    //};


    /////////////////////////// DFTE SECTION ///////////////////////////////

    $scope.dftedata = {};
    $scope.ndftecontractdetails = {};
    $scope.ndwbs = {};
   
    $scope.yearSelecteddfte = function (yyyy) {
        $scope.selecteddfteYear = yyyy;
        //$scope.getdftedetails();
        if (!_.isUndefined($scope.dfteaccountId) || $scope.dfteaccountId === '') {
            $scope.getdftedetails();
            $scope.getCr();
            $scope.getNDFTEContractIdByAccountId();
            $scope.getNDWBSByAccountId();
        }
    };
    $scope.accountIdselecteddfte = function (accountId) {
        $scope.dfteaccountId = accountId;
        //$scope.getdftedetails();
        if (!_.isUndefined($scope.selecteddfteYear) || $scope.selecteddfteYear === '') {
            $scope.getdftedetails();
            $scope.getCr();
            $scope.getNDFTEContractIdByAccountId();
            $scope.getNDWBSByAccountId();
        }
    };

    $scope.getdftedetails = function () {
        var promise = $http({
            method: 'GET',
            url: 'app/getdfte/' + $scope.dfteaccountId + '/' + $scope.selecteddfteYear
        });
        promise.success(function (data) {
            $scope.dftedata = {};
            if (data !== null) {
                $scope.costbaselineddataNov = _.find(data, function (dftedata) { return dftedata.month === 'Nov'; });
                $scope.dftedata.costBaselinedNov = $scope.costbaselineddataNov.costBaselined;
                $scope.dftedata.additionalCostNov = $scope.costbaselineddataNov.additionalCost;
                $scope.dftedata.riskAmountNov = $scope.costbaselineddataNov.riskAmount;
                $scope.dftedata.baselineNov = $scope.costbaselineddataNov.revenueBaseline;
                $scope.costbaselineddataDec = _.find(data, function (dftedata) { return dftedata.month === 'Dec'; });
                $scope.dftedata.costBaselinedDec = $scope.costbaselineddataDec.costBaselined;
                $scope.dftedata.additionalCostDec = $scope.costbaselineddataDec.additionalCost;
                $scope.dftedata.riskAmountDec = $scope.costbaselineddataDec.riskAmount;
                $scope.dftedata.baselineDec = $scope.costbaselineddataDec.revenueBaseline;
                $scope.costbaselineddataJan = _.find(data, function (dftedata) { return dftedata.month === 'Jan'; });
                $scope.dftedata.costBaselinedJan = $scope.costbaselineddataJan.costBaselined;
                $scope.dftedata.additionalCostJan = $scope.costbaselineddataJan.additionalCost;
                $scope.dftedata.riskAmountJan = $scope.costbaselineddataJan.riskAmount;
                $scope.dftedata.baselineJan = $scope.costbaselineddataJan.revenueBaseline;
                $scope.costbaselineddataFeb = _.find(data, function (dftedata) { return dftedata.month === 'Feb'; });
                $scope.dftedata.costBaselinedFeb = $scope.costbaselineddataFeb.costBaselined;
                $scope.dftedata.additionalCostFeb = $scope.costbaselineddataFeb.additionalCost;
                $scope.dftedata.riskAmountFeb = $scope.costbaselineddataFeb.riskAmount;
                $scope.dftedata.baselineFeb = $scope.costbaselineddataFeb.revenueBaseline;
                $scope.costbaselineddataMar = _.find(data, function (dftedata) { return dftedata.month === 'Mar'; });
                $scope.dftedata.costBaselinedMar = $scope.costbaselineddataMar.costBaselined;
                $scope.dftedata.additionalCostMar = $scope.costbaselineddataMar.additionalCost;
                $scope.dftedata.riskAmountMar = $scope.costbaselineddataMar.riskAmount;
                $scope.dftedata.baselineMar = $scope.costbaselineddataMar.revenueBaseline;
                $scope.costbaselineddataApr = _.find(data, function (dftedata) { return dftedata.month === 'Apr'; });
                $scope.dftedata.costBaselinedApr = $scope.costbaselineddataApr.costBaselined;
                $scope.dftedata.additionalCostApr = $scope.costbaselineddataApr.additionalCost;
                $scope.dftedata.riskAmountApr = $scope.costbaselineddataApr.riskAmount;
                $scope.dftedata.baselineApr = $scope.costbaselineddataApr.revenueBaseline;
                $scope.costbaselineddataMay = _.find(data, function (dftedata) { return dftedata.month === 'May'; });
                $scope.dftedata.costBaselinedMay = $scope.costbaselineddataMay.costBaselined;
                $scope.dftedata.additionalCostMay = $scope.costbaselineddataMay.additionalCost;
                $scope.dftedata.riskAmountMay = $scope.costbaselineddataMay.riskAmount;
                $scope.dftedata.baselineMay = $scope.costbaselineddataMay.revenueBaseline;
                $scope.costbaselineddataJun = _.find(data, function (dftedata) { return dftedata.month === 'Jun'; });
                $scope.dftedata.costBaselinedJun = $scope.costbaselineddataJun.costBaselined;
                $scope.dftedata.additionalCostJun = $scope.costbaselineddataJun.additionalCost;
                $scope.dftedata.riskAmountJun = $scope.costbaselineddataJun.riskAmount;
                $scope.dftedata.baselineJun = $scope.costbaselineddataJun.revenueBaseline;
                $scope.costbaselineddataJul = _.find(data, function (dftedata) { return dftedata.month === 'Jul'; });
                $scope.dftedata.costBaselinedJul = $scope.costbaselineddataJul.costBaselined;
                $scope.dftedata.additionalCostJul = $scope.costbaselineddataJul.additionalCost;
                $scope.dftedata.riskAmountJul = $scope.costbaselineddataJul.riskAmount;
                $scope.dftedata.baselineJul = $scope.costbaselineddataJul.revenueBaseline;
                $scope.costbaselineddataAug = _.find(data, function (dftedata) { return dftedata.month === 'Aug';});
                $scope.dftedata.costBaselinedAug = $scope.costbaselineddataAug.costBaselined;
                $scope.dftedata.additionalCostAug = $scope.costbaselineddataAug.additionalCost;
                $scope.dftedata.riskAmountAug = $scope.costbaselineddataAug.riskAmount;
                $scope.dftedata.baselineAug = $scope.costbaselineddataAug.revenueBaseline;
                $scope.costbaselineddataSep = _.find(data, function (dftedata) { return dftedata.month === 'Sep'; });
                $scope.dftedata.costBaselinedSep = $scope.costbaselineddataSep.costBaselined;
                $scope.dftedata.additionalCostSep = $scope.costbaselineddataSep.additionalCost;
                $scope.dftedata.riskAmountSep = $scope.costbaselineddataSep.riskAmount;
                $scope.dftedata.baselineSep = $scope.costbaselineddataSep.revenueBaseline;
                $scope.costbaselineddataOct = _.find(data, function (dftedata) { return dftedata.month === 'Oct'; });
                $scope.dftedata.costBaselinedOct = $scope.costbaselineddataOct.costBaselined;
                $scope.dftedata.additionalCostOct = $scope.costbaselineddataOct.additionalCost;
                $scope.dftedata.riskAmountOct = $scope.costbaselineddataOct.riskAmount;
                $scope.dftedata.baselineOct = $scope.costbaselineddataOct.revenueBaseline;

            }

        });
        /* jshint ignore:start */
        promise.error(function (response) {
            // TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };
    
    
    $scope.dftesave = function () {
        $scope.dftedetails = [
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedNov, AdditionalCost: $scope.dftedata.additionalCostNov, RiskAmount: $scope.dftedata.riskAmountNov, RevenueBaseline: $scope.dftedata.baselineNov, Month: 'Nov', Year: $scope.selecteddfteYear -1 },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedDec, AdditionalCost: $scope.dftedata.additionalCostDec, RiskAmount: $scope.dftedata.riskAmountDec, RevenueBaseline: $scope.dftedata.baselineDec, Month: 'Dec', Year: $scope.selecteddfteYear -1 },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedJan, AdditionalCost: $scope.dftedata.additionalCostJan, RiskAmount: $scope.dftedata.riskAmountJan, RevenueBaseline: $scope.dftedata.baselineJan, Month: 'Jan', Year: $scope.selecteddfteYear },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedFeb, AdditionalCost: $scope.dftedata.additionalCostFeb, RiskAmount: $scope.dftedata.riskAmountFeb, RevenueBaseline: $scope.dftedata.baselineFeb, Month: 'Feb', Year: $scope.selecteddfteYear },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedMar, AdditionalCost: $scope.dftedata.additionalCostMar, RiskAmount: $scope.dftedata.riskAmountMar, RevenueBaseline: $scope.dftedata.baselineMar, Month: 'Mar', Year: $scope.selecteddfteYear },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedApr, AdditionalCost: $scope.dftedata.additionalCostApr, RiskAmount: $scope.dftedata.riskAmountApr, RevenueBaseline: $scope.dftedata.baselineApr, Month: 'Apr', Year: $scope.selecteddfteYear },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedMay, AdditionalCost: $scope.dftedata.additionalCostMay, RiskAmount: $scope.dftedata.riskAmountMay, RevenueBaseline: $scope.dftedata.baselineMay, Month: 'May', Year: $scope.selecteddfteYear },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedJun, AdditionalCost: $scope.dftedata.additionalCostJun, RiskAmount: $scope.dftedata.riskAmountJun, RevenueBaseline: $scope.dftedata.baselineJun, Month: 'Jun', Year: $scope.selecteddfteYear },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedJul, AdditionalCost: $scope.dftedata.additionalCostJul, RiskAmount: $scope.dftedata.riskAmountJul, RevenueBaseline: $scope.dftedata.baselineJul, Month: 'Jul', Year: $scope.selecteddfteYear },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedAug, AdditionalCost: $scope.dftedata.additionalCostAug, RiskAmount: $scope.dftedata.riskAmountAug, RevenueBaseline: $scope.dftedata.baselineAug, Month: 'Aug', Year: $scope.selecteddfteYear },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedSep, AdditionalCost: $scope.dftedata.additionalCostSep, RiskAmount: $scope.dftedata.riskAmountSep, RevenueBaseline: $scope.dftedata.baselineSep, Month: 'Sep', Year: $scope.selecteddfteYear },
            { accountId: $scope.dfteaccountId, Costbaselined: $scope.dftedata.costBaselinedOct, AdditionalCost: $scope.dftedata.additionalCostOct, RiskAmount: $scope.dftedata.riskAmountOct, RevenueBaseline: $scope.dftedata.baselineOct, Month: 'Oct', Year: $scope.selecteddfteYear }


        ];
        $http({
            method: 'POST',
            url: 'app/postdfte',
            data: $scope.dftedetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
                    .success(function (data) {
                        $scope.test = data;
                        $scope.getdftedetails();
                        //$scope.accountIdselected($scope.accountId);
                        //$scope.savecontractandwbsdetails();
                        //$scope.succes_message = 'Success';
                        /* jshint ignore:end */
                    })
                    /* jshint ignore:start */
                    .error(function (response) {
                        // TODO correct [error_message] -> [errorMessage]
                        //$scope.error_message = response.error_message;
                    })
        /* jshint ignore:end */
        ;

        //alert("Data Saved !!... Thank You")
    };
    $scope.addCR = function () {
        //alert("test Kiran");
        $scope.dfteCRDetails = {
            AccountId: $scope.dfteaccountId, Year: $scope.selecteddfteYear,Month : $scope.dftedata.selectedNDFTECRMonth, cr: $scope.dftedata.cr, Reason: $scope.dftedata.reason, AdditionalFTE: $scope.dftedata.additionalFte, Amount: $scope.dftedata.amount
        };
        $http({
            method: 'POST',
            url: 'app/savecr/',
            data: $scope.dfteCRDetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
              .success(function (data) {
                  $scope.test = data;
                  $scope.getCr();
                  //$scope.succes_message = 'Success';
                  $scope.clearCrForm();
              })

              .error(function (response) {
                  $scope.error = response;
                  // $scope.error_message = response.error_message;
              });



    };
    $scope.getCr = function () {
        var promise = $http({
            method: 'GET',
            url: 'app/GetCr/' + $scope.dfteaccountId + '/' + $scope.selecteddfteYear
        });
        promise.success(function (crData) {

            $scope.changeRequests = crData;
            //$scope.getCr();
        });
        /* jshint ignore:start */
        promise.error(function (response) {
            //TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };
    //$scope.getCr();

    $scope.approveCR = function (id) {
        $scope.dfteCRApprovalDetails = {
            crId: id, Status: 1
        };
        $http({
            method: 'POST',
            url: 'app/UpdateCrStatus/',
            data: $scope.dfteCRApprovalDetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
                .success(function (data) {
                    $scope.test = data;
                    $scope.getCr();
                    //$scope.succes_message = 'Success';
                })
                .error(function (response) {
                    $scope.error = response;
                    //  $scope.error_message = response.error_message;
                });
    };
    $scope.rejectCR = function (id) {
        $scope.dfteCRApprovalDetails = {
            crId: id, Status: 0
        };
        $http({
            method: 'POST',
            url: 'app/UpdateCrStatus/',
            data: $scope.dfteCRApprovalDetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
                .success(function (data) {
                    $scope.test = data;
                    $scope.getCr();
                    //$scope.succes_message = 'Success';
                })
                .error(function (response) {
                    $scope.error = response;

                    //$scope.error_message = response.error_message;
                });

    };
    $scope.clearCrForm = function () {
        //alert('Change Request has been sent');
        $scope.dftedata.cr = '';
        $scope.dftedata.reason = '';
        $scope.dftedata.additionalFte = '';
        $scope.dftedata.amount = '';
    };
    //$scope.icon = false;
    //$scope.orderByVar = '';
    //$scope.crOrder = true;
    //$scope.reasonOrder = true;
    //$scope.additionalFTEOrder = true;
    //$scope.amountOrder = true;
    //$scope.statusOrder = true;
    //$scope.ascOrdsc = false;
    //$scope.orderByfilter = function (dataIn) {

    //    $scope.icon = true;
    //    $scope.orderByVar = dataIn;
    //    $scope.ascOrdsc = !$scope.ascOrdsc;
    //    $scope.crOrder = !$scope.crOrder;
    //    $scope.reasonOrder = !$scope.reasonOrder;
    //    $scope.additionalFTEOrder = !$scope.additionalFTEOrder;
    //    $scope.amountOrder = !$scope.amountOrder;
    //    $scope.statusOrder = !$scope.statusOrder;


    //};

    $scope.sortColumn = '';
    $scope.reverseSort = false;
    $scope.sortData = function (column) {
        $scope.reverseSort = ($scope.sortColumn === column) ?
            !$scope.reverseSort : false;
        $scope.sortColumn = column;
    };

    $scope.getSortClass = function (column) {

        if ($scope.sortColumn === column) {
            var x = $scope.reverseSort
             ? 'glyphicon glyphicon-sort-by-attributes-alt'
            : 'glyphicon glyphicon-sort-by-attributes';
            return x;
        }

        return '';
    };

    $scope.getNDFTEContractIdByAccountId = function () {
        var promise = $http({
            method: 'GET',
            url: 'app/getNDFTEContract/' + $scope.dfteaccountId
        });
        promise.success(function (contract) {
            $scope.ndftecontractdetail = contract;
            //$scope.contractId = contract;
            //$scope.contractId = _.filter(contract, function (filtercontract) { return (filtercontract.month === $scope.currentmonthint.toString() && filtercontract.year === $scope.currentyear.toString()); });
            //$scope.showwbs = true;
        });
        /* jshint ignore:start 
        promise.error(function (response) {
             TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };

    $scope.getNDWBSByAccountId = function () {
        var promise = $http({
            method: 'GET',
            url: 'app/NDWBSById/' + $scope.dfteaccountId + '/' + $scope.selecteddfteYear,
        });
        promise.success(function (wbs) {

            $scope.ndwbsdetail = wbs;
            $scope.showwbs = true;
        });
        /* jshint ignore:start 
        promise.error(function (response) {
             TODO correct [error_message] -> [errorMessage]
            $scope.error_message = response.error_message;
        });
        /* jshint ignore:end */
    };


    $scope.addNDFTEcontract = function () {
       
        $scope.ndftecontractdetails.Month = $scope.ndftecontractdetails.contractMonth;
            $http({
                method: 'POST',
                url: 'app/ndftecontractpost/' + $scope.dfteaccountId + '/' + $scope.selecteddfteYear,
                data: $scope.ndftecontractdetails,
                headers: {
                    'Content-Type': 'application/json'
                }

            })
                   .success(function (data) {
                       $scope.test = data;
                       $scope.getNDFTEContractIdByAccountId();
                       $scope.ndftecontractdetails.contractId = '';
                       //document.getElementById('contract').value = '';
                       //data.raisedByName = $scope.currentuser.fullName;
                       //data.closedByName = $scope.currentuser.fullName;
                       //modalObjects.issues.push(data);
                       //$modalInstance.dismiss('dismiss');

                       /* jshint ignore:start */
                       // TODO correct [success_message] -> [success_message]
                       $scope.succes_message = 'Success';
                       /* jshint ignore:end */
                   })
                   /* jshint ignore:start */
                   .error(function (response) {
                       // TODO correct [error_message] -> [errorMessage]
                       $scope.error_message = response.error_message;
                   })
            /* jshint ignore:end */
            ;
    };

    $scope.delndftecontract = function (contract) {

        $scope.ndftecontractdetails.contractId = contract;
        $http({
            method: 'POST',
            url: 'app/ndftecontractdel/' + $scope.dfteaccountId + '/' + $scope.selecteddfteYear,
            data: $scope.ndftecontractdetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
                    .success(function (data) {
                        $scope.test = data;
                        //$scope.accountIdselected($scope.accountId);
                        $scope.getNDFTEContractIdByAccountId();
                        $scope.ndftecontractdetails = '';
                        //$scope.succes_message = 'Success';
                        /* jshint ignore:end */
                    })
                    /* jshint ignore:start */
                    .error(function (response) {
                        // TODO correct [error_message] -> [errorMessage]
                        //$scope.error_message = response.error_message;
                    })
        /* jshint ignore:end */
        ;
    };

    $scope.ndaddrow = function () {
       
        
            $http({
                method: 'POST',
                url: 'app/ndwbspost/' + $scope.dfteaccountId + '/' + $scope.selecteddfteYear + '/' + $scope.ndwbs.Month,
                data: $scope.ndwbs,
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                    .success(function (data) {
                        //$scope.accountIdselected($scope.accountId);
                        $scope.test = data;
                        document.getElementById('ndwbscode').value = '';
                        document.getElementById('ndwbsdescription').value = '';
                        $scope.ndwbs.wbscode = '';
                        $scope.getNDWBSByAccountId();

                        //data.raisedByName = $scope.currentuser.fullName;
                        //data.closedByName = $scope.currentuser.fullName;
                        //modalObjects.issues.push(data);
                        //$modalInstance.dismiss('dismiss');

                        /* jshint ignore:start */
                        // TODO correct [success_message] -> [success_message]
                        $scope.succes_message = 'Success';
                        /* jshint ignore:end */
                    })
                    /* jshint ignore:start */
                    .error(function (response) {
                        // TODO correct [error_message] -> [errorMessage]
                        $scope.error_message = response.error_message;
                    })
            /* jshint ignore:end */
            ;
        
    };

    $scope.nddelrow = function (wbscode) {
        // $scope.show = false;
        $scope.ndwbsdeldetails = {};
        $scope.ndwbsdeldetails.accountId = $scope.dfteaccountId;
        $scope.ndwbsdeldetails.wbsCode = wbscode;
        $scope.ndwbsdeldetails.isActive = false;
        //$scope.show = true;
        $http({
            method: 'POST',
            url: 'app/ndwbsdel/' + $scope.dfteaccountId,
            data: $scope.ndwbsdeldetails,
            headers: {
                'Content-Type': 'application/json'
            }
        })
                .success(function (data) {
                    $scope.test = data;
                    //$scope.accountIdselected($scope.accountId);
                    $scope.getNDWBSByAccountId();
                    //$scope.succes_message = 'Success';
                    /* jshint ignore:end */
                })
                /* jshint ignore:start */
                .error(function (response) {
                    // TODO correct [error_message] -> [errorMessage]
                    //$scope.error_message = response.error_message;
                })
        /* jshint ignore:end */
        ;

    };
});