﻿'use strict';

angular.module('RapidReportsModule')
.config(function ($stateProvider) {
    $stateProvider
    .state('overview.fte', {
        url: '/fte',
        templateUrl: 'client/app/overview/fte/fte.html',
        controller: 'FTECtrl'
    });
});