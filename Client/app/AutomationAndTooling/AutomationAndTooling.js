﻿RapidReportsModule.config(function ($stateProvider) {
    $stateProvider
    .state('automationandtooling', {
        url: '/AutomationAndTooling',
        templateUrl: 'client/app/CommonSubTile/CommonSubTile.html',
        controller: "AutomationAndToolingController"
    });
});