﻿'use strict';

angular.module('RapidReportsModule')
.factory('PageHeaderService', function () {
    var pageHeaderDefaults = {
        pageTitle: 'Home'
    };

    var api = {
        getPageHeaderDefaults: function () {
            return pageHeaderDefaults;
        },
        setPageHeaderTitle: function (val) {
            pageHeaderDefaults.pageTitle = val;
        }
    };

    return api;
});