﻿'use strict';

angular.module('RapidReportsModule')
.factory('VdfChartService', function ($resource) {
    return $resource(
           'api/report/:month/:year',
            {
                month: '@month',
                year: '@year'
            },
            {
                getVdfmoncustomerChartdata: {
                    url: 'app/vdfmonDollarCustomerchart/:accountname',
                    method: 'GET',
                    isArray: true,
                    params: {

                        accountname: '@accountname'

                    }
                },
                getVdfmonHPChartdata: {
                    url: 'app/vdfmonDollarHPchart/:accountname',
                    method: 'GET',
                    isArray: true,
                    params: {

                        accountname: '@accountname'

                    }
                },
                getVdfmonSavingChartdata: {
                    url: 'app/vdfmonEffortSavingchart/:accountname',
                    method: 'GET',
                    isArray: true,
                    params: {

                        accountname: '@accountname'

                    }
                },
                getVdfmonIncidentReductionChartdata: {
                    url: 'app/vdfmonIncidentreductionchart/:accountname',
                    method: 'GET',
                    isArray: true,
                    params: {

                        accountname: '@accountname'

                    }
                },
                getVdfquacustomerChartdata: {
                    url: 'app/vdfQuaDollarCustomerchart/:accountname',
                    method: 'GET',
                    isArray: true,
                    params: {

                        accountname: '@accountname'

                    }
                },
                getVdfquaHPChartdata: {
                    url: 'app/vdfQuaDollarHPchart/:accountname',
                    method: 'GET',
                    isArray: true,
                    params: {

                        accountname: '@accountname'

                    }
                },
                getVdfquaSavingChartdata: {
                    url: 'app/vdfQuaEffortSavingchart/:accountname',
                    method: 'GET',
                    isArray: true,
                    params: {

                        accountname: '@accountname'

                    }
                },
                getVdfquaIncidentReductionChartdata: {
                    url: 'app/vdfQuaIncidentreductionchart/:accountname',
                    method: 'GET',
                    isArray: true,
                    params: {

                        accountname: '@accountname'

                    }
                },
                getVdfAccountName: {
                    url: 'app/vdfaccountname',
                    method: 'GET',
                    isArray: true

                }
            });
});