﻿'use strict';

angular.module('RapidReportsModule')
.factory('ReportService', function ($resource) {
    return $resource(
            'api/report/:month/:year',
            {
                month: '@month',
                year:'@year'
            },
            {
                getContractCost: {
                    url: 'api/report/contactcost/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                },
                getBillingCost: {
                    url: 'api/report/billingtotal/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                },
                getAccounts: {
                    url: 'api/report/accounts/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                },
                getFTE: {
                    url: 'api/report/monthlyfte/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                },
                getRateCard: {
                    url: 'api/report/monthlyratecard/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                },
                getYearlyContractCost: {
                    url: 'api/report/contactcostyearly/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        year: '@year'
                    }
                },
                getYearlyBillingCost: {
                    url: 'api/report/billingtotalyearly/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        year: '@year'
                    }
                },
                getYearlyFTE: {
                    url: 'api/report/fte/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        year: '@year'
                    }
                },
                getYearlyRateCard: {
                    url: 'api/report/yearlyratecard/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        year: '@year'
                    }
                },
                getYearlyAccountType: {
                    url: 'api/report/accounttypeyearly/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        year: '@year'
                    }
                },
                getAllTimeWBS: {
                    url: 'api/report/alltimewbs',
                    method: 'GET',
                    isArray: true
                },
                getMonthWBS: {
                    url: 'app/WBSById/:accountid/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        accountid:'@accountid',
                        month: '@month',
                        year: '@year'
                    }
                },
                getMonthNDFTE: {
                    url: 'api/report/monthlyndfte/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                },
                getMonthNDWBS: {
                    url: 'api/report/monthlyndwbs/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                },
                getMonthNDContract: {
                    url: 'api/report/monthlyndcontract/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                },
                getMonthNDChangeRequest: {
                    url: 'api/report/monthlyndchangereq/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                },
                getMonthMasterWBS: {
                    url: 'api/report/monthlymasterwbs/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                },
                getMonthMasterContract: {
                    url: 'api/report/monthlymastercontract/:month/:year',
                    method: 'GET',
                    isArray: true,
                    params: {
                        month: '@month',
                        year: '@year'
                    }
                }
            }
        );
});