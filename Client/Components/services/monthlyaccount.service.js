﻿'use strict';

angular.module('RapidReportsModule')
.factory('MonthlyAccountService', function () {
    var accountDetails = {
        monthlyFTE: [],
        billingCost: [],
        contractCost: [],
        allAccounts: [],
        allWBS: [],
        monthWBS: [],
        availableWBS: [],
        availableServiceLines: [],
        account: {},
        month: '',
        year: 0
    };

    var api = {
        getAccountDetails: function () {
            return accountDetails;
        },
        setAccountDetails: function (val) {
            if (_.isPlainObject(val)) {
                accountDetails = val;
            }
        }
    };

    return api;
});