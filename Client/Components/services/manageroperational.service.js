﻿'use strict';

angular.module('RapidReportsModule')
.factory('ManagerDetailService', function () {
    var mgrDetails = {
        mangeroperational: [],
        allmanagers: [],
        allmanagersoperation: [],
        servicelinetype: '',
        calculationtyp: '',
        manager: '',
        year: 0
    };

    var api = {
        getOperationalDetails: function () {
            return mgrDetails;
        },
        setOperationalDetails: function (val) {
            if (_.isPlainObject(val)) {
                mgrDetails = val;
            }
        }
    };

    return api;
});