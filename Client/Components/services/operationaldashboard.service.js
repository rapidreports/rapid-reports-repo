﻿'use strict';

angular.module('RapidReportsModule')
.factory('OperationalDashboardService', function ($resource) {
    return $resource(
        'api/report/:month/:year',
        {
            month: '@month',
            year:'@year'
        },
        {
            getResourceUtilization: {
                url: 'api/operational/dashboard/resourceutiliztion',
                method: 'GET',
                isArray: true
            },
            getManagerOperational: {
                url: 'api/operational/dashboard/details/manageroperational/:year/:manager',
                method: 'GET',
                isArray: true,
                params: {
                    manager: '@manager',
                    year: '@year'
                }
            }
        }
        );
});