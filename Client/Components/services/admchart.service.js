﻿'use strict';

angular.module('RapidReportsModule')
.factory('AdmChartService', function ($resource) {
    return $resource(
           'api/report/:month/:year',
            {
                month: '@month',
                year: '@year'
            },
            {
                getAdmChartdata: {
                    url: 'app/admchart/:category/:accountname/:serviceline',
                    method: 'GET',
                    isArray: true,
                    params: {
                        category: '@category',
                        accountname: '@accountname',
                        serviceline: '@serviceline'
                    }
                },
                getAdmChartPrioritydata: {
                    url: 'app/admprioritychart/:category/:accountname/:serviceline',
                    method: 'GET',
                    isArray: true,
                    params: {
                        category: '@category',
                        accountname: '@accountname',
                        serviceline: '@serviceline'
                    }
                },
                getAdmChartBeyondSLAdata: {
                    url: 'app/admbeyondSLAchart/:category/:accountname/:serviceline',
                    method: 'GET',
                    isArray: true,
                    params: {
                        category: '@category',
                        accountname: '@accountname',
                        serviceline: '@serviceline'
                    }
                },
                getAdmChartBacklogPrioritydata: {
                    url: 'app/admbacklogprioritychart/:category/:accountname/:serviceline',
                    method: 'GET',
                    isArray: true,
                    params: {
                        category: '@category',
                        accountname: '@accountname',
                        serviceline: '@serviceline'
                    }
                },
                getAdmChartEffortPerTicketdata: {
                    url: 'app/admeffortperticketchart/:category/:accountname/:serviceline',
                    method: 'GET',
                    isArray: true,
                    params: {
                        category: '@category',
                        accountname: '@accountname',
                        serviceline: '@serviceline'
                    }
                },
                getAdmChartCostPerTicketdata: {
                    url: 'app/admcostperticketchart/:category/:accountname/:serviceline',
                    method: 'GET',
                    isArray: true,
                    params: {
                        category: '@category',
                        accountname: '@accountname',
                        serviceline: '@serviceline'
                    }
                },
                getAdmAccountName: {
                    url: 'app/accountname',
                    method: 'GET',
                    isArray: true

                },
                getAdmServiceLine: {
                    url: 'app/serviceline',
                    method: 'GET',
                    isArray: true

                }
            });
});