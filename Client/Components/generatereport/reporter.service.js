﻿'use strict';

angular.module('RapidReportsModule')
.factory('GenerateReportService', function (
    ExportService,
    $filter
    ) {
    var reportprop = {
        pageitems: [],
        pageitemheaders: [],
        reportname: '',
        hascharts: false,
        hastables: false,
    };

    var getImageFromUrl = function (canvas) {
        // Grab the image as a jpeg encoded in base64, but only the data
        var data = canvas.toDataURL('png');

        return data;
    }

    var api = {
        getReportProps: function () {
            return reportprop;
        },
        setReportProps: function (val) {
            reportprop = val;
        },
        createReport: function () {
            var doc = new jsPDF();
            var i = 0;
            var ypos = 10;
            doc.autoTableResetEndPosY();

            doc.text(reportprop.reportname, 10, ypos);
            var hplogo = new Image();
            hplogo.src = 'client/assets/images/logo.png';
            doc.addImage(hplogo, 'png', 180, 5, 25, 8);

            _.forEach(reportprop.pageitems, function (loopitem) {
                doc.setFontSize(12);
                var header = reportprop.pageitemheaders[i];

                if (i !== 0) {
                    ypos = doc.autoTableEndPosY();
                    ypos += 10;
                }

                if (loopitem.type === 'chart') {
                    if (i === 0) {
                        ypos += 10;
                    }
                    else if (!reportprop.hastables) {
                        ypos += 100;
                    }

                    doc.text(header, 10, ypos);
                    ypos += 10;
                    doc.addImage(getImageFromUrl(loopitem.item), 'png', 10, ypos, 190, 65);
                }
                else if (loopitem.type === 'table') {
                    var columns = loopitem.columns;
                    var rows = loopitem.item;

                    if (reportprop.hascharts) {
                        ypos += 100;
                    }
                    else {
                        ypos += 30;
                    }

                    doc.autoTable(columns, rows, {
                        headerStyles: {
                            fillColor: [44, 62, 80],
                            rowHeight: 10
                        },
                        bodyStyles: {
                            rowHeight: 10,
                            valign: 'top'
                        },
                        margin: {
                            left: 10,
                            horizontal: 10
                        },
                        pageBreak: 'avoid',
                        startY: ypos,
                        styles: {overflow: 'linebreak'},
                        beforePageContent: function (data) {
                            if (ypos > 200) {
                                ypos = 20;
                            }
                            doc.text(reportprop.pageitemheaders[i], 10, ypos - 10);
                        },
                        createdCell: function (cell, data) {
                            var col = _.find(loopitem.columns, function (key) { return key.dataKey === data.column.dataKey; });

                            if (!_.isUndefined(col.format)) {
                                if (col.format === 'currency') {
                                    cell.text = $filter('currency')(cell.text);
                                }
                                else if (col.format === 'percent') {
                                    cell.text = cell.text + '%';
                                }
                            }
                        }
                    });
                }

                i += 1;
            });

            doc.save(reportprop.reportname);

            reportprop = {
                pageitems: [],
                pageitemheaders: [],
                reportname: '',
                hascharts: false
            };
            doc = {};
        }
    };

    return api;
});