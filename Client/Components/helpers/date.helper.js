﻿'use strict';

angular.module('RapidReportsModule')
.factory('DateHelper', function () {
    var monthObj = [
        { num: 1, shortMon: 'Jan', month: 'January' },
        { num: 2, shortMon: 'Feb', month: 'February' },
        { num: 3, shortMon: 'Mar', month: 'March' },
        { num: 4, shortMon: 'Apr', month: 'April' },
        { num: 5, shortMon: 'May', month: 'May' },
        { num: 6, shortMon: 'Jun', month: 'June' },
        { num: 7, shortMon: 'Jul', month: 'July' },
        { num: 8, shortMon: 'Aug', month: 'August' },
        { num: 9, shortMon: 'Sep', month: 'September' },
        { num: 10, shortMon: 'Oct', month: 'October' },
        { num: 11, shortMon: 'Nov', month: 'November' },
        { num: 12, shortMon: 'Dec', month: 'December' }
    ];

    var api = {
        getMonthArray: function () {
            return monthObj;
        },
        getYearArray: function () {
            var yearAry = [];
            var year = (new Date().getFullYear()) - 5;

            yearAry.push(year);
            for (var i = 1; i < 20; i++) {
                yearAry.push(year + i);
            }

            return yearAry;
        },
        financialMonthsAry: ['Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct']
    };

    return api;
});