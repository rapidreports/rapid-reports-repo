﻿'use strict';

angular.module('RapidReportsModule')
.constant('AppDefault', {
    pyramidJobLevel: ['Intermediate', 'Entry', 'Core', 'Advanced'],
    chartTypes: { line: 'line', spline: 'spline', bar: 'bar' },
    serviceLineManagers: {
        ADM: ['Ramachandran, Prem Kumar'],
        SCS: ['Bitra, Kalyan'],
        SAP: ['Ramachandran, Prem Kumar'],
        Testing: ['Tk, Shyamprasad', 'Sai Krishna', 'Nadeem, Syed Abdul'],
        Oracle: ['Ps, Madhukumar', 'Krishna, Vaidhi'],
        HPIT: ['Srinivasan, Vijayaraghavan Chakravarthi'],
        AADM: ['Taget, Chandrakanth Gajanan'],
        FSI: ['Baalebail, Manoj'],
        SIS: ['M, Raghavendra'],
        SAPMRU: ['D270', 'D272']
    }
});