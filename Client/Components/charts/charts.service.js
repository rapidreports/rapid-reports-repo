﻿'use strict';

angular.module('RapidReportsModule')
.factory('ChartService', function () {
    var multiBarChartSetting = {
        chart: {
            type: 'multiBarChart',
            height: 380,
            margin: {
                top: 20,
                right: 20,
                bottom: 60,
                left: 100
            },
            clipEdge: true,
            staggerLabels: true,
            transitionDuration: 1000,
            tooltips: true,
            tooltipContent: function (key, x, y, e, graph) {
                return '<p>' + key + ': ' + y + '</p>';
            },
            stacked: true,
            showControls: true,
            reduceXTicks: true,
            xAxis: {
                axisLabel: '',
                showMaxMin: false,
                tickFormat: function (d) { return d; }
            },
            yAxis: {
                axisLabel: '',
                axisLabelDistance: 0,
                tickFormat: function (d) {
                    return d3.format(',.f')(d);
                }
            }
        }
    };

    var cumulativeLineChartSetting = {
        chart: {
            type: 'cumulativeLineChart',
            height: 450,
            margin: {
                top: 20,
                right: 20,
                bottom: 60,
                left: 65
            },
            x: function (d) { return d[0]; },
            y: function (d) { return d[1]; },
            average: function (d) { return d.mean / 10; },

            color: d3.scale.category10().range(),
            duration: 300,
            useInteractiveGuideline: true,
            clipVoronoi: false,

            xAxis: {
                axisLabel: '',
                tickFormat: function (d) {
                    return d;
                },
                showMaxMin: false,
                staggerLabels: false
            },

            yAxis: {
                axisLabel: '',
                tickFormat: function (d) {
                    return d3.format(',.f')(d);
                },
                axisLabelDistance: 1
            }
        }
    };

    var discreteBarChartSetting = {
        chart: {
            type: 'discreteBarChart',
            height: 450,
            margin: {
                top: 20,
                right: 20,
                bottom: 50,
                left: 55
            },
            x: function (d) { return d.label; },
            y: function (d) { return d.value; },
            showValues: true,
            valueFormat: function (d) {
                return d3.format(',.2f')(d);
            },
            duration: 500,
            xAxis: {
                axisLabel: ''
            },
            yAxis: {
                axisLabel: '',
                axisLabelDistance: -10
            }
        }
    };

    var api={
        getMultiBarDefault: function () {
            return multiBarChartSetting;
        },
        getMultiBarCustom: function (margin, staglbls, stacked, showcntrls, reduceXticks) {
            var custsetting = {};
            angular.copy(multiBarChartSetting, custsetting);
            custsetting.chart.margin = margin;
            custsetting.chart.staggerLabels = staglbls;
            custsetting.chart.stacked = stacked;
            custsetting.chart.showControls = showcntrls;
            custsetting.chart.reduceXTicks = reduceXticks;
            return custsetting;
        },
        getLineChartDefault: function () {
            return cumulativeLineChartSetting;
        },
        getBarChartDefault: function () {
            return discreteBarChartSetting;
        },
        getBarChartCustom: function (margin, showcntrls, xlabel, yformat) {
            var custsetting = {};
            angular.copy(discreteBarChartSetting, custsetting);
            custsetting.chart.margin = margin;
            custsetting.chart.showControls = showcntrls;
            custsetting.chart.xAxis.axisLabel = xlabel;
            if (!_.isNull(yformat)) {
                custsetting.chart.valueFormat = function (d) {
                    return d3.format('p')(d);
                };
            }
            return custsetting;
        }
    };

    return api;
});